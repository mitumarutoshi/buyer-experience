import { CtfCard, CtfCardGroup } from '~/models';
import { getUrlFromContentfulImage } from '~/common/util';
import { COMPONENT_NAMES } from '~/common/constants';

export function mapBySolutionBenefits(ctfCardGroup: CtfCardGroup) {
  const items = ctfCardGroup.card.map((card: any) => ({
    ...card.fields?.customFields,
    header: card.fields?.title,
    text: card.fields?.description,
    link_text: card.fields?.button?.fields.text,
    link_url: card.fields?.button?.fields.externalUrl,
    ga_name: card.fields?.button?.fields.dataGaName,
    ga_location: card.fields?.button?.fields.dataGaLocation,
  }));

  const image = ctfCardGroup.image && {
    image_url: getUrlFromContentfulImage(ctfCardGroup.image?.fields.image),
    alt: ctfCardGroup.image?.fields?.altText,
  };

  const video = ctfCardGroup.video && {
    video_url: ctfCardGroup.video.fields.url,
  };

  const link = ctfCardGroup.cta && {
    url: ctfCardGroup.cta.fields.externalUrl,
    text: ctfCardGroup.cta.fields.text,
    data_ga_name: ctfCardGroup.cta.fields.dataGaName,
    data_ga_location: ctfCardGroup.cta.fields.dataGaLocation,
  };

  return {
    name: COMPONENT_NAMES.BY_SOLUTION_BENEFITS,
    data: {
      ...ctfCardGroup.customFields,
      title: ctfCardGroup.header,
      subtitle: ctfCardGroup.description,
      image,
      video,
      link,
      items,
    },
  };
}

export function mapBySolutionValueProp(ctfCardGroup: CtfCardGroup) {
  const { card: fetchedCards, customFields } = ctfCardGroup;

  const cards = fetchedCards.map((card: any) => {
    let result = {
      ...card?.fields?.customFields,
      title: card?.fields?.title,
      description: card?.fields?.description,
    };

    if (card?.fields?.button?.fields?.externalUrl) {
      result = {
        ...result,
        href: card?.fields?.button?.fields?.externalUrl,
        data_ga_name: card?.fields?.button?.fields?.dataGaName,
        data_ga_location: card?.fields?.button?.fields?.dataGaLocation,
        cta: card?.fields?.button?.fields.text,
      };
    } else if (card?.fields?.cardLink) {
      result = {
        ...result,
        href: card?.fields?.cardLink,
        data_ga_name: card?.fields?.cardLinkDataGaName,
        data_ga_locatiom: card?.fields?.cardLinkDataGaLocation,
      };
    }

    if (card?.fields?.list) {
      const mutatedList = card.fields?.list.map((item: string) => ({
        text: item,
      }));

      if (card?.fields?.list == 'demo') {
        result = {
          ...result,
          demoOn: mutatedList,
          demo: {
            subtitle: card.fields.contentArea[0].fields.subtitle,
            video: {
              url: card.fields.contentArea[0].fields.video.fields.url,
              altText: '',
            },
            launchButton: {
              text: card.fields.contentArea[0].fields.contentArea[0].fields
                .text,
              url: card.fields.contentArea[0].fields.contentArea[0].fields
                .externalUrl,
              icon_name:
                card.fields.contentArea[0].fields.contentArea[0].fields
                  .iconName,
              icon_variant:
                card.fields.contentArea[0].fields.contentArea[0].fields
                  .iconVariant,
              data_ga_name:
                card.fields.contentArea[0].fields.contentArea[0].fields
                  .dataGaName,
              data_ga_location:
                card.fields.contentArea[0].fields.contentArea[0].fields
                  .dataGaLocation,
            },
            scheduleButton: {
              text: card.fields.contentArea[0].fields.button.fields.text,
              href: card.fields.contentArea[0].fields.button.fields.externalUrl,
              data_ga_name:
                card.fields.contentArea[0].fields.button.fields.dataGaName,
              data_ga_location:
                card.fields.contentArea[0].fields.button.fields.dataGaLocation,
            },
          },
        };
      }
    }

    if (card?.fields?.pills) {
      result = {
        ...result,
        pill: card.fields.pills,
      };
    }

    if (!result.icon) {
      result = {
        ...result,
        icon: {
          name: card?.fields?.iconName,
          alt: `${card?.fields?.iconName} icon`,
          variant: 'marketing',
        },
      };
    }
    return result;
  });

  if (ctfCardGroup?.video) {
    return {
      name: COMPONENT_NAMES.BY_SOLUTION_VALUE_PROP,
      data: {
        ...ctfCardGroup?.customFields,
        title: ctfCardGroup?.header,
        cards,
        video: {
          url: ctfCardGroup?.video.fields.url,
        },
        ...customFields,
      },
    };
  }

  return {
    name: COMPONENT_NAMES.BY_SOLUTION_VALUE_PROP,
    data: {
      ...ctfCardGroup?.customFields,
      id: ctfCardGroup.headerAnchorId,
      title: ctfCardGroup?.header,
      cards,
      ...customFields,
    },
  };
}

export function mapBySolutionIntro(ctfCard: CtfCard) {
  const cta = ctfCard.button && {
    text: ctfCard.button.fields.text,
    href: ctfCard.button.fields.externalUrl,
    data_ga_name: ctfCard.button.fields.dataGaName,
    data_ga_location: ctfCard.button.fields.dataGaLocation,
  };

  return {
    name: COMPONENT_NAMES.BY_SOLUTION_INTRO,
    data: {
      ...ctfCard.customFields,
      header: ctfCard.title,
      text: {
        highlight: ctfCard.subtitle,
        description: ctfCard.description,
      },
      cta,
    },
  };
}

export function mapBySolutionLink(ctfCard: CtfCard) {
  return {
    name: COMPONENT_NAMES.BY_SOLUTION_LINK,
    data: {
      ...ctfCard.customFields,
      title: ctfCard.title,
      description: ctfCard.description,
      link: ctfCard.cardLink,
      image: getUrlFromContentfulImage(ctfCard.image),
      alt: ctfCard.image?.fields?.description,
      icon: ctfCard.iconName,
    },
  };
}

export function mapBySolutionShowcase(ctfCardGroup: CtfCardGroup) {
  const items = ctfCardGroup.card.map((card: any) => {
    const { fields } = card;
    const link = fields.button && {
      text: fields.button.fields.text,
      href: fields.button.fields.externalUrl,
      data_ga_name: fields.button.fields.dataGaName,
      data_ga_location: fields.button.fields.dataGaLocation,
    };

    return {
      ...fields.customFields,
      title: fields.title,
      description: fields.description,
      list: fields.list,
      video: fields.video?.fields?.url,
      link,
    };
  });

  return {
    name: COMPONENT_NAMES.BY_SOLUTION_SHOWCASE,
    data: {
      ...ctfCardGroup.customFields,
      title: ctfCardGroup.header,
      description: ctfCardGroup.description,
      image_url: getUrlFromContentfulImage(ctfCardGroup.image?.fields?.image),
      items,
    },
  };
}

export function mapBySolutionList(ctfCard: CtfCard) {
  const cta = ctfCard.button && {
    text: ctfCard.button.fields.text,
    href: ctfCard.button.fields.externalUrl,
    video: ctfCard.video && ctfCard.video.fields.url,
    ga_name: ctfCard.button.fields.dataGaName,
    ga_location: ctfCard.button.fields.dataGaLocation,
  };

  return {
    name: COMPONENT_NAMES.BY_SOLUTION_LIST,
    data: {
      ...ctfCard.customFields,
      title: ctfCard.title,
      header: ctfCard.description,
      footer: ctfCard.subtitle,
      icon: ctfCard.iconName,
      items: ctfCard.list,
      cta,
    },
  };
}
