import { CtfHeaderAndText } from '~/models';
import { COMPONENT_NAMES } from '../../common/constants';
import { getUrlFromContentfulImage } from '../../common/util';
import { CtfCard, CtfCardGroup, CtfEntry } from '~/models';

export function mapIndustryIntro(ctfCustomerLogo: any) {
  const logos = ctfCustomerLogo.logo.map((logo: any) => ({
    ...logo.fields?.customFields,
    name: logo.fields?.altText,
    image: getUrlFromContentfulImage(logo.fields?.image),
    url: logo.fields?.link,
  }));

  return {
    name: COMPONENT_NAMES.BY_INDUSTRY_INTRO,
    data: {
      intro_text: ctfCustomerLogo.text,
      logos,
    },
  };
}

export function mapByIndustrySolutionsBlock(ctfCardGroup: any) {
  const solutions = ctfCardGroup.card.map((card: any) => ({
    ...card.fields?.customFields,
    title: card.fields?.title,
    description: card.fields?.description || '',
    list: card.fields?.list,
    link_text: card.fields?.button?.fields?.text,
    link_url: card.fields?.button?.fields?.externalUrl,
    data_ga_name: card.fields?.button?.fields?.dataGaName,
    data_ga_location: card.fields?.button?.fields?.dataGaLocation,
    launchbutton: {
      text: card.fields?.secondaryButton?.fields?.text,
      icon_name: card.fields?.secondaryButton?.fields?.iconName,
      icon_variant: card.fields?.secondaryButton?.fields?.iconVariant,
      data_ga_name: card.fields?.secondaryButton?.fields?.dataGaName,
      data_ga_location: card.fields?.secondaryButton?.fields?.dataGaLocation,
    },

    demo: card.fields.contentArea
      ? {
          title: card.fields?.contentArea[0]?.fields?.title || '',
          subtitle: card.fields?.contentArea[0]?.fields?.subtitle || '',
          description: card.fields?.contentArea[0]?.fields?.description || '',
          mobileVideo:
            card.fields?.contentArea[0]?.fields?.contentArea[1]?.fields?.url ||
            '',
          video: {
            url: card.fields?.contentArea[0]?.fields?.video?.fields?.url || '',
          },
          scheduleButton: {
            text:
              card.fields?.contentArea[0]?.fields?.button?.fields?.text || '',
            href:
              card.fields?.contentArea[0]?.fields?.button?.fields
                ?.externalUrl || '',
            data_ga_name:
              card.fields?.contentArea[0]?.fields?.button?.fields?.dataGaName ||
              '',
            data_ga_location:
              card.fields?.contentArea[0]?.fields?.button?.fields
                ?.dataGaLocation || '',
          },
        }
      : null,
  }));

  return {
    name: COMPONENT_NAMES.BY_INDUSTRY_SOLUTIONS_BLOCK,
    data: {
      ...ctfCardGroup.customFields,
      subtitle: ctfCardGroup.header,
      sub_description: ctfCardGroup.description,
      sub_image: getUrlFromContentfulImage(ctfCardGroup.image?.fields?.image),
      alt: ctfCardGroup.image?.fields?.altText,
      solutions,
    },
  };
}

export function mapByIndustryQuotesCarousel(ctfBlockGroup: any) {
  const quotes = ctfBlockGroup.blocks.map((block: any) => {
    const { fields } = block;
    const title_img = fields.authorHeadshot && {
      url: getUrlFromContentfulImage(fields.authorHeadshot.fields.image),
      alt: fields.authorHeadshot.fields.altText,
    };

    return {
      ...fields.customFields,
      quote: fields.quoteText,
      author: fields.author,
      position: fields.authorTitle,
      company: fields.authorCompany,
      url: fields.cta?.fields.externalUrl,
      title_img,
    };
  });

  return {
    name: COMPONENT_NAMES.BY_INDUSTRY_QUOTES_CAROUSEL,
    data: {
      ...ctfBlockGroup.customFields,
      header: ctfBlockGroup.header,
      quotes,
    },
  };
}

export function mapByIndustryCaseStudies(ctfCardGroup: any) {
  const rows = ctfCardGroup.card.map((card: any) => {
    const { fields } = card;
    const image = fields.image && {
      url: getUrlFromContentfulImage(fields.image),
      alt: fields.image.description,
    };

    const button = fields.button && {
      href: fields.button.fields.externalUrl,
      text: fields.button.fields.text,
      data_ga_name: fields.button.fields.dataGaName,
      data_ga_location: fields.button.fields.dataGaLocation,
    };

    return {
      ...fields.customFields,
      title: fields.title,
      subtitle: fields.description,
      button,
      image,
    };
  });

  return {
    name: COMPONENT_NAMES.BY_INDUSTRY_CASE_STUDIES,
    data: {
      ...ctfCardGroup.customFields,
      title: ctfCardGroup.header,
      rows,
    },
  };
}

export function mapHomeSolutions(ctfCardGroup: any) {
  const solutions = ctfCardGroup.card.map((card: any) => ({
    ...card.fields?.customFields,
    title: card.fields?.title,
    description: card.fields?.description || '',
    list: card.fields?.list,
    link_text: card.fields?.button?.fields?.text,
    link_url: card.fields?.button?.fields?.externalUrl,
    data_ga_name: card.fields?.button?.fields?.dataGaName,
    data_ga_location: card.fields?.button?.fields?.dataGaLocation,
    image: getUrlFromContentfulImage(card.fields?.image),
    alt: card.fields?.image?.fields.altText,
  }));

  return {
    name: COMPONENT_NAMES.HOME_SOLUTIONS_CONTAINER,
    data: {
      ...ctfCardGroup.customFields,
      subtitle: ctfCardGroup.subtitle,
      solutions,
    },
  };
}

export function mapFaq(ctfFaq: any) {
  const {
    title,
    multipleAccordionGroups,
    singleAccordionGroupItems,
    showAllButton,
    background,
    customFields,
  } = ctfFaq;

  if (singleAccordionGroupItems) {
    const questions = singleAccordionGroupItems.map((element: any) => ({
      question: element.fields.header,
      answer: element.fields.text,
    }));

    return {
      name: COMPONENT_NAMES.FAQ,
      data: {
        header: title,
        show_all_button: showAllButton,
        background,
        groups: [{ header: '', questions }],
        ...customFields,
      },
    };
  }

  // Build groups in case we have a multipleAccordionGroups element
  const groups = multipleAccordionGroups.map((group: any) => {
    const questions = group.fields.singleAccordionGroupItems.map(
      (element: any) => ({
        question: element.fields.header,
        answer: element.fields.text,
      }),
    );

    return {
      header: group.fields.title,
      questions,
    };
  });

  return {
    name: COMPONENT_NAMES.FAQ,
    data: {
      header: title,
      groups,
      ...customFields,
    },
  };
}

export function mapDemoCtaCard(ctfCard: any) {
  const { title, subtitle, description } = ctfCard;
  return {
    name: COMPONENT_NAMES.DEMO_CTA_CARD,
    data: {
      title: title,
      description: description,
      demo: {
        subtitle: subtitle,
        video: {
          url: ctfCard.video.fields.url,
          thumbnail:
            ctfCard.video.fields.thumbnail.fields.image.fields.file.url,
          altText: ctfCard.video.fields.thumbnail.fields.image.fields.title,
        },
        scheduleButton: {
          text: ctfCard.button.fields.text,
          href: ctfCard.button.fields.externalUrl,
          data_ga_name: ctfCard.button.fields.dataGaName,
          data_ga_location: ctfCard.button.fields.dataGaLocation,
        },
      },
      launchButton: {
        text: ctfCard.secondaryButton.fields.text,
        href: ctfCard.secondaryButton.fields.externalUrl,
        icon_name: ctfCard.secondaryButton.fields.iconName,
        icon_variant: ctfCard.secondaryButton.fields.iconVariant,
        data_ga_name: ctfCard.secondaryButton.fields.dataGaName,
        data_ga_location: ctfCard.secondaryButton.fields.dataGaLocation,
      },
    },
  };
}

export function mapOpenSourceBlog(ctfOSBlog: any) {
  const { header, subheader, card, cta, customFields } = ctfOSBlog;

  return {
    name: COMPONENT_NAMES.OPEN_SOURCE_BLOG,
    data: {
      header,
      subheader,
      link: {
        text: cta.fields.text,
        url: cta.fields.externalUrl,
      },
      entries: card.map(
        ({ fields: { description, cardLink, image, video, customFields } }) => {
          const entry: any = { description };

          if (cardLink) {
            entry.link = { href: cardLink };
          }

          if (video) {
            entry.link = { href: video.fields.url };
          }

          if (image?.fields?.file?.url) {
            entry.image = image.fields.file.url;
          }

          return { ...entry, ...customFields };
        },
      ),
      ...customFields,
    },
  };
}

export function mapOpenSourcePartnersGrid(ctfOSPartnersGrid: any) {
  const { header, text, cta, assets, customFields } = ctfOSPartnersGrid;

  return {
    name: COMPONENT_NAMES.OPEN_SOURCE_PARTNERS_GRID,
    data: {
      header,
      description: text,
      link: cta.fields.externalUrl,
      ga_name: cta.fields.dataGaName,
      ga_location: cta.fields.dataGaLocation,
      partners: assets.map((asset: any) => ({
        logo: asset.fields.image.fields.file.url,
        alt: asset.fields.image.fields.title,
      })),
      ...customFields,
    },
  };
}

export function mapOpenSourcePartnersGridFull(ctfOSPartnersGrid: any) {
  const { header, text, assets, customFields } = ctfOSPartnersGrid;

  return {
    name: COMPONENT_NAMES.OPEN_SOURCE_PARTNERS_GRID_FULL,
    data: {
      header,
      subheader: text,
      partners: assets.map((asset: any) => ({
        logo: asset.fields.image.fields.file.url,
        alt: asset.fields.image.fields.title,
      })),
      ...customFields,
    },
  };
}

/**
 * IMPORTANT: NEEDS TO BE REFACTORED ASAP -- Could be merged into generic solutions-default.helper.ts --> mapFormSection()
 * @param {Object} ctfOSFormSection - The original section object retrieved from contentful.
 * @param {Object[]} ctfOSFormSection.blockGroup - An array of card objects containing the information for each block within the section.
 * @param {string} ctfOSFormSection.header - The header/title of the open source form section.
 * @param {string} ctfOSFormSection.text - The main text content of the open source form section.
 * @returns {Object} A mapped object containing a standardized structure of the open source form section,
 *                   including name, data with title, blocks, disclaimer, and a card.
 * @returns {string} return.name - The constant name identifier for the component.
 * @returns {Object} return.data - The structured data of the form section, including title, blocks, and disclaimer.
 * @returns {string} return.data.title - The title of the open source form section.
 * @returns {Object[]} return.data.blocks - An array of block objects, each with a title and content.
 * @returns {Object} return.data.disclaimer - An object containing a description and an icon for the disclaimer part of the section.
 * @returns {Object} return.data.card - An object containing the title and description for a specific card in the section.
 */
export function mapOpenSourceFormSection(ctfOSFormSection: any) {
  const cards = ctfOSFormSection.blockGroup.map((card) => ({
    title: card.fields.title || '',
    description: card.fields.description,
    icon:
      card.fields.iconName && card.fields.iconName !== ''
        ? { name: card.fields.iconName, alt: `Error ${card.fields.iconName}` }
        : {},
  }));

  return {
    name: COMPONENT_NAMES.OPEN_SOURCE_FORM_SECTION,
    data: {
      title: ctfOSFormSection.header,
      blocks: [{ title: '', content: ctfOSFormSection.text }],
      disclaimer: { description: cards[0].description, icon: cards[0].icon },
      card: { title: cards[1].title, description: cards[1].description },
    },
  };
}

/**
 * IMPORTANT: NEEDS TO BE REFACTORED ASAP
 * Maps and transforms Contentful data representing an open-source overview into a structured format.
 * It only supports up to 3 cards, where the last one (if used) acts as a CTA card with a different style a CTA button.
 * Find an example reference entry here: https://app.contentful.com/spaces/xz1dnu24egyd/entries/fUjKBsaHNrfUAXoLCblx3
 * All blocks in Contentful should be sorted in the same order they should be presented in the page.
 *
 * @param ctfOSOverview - Contentful data for the open-source overview.
 * @returns {Object} - Mapped data in a structured format.
 */
export function mapOpenSourceOverview(ctfOSOverview: CtfCardGroup): object {
  const { card: cards, customFields } = ctfOSOverview;

  const mapBlock = (block: CtfEntry<CtfCard>) => ({
    header: block.fields.title,
    description: block.fields.description,
    list: block.fields.list,
    ...block.fields.customFields,
  });

  const primary = mapBlock(cards[0]);
  const secondary = mapBlock(cards[1]);
  const lastBlock: CtfEntry<CtfCard> = cards[2];

  const cta = lastBlock && {
    header: lastBlock.fields.title,
    link: lastBlock.fields.button && {
      href: lastBlock.fields.button.fields.externalUrl,
      text: lastBlock.fields.button.fields.text,
      variant: lastBlock.fields.button.fields.variation || 'tertiary',
      icon: {
        name: lastBlock.fields.button.fields.iconName,
        alt: `${lastBlock.fields.button.fields.iconName} Icon`,
      },
    },
    description: lastBlock.fields.description,
    ...lastBlock.fields.customFields,
  };

  return {
    name: COMPONENT_NAMES.OVERVIEW_BLOCKS,
    data: { primary, secondary, cta, ...customFields },
  };
}

export function mapStartupsIntro(ctfHeaderAndText: CtfHeaderAndText) {
  const { header, text, headerAnchorId, customFields } = ctfHeaderAndText;

  return {
    name: COMPONENT_NAMES.STARTUPS_INTRO,
    data: {
      id: headerAnchorId,
      title: header,
      subtitle: text,
      ...customFields,
    },
  };
}

export function mapNonprofitIntro(ctfHeaderAndText: CtfHeaderAndText) {
  const { text, headerAnchorId, customFields } = ctfHeaderAndText;

  return {
    name: COMPONENT_NAMES.NONPROFIT_INTRO,
    data: {
      id: headerAnchorId,
      text: {
        description: text,
      },
      ...customFields,
    },
  };
}

export function mapHeaderText(ctfHeaderAndText: any) {
  const { header, text } = ctfHeaderAndText;

  return {
    name: 'HeaderAndText',
    data: {
      title: header,
      subtitle: text,
    },
  };
}

/**
 * IMPORTANT: NEEDS TO BE REFACTORED ASAP
 * Maps and transforms Contentful data representing an open-source overview into a structured format.
 * Find an example reference entry here:https://app.contentful.com/spaces/xz1dnu24egyd/entries/2W0MZ8h7wmX1bOpngEpA2r
 * All blocks in Contentful should be sorted in the same order they should be presented in the page.
 *
 * @param ctfOSOverview - Contentful data for the open-source overview.
 * @returns {Object} - Mapped data in a structured format.
 */
export function mapStartupsOverview(ctfStartupsOverview: any) {
  const { header, description, card: cards } = ctfStartupsOverview;

  const blocks = [];

  const lastBlockIndex = cards.findIndex(
    (card) => card.sys.id === '6smEjCS0baaHD4Du5jOIOt',
  );

  const firstBlock = cards.slice(0, lastBlockIndex);
  const lastBlock = cards.slice(lastBlockIndex);

  const firstBlockHeader = cards.find(
    (card) => card.sys.id === '4t3QqoUOndpd5Zt45RTLhy',
  );
  const lastBlockHeader = cards.find(
    (card) => card.sys.id === '6smEjCS0baaHD4Du5jOIOt',
  );

  // BUILD FIRST BLOCK
  const newFirstBlock: any = {
    header: firstBlockHeader.fields.title,
    offers: [],
  };

  firstBlock.forEach((entry) => {
    const isFirstBlock =
      firstBlock.includes(entry) && entry === firstBlockHeader;

    if (isFirstBlock) {
      newFirstBlock.offers.push({
        list: entry.fields.list,
      });
    } else {
      newFirstBlock.offers.push({
        title: entry.fields.title,
        list: entry.fields.list,
      });
    }
  });
  blocks.push(newFirstBlock);

  // BUILD SECOND BLOCK
  const newLastBlock: any = {
    header: lastBlockHeader.fields.title,
    offers: [],
  };

  // REMOVE ADDITIONAL INFO SECTION, ADDED LATER
  lastBlock.pop();

  lastBlock.forEach((entry) => {
    const isLastBlock = lastBlock.includes(entry) && entry === lastBlockHeader;

    if (isLastBlock) {
      newLastBlock.offers.push({
        list: entry.fields.list,
      });
    } else {
      newLastBlock.offers.push({
        title: entry.fields.title,
        list: entry.fields.list,
      });
    }
  });

  blocks.push(newLastBlock);

  // FIND ADDITIONAL REQUIREMENTS SECTION
  const [additionalRequirements] = cards.filter(
    (entry) => entry.sys.id === '2DOOH6aMLwzNha5TTBwRwk',
  );

  const informationBlock = {
    header: additionalRequirements.fields.title,
    list: additionalRequirements.fields.list,
  };

  const result = {
    name: COMPONENT_NAMES.STARTUPS_OVERVIEW,
    data: {
      header,
      description,
      blocks,
      information: informationBlock,
    },
  };

  return result;
}

/**
 * IMPORTANT: NEEDS TO BE REFACTORED ASAP
 * Maps and transforms Contentful data representing an non-profit overview into a structured format.
 * Find an example reference entry here: https://app.contentful.com/spaces/xz1dnu24egyd/entries/1WoDKaD2GRz8xJ2OX4dlvh
 * All blocks in Contentful should be sorted in the same order they should be presented in the page.
 *
 * @param ctfNonprofitOverview - Contentful data for the non-profit overview.
 * @returns {Object} - Mapped data in a structured format.
 */
export function mapNonprofitOverview(ctfNonprofitOverview: any) {
  const { description, card: cards } = ctfNonprofitOverview;

  const blocks = [];

  const firstBlockHeader = cards.find(
    (card) => card.sys.id === '6Tt4rZbKHZXh0QervjNVja',
  );

  // BUILD FIRST BLOCK
  const newFirstBlock: any = {
    title: firstBlockHeader.fields.title,
    offers: [{ list: firstBlockHeader.fields.list }],
  };

  blocks.push(newFirstBlock);

  // FIND ADDITIONAL REQUIREMENTS SECTION
  const [additionalRequirements] = cards.filter(
    (entry) => entry.sys.id === '19axL4IlF7auuCrIIVp5xU',
  );

  const informationBlock = {
    title: additionalRequirements.fields.title,
    text: additionalRequirements.fields.list,
    link: {
      url: additionalRequirements.fields.button.fields.externalUrl,
    },
  };

  const result = {
    name: COMPONENT_NAMES.NONPROFIT_OVERVIEW,
    data: {
      blocks,
      footnote: description,
      information: informationBlock,
    },
  };

  return result;
}

export function mapStartupsLink(ctfCard: any) {
  const { title, description, button, image, iconName } = ctfCard;

  return {
    name: COMPONENT_NAMES.STARTUPS_LINK,
    data: {
      header: title,
      description,
      button: {
        href: button.fields.externalUrl,
        text: button.fields.text,
      },
      image: image.fields.file.url,
      alt: image.fields.title,
      icon: iconName,
    },
  };
}
