import { CONTENT_TYPES } from '~/common/content-types';
import { capitalizeFirst } from '~/common/util';
interface CSSAlesData {
  title?: string;
  description?: string;
  hero?: any;
  resources?: any;
  section?: any;
  form?: any;
  navigation?: any;
  nextSteps?: any;
  submittedHero?: any;
}

function sectionBuilder(section) {
  const listBuilder = section.contentArea.map((item) => {
    return {
      ...item.fields,
    };
  });
  return {
    button: section.button?.fields && {
      ...section.button?.fields,
    },
    video: section.video.fields && {
      ...section.video.fields,
    },
    list: listBuilder,
    description: section.description && section.description,
  };
}

function resourcesCards(resources) {
  const cards = resources.card.map((card) => {
    return {
      header: card.fields.title,
      icon: {
        name: card.fields.iconName,
        variant: 'marketing',
        alt: `${card.fields.iconName} icon`,
      },
      image: card.fields.image.fields.file.url,
      link_text: card.fields.button.fields.text,
      href: card.fields.cardLink && card.fields.cardLink,
      data_ga_name:
        card.fields.cardLinkDataGaName && card.fields.cardLinkDataGaName,
      data_ga_location:
        card.fields.cardLinkDataGaLocation &&
        card.fields.cardLinkDataGaLocation,
      event_type: card.fields.subtitle && card.fields.subtitle,
    };
  });

  return {
    data: {
      title: resources.header && resources.header,
      column_size: 4,
      cards,
    },
  };
}

function heroBuilder(hero, id) {
  const { title, description, backgroundImage, customFields, subheader } = hero;
  return {
    title,
    description,
    id,
    backgroundImage: backgroundImage.fields && {
      src: backgroundImage.fields.file.url,
      alt: backgroundImage.fields.file.title,
    },
    icon: customFields?.icon && customFields?.icon,
    subheader,
  };
}

function pageBuilder(page) {
  const result: CSSAlesData = {};

  for (const section of page) {
    const id = section.sys.contentType?.sys.id;
    switch (id) {
      case CONTENT_TYPES.CARD:
        result.section = { ...sectionBuilder(section.fields) };
        break;
      case CONTENT_TYPES.HERO:
        if (
          section.fields.componentName &&
          section.fields.componentName === 'thank-you-hero'
        ) {
          result.submittedHero = {
            ...heroBuilder(section.fields, 'thank-you-hero'),
          };
        } else {
          result.hero = { ...heroBuilder(section.fields) };
        }

        break;
      case CONTENT_TYPES.CARD_GROUP:
        result.resources = { ...resourcesCards(section.fields) };
        break;
      case CONTENT_TYPES.FORM:
        result.form = { ...section.fields };
        break;
      case CONTENT_TYPES.NEXT_STEPS:
        result.nextSteps = section.fields.variant;
        break;
    }
  }

  return result;
}

function metadataHelper(data) {
  const seo = data[0].fields;
  return {
    title: seo.title,
    og_title: seo.title,
    description: seo.description,
    twitter_description: seo.description,
    image_title: seo?.ogImage && seo.ogImage.fields.image.fields.file.url,
    og_description: seo.description,
    og_image: seo?.ogImage && seo.ogImage.fields.image.fields.file.url,
    twitter_image: seo?.ogImage && seo.ogImage.fields.image.fields.file.url,
    noIndex: seo.noIndex,
  };
}

export function CSSAlesHelper(data) {
  const { pageContent, seoMetadata } = data;
  const cleanPagecontent = pageBuilder(pageContent);

  const metadata = metadataHelper(seoMetadata);
  const transformedData: CSSAlesData = {
    ...metadata,
    ...cleanPagecontent,
  };
  return transformedData;
}
