export function smallBusinessDataHelper(items: any) {
  //  Data Mapping
  const data = items[0].fields.pageContent;
  const metadata = items[0].fields.seoMetadata[0];
  const [
    hero,
    navLinks,
    valueProp,
    demo,
    caseStudies,
    card,
    pricing,
    integratedApplications,
  ] = data;
  const doc = {} as any;

  // Hero Component:
  doc.solutions_hero = {
    title: hero.fields.title,
    subtitle: hero.fields.subheader,
    image: {
      image_url: hero.fields.backgroundImage.fields.file.url,
      alt: 'Small business Header image',
      bordered: true,
    },
    primary_btn: {
      text: hero.fields.primaryCta.fields.text,
      url: hero.fields.primaryCta.fields.externalUrl,
      data_ga_name: hero.fields.primaryCta.fields.dataGaName,
      data_ga_location: 'hero',
    },
    secondary_btn: {
      text: hero.fields.secondaryCta.fields.text,
      url: hero.fields.secondaryCta.fields.externalUrl,
      data_ga_name: hero.fields.secondaryCta.fields.dataGaName,
      data_ga_location: 'hero',
    },
  };

  // Side Navigation
  doc.side_navigation_links = navLinks.fields.anchors.map((link: any) => ({
    href: link.fields.anchorLink,
    title: link.fields.linkText,
  }));

  // Value prop

  doc.by_solution_value_prop = {
    title: valueProp.fields.header,
    light_background: true,
    full_header: true,
    last_card_gradient: true,
    gradient_cta: true,
    cards: valueProp.fields.card.map((card: any) => ({
      title: card.fields.title,
      description: card.fields.description,
      href: card.fields.cardLink,
      pill: card.fields.pills,
      demoOn: card.fields.list,
      data_ga_name: card.fields.title,
      icon: {
        name: card.fields.iconName,
        alt: `${card.fields.iconName} icon`,
        variant: 'marketing',
      },
    })),
    demo: {
      subtitle: valueProp.fields.card[1].fields.contentArea[0].fields.subtitle,
      video: {
        url: valueProp.fields.card[1].fields.contentArea[0].fields.video.fields
          .url,
        altText: demo.fields.video.fields.thumbnail.fields.image.fields.title,
      },
      scheduleButton: {
        text: valueProp.fields.card[1].fields.contentArea[0].fields.button
          .fields.text,
        href: valueProp.fields.card[1].fields.contentArea[0].fields.button
          .fields.externalUrl,
        data_ga_name:
          valueProp.fields.card[1].fields.contentArea[0].fields.button.fields
            .dataGaName,
        data_ga_location:
          valueProp.fields.card[1].fields.contentArea[0].fields.button.fields
            .dataGaLocation,
      },
      launchButton: {
        text: valueProp.fields.card[1].fields.contentArea[0].fields
          .contentArea[0].fields.text,
        url: valueProp.fields.card[1].fields.contentArea[0].fields
          .contentArea[0].fields.externalUrl,
        icon_name:
          valueProp.fields.card[1].fields.contentArea[0].fields.contentArea[0]
            .fields.iconName,
        icon_variant:
          valueProp.fields.card[1].fields.contentArea[0].fields.contentArea[0]
            .fields.iconVariant,
        data_ga_name:
          valueProp.fields.card[1].fields.contentArea[0].fields.contentArea[0]
            .fields.dataGaName,
        data_ga_location:
          valueProp.fields.card[1].fields.contentArea[0].fields.contentArea[0]
            .fields.dataGaLocation,
      },
      exitButton: {
        text: valueProp.fields.card[1].fields.contentArea[0].fields
          .contentArea[2].text,
        url: valueProp.fields.card[1].fields.contentArea[0].fields
          .contentArea[2].externalUrl,
        icon_name:
          valueProp.fields.card[1].fields.contentArea[0].fields.contentArea[2]
            .iconName,
        icon_variant:
          valueProp.fields.card[1].fields.contentArea[0].fields.contentArea[2]
            .iconVariant,
        data_ga_name:
          valueProp.fields.card[1].fields.contentArea[0].fields.contentArea[2]
            .dataGaName,
        data_ga_location:
          valueProp.fields.card[1].fields.contentArea[0].fields.contentArea[2]
            .dataGaLocation,
      },
    },
  };

  // Demo
  doc.demo = {
    demo: {
      title: demo.fields.title,
      subtitle: demo.fields.subtitle,
      description: demo.fields.description,
      video: {
        url: demo.fields.video.fields.url,
        thumbnail:
          demo.fields.video.fields.thumbnail.fields.image.fields.file.url,
        altText: demo.fields.video.fields.thumbnail.fields.image.fields.title,
      },

      scheduleButton: {
        text: demo.fields.button.fields.text,
        href: demo.fields.button.fields.externalUrl,
        data_ga_name: demo.fields.button.fields.dataGaName,
        data_ga_location: demo.fields.button.fields.dataGaLocation,
      },
    },
    launchButton: {
      text: demo.fields.secondaryButton.fields.text,
      href: demo.fields.secondaryButton.fields.externalUrl,
      icon_name: demo.fields.secondaryButton.fields.iconName,
      icon_variant: demo.fields.secondaryButton.fields.iconVariant,
      data_ga_name: demo.fields.secondaryButton.fields.dataGaName,
      data_ga_location: demo.fields.secondaryButton.fields.dataGaLocation,
    },
  };

  // Rip and Replace a.k.a. Integrated Applications
  doc.integrated_applications = {
    title: integratedApplications.fields.header,
    subtitle: integratedApplications.fields.description,
    solutions: integratedApplications.fields.card.map((application) => {
      return {
        title: application.fields.title,
        subtitle: application.fields.description,
      };
    }),
  };

  // Case studies
  doc.by_industry_case_studies = {
    title: caseStudies.fields.header,
    link: {
      text: 'All case studies',
    },
    rows: caseStudies.fields.card.map((card: any) => ({
      title: card.fields.title,
      subtitle: card.fields.subtitle,
      description: card.fields.description,
      image: {
        alt: card.fields.image.fields.title,
        url: card.fields.image.fields.file.url,
      },
      button: {
        text: 'Read Case Study',
        href: card.fields.button.fields.externalUrl,
        data_ga_name: `${card.fields.title}`,
        data_ga_location: 'body',
      },
    })),
  };

  // Card
  doc.card_content = {
    title: card.fields.title,
    description: card.fields.description,
    button: {
      text: 'Learn More about GitLab’s Startup Program',
      href: card.fields.cardLink,
      data_ga_name: card.fields.button.fields.dataGaName,
      data_ga_location: 'body',
    },
  };

  doc.pricing_tier = {
    title: pricing.fields.header,
    subtitle: pricing.fields.cta.fields.text,
    description: pricing.fields.description,
    cards: pricing.fields.card,
  };

  // Metadata
  doc.title = metadata.fields.ogTitle;
  doc.ogTitle = metadata.fields.ogTitle;
  doc.description = metadata.fields.description;
  doc.ogDescription = metadata.fields.ogDescription;
  doc.ogType = metadata.fields.ogType;
  doc.ogUrl = metadata.fields.ogUrl;
  doc.ogSiteName = metadata.fields.ogSiteName;
  doc.ogImage = metadata.fields.ogImage;

  return doc;
}
