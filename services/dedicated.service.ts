import { Context } from '@nuxt/types';
import { getClient } from '../plugins/contentful.js';
import { CONTENT_TYPES } from '../common/content-types';
import { getUrlFromContentfulImage } from '../common/util';
import { COMPONENT_NAMES } from '../common/constants';
import { CtfCardGroup } from '../models';
import {
  mapSolutionsHero,
  mapSolutionsResourceCards,
  mapEducationCaseStudyCarousel,
  mapReportCta,
  mapSolutionsCards,
} from './solutions/solutions-default.helper';
import { CtfHero, CtfEntry } from '../models';
import { mapBySolutionBenefits } from './solutions/solutions-by-solution.helper';

export class DedicatedService {
  private readonly $ctx: Context;

  constructor($context: Context) {
    this.$ctx = $context;
  }

  getContent(slug: string, isIndexSlug: boolean) {
    // Default locale data - Contentful
    return this.getContentfulData(slug, isIndexSlug);
  }

  private async getContentfulData(slug: string, isIndexSlug: boolean) {
    const dedicatedEntry = await getClient().getEntries({
      content_type: CONTENT_TYPES.PAGE,
      'fields.slug': slug,
      include: 10,
    });

    if (dedicatedEntry.items.length === 0) {
      throw new Error(`Contentful: '${slug}' Not found`);
    }

    const [dedicated] = dedicatedEntry.items;

    return this.transformLandingData(dedicated);
  }

  private transformLandingData(ctfData: any) {
    const { pageContent, seoMetadata, schema } = ctfData.fields;

    const seoImage = getUrlFromContentfulImage(
      seoMetadata[0]?.fields?.ogImage?.fields?.image,
    );

    const components = pageContent
      .map((component: any) => this.getComponentFromEntry(component))
      .filter((component: any) => component);

    let pageData = {
      ...seoMetadata[0]?.fields,
      ...schema,
      twitter_image: seoImage,
      og_image: seoImage,
      image_title: seoImage,
      components: components,
    };

    return pageData;
  }

  private getComponentFromEntry(ctfEntry: CtfEntry<any>) {
    const { fields } = ctfEntry;
    const { id } = ctfEntry.sys.contentType.sys;
    let component;

    switch (id) {
      case CONTENT_TYPES.HERO: {
        component = this.mapHeroComponent(fields);
        break;
      }
      case CONTENT_TYPES.CARD_GROUP: {
        component = this.mapCardGroupComponent(fields);
        break;
      }
      case CONTENT_TYPES.CUSTOM_COMPONENT: {
        component = { ...fields };
        break;
      }
      case CONTENT_TYPES.NEXT_STEPS: {
        component = this.mapNextStep(fields);
        break;
      }
      case CONTENT_TYPES.SIDE_MENU: {
        component = this.mapSideNavigation(fields);
        break;
      }
    }
    return component;
  }

  private mapHeroComponent(hero: CtfHero) {
    return mapSolutionsHero(hero);
  }

  private mapOverviewBlocks(ctfCardGroup: CtfCardGroup) {
    return {
      name: COMPONENT_NAMES.OVERVIEW_CARDS,
      data: {
        header: ctfCardGroup?.header,
        cards: ctfCardGroup.card.map((card) => ({
          title: card?.fields?.title,
          description: card?.fields?.description,
        })),
      },
    };
  }

  private mapTabsComponent(ctfTabs: any) {
    return mapEducationCaseStudyCarousel(ctfTabs);
  }

  private mapCtaBlock(ctfCardGroup: CtfCardGroup) {
    return {
      name: COMPONENT_NAMES.CTA_BLOCK,
      data: {
        cards: ctfCardGroup.card.map((card) => {
          return {
            header: card.fields.title,
            icon: card.fields.iconName,
            link: {
              text: card.fields.button.fields.text,
              url: card.fields.button.fields.externalUrl,
              data_ga_name: card.fields.button.fields.dataGaName,
            },
          };
        }),
      },
    };
  }

  private mapSideNavigation(ctfSideMenu: any) {
    const links = ctfSideMenu.anchors
      .map((anchor: any) => anchor.fields)
      .map((anchor: any) => ({
        title: anchor.linkText,
        href: anchor.anchorLink,
        data_ga_name: anchor.dataGaName,
        data_ga_location: anchor.dataGaLocation,
      }));

    return {
      ...ctfSideMenu.customFields,
      name: COMPONENT_NAMES.SIDE_NAVIGATION_VARIANT,
      links,
      slot_content: ctfSideMenu.content.map((item) =>
        this.getComponentFromEntry(item),
      ),
    };
  }

  private mapNextStep(fields) {
    return {
      name: COMPONENT_NAMES.NEXT_STEP,
      data: {
        variant: fields.variant,
      },
    };
  }

  private mapCardGroupComponent(ctfCardGroup: CtfCardGroup) {
    let component;

    switch (ctfCardGroup.componentName) {
      case COMPONENT_NAMES.OVERVIEW_CARDS: {
        component = this.mapOverviewBlocks(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.BY_SOLUTION_BENEFITS: {
        component = mapBySolutionBenefits(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.BY_SOLUTION_EDUCATION_CAROUSEL: {
        component = this.mapTabsComponent(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.REPORT_CTA: {
        component = mapReportCta(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.SOLUTIONS_CARDS: {
        component = mapSolutionsCards(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.CTA_BLOCK: {
        component = this.mapCtaBlock(ctfCardGroup);
        break;
      }
    }
    return component;
  }
}
