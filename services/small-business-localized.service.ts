// Since the localized pages are different from the english one this service file creates the data used ONLY on the localized versions of small-business page, the english file name is 'small-business.service.ts'
export function smallBusinessLocalizedDataHelper(items: any) {
  //  Data Mapping
  const data = items[0].fields.pageContent;
  const metadata = items[0].fields.seoMetadata[0];
  const [
    hero,
    logos,
    intro,
    benefits,
    solutions,
    valueProp,
    caseStudies,
    resources,
    navLinks,
  ] = data;
  const doc = {} as any;

  // Hero Component:
  doc.solutions_hero = {
    title: hero.fields.title,
    subtitle: hero.fields.subheader,
    image: {
      image_url: hero.fields.backgroundImage.fields.file.url,
      alt: 'Small business Header image',
      rounded: true,
    },
    primary_btn: {
      text: hero.fields.primaryCta.fields.text,
      url: hero.fields.primaryCta.fields.externalUrl,
      data_ga_name: hero.fields?.primaryCta?.fields?.dataGaName,
      data_ga_location: 'hero',
    },
    secondary_btn: {
      text: hero.fields.secondaryCta.fields.text,
      url: hero.fields.secondaryCta.fields.externalUrl,
      data_ga_name: hero.fields?.secondaryCta?.fields?.dataGaName,
      data_ga_location: 'hero',
    },
  };

  // Intro logos
  doc.by_industry_intro = {
    logos: logos.fields.logo.map((logo: any) => ({
      image: logo.fields.image.fields.file.url,
      name: logo.fields?.altText,
      url: logo.fields.link,
    })),
  };

  // Intro
  doc.by_solution_intro = {
    text: {
      highlight: intro.fields.subtitle,
      description: intro.fields.description,
    },
  };

  // Benefits
  doc.by_solution_benefits = benefits?.fields?.data;

  //  Solutions block
  doc.by_industry_solutions_block = {
    white_bg: true,
    markdown: true,
    subtitle: solutions.fields.header,
    sub_description: solutions.fields.description,
    sub_image: solutions.fields.image.fields.image.fields.file.url,
    solutions: solutions.fields.card.map((card: any) => {
      const mappedCard: any = {
        title: card.fields.title,
        description: card.fields.description,
        icon: {
          name: card.fields.iconName,
          alt: `${card.fields.iconName} icon`,
          variant: 'marketing',
        },
      };

      if (card.fields.button) {
        mappedCard.link_url = card.fields.button.fields.externalUrl;
        mappedCard.link_text = card.fields.button.fields.text;
        mappedCard.data_ga_name =
          card.fields.button.fields.dataGaName || card.fields.title;
        mappedCard.data_ga_location =
          card.fields.button.dataGaLocation || 'body';
      }

      return mappedCard;
    }),
  };

  // Value prop
  doc.by_solution_value_prop = {
    title: valueProp.fields.header,
    cards: valueProp.fields.card.map((card: any) => ({
      title: card.fields.title,
      description: card.fields.description,
      icon: {
        name: card.fields.iconName,
        alt: `${card.fields.iconName} icon`,
        variant: 'marketing',
      },
    })),
  };

  doc.by_industry_case_studies = {
    charcoal_bg: true,
    title: caseStudies.fields.header,
    rows: caseStudies.fields.card.map((card: any) => {
      return {
        title: card.fields.title,
        subtitle: card.fields.subtitle,
        image: {
          alt: card.fields.image.fields.title,
          url: card.fields.image.fields.file.url,
        },
        button: {
          text: card.fields.button.fields.text,
          href: card.fields.button.fields.externalUrl || card.fields.cardLink,
          data_ga_name:
            card.fields.button.fields.dataGaName ||
            `${card.fields.title} Learn more`,
          data_ga_location: card.fields.button.fields.dataGaLocation || 'body',
        },
      };
    }),
  };

  //  Resources
  doc.solutions_resource_cards = {
    column_size: 4,
    title: resources.fields.header,
    link: {
      text: resources.fields.cta?.fields.text,
      href: resources.fields.cta?.fields.externalUrl,
      data_ga_name: resources.fields.cta?.fields.dataGaName,
      data_ga_location: resources.fields.cta?.fields.dataGaLocation,
    },
    cards: resources.fields.card.map((card: any) => {
      return {
        link_text: card.fields.button?.fields.text,
        data_ga_location:
          card.fields.button?.fields.dataGaLocation ||
          card.fields.cardLinkDataGaLocation ||
          'body',
        event_type: card.fields.subtitle,
        data_ga_name:
          card.fields.button?.fields.dataGaName ||
          card.fields.cardLinkDataGaName ||
          card.fields.title,
        header: card.fields.title,
        href: card.fields.button?.fields.externalUrl || card.fields.cardLink,
        icon: {
          name: card.fields.iconName,
          variant: 'marketing',
          alt: '',
        },
        image: card.fields.image.fields.file.url,
      };
    }),
  };

  doc.side_navigation_links = navLinks.fields.anchors.map((link: any) => ({
    href: link.fields.anchorLink,
    title: link.fields.linkText,
  }));

  // Metadata
  doc.title = metadata.fields.ogTitle;
  doc.ogTitle = metadata.fields.ogTitle;
  doc.description = metadata.fields.description;
  doc.ogDescription = metadata.fields.ogDescription;
  doc.ogType = metadata.fields.ogType;
  doc.ogUrl = metadata.fields.ogUrl;
  doc.ogSiteName = metadata.fields.ogSiteName;
  doc.ogImage = metadata.fields.ogImage;

  return doc;
}
