import { Context } from '@nuxt/types';
import { getClient } from '../plugins/contentful.js';
import { CONTENT_TYPES } from '../common/content-types';
import { getUrlFromContentfulImage } from '../common/util';
import { COMPONENT_NAMES } from '../common/constants';
import { convertToCaseSensitiveLocale } from '~/common/util';

import {
  CtfAnchorLink,
  CtfBlockGroup,
  CtfButton,
  CtfCard,
  CtfCardGroup,
  CtfEntry,
  CtfFaq,
  CtfHeaderAndText,
  CtfHero,
  CtfSideNavigation,
  CtfTwoColumnBlock,
} from '../models';
import {
  mapSolutionsHero,
  mapSolutionsResourceCards,
} from './solutions/solutions-default.helper';

export class GetStartedService {
  private readonly $ctx: Context;

  constructor($context: Context) {
    this.$ctx = $context;
  }

  getContent(slug: string, isIndexSlug: boolean) {
    try {
      return this.getContentfulData(slug, isIndexSlug);
    } catch (e) {
      throw new Error(e);
    }
  }

  private async getContentfulData(slug: string, isIndexSlug: boolean) {
    const isDefaultLocale =
      this.$ctx.i18n.locale === this.$ctx.i18n.defaultLocale;

    const props = {
      content_type: CONTENT_TYPES.PAGE,
      'fields.slug': slug,
      include: 10,
    };

    if (!isDefaultLocale) {
      props['locale'] = convertToCaseSensitiveLocale(this.$ctx.i18n.locale);
    }

    const getStartedEntry = await getClient().getEntries(props);

    if (getStartedEntry.items.length === 0) {
      throw new Error(`Contentful: '${slug}' Not found`);
    }

    const [getStarted] = getStartedEntry.items;

    return this.transformLandingData(getStarted, isIndexSlug);
  }

  private transformLandingData(ctfData: any, isIndexSlug: boolean) {
    const { pageContent, seoMetadata, schema } = ctfData.fields;

    const seoImage = getUrlFromContentfulImage(
      seoMetadata[0]?.fields?.ogImage?.fields?.image,
    );

    const components = pageContent
      .map((component: any) => this.getComponentFromEntry(component))
      .filter((component: any) => component);

    let pageData = {
      ...seoMetadata[0]?.fields,
      ...schema,
      twitter_image: seoImage,
      og_image: seoImage,
      image_title: seoImage,
      components: isIndexSlug && components,
    };

    if (!isIndexSlug) {
      pageData = {
        ...pageData,
        ...this.mapChildPageContent(components),
      };
    }

    return pageData;
  }

  private getComponentFromEntry(ctfEntry: CtfEntry<any>) {
    const { fields } = ctfEntry;
    const { id } = ctfEntry.sys.contentType.sys;
    let component;

    switch (id) {
      case CONTENT_TYPES.HERO: {
        component = this.mapHeroComponent(fields);
        break;
      }
      case CONTENT_TYPES.BLOCK_GROUP: {
        component = this.mapGetStartedMenu(fields);
        break;
      }
      case CONTENT_TYPES.CARD_GROUP: {
        component = this.mapCardGroupComponent(fields);
        break;
      }
      case CONTENT_TYPES.SIDE_MENU: {
        component = this.mapSideNavigation(fields);
        break;
      }
      case CONTENT_TYPES.FAQ: {
        component = this.mapGetStartedSteps(fields);
        break;
      }
      case CONTENT_TYPES.HEADER_AND_TEXT: {
        component = this.mapGetStartedCopyInfo(fields);
        break;
      }
    }

    return component;
  }

  /**
   * Child pages are not dynamic, this method maps every component to their respective object attribute
   * @param components
   * @private
   */
  private mapChildPageContent(components: any) {
    const childPage: any = {};

    components.forEach((component: any) => {
      switch (component.name) {
        case COMPONENT_NAMES.GET_STARTED_HERO: {
          childPage.hero = component.data;
          break;
        }
        case COMPONENT_NAMES.SLP_SIDE_NAVIGATION: {
          childPage.side_menu = component.data;
          break;
        }
        case COMPONENT_NAMES.GET_STARTED_STEPS: {
          childPage.steps = component.data;
          break;
        }
        case COMPONENT_NAMES.SOLUTIONS_RESOURCE_CARDS: {
          childPage.resource_card_block = component.data;
          break;
        }
        case COMPONENT_NAMES.GET_STARTED_COPY_INFO: {
          childPage.copy_info = component.data;
          break;
        }
        case COMPONENT_NAMES.GET_STARTED_NEXT_STEPS: {
          childPage.next_steps = component.data;
          break;
        }
      }
    });

    return childPage;
  }

  private mapHeroComponent(ctfHero: CtfHero) {
    let component;

    switch (ctfHero.componentName) {
      case COMPONENT_NAMES.SOLUTIONS_HERO: {
        component = mapSolutionsHero(ctfHero);
        break;
      }
      case COMPONENT_NAMES.GET_STARTED_HERO: {
        component = this.mapGetStartedHero(ctfHero);
      }
    }

    return component;
  }

  private mapCardGroupComponent(ctfCardGroup: CtfCardGroup) {
    let component;

    switch (ctfCardGroup.componentName) {
      case COMPONENT_NAMES.GET_STARTED_RESOURCES: {
        component = this.mapGetStartedResources(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.SOLUTIONS_RESOURCE_CARDS: {
        component = mapSolutionsResourceCards(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.GET_STARTED_NEXT_STEPS: {
        component = this.mapGetStartedNextSteps(ctfCardGroup);
        break;
      }
    }
    return component;
  }

  private mapSideNavigation(ctfSideMenu: CtfSideNavigation) {
    const anchors = ctfSideMenu.anchors.map(
      (anchor: CtfEntry<CtfAnchorLink>) => ({
        text: anchor.fields.linkText,
        href: anchor.fields.anchorLink,
        data_ga_name: anchor.fields.dataGaName,
        data_ga_location: anchor.fields.dataGaLocation,
      }),
    );

    return {
      name: COMPONENT_NAMES.SLP_SIDE_NAVIGATION,
      data: {
        text: ctfSideMenu.header,
        anchors: {
          text: ctfSideMenu.header,
          data: anchors,
        },
      },
    };
  }

  private mapGetStartedHero(ctfHero: CtfHero) {
    return {
      name: COMPONENT_NAMES.GET_STARTED_HERO,
      data: {
        title: ctfHero.title,
        header_label: ctfHero.subheader,
        content: ctfHero.description,
        crumbs:
          ctfHero.crumbs &&
          ctfHero.crumbs.map((crumb) => ({
            title: crumb.fields.text,
            href: crumb.fields.externalUrl,
            data_ga_name: crumb.fields.dataGaName,
            data_ga_location: crumb.fields.dataGaLocation,
          })),
      },
    };
  }

  private mapGetStartedMenu(ctfBlockGroup: CtfBlockGroup) {
    const block = ctfBlockGroup.blocks.map(
      (ctfBlock: CtfEntry<CtfTwoColumnBlock>) => ({
        name: ctfBlock.fields.header,
        links: ctfBlock.fields.blockGroup.map(
          (ctfGroup: CtfEntry<CtfButton>) => ({
            text: ctfGroup.fields.text,
            href: ctfGroup.fields.externalUrl,
            data_ga_name: ctfGroup.fields.dataGaName,
            data_ga_location: ctfGroup.fields.dataGaLocation,
            icon: ctfGroup.fields.iconName && {
              name: ctfGroup.fields.iconName,
            },
          }),
        ),
      }),
    );

    return {
      name: COMPONENT_NAMES.GET_STARTED_MENU,
      data: {
        title: ctfBlockGroup.header,
        block,
      },
    };
  }

  private mapGetStartedResources(ctfCardGroup: CtfCardGroup) {
    return {
      name: COMPONENT_NAMES.GET_STARTED_RESOURCES,
      data: {
        title: ctfCardGroup.header,
        cards: ctfCardGroup.card.map((card) => ({
          title: card.fields.title,
          description: card.fields.description,
          icon: card.fields.iconName && {
            name: card.fields.iconName,
          },
          link: card.fields.button && {
            text: card.fields.button.fields.text,
            url: card.fields.button.fields.externalUrl,
            ga_name: card.fields.button.fields.dataGaName,
            ga_location: card.fields.button.fields.dataGaLocation,
          },
        })),
      },
    };
  }

  private mapGetStartedSteps(ctfFaq: CtfFaq) {
    const groups = ctfFaq.multipleAccordionGroups.map(
      (group: CtfEntry<CtfFaq>) => {
        const { fields } = group;

        const items = fields.singleAccordionGroupItems?.map(
          (item: CtfEntry<any>) => ({
            title: item.fields.header,
            copies: item.fields.card.map((card: CtfEntry<CtfCard>) => ({
              title: card.fields.title,
              text: card.fields.description,
              link: card.fields.button && {
                text: card.fields.button?.fields?.text,
                href: card.fields.button?.fields?.externalUrl,
                ga_name: card.fields.button?.fields?.dataGaName,
                ga_location: card.fields.button?.fields?.dataGaLocation,
              },
            })),
          }),
        );

        return {
          id: fields.id,
          header: fields.title,
          show: fields.expandAllCarouselItemsText,
          hide: fields.hideAllCarouselItemsText,
          items,
        };
      },
    );

    return {
      name: COMPONENT_NAMES.GET_STARTED_STEPS,
      data: {
        groups,
      },
    };
  }

  private mapGetStartedCopyInfo(ctfHeaderAndText: CtfHeaderAndText) {
    return {
      name: COMPONENT_NAMES.GET_STARTED_COPY_INFO,
      data: {
        title: ctfHeaderAndText.header,
        text: ctfHeaderAndText.text,
      },
    };
  }

  private mapGetStartedNextSteps(ctfCardGroup: CtfCardGroup) {
    return {
      name: COMPONENT_NAMES.GET_STARTED_NEXT_STEPS,
      data: {
        ...ctfCardGroup.customFields,
        header: ctfCardGroup.header,
        cards: ctfCardGroup.card.map((card: CtfEntry<CtfCard>) => ({
          ...card.fields.customFields,
          title: card.fields.title,
          text: card.fields.description,
          avatar: getUrlFromContentfulImage(card.fields.image),
          link: card.fields.button && {
            text: card.fields.button.fields.text,
            url: card.fields.button.fields.externalUrl,
            data_ga_name: card.fields.button.fields.dataGaName,
            data_ga_location: card.fields.button.fields.dataGaLocation,
          },
        })),
      },
    };
  }
}
