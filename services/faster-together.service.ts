export function softwareFasterDataHelper(pageContent: any[], metadata) {
  let hero,
    featured_content,
    customer_logos,
    devsecopsHero,
    devsecopsCards,
    by_industry_case_studies;

  pageContent.forEach((item) => {
    switch (item.fields.componentName) {
      case 'SoftwareFasterHero':
        hero = {
          title: item.fields?.title,
          image: {
            url: item.fields?.backgroundImage?.fields.file.url,
            alt: item.fields?.backgroundImage?.fields?.alt,
          },
          secondary_button: {
            video_url: item.fields?.video?.fields.url,
            text: item.fields?.primaryCta?.fields.text,
            data_ga_name: item.fields?.primaryCta?.fields.dataGaName,
            data_ga_location: item.fields?.primaryCta?.fields.dataGaLocation,
          },
        };
        break;
      case 'SoftwareFasterFeaturedContent':
        featured_content = {
          col_size: 4,
          header: item.fields?.header,
          case_studies: item.fields?.card.map((item) => ({
            header: item.fields?.title,
            description: item.fields?.description,
            showcase_img: {
              url: item.fields?.image?.fields.file.url,
              alt: '',
            },
            logo_img: {
              url: item.fields?.contentArea.filter(
                (item) => item.sys.contentType.sys.id === 'asset',
              )[0]?.fields?.image.fields.file.url,
              alt: item.fields?.contentArea.filter(
                (item) => item.sys.contentType.sys.id === 'asset',
              )[0]?.fields?.altText,
            },
            link: {
              video_url: item.fields?.contentArea.filter(
                (item) => item.sys.contentType.sys.id === 'externalVideo',
              )[0]?.fields?.url,
              href: item.fields?.button?.fields?.externalUrl,
              text: item.fields.button.fields.text,
              data_ga_name: item.fields?.button.fields.dataGaName,
              data_ga_location: item.fields.button.fields.dataGaLocation
                ? item.fields.button.fields.dataGaLocation
                : 'body',
            },
          })),
        };
        break;
      case 'SoftwareFasterCustomerLogos':
        customer_logos = {
          text: item.fields?.text,
          showcased_enterprises: item.fields?.logo.map((item) => ({
            image_url: item.fields?.image?.fields.file.url,
            alt: item.fields.altText,
            url: item.fields.link,
          })),
        };
        break;
      case 'SoftwareFasterDevSecOps':
        devsecopsHero = {
          header: item.fields?.title,
          image_url: item.fields?.backgroundImage.fields.file.url,
          link: {
            text: item.fields?.primaryCta?.fields.text,
            data_ga_name: item.fields?.primaryCta?.fields.dataGaName,
            data_ga_location: item.fields.primaryCta.fields.dataGaLocation,
          },
        };
        break;
      case 'SoftwareFasterDevSecOpsCards':
        devsecopsCards = {
          cards: item.fields?.card.map((item) => ({
            header: item.fields?.title,
            description: item.fields?.description,
            icon: item.fields?.iconName,
          })),
        };
        break;
      case 'SoftwareFasterCaseStudies':
        by_industry_case_studies = {
          title: item.fields?.header,
          charcoal_bg: true,
          rows: item.fields?.card.map((item) => ({
            title: item.fields?.title,
            subtitle: item.fields?.description,
            image: {
              url: item.fields?.image.fields.file.url,
              alt: '',
            },
            button: {
              href: item.fields?.button?.fields.externalUrl,
              text: item.fields?.button?.fields.text,
              data_ga_name: item.fields?.button?.fields.dataGaName,
              data_ga_location: item.fields?.button?.fields.dataGaLocation,
            },
            icon: {
              name: item.fields?.iconName,
              alt: '',
            },
          })),
        };
        break;
      default:
        break;
    }
  });

  const devsecops = { ...devsecopsHero, ...devsecopsCards };

  return {
    title: metadata[0].fields?.ogTitle,
    description: metadata[0].fields?.ogDescription,
    title_image: metadata[0].fields?.ogImage?.fields.image.fields.file.url,
    twitter_image: metadata[0].fields?.ogImage?.fields.image.fields.file.url,

    hero,
    featured_content,
    customer_logos,
    devsecops,
    by_industry_case_studies,
  };
}
