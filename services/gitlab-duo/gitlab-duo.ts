export function mapGitlabDuo(items) {
  let [
    hero,
    journey,
    video,
    featureCards,
    principles,
    resources,
    reportCta,
    // eslint-disable-next-line prefer-const
    benefits,
  ] = items[0].fields.pageContent;
  //  Format content from contentful
  // Hero content

  hero = {
    button: {
      href: hero?.fields?.primaryCta?.fields?.externalUrl,
      text: hero?.fields?.primaryCta?.fields?.text,
      dataGaName: hero?.fields?.primaryCta?.fields?.dataGaName,
      dataGaLocation: hero?.fields?.primaryCta?.fields?.dataGaLocation,
      variant: 'secondary',
    },
    secondaryButton: hero?.fields?.secondaryCta?.fields?.text && {
      href: hero?.fields?.secondaryCta?.fields?.externalUrl,
      text: hero?.fields?.secondaryCta?.fields?.text,
      dataGaName: hero?.fields?.secondaryCta?.fields?.dataGaName,
      dataGaLocation: hero?.fields?.secondaryCta?.fields?.dataGaLocation,
      variant: 'primary',
    },
    description: hero?.fields?.subheader,
    image: {
      alt: hero?.fields?.backgroundImage?.fields?.title,
      src: hero?.fields?.backgroundImage?.fields?.file?.url,
    },
    ...hero.fields.customFields,
  };

  // Journey cards content

  journey = {
    features: journey?.fields?.data?.features,
  };

  // Video

  video = {
    text: video?.fields?.title,
    source: video?.fields?.url,
  };

  // Feature cards

  featureCards = {
    title: featureCards?.fields?.header,
    cards: featureCards?.fields?.card?.map((card: any) => ({
      name: card?.fields?.title,
      description: card?.fields?.description,
      href: card?.fields?.cardLink,
      icon: card?.fields?.iconName,
      data_ga_location: card?.fields?.cardLinkDataGaLocation,
      data_ga_name: card?.fields?.cardLinkDataGaName,
    })),
  };

  // Principles

  principles = principles?.fields?.data;

  //  Resources

  resources = {
    data: {
      column_size: 4,
      title: resources?.fields?.header,
      cards: resources?.fields?.card?.map((card: any) => ({
        link_text: 'Read more',
        data_ga_location: 'body',
        event_type: 'Blog',
        data_ga_name: card?.fields?.title,
        header: card?.fields?.title,
        href: card?.fields?.cardLink,
        icon: {
          name: card?.fields?.iconName,
          variant: 'marketing',
          alt: 'blog icon',
        },
        image: card?.fields?.image?.fields?.file?.url,
      })),
    },
  };

  //  Report CTA

  reportCta = {
    layout: 'dark',
    title: reportCta?.fields?.header,
    reports: reportCta?.fields?.card?.map((report: any) => ({
      url: report?.fields?.button?.fields?.externalUrl,
      description: report?.fields?.description,
      data_ga_name: report?.fields?.cardLinkDataGaName,
      data_ga_location: report?.fields?.cardLinkDataGaLocation,
    })),
  };

  const metadata = {
    title: items[0]?.fields?.seoMetadata[0]?.fields?.ogTitle,
    ogTitle: items[0]?.fields?.seoMetadata[0]?.fields?.ogTitle,
    description: items[0]?.fields?.seoMetadata[0]?.fields?.description,
    ogDescription: items[0]?.fields?.seoMetadata[0]?.fields?.ogDescription,
    ogType: items[0]?.fields?.seoMetadata[0]?.fields?.ogType,
    ogUrl: items[0]?.fields?.seoMetadata[0]?.fields?.ogUrl,
    ogSiteName: items[0]?.fields?.seoMetadata[0]?.fields?.ogSiteName,
    ogImage: items[0]?.fields?.seoMetadata[0]?.fields?.ogImage,
  };
  return {
    hero,
    journey,
    video,
    featureCards,
    principles,
    resources,
    reportCta,
    metadata,
    benefits,
  };
}

export function mapGitlabDuoEnglishContent(items) {
  let [hero] = items[0].fields.pageContent;

  hero = {
    button: {
      href: hero?.fields?.primaryCta?.fields?.externalUrl,
      text: hero?.fields?.primaryCta?.fields?.text,
      dataGaName: hero?.fields?.primaryCta?.fields?.dataGaName,
      dataGaLocation: hero?.fields?.primaryCta?.fields?.dataGaLocation,
      variant: 'secondary',
    },
    secondaryButton: hero?.fields?.secondaryCta?.fields?.text && {
      href: hero?.fields?.secondaryCta?.fields?.externalUrl,
      text: hero?.fields?.secondaryCta?.fields?.text,
      dataGaName: hero?.fields?.secondaryCta?.fields?.dataGaName,
      dataGaLocation: hero?.fields?.secondaryCta?.fields?.dataGaLocation,
      variant: 'primary',
    },
    description: hero?.fields?.subheader,
    image: {
      alt: '',
      src: 'https://images.ctfassets.net/xz1dnu24egyd/2POqAdE7x75qplvXOgPJqA/1aec09006ae6e1f5eaa59452cf0216ec/Meet-GitLab-Duo-Thumbnail.png',
    },
    video: {
      video_url:
        'https://player.vimeo.com/video/855805049?title=0&byline=0&portrait=0&badge=0&autopause=0&player_id=0&app_id=58479',
    },
  };

  const metadata = {
    title: items[0]?.fields?.seoMetadata[0]?.fields?.ogTitle,
    ogTitle: items[0]?.fields?.seoMetadata[0]?.fields?.ogTitle,
    description: items[0]?.fields?.seoMetadata[0]?.fields?.description,
    ogDescription: items[0]?.fields?.seoMetadata[0]?.fields?.ogDescription,
    ogType: items[0]?.fields?.seoMetadata[0]?.fields?.ogType,
    ogUrl: items[0]?.fields?.seoMetadata[0]?.fields?.ogUrl,
    ogSiteName: items[0]?.fields?.seoMetadata[0]?.fields?.ogSiteName,
    ogImage: items[0]?.fields?.seoMetadata[0]?.fields?.ogImage,
  };

  const pricing = [
    {
      variant: 'tier',
      featured: true,
      data: {
        featured: false,
        price: 19,
        price_info: 'per user/month,',
        description: 'billed annually',
        hide_on_mobile: true,
        background_gradient: false,
        features: {
          list: [
            {
              text: '__Features include:__\n\n__Organizational User Controls__\n\n- User permissions for AI capabilities\n\n__Code Suggestions__\n\n- Code generation\n- Code completion\n- Available in many popular IDEs and supports 15 programming languages\n\n__Chat*__\n\n- Code explanation*\n- Text generation*\n- Code refactoring*',
            },
          ],
        },
        pill: 'New',
        title: 'GitLab Duo',
        subtitle: 'Pro',
        cardDescription:
          'For developers that want to focus on innovation and deliver high-quality software',
        card_note:
          '*In Beta\n\nOnly available for Premium and Ultimate customers.',
        primaryButton: {
          text: 'Contact us',
          disabled: false,
          url: 'https://about.gitlab.com/solutions/gitlab-duo-pro/sales/',
          data_ga_name: 'contact us',
          data_ga_location: 'body',
        },
        secondaryButton: {
          text: 'Existing customer? Buy now',
          url: 'https://docs.gitlab.com/ee/subscriptions/subscription-add-ons.html ',
          data_ga_name: 'duo pro subscription',
          data_ga_location: 'body',
        },
      },
    },
    {
      variant: 'tier',
      featured: true,
      data: {
        featured: false,
        price: 39,
        price_info: 'per user/month,',
        description: 'billed annually',
        hide_on_mobile: true,
        background_gradient: true,
        features: {
          list: [
            {
              text: '__Everything from GitLab Duo Pro, plus:__\n\n__Summarization and Templating tools__\n\n- Discussion summary\n- Merge request summary\n- Code review summary\n- Merge request template population\n\n__Security and vulnerability tools__\n\n- Vulnerability explanation\n- Vulnerability resolution\n\n__Advanced Troubleshooting__\n\n- Root Cause Analysis\n\n__AI analytics__\n\n- Value stream forecasting\n- AI Impact and Productivity Reporting\n\n__Personalize GitLab Duo__\n\n- Self-Hosted Model Deployment*\n- Model Personalization*',
            },
          ],
        },
        pill: 'Coming soon',
        title: 'GitLab Duo',
        subtitle: 'Enterprise',
        cardDescription:
          'For organizations that want AI throughout the software development lifecycle',
        card_note:
          '*Planned. Additional terms and fees may apply\n\nOnly available for Ultimate customers.\n',
        primaryButton: {
          text: 'Coming soon',
          disabled: true,
        },
      },
    },
  ];

  const resources = {
    id: 'resources',
    align: 'left',
    grouped: true,
    column_size: 4,
    title: 'Learn more about GitLab Duo',
    header_cta_text: 'View all resources',
    header_cta_href: '/resources/',
    header_cta_ga_name: 'View all resources',
    header_cta_ga_location: 'body',
    cards: [
      {
        icon: { name: 'blog', variant: 'marketing' },
        event_type: 'Blog',
        header:
          'Understand and resolve vulnerabilities with AI-powered GitLab Duo',
        image:
          'https://s3-alpha-sig.figma.com/img/07e8/cb30/e15d3f64360a3fd79af4b4124339621c?Expires=1713139200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=gr~qG3OzIrrcvl3MkaYEoX7M9UaVEexWLtVdY712Wm2I-L8huQC~dcXQW2J9w-SHova8LRp1TSsd2UzvftOmYrpmSRlNp6nfTOdMJskv2~SH-d2lzGytqusEmfPMwQuuHdoUurNYOlezhSRGt3uBpm6GJ4ERrY-bRPyJ3SKnIgP-Eh9-MnWmiS7~USS-wKiO2juMi4WHWe5Vlouc-hcznEO66I-KUi-3g~WKf-cdDbk0CZlg06rtMxc59e-kuz10~0Zur-H73ynaZgCmEQXrLsyjBlnufeyTHjHs8g5l5R8nzZRp8F0R-EZuMKEO8xF62Rfy8wJZP~oyroF1Y3D7jA__',
        href: '/blog/2024/02/21/understand-and-resolve-vulnerabilities-with-ai-powered-gitlab-duo/',
        data_ga_name:
          'Understand and resolve vulnerabilities with AI-powered GitLab Duo',
      },
      {
        icon: { name: 'blog', variant: 'marketing' },
        event_type: 'Blog',
        header:
          'Measuring AI effectiveness beyond developer productivity metrics',
        image:
          'https://s3-alpha-sig.figma.com/img/ba8e/9c1e/eea7a9ccea7d01c8bf3d5391a0900cbf?Expires=1713139200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=lxaFmBqpJ9~aqgpzblBnx9sSp4ornH2NSr8Ll9CTvP6g227K7gtjq4wUy4285NGTh2q-li~2xc3dPFu3WJwyV68nnd0jVr-12lmedHA5AsByOizNTVhrJDW3vvKKQQfcWTGC9qWv8Y36GnMq15lM4jjCoHZZIYP-XYp91jM0UGWgApymJC2wwmyZBrDUbUjhEry-PDZFRraP-5ozDX81hibEf5L8Rl0~UAESnRQalNZ5pRUE22T2PHH4pvLjMR8VUsJgWafJTQ0A~7HBmfQiWIiIhQ~H8HyUhnvHWaFzZgjpp8tW--oI98i75xP45JGXo19~V4VE0nD7X3QHIUY3jw__',
        href: '/blog/2024/02/20/measuring-ai-effectiveness-beyond-developer-productivity-metrics/',
        data_ga_name:
          'Measuring AI effectiveness beyond developer productivity metrics',
      },
      {
        icon: { name: 'blog', variant: 'marketing' },
        event_type: 'Blog',
        header:
          'New report on AI-assisted tools points to rising stakes for DevSecOps',
        image:
          'https://s3-alpha-sig.figma.com/img/bf8f/8ea9/579e14ca5b0fe21f0f7c44255efb76e2?Expires=1713139200&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=MGcjXAET8lT97tcu5flvMa6B3URHhYmQRd86YtmatgRgdN6ccILYWNtRKieaB1mxf0t~E19d2FVni1O135khyqQXVQ9gBA1In~2PdpIoLcNuLxubveRpQ4miiVmaefYtjSI7qZ~u8btSjXwDj6vDEXmbSU1fHYPDqxaHnHfTq~3MUIz-W3EhVsnYPW7PGyPGEhCbFLMcYx6jn3AYvMqKraUu-NbK4ai6JAg0-dr-sesWbSawaTMrLqeQ80mdvI-q0vxdtS7NZTai95ZXyJftnAjH65ig7vROcOs645xNbQJvNTEve9Opo5LoogxbDE9R9LeFTMN~SW2VIp3LT2KYfg__',
        href: '/blog/2024/02/14/new-report-on-ai-assisted-tools-points-to-rising-stakes-for-devsecops/',
        data_ga_name:
          'New report on AI-assisted tools points to rising stakes for DevSecOps',
      },
    ],
  };

  return {
    hero,
    metadata,
    pricing,
    resources,
  };
}
