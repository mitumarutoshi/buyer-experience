import { Context } from '@nuxt/types';
import { getClient } from '~/plugins/contentful';
import { CONTENT_TYPES } from '~/common/content-types';
import {
  CtfCardGroup,
  CtfEntry,
  CtfHeaderAndText,
  CtfHero,
  CtfQuote,
} from '~/models';
import { getUrlFromContentfulImage } from '~/common/util';

export class EnvironmentalSocialGovernanceService {
  private readonly $ctx: Context;

  private readonly componentNames = {
    HIGHLIGHTS: 'highlights',
    LEARN_MORE: 'learn-more',
    SHOWCASE: 'showcase',
  };

  constructor($context: Context) {
    this.$ctx = $context;
  }

  async getContent() {
    const environmentalSocialGovernanceEntry = await getClient().getEntries({
      content_type: CONTENT_TYPES.PAGE,
      'fields.slug': 'environmental-social-governance',
      include: 10,
    });

    if (environmentalSocialGovernanceEntry.items.length === 0) {
      throw new Error('Not found');
    }

    const [items] = environmentalSocialGovernanceEntry.items;
    return { items, esg: this.transformContentfulData(items) };
  }

  transformContentfulData(ctfData: any) {
    const { pageContent, seoMetadata } = ctfData.fields;

    const mappedContent = this.getCtfComponents(pageContent);

    return {
      ...seoMetadata[0]?.fields,
      ...mappedContent,
    };
  }

  private getCtfComponents(pageContent: CtfEntry<any>[]) {
    const result = {
      showcase: {
        overview: null,
        sections: [],
      },
    };

    for (const component of pageContent) {
      const { id } = component.sys.contentType.sys;
      const { componentName } = component.fields;

      switch (id) {
        case CONTENT_TYPES.HERO: {
          result.hero = this.mapHero(component.fields);
          break;
        }
        case CONTENT_TYPES.QUOTE: {
          result.letter = this.mapLetter(component.fields);
          break;
        }
        case CONTENT_TYPES.CARD_GROUP: {
          if (componentName === this.componentNames.HIGHLIGHTS) {
            result.highlights = this.mapHighlights(component.fields);
          }

          if (componentName === this.componentNames.LEARN_MORE) {
            result.learn_more = this.mapLearnMore(component.fields);
          }

          if (componentName === this.componentNames.SHOWCASE) {
            result.showcase.sections.push(
              this.mapShowcaseSection(component.fields),
            );
          }

          break;
        }
        case CONTENT_TYPES.HEADER_AND_TEXT: {
          result.showcase.overview = this.mapShowcaseOverview(component.fields);
        }
      }
    }

    return result;
  }

  private mapHero(ctfHero: CtfHero) {
    const button = ctfHero.primaryCta && {
      text: ctfHero.primaryCta.fields.text,
      href: ctfHero.primaryCta.fields.externalUrl,
      icon: {
        variant: ctfHero.primaryCta.fields.iconVariant,
        name: ctfHero.primaryCta.fields.iconName,
      },
    };

    const image = ctfHero.backgroundImage && {
      src: getUrlFromContentfulImage(ctfHero?.backgroundImage),
      alt: ctfHero?.backgroundImage?.fields?.title,
    };

    return {
      header: ctfHero.title,
      description: ctfHero.description,
      image,
      button,
    };
  }

  private mapLetter(ctfQuote: CtfQuote) {
    const image = ctfQuote.authorHeadshot && {
      src: getUrlFromContentfulImage(ctfQuote.authorHeadshot.fields.image),
      alt: ctfQuote.authorHeadshot.fields.altText,
    };

    return {
      author: {
        name: ctfQuote.author,
        role: ctfQuote.authorTitle,
        image,
      },
      text: ctfQuote.quoteText,
    };
  }

  private mapHighlights(ctfCardGroup: CtfCardGroup) {
    return {
      header: ctfCardGroup.header,
      highlights:
        ctfCardGroup.card?.map((card) => ({
          image: {
            src: getUrlFromContentfulImage(card.fields.image),
            alt: card.fields.image.fields.description,
          },
          text: card.fields.description,
        })) || [],
    };
  }

  private mapLearnMore(ctfCardGroup: CtfCardGroup) {
    return {
      header: ctfCardGroup.header,
      cards:
        ctfCardGroup.card?.map((card) => ({
          title: card.fields.title,
          icon: card.fields.iconName,
          type: card.fields.subtitle,
          href: card.fields.cardLink,
        })) || [],
    };
  }

  private mapShowcaseSection(ctfCardGroup: CtfCardGroup) {
    return {
      id: ctfCardGroup.headerAnchorId,
      name: ctfCardGroup.header,
      blocks:
        ctfCardGroup.card?.map((card) => ({
          header: card.fields.title,
          description: card.fields.description,
          image: card.fields.image && {
            src: getUrlFromContentfulImage(card.fields.image),
            alt: card.fields.image.fields.description,
          },
        })) || [],
      ...ctfCardGroup.customFields,
    };
  }

  private mapShowcaseOverview(ctfHeaderAndText: CtfHeaderAndText) {
    return {
      id: ctfHeaderAndText.headerAnchorId,
      title: ctfHeaderAndText.header,
      text: {
        description: ctfHeaderAndText.text,
      },
      textVariant: 'heading4',
    };
  }
}
