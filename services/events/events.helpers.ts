import { CONTENT_TYPES } from '~/common/content-types';
import { CtfEventPage, CtfFaq, CtfHeaderAndText } from '~/models';

export function faqItems(group: CtfHeaderAndText[]) {
  return group.map((item: CtfHeaderAndText) => {
    return {
      question: item.header,
      answer: item.text,
    };
  });
}

export function faqHelper(data: CtfFaq) {
  const questions =
    data.singleAccordionGroupItems &&
    faqItems(data.singleAccordionGroupItems as CtfHeaderAndText[]);

  return {
    header: data.title,
    data_inbound_analytics: data.dataInboundAnalytics,
    background: data.background,
    show_all_button: data.showAllButton,
    texts: {
      show: data.expandAllCarouselItemsText,
      hide: data.hideAllCarouselItemsText,
    },
    groups: [
      {
        header: data.subtitle,
        questions,
      },
    ],
  };
}

export function metaDataHelper(data) {
  const seo =
    data.seo?.fields || data.seoMetadata?.metadata || data.fields || {};
  return {
    title: seo.title,
    og_title: seo.title,
    description: seo.description,
    twitter_description: seo.description,
    image_title: seo?.ogImage && seo.ogImage.fields.image.fields.file.url,
    og_description: seo.description,
    og_image: seo?.ogImage && seo.ogImage.fields.image.fields.file.url,
    twitter_image: seo?.ogImage && seo.ogImage.fields.image.fields.file.url,
    noIndex: seo.noIndex,
  };
}

export function imageBuilder(data) {
  return {
    alt: data?.altText || data?.title || '',
    src: data?.image?.fields?.file?.url || data?.file?.url || '',
    description: data?.image?.fields?.description || '',
  };
}

export function dateHelper(
  date,
  options = { year: 'numeric', month: 'long', day: 'numeric' },
) {
  const tempDate = new Date(date);
  return tempDate.toLocaleDateString('en-US', options);
}

export function createEventCards(card: CtfEventPage) {
  const date = dateHelper(card.date, { month: 'long', day: 'numeric' });
  const image =
    card.seo?.fields?.ogImage?.fields &&
    imageBuilder(card.seo.fields.ogImage?.fields);

  return {
    location: card.location,
    locationState: card.locationState,
    date,
    image,
    slug: card.slug,
    eventCardEyebrowImage:
      card.eventCardEyebrowImage &&
      imageBuilder(card.eventCardEyebrowImage.fields),
  };
}

export function objectCleanUp(element) {
  if (element.fields.assets) {
    element.fields.assets = element.fields.assets.map((asset) => asset.fields);
  }
  if (element.fields.cta) {
    element.fields.cta = element.fields.cta.fields;
  }
  if (element.fields.backgroundImage) {
    element.fields.backgroundImage = imageBuilder(
      element.fields.backgroundImage.fields,
    );
  }
  if (element.fields.primaryCta) {
    element.fields.primaryCta = element.fields.primaryCta.fields;
  }
  if (element.fields.card) {
    element.fields.card = element.fields.card.map(
      (card) => card.fields && createEventCards(card.fields),
    );
  }
  if (element.fields.singleAccordionGroupItems) {
    element.fields.singleAccordionGroupItems =
      element.fields.singleAccordionGroupItems.map(
        (accordion) => accordion.fields,
      );
  }
  return element.fields;
}

export function mapPage(array) {
  let hero = {};
  const regions = [];
  let form = {};
  let faq = {};
  let banner = {};
  let nextSteps;
  let metadata;

  array.forEach((element) => {
    switch (element.sys?.contentType?.sys?.id) {
      case CONTENT_TYPES.HERO:
        hero = objectCleanUp(element);
        break;
      case CONTENT_TYPES.CARD_GROUP:
        regions.push({ ...objectCleanUp(element) });
        break;
      case CONTENT_TYPES.FORM:
        form = element.fields;
        break;
      case CONTENT_TYPES.FAQ:
        faq = objectCleanUp(element);
        break;
      case CONTENT_TYPES.HEADER_AND_TEXT:
        banner = objectCleanUp(element);
        break;
      case CONTENT_TYPES.NEXT_STEPS:
        nextSteps = element.fields.variant;
        break;
      case CONTENT_TYPES.SEO:
        metadata = element.fields;
        break;
      default:
        break;
    }
  });
  return { metadata, hero, regions, form, faq, banner, nextSteps };
}

export function pageCleanup(data) {
  const mappedData = {};

  Object.keys(data).forEach((key) => {
    if (Array.isArray(data[key])) {
      mappedData[key] = mapPage(data[key]);
    } else {
      mappedData[key] = data[key];
    }
  });

  return mappedData;
}

export function speakerBuilder(speaker) {
  return {
    name: speaker.author || '',
    title: speaker.authorTitle || '',
    company: speaker.authorCompany || '',
    image:
      speaker.authorHeadshot && imageBuilder(speaker.authorHeadshot?.fields),
    biography: speaker.quoteText || '',
    class: speaker.authorHeadshotCustomClass || '',
  };
}

export function mapArray(array) {
  return array
    .filter((element) => element.fields)
    .map((element) => objectCleanUp(element));
}

export function dataCleanUp(data) {
  const mappedData = {};

  Object.keys(data).forEach((key) => {
    if (Array.isArray(data[key])) {
      mappedData[key] = mapArray(data[key]);
    } else {
      mappedData[key] = data[key];
    }
  });

  return mappedData;
}

export function heroHelper(data) {
  const herofields = data.hero?.fields || null;
  const hero = {
    headline: data.name || '',
    accent: herofields?.title || '',
    subHeading: herofields?.subheader || '',
    list: (data.hero && herofields?.description?.split('\n')) || [],
    cta: herofields?.primaryCta?.fields || {},
  };

  return hero;
}

export function worldTourHeroHelper(data, breadcrumbs) {
  return {
    breadcrumbs,
    header: data.title,
    text: data.description,
    image: data.backgroundImage && imageBuilder(data.backgroundImage?.fields),
    cta: data.cta || data.primaryCta?.fields || {},
  };
}

export function worldTourLandingHeroBuilder(data) {
  const hero = {
    header: data.title || '',
    subtitle: data.description || '',
    image: data.backgroundImage,
    button: data.cta || data.primaryCta || {},
  };
  return hero;
}

export function sectionBuilder(section, key) {
  const { header, text, subheader, cta, assets } = section;
  let parsedAssets = assets;
  if (assets) {
    parsedAssets = assets.map((asset) => {
      if (asset.image) {
        return imageBuilder(asset);
      }
      if (asset.authorHeadshot) {
        return speakerBuilder(asset);
      }
      return parsedAssets;
    });
  }
  return { header, text, subheader, cta, [key || 'assets']: parsedAssets };
}

export function sectionsHelper(data, key) {
  let sections = [];
  if (data) {
    sections = data.map((section) => {
      return sectionBuilder(section, key);
    });
  }
  return sections;
}

export function worldTourAgendaHelper(data) {
  const agendaIntro = sectionBuilder(data?.shift());
  const agenda = data.map((session) => {
    const speakers =
      session.assets && session.assets.map((asset) => speakerBuilder(asset));
    return {
      time: session.header,
      name: session.subheader,
      description: session.text,
      speakers,
    };
  });

  return { ...agendaIntro, agenda };
}

export function formHelper(data) {
  return dataCleanUp(data);
}

export function regionsBuilder(data) {
  let regions = [];
  if (data) {
    regions = data.map((region) => {
      return { name: region.header, cities: region.card };
    });
  }
  return regions;
}
