import { Context } from '@nuxt/types';
import {
  dataCleanUp,
  dateHelper,
  faqHelper,
  formHelper,
  metaDataHelper,
  objectCleanUp,
  pageCleanup,
  regionsBuilder,
  sectionsHelper,
  worldTourAgendaHelper,
  worldTourHeroHelper,
  worldTourLandingHeroBuilder,
} from '~/services/events/events.helpers';
import { getClient } from '~/plugins/contentful';
import { CONTENT_TYPES } from '~/common/content-types';
import { CtfEntry, CtfEventPage, CtfPage } from '~/models';
import {
  WorldTourEventsData,
  WorldTourExecutiveEventsData,
  WorldTourLandingData,
} from '~/models/events/world-tour.dto';

export class WorldTourEventsService {
  private readonly $ctx: Context;

  constructor($context: Context) {
    this.$ctx = $context;
  }

  private async getContentfulData(slug: string): Promise<CtfEntry<CtfPage>> {
    const { items } = await getClient().getEntries({
      content_type: CONTENT_TYPES.PAGE,
      include: 10,
      'fields.slug': slug,
    });

    if (items.length === 0) {
      throw new Error('World Tour page not found in Contentful');
    }

    const [contentfulPage] = items;

    return contentfulPage as CtfEntry<CtfPage>;
  }

  private async getEventsContentfulData(slug: string) {
    const { items } = await getClient().getEntries({
      content_type: CONTENT_TYPES.EVENT,
      include: 10,
      'fields.slug': slug,
    });

    if (items.length === 0) {
      throw new Error('World Tour page not found in Contentful');
    }

    const [contentfulEventPage] = items;

    return contentfulEventPage as CtfEntry<CtfEventPage>;
  }

  async getLandingContent() {
    const { fields, sys } = await this.getContentfulData(
      'devsecops-world-tour',
    );
    const spaceId = sys.space.sys.id;
    const entryId = sys.id;

    return {
      content: this.worldTourLandingHelper(fields),
      spaceId,
      entryId,
    };
  }

  async getWorldTourEvent(slug: string) {
    const { fields, sys } = await this.getEventsContentfulData(slug);
    const spaceId = sys.space.sys.id;
    const entryId = sys.id;
    const isExecutive = slug.includes('executive');
    const city = isExecutive
      ? this.worldTourExecutiveEventsDataHelper(fields)
      : this.worldTourEventsDataHelper(fields);

    return {
      city,
      spaceId,
      entryId,
    };
  }

  worldTourLandingHelper(data: CtfEventPage): WorldTourLandingData {
    const rawData = pageCleanup(data) as WorldTourLandingData;

    return {
      title: rawData.title,
      slug: rawData.slug,
      description: rawData.description,
      hero:
        rawData.pageContent &&
        worldTourLandingHeroBuilder(rawData.pageContent.hero),
      regions:
        rawData.pageContent && regionsBuilder(rawData.pageContent.regions),
      form: rawData.pageContent?.form,
      faq:
        rawData.pageContent &&
        rawData.pageContent.faq &&
        faqHelper(rawData.pageContent.faq),
      metadata: metaDataHelper(rawData),
      banner: rawData.pageContent?.banner,
      nextSteps: rawData.pageContent?.nextSteps,
    };
  }

  worldTourEventsDataHelper(data: CtfEventPage): WorldTourEventsData {
    const rawData = dataCleanUp(data) as WorldTourEventsData;
    const date = rawData.date && dateHelper(rawData.date);

    return {
      metadata: metaDataHelper(rawData),
      name: rawData.name,
      internalName: rawData.internalName,
      slug: rawData.slug,
      eventType: rawData.eventType,
      description: rawData.description,
      date,
      endDate: rawData.endDate,
      location: rawData.location,
      locationState: rawData.locationState,
      region: rawData.region,
      hero:
        rawData.hero &&
        worldTourHeroHelper(
          rawData.hero.fields,
          rawData.staticFields.breadcrumbs,
        ),
      agenda: rawData.agenda && worldTourAgendaHelper(rawData.agenda),
      form: formHelper(rawData.form.fields),
      sponsors:
        rawData.featuredContent &&
        sectionsHelper(rawData.featuredContent, 'sponsors'),
      footnote: rawData.footnote,
      nextSteps: rawData.nextSteps?.fields.variant,
    };
  }

  worldTourExecutiveEventsDataHelper(
    data: CtfEventPage,
  ): WorldTourExecutiveEventsData {
    const rawData = dataCleanUp(data) as WorldTourExecutiveEventsData;
    const date = rawData.date && dateHelper(rawData.date);

    return {
      metadata: metaDataHelper(rawData),
      name: rawData.name,
      internalName: rawData.internalName,
      slug: rawData.slug,
      eventType: rawData.eventType,
      description: rawData.description,
      date,
      endDate: rawData.endDate,
      location: rawData.location,
      region: rawData.region,
      hero:
        rawData.hero &&
        rawData.hero.fields &&
        worldTourHeroHelper(
          rawData.hero.fields,
          rawData.staticFields.breadcrumbs,
        ),
      blurb: objectCleanUp(rawData.blurb),
      form: rawData.form && formHelper(rawData.form.fields),
      sponsors:
        rawData.featuredContent &&
        sectionsHelper(rawData.featuredContent, 'sponsors'),
      footnote: rawData.footnote,
      nextSteps: rawData.nextSteps?.fields?.variant,
    };
  }
}
