import { Context } from '@nuxt/types';
import { getClient } from '~/plugins/contentful';
import { CONTENT_TYPES } from '~/common/content-types';
import { CtfEventPage } from '~/models';
import { imageBuilder, metaDataHelper } from '~/services/events/events.helpers';

export class SummitEventService {
  private readonly $ctx: Context;

  constructor($context: Context) {
    this.$ctx = $context;
  }

  private async getContentfulEvent() {
    const { items } = await getClient().getEntries({
      content_type: CONTENT_TYPES.EVENT,
      'fields.slug': 'summit-las-vegas',
      include: 4,
    });

    if (items.length === 0) {
      throw new Error('Event page not found in Contentful');
    }

    return items;
  }

  async getContent() {
    const items = await this.getContentfulEvent();
    const [summitPage] = items;

    return {
      items,
      content: this.transformSummitData(summitPage.fields),
      spaceId: summitPage.sys.space.sys.id,
      entryId: summitPage.sys.id,
    };
  }

  transformSummitData(data: CtfEventPage) {
    const rawData = this.sectionBuilder(data);
    const hero = this.heroBuilder(rawData.hero);

    return {
      title: data.title,
      slug: data.slug,
      date: data.dateWithTime,
      description: data.description,
      metadata: metaDataHelper(data.seo),
      hero,
      blurb: rawData.blurb && {
        id: rawData.blurb?.headerAnchorId,
        ...rawData.blurb,
      },
      carousel: rawData.social && {
        subheader: rawData.social[0]?.header,
        info: rawData.social[0]?.text,
        cards: this.cardsBuilder(rawData.social[0]?.assets),
      },
      faq:
        rawData.featuredContent && this.faqHelper(rawData.featuredContent[0]),
      questions: rawData.footnote && {
        id: rawData.footnote[0]?.headerAnchorId,
        ...rawData.footnote[0],
      },
    };
  }

  heroBuilder(data) {
    return {
      title: data.title || '',
      text: data.description || '',
      video:
        (data.video && data.video?.fields?.image?.fields.file?.url) || null,
      primaryButton: data.primaryCta?.fields || {},
    };
  }

  cardsBuilder(array) {
    return array?.map((item) => imageBuilder(item.fields));
  }

  arrayCleanUp(array) {
    return array.map((item) => item.fields);
  }

  sectionBuilder(section) {
    const mappedData = {};

    Object.keys(section).forEach((key) => {
      if (Array.isArray(section[key])) {
        mappedData[key] = this.arrayCleanUp(section[key]);
      } else if (typeof section[key] === 'object') {
        mappedData[key] = section[key].fields;
      } else {
        mappedData[key] = section[key];
      }
    });
    return mappedData;
  }

  faqItems(group) {
    return group.map((item) => {
      return {
        question: item?.fields.header || '',
        answer: item?.fields.text || '',
      };
    });
  }

  faqHelper(data) {
    const questions =
      data.singleAccordionGroupItems &&
      this.faqItems(data.singleAccordionGroupItems);

    return {
      header: data.title || null,
      id: data.id || null,
      data_inbound_analytics: data.data_inbound_analytics || null,
      background: data.background || null,
      texts: {
        show: data.expandAllCarouselItemsText || null,
        hide: data.hideAllCarouselItemsText || null,
      },
      show_all_button: true,
      groups: [
        {
          header: data.subtitle || '',
          questions,
        },
      ],
    };
  }
}
