import { getUrlFromContentfulImage } from '~/common/util';

export function assemblePageContent(contentfulData) {
  const seoMetadata = contentfulData[0].fields.seoMetadata;
  const data = contentfulData[0].fields;
  let components = [];

  // build metadata
  const seoImage = getUrlFromContentfulImage(
    seoMetadata[0]?.fields?.ogImage?.fields?.image,
  );

  const metadata = {
    ...seoMetadata[0]?.fields,
    twitter_image: seoImage,
    og_image: seoImage,
    image_title: seoImage,
  };

  // Grab hero
  const hero = data.pageContent.filter(
    (obj) => obj.sys.contentType.sys.id === 'eventHero',
  );

  components.push({
    name: 'jobs-hero',
    data: {
      title: hero[0]?.fields.title,
      subheader: hero[0]?.fields.subheader,
    },
  });

  // Grab job division accordions component
  const divisionsBlock = data.pageContent.filter(
    (obj) => obj.sys.contentType.sys.id === 'faq',
  );

  const completeDivisionsList =
    divisionsBlock[0]?.fields.singleAccordionGroupItems.map((family) => {
      return {
        family: family.fields.header,
        departments: family.fields.blocks.map((block) => {
          return {
            departmentName: block.fields.jobDepartmentName,
            bucketIds: block.fields.greenhouseJobBucketIDs,
          };
        }),
      };
    });

  components.push({
    name: 'jobs-controls',
    data: {
      controls: data.schema,
      divisions: completeDivisionsList,
      expandText: {
        showAll:
          divisionsBlock[0]?.fields.expandAllCarouselItemsText || 'Show all',
        hideAll:
          divisionsBlock[0]?.fields.hideAllCarouselItemsText || 'Hide all',
      },
    },
  });

  const page = {
    metadata,
    components: components,
  };

  // Next Steps
  const nextSteps = data.pageContent.filter(
    (obj) => obj.sys.contentType.sys.id === 'nextSteps',
  );

  if (nextSteps) {
    page.nextStepsVariant = nextSteps[0]?.fields.variant;
  }

  return page;
}
