import { CONTENT_TYPES } from '~/common/content-types';
import { COMPONENT_NAMES } from '~/common/constants';
import {
  CtfCardGroup,
  CtfEntry,
  CtfTabControlsContainer,
  CtfTwoColumnBlock,
} from '~/models';

interface homePageData {
  title?: string;
  description?: string;
  hero_scroll_gallery?: any;
  customer_logos_block?: any;
  personalized_customer_logos?: any;
  intro_block?: any;
  blurb_grid?: any;
  feature_showcase?: any;
  tabbed_features?: any;
  recognition_spotlight?: any;
  quotes_carousel_block?: any;
  personalized_tabbed_case_studies?: any;
}

function buttonBuilder(buttonFields) {
  const isModal =
    buttonFields?.externalUrl && buttonFields?.externalUrl?.includes('vimeo');
  return {
    text: buttonFields?.text,
    link: (!isModal && buttonFields?.externalUrl) || '',
    href: buttonFields?.externalUrl || '',
    icon_name: buttonFields?.icon_name,
    icon_variant: buttonFields?.icon_variant,
    data_ga_name: buttonFields?.dataGaName,
    data_ga_location: buttonFields?.dataGaLocation,
    modal: isModal &&
      buttonFields?.externalUrl?.includes('vimeo') && {
        video_link: buttonFields?.externalUrl,
        video_title: buttonFields?.dataGaName,
      },
  };
}
function imageBuilder(imageData) {
  return {
    src: imageData.file.url,
    alt: imageData.title,
  };
}
function cardBuilder(cards) {
  return cards.map((card) => {
    const button =
      card.fields.button?.fields && buttonBuilder(card.fields.button.fields);
    const secondaryButton =
      card.fields.secondaryButton?.fields &&
      buttonBuilder(card.fields.secondaryButton.fields);
    return {
      type: card.fields.customFields?.type ?? 'card',
      title: card.fields?.title,
      subtitle: card.fields.subtitle && card.fields?.subtitle,
      blurb: card.fields?.description,
      button,
      primaryButton: button,
      secondaryButton,
      image:
        card.fields.image?.fields && imageBuilder(card.fields.image?.fields),
    };
  });
}
function customerBuilder(customers) {
  return customers.map((customer) => {
    return {
      image_url: customer.fields.image?.fields?.file?.url,
      alt: customer.fields.altText || '',
      url: customer.fields.link,
      size: customer.fields.customFields?.size,
    };
  });
}
function heroBuilder(fields: CtfCardGroup) {
  return {
    controls: { prev: 'previous slide', next: 'next slide' },
    slides: cardBuilder(fields.card),
  };
}

function customerLogosBuilder(section) {
  const sectionData = {
    text: {
      legend: section.text,
      url: section.link[0],
      data_ga_name: section.text,
      data_ga_location: section.componentName,
    },
    showcased_enterprises: customerBuilder(section.logo),
    segment: section.segment && section.segment,
  };

  return sectionData;
}
function introBuilder(section) {
  return {
    title: section.header,
    blurb: section.text,
    primaryButton: section.cta.fields && buttonBuilder(section.cta.fields),
    secondary_button:
      section.secondaryCta.fields && buttonBuilder(section.secondaryCta.fields),
    image:
      section.assets?.length &&
      imageBuilder(section.assets[0].fields.image.fields),
  };
}
function blurbGridBuilder(section) {
  return section.card.map((card) => {
    return {
      icon: { name: card.fields.iconName, size: 'md', variant: 'marketing' },
      title: card.fields.title,
      blurb: card.fields.description,
      list: card.fields.list,
      button:
        card.fields.button?.fields && buttonBuilder(card.fields.button.fields),
      demo: card.fields.contentArea?.length
        ? {
            title: card.fields.contentArea[0].fields?.title,
            subtitle: card.fields.contentArea[0].fields?.subtitle,
            description: card.fields.contentArea[0].fields?.description,
            video: {
              url: card.fields.contentArea[0].fields?.video?.fields.url,
              thumbnail:
                card.fields.contentArea[0].fields?.video?.fields?.thumbnail
                  ?.fields?.image?.fields.file.url,
              altText:
                card.fields.contentArea[0].fields?.video?.fields?.thumbnail
                  ?.fields?.image?.fields.title,
            },
            scheduleButton: {
              text: card.fields.contentArea[0].fields?.button?.fields.text,
              href: card.fields.contentArea[0].fields?.button?.fields
                .externalUrl,
              data_ga_name:
                card.fields.contentArea[0].fields?.button.fields.dataGaName,
              data_ga_location:
                card.fields.contentArea[0].fields?.button.fields.dataGaLocation,
            },
            launchButton: {
              text: card.fields.contentArea[0].fields?.secondaryButton?.fields
                .text,
              href: card.fields.contentArea[0].fields?.secondaryButton?.fields
                .externalUrl,
              icon_name:
                card.fields.contentArea[0].fields?.secondaryButton?.fields
                  .iconName,
              icon_variant:
                card.fields.contentArea[0].fields?.secondaryButton?.fields
                  .iconVariant,
              data_ga_name:
                card.fields.contentArea[0].fields?.secondaryButton?.fields
                  .dataGaName,
              data_ga_location:
                card.fields.contentArea[0].fields?.secondaryButton?.fields
                  .dataGaLocation,
            },
          }
        : '',
    };
  });
}
function featureShowcaseBuilder(section) {
  return section.card.map((card) => {
    const stats =
      card.fields.pills &&
      card.fields.pills.map((pill, index) => {
        const getStatArray = pill.split(',');
        return {
          value: getStatArray[0],
          blurb: getStatArray[1],
          icon:
            card.fields.customFields.icons[index] &&
            card.fields.customFields?.icons[index],
        };
      });
    return {
      title: card.fields.title,
      blurb: card.fields.description || '',
      button:
        card.fields.button?.fields && buttonBuilder(card.fields.button?.fields),
      icon: { name: card.fields?.iconName, size: 'md', variant: 'marketing' },
      stats,
    };
  });
}
function featuresBuilder(features) {
  return features.map((feature) => {
    return {
      text: feature.fields?.text,
      icon: {
        name: feature.fields?.iconName,
        size: 'md',
        variant: 'marketing',
      },
      href: feature.fields?.externalUrl,
      data_ga_name: feature.fields?.dataGaName,
      data_ga_location: feature.fields?.dataGaLocation,
    };
  });
}
function tabbedFeatures(section) {
  const tabs = section.tabs.map((tab) => {
    const tabPanel =
      tab.fields.tabPanelContent && tab.fields.tabPanelContent[0];
    const features =
      tabPanel.fields.blockGroup && featuresBuilder(tabPanel.fields.blockGroup);

    const asset = (tabPanel.fields.assets && tabPanel.fields.assets[0]) || null;
    const codeBlocks =
      asset &&
      asset.sys.contentType.sys.id === 'codeSuggestionsIde' &&
      asset.fields.codeBlocks.code_blocks;
    const demoData =
      asset.sys.contentType.sys.id === 'card' ? asset.fields : null;
    return {
      header: tab.fields.tabButtonText,
      header_mobile: tab.fields.tabId,
      data_ga_name: tab.fields.tabGaName,
      data_ga_location: tab.fields.tabGaLocation,
      blurb: tabPanel.fields.text,
      features: tabPanel.fields.blockGroup && features,
      demo: demoData
        ? {
            subtitle: demoData?.subtitle,
            video: {
              url: demoData?.video?.fields.url,
              thumbnail:
                demoData?.video?.fields?.thumbnail?.fields?.image?.fields.file
                  .url,
              altText:
                demoData?.video?.fields?.thumbnail?.fields?.image?.fields.title,
            },
            scheduleButton: {
              text: demoData?.button?.fields.text,
              href: demoData?.button?.fields.externalUrl,
              data_ga_name: demoData?.button.fields.dataGaName,
              data_ga_location: demoData?.button.fields.dataGaLocation,
            },
            launchButton: {
              text: demoData?.secondaryButton?.fields.text,
              href: demoData?.secondaryButton?.fields.externalUrl,
              icon_name: demoData?.secondaryButton?.fields.iconName,
              icon_variant: demoData?.secondaryButton?.fields.iconVariant,
              data_ga_name: demoData?.secondaryButton?.fields.dataGaName,
              data_ga_location:
                demoData?.secondaryButton?.fields.dataGaLocation,
            },
          }
        : null,
      code_blocks: codeBlocks,
      // edit asset and image builder here for demos on homepage
      image:
        asset &&
        !codeBlocks &&
        !demoData &&
        imageBuilder(asset.fields.image.fields),
    };
  });
  return {
    header: section.header,
    blurb: section.tabControlsSubtext,
    tabs,
  };
}
function quotesCarouselBlock(section) {
  const tabs = section.tabs.map((tab) => {
    const sectionFields = tab?.fields;
    const button = tab.fields.tabCta?.fields && { ...tab.fields.tabCta.fields };
    const tabContentArray = sectionFields.tabPanelContent.map(
      (block) => block.fields,
    );
    const tabContentObjects = tabContentArray.reduce(
      (previous: any, current: any) => {
        const mergedArray = {
          ...previous,
          ...current,
        };

        return mergedArray;
      },
    );
    const stats =
      tabContentObjects.pills &&
      tabContentObjects.pills.map((pill) => {
        const getStatArray = pill.split(':');
        return {
          data: {
            highlight: getStatArray[0],
            subtitle: getStatArray[1],
          },
        };
      });
    const isModal =
      button?.externalUrl?.includes('vimeo') ||
      button?.externalUrl?.includes('youtube');
    return {
      logo: {
        url: sectionFields.tabButtonLogo?.fields?.image?.fields?.file?.url,
        alt: sectionFields.tabButtonLogo?.fields?.altText,
      },
      header: tabContentObjects.description,
      url: button?.externalUrl,
      data_ga_name: button?.dataGaName,
      data_ga_location: button?.dataGaLocation,
      cta_text: button?.text,
      quote: tabContentObjects?.quoteText,
      author: tabContentObjects?.author,
      role: tabContentObjects?.authorTitle,
      company: tabContentObjects?.authorCompany,
      modal: isModal,
      main_img: {
        url: tabContentObjects.authorHeadshot?.fields?.image?.fields?.file?.url,
        alt: tabContentObjects.authorHeadshot?.fields.altText,
      },
      statistic_samples: stats,
    };
  });

  return { quotes: tabs, ...section.customFields };
}
function recognitionSpotlight(block) {
  const statArray = block.description && block.description.split('\n');

  const stats = statArray.map((pill) => {
    const getStatArray = pill.split(',');
    return {
      value: getStatArray[0],
      blurb: getStatArray[1],
    };
  });

  const cards = block.card.map((card) => {
    const badges =
      card.fields.contentArea &&
      card.fields.contentArea.map((badge) => {
        return {
          src: badge.fields.image.fields.file.url,
          alt: badge.fields.altText,
        };
      });
    return {
      header: card.fields.title,
      text: card.fields.description,
      logo: card.fields.image?.fields.file.url || '',
      alt: card.fields.image?.fields.title || '',
      link: card.fields.button && buttonBuilder(card.fields.button.fields),
      badges,
    };
  });
  return {
    heading: block.header,
    stats,
    cards,
  };
}

function cardGroupBuilder(fields: CtfCardGroup) {
  const cardGroup: homePageData = {};

  switch (fields.componentName) {
    case COMPONENT_NAMES.HERO_SCROLL_GALLERY: {
      cardGroup.hero_scroll_gallery = heroBuilder(fields);
      break;
    }
    case COMPONENT_NAMES.BLURB_GRID: {
      cardGroup.blurb_grid = blurbGridBuilder(fields);
      break;
    }
    case COMPONENT_NAMES.FEATURE_SHOWCASE: {
      cardGroup.feature_showcase = featureShowcaseBuilder(fields);
      break;
    }
    case COMPONENT_NAMES.RECOGNITION_SPOTLIGHT: {
      cardGroup.recognition_spotlight = recognitionSpotlight(fields);
      break;
    }
  }

  return cardGroup;
}

function twoColumnBlockBuilder(fields: CtfTwoColumnBlock) {
  const twoColumnBlock: homePageData = {};

  switch (fields.componentName) {
    case COMPONENT_NAMES.INTRO_BLOCK: {
      twoColumnBlock.intro_block = introBuilder(fields);
      break;
    }
  }

  return twoColumnBlock;
}

function tabControlsBuilder(fields: CtfTabControlsContainer) {
  const tabControls: homePageData = {};

  switch (fields.componentName) {
    case COMPONENT_NAMES.TABBED_FEATURES: {
      tabControls.tabbed_features = tabbedFeatures(fields);
      break;
    }
    case COMPONENT_NAMES.TABBED_CASE_STUDIES: {
      tabControls.quotes_carousel_block = quotesCarouselBlock(fields);
      break;
    }
  }

  return tabControls;
}

function pageBuilder(page: CtfEntry<any>[]): homePageData {
  let result: homePageData = {};

  for (const section of page) {
    const id = section.sys.contentType?.sys.id;
    switch (id) {
      case CONTENT_TYPES.CARD_GROUP:
        result = { ...result, ...cardGroupBuilder(section?.fields) };
        break;
      case CONTENT_TYPES.CUSTOMER_LOGOS:
        if (!section.fields.segment) {
          result.customer_logos_block = customerLogosBuilder(section?.fields);
          break;
        }
        if (!result.personalized_customer_logos) {
          result.personalized_customer_logos = [
            {
              segment: section?.fields?.segment,
              ...customerLogosBuilder(section.fields),
            },
          ];
        }

        result.personalized_customer_logos?.push({
          segment: section?.fields?.segment,
          ...customerLogosBuilder(section?.fields),
        });
        break;
      case CONTENT_TYPES.TWO_COLUMN_BLOCK:
        result = { ...result, ...twoColumnBlockBuilder(section?.fields) };
        break;
      case CONTENT_TYPES.TAB_CONTROLS_CONTAINER: {
        if (section.fields.segment) {
          if (!result.personalized_tabbed_case_studies) {
            result.personalized_tabbed_case_studies = [
              {
                segment: section.fields?.segment,
                ...quotesCarouselBlock(section.fields),
              },
            ];
          }

          result.personalized_tabbed_case_studies?.push({
            segment: section.fields?.segment,
            ...quotesCarouselBlock(section.fields),
          });

          break;
        }

        result = {
          ...result,
          ...tabControlsBuilder(section?.fields),
        };
        break;
      }
    }
  }

  return result;
}

function metadataHelper(data) {
  const seo = data[0]?.fields;
  return {
    title: seo.title,
    og_title: seo.title,
    description: seo.description,
    twitter_description: seo.description,
    image_title: seo?.ogImage && seo.ogImage.fields?.image?.fields?.file?.url,
    og_description: seo.description,
    og_image: seo?.ogImage && seo.ogImage.fields?.image?.fields?.file?.url,
    twitter_image: seo?.ogImage && seo.ogImage.fields?.image?.fields?.file?.url,
    noIndex: seo.noIndex,
  };
}

export function homepageHelper(data) {
  const { pageContent, seoMetadata } = data;
  const metadata = metadataHelper(seoMetadata);

  const cleanPagecontent = pageBuilder(pageContent);

  const transformedData: homePageData = {
    ...metadata,
    ...cleanPagecontent,
  };

  return transformedData;
}
