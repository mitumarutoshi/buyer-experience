import { Context } from '@nuxt/types';
import { getClient } from '~/plugins/contentful';
import { CONTENT_TYPES } from '~/common/content-types';
import { CtfCard, CtfCardGroup, CtfEntry, CtfHero } from '~/models';
import {
  convertToCaseSensitiveLocale,
  getUrlFromContentfulImage,
} from '~/common/util';
import { COMPONENT_NAMES } from '~/common/constants';
import { mapSolutionsResourceCards } from '~/services/solutions/solutions-default.helper';

export class GartnerMagicQuadrantService {
  private readonly $ctx: Context;

  private readonly componentNames = {
    INTRO_CARDS: 'intro-cards',
    VIDEO_GROUP: 'video-group',
    QUOTES: 'quotes',
    DISCLAIMER: 'disclaimer',
  };

  constructor($context: Context) {
    this.$ctx = $context;
  }

  async getContent() {
    const gartnerMagicQuadrantEntry = await getClient().getEntries({
      content_type: CONTENT_TYPES.PAGE,
      'fields.slug': 'gartner-magic-quadrant',
      include: 10,
      locale: convertToCaseSensitiveLocale(this.$ctx.i18n.locale),
    });

    if (gartnerMagicQuadrantEntry.items.length === 0) {
      throw new Error('Not found');
    }

    const [items] = gartnerMagicQuadrantEntry.items;
    return { items, gartner: this.transformContentfulData(items) };
  }

  transformContentfulData(ctfData: any) {
    const { pageContent, seoMetadata } = ctfData.fields;

    const mappedContent = this.getCtfComponents(pageContent);

    return {
      title: seoMetadata[0]?.fields.title,
      description: seoMetadata[0]?.fields.description,
      ...mappedContent,
    };
  }

  private getCtfComponents(pageContent: CtfEntry<any>[]) {
    let result = {};

    for (const component of pageContent) {
      const { id } = component.sys.contentType.sys;

      switch (id) {
        case CONTENT_TYPES.HERO: {
          result.hero = this.mapHero(component.fields);
          break;
        }
        case CONTENT_TYPES.CARD_GROUP: {
          result = { ...result, ...this.mapCardGroup(component.fields) };
          break;
        }
        case CONTENT_TYPES.FORM: {
          result.form = this.mapForm(component.fields);
          break;
        }
        case CONTENT_TYPES.CARD: {
          result.disclaimer = this.mapDisclaimer(component.fields);
          break;
        }
      }
    }

    return result;
  }

  private mapCardGroup(ctfCardGroup: CtfCardGroup) {
    const result = {};

    switch (ctfCardGroup.componentName) {
      case this.componentNames.INTRO_CARDS: {
        result.intro_cards = this.mapIntroCards(ctfCardGroup);
        break;
      }
      case this.componentNames.VIDEO_GROUP: {
        result.videos = this.mapVideoGroup(ctfCardGroup);
        break;
      }
      case this.componentNames.QUOTES: {
        result.quotes = this.mapQuotes(ctfCardGroup);
        break;
      }
      case COMPONENT_NAMES.SOLUTIONS_RESOURCE_CARDS: {
        result.resources = mapSolutionsResourceCards(ctfCardGroup);
        break;
      }
    }

    return result;
  }

  private mapHero(ctfHero: CtfHero) {
    const button = ctfHero.primaryCta && {
      text: ctfHero.primaryCta.fields.text,
      href: ctfHero.primaryCta.fields.externalUrl,
      icon: {
        variant: ctfHero.primaryCta.fields.iconVariant,
        name: ctfHero.primaryCta.fields.iconName,
      },
    };

    const image = ctfHero.backgroundImage && {
      src: getUrlFromContentfulImage(ctfHero?.backgroundImage),
      alt: ctfHero?.backgroundImage?.fields?.title,
    };

    return {
      note: ctfHero.title,
      header: ctfHero.subheader,
      description: ctfHero.description,
      image,
      button,
    };
  }

  private mapIntroCards(ctfCardGroup: CtfCardGroup) {
    const cards = ctfCardGroup.card?.map((card) => ({
      name: card.fields.title,
      icon: card.fields.iconName,
      description: card.fields.description,
      href: card.fields.button && card.fields.button.fields.externalUrl,
      linkText: card.fields.button && card.fields.button.fields.text,
    }));

    return {
      header: ctfCardGroup.header,
      icon: ctfCardGroup.customFields.icon,
      cards,
    };
  }

  private mapVideoGroup(ctfCardGroup: CtfCardGroup) {
    return ctfCardGroup.card?.map((card) => ({
      header: card.fields.title,
      description: card.fields.description,
      url: card.fields.video?.fields.url,
      image: card.fields.image?.fields,
      button: card.fields.button && {
        text: card.fields.button.fields.text,
        href: card.fields.button.fields.externalUrl,
        gaName: card.fields.button.fields.dataGaName,
      },
    }));
  }

  private mapQuotes(ctfCardGroup: CtfCardGroup) {
    return {
      header: ctfCardGroup.header,
      description: ctfCardGroup.description,
      cta: {
        text: ctfCardGroup.cta?.fields.text,
        href: ctfCardGroup.cta?.fields.externalUrl,
        dataGaName: ctfCardGroup.cta?.fields.dataGaName,
        dataGaLocation: ctfCardGroup.cta?.fields.dataGaLocation,
      },
      cards: ctfCardGroup.card?.map((card) => ({
        header: card.fields.title,
        quote: card.fields.description,
        href: card.fields.cardLink,
        industry: card.fields.subtitle && card.fields.subtitle.split(';')[0],
        firmSize: card.fields.subtitle && card.fields.subtitle.split(';')[1],
      })),
    };
  }

  private mapForm(ctfForm) {
    return {
      header: ctfForm.header,
      formId: ctfForm.formId,
      required_text: ctfForm.requiredFieldsText,
    };
  }

  private mapDisclaimer(ctfCard: CtfCard) {
    return {
      text: ctfCard.title,
      details: ctfCard.description,
      urlText: ctfCard.button && ctfCard.button.fields.text,
      url: ctfCard.button && ctfCard.button.fields.externalUrl,
      ...ctfCard.customFields,
    };
  }
}
