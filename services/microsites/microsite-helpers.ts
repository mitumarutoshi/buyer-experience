import { COMPONENT_NAMES } from '~/common/constants';
import { CONTENT_TYPES } from '~/common/content-types';

/****
 * Main helper functions
 ***/

function addQueryParam(url: string, paramName: string, paramValue: string) {
  if (url.includes('?')) {
    return `${url}&${paramName}=${paramValue}`;
  } else {
    return `${url}?${paramName}=${paramValue}`;
  }
}

export function formatLink(url?: string, partnerId?: string) {
  // All links from our partner webpages must include the unique Partner ID for proper marketing platform tracking. This is tracked through a query param.

  // GitLab.com trial signups use a different param key than marketing-specific pages. This is why there is a little extra logic to check the difference between URLs.
  if (url.includes('mailto')) return url;

  const parsedUrl = new URL(url);
  const params = parsedUrl.searchParams;

  if (parsedUrl.hostname === 'gitlab.com') {
    if (params.has('glm_content')) {
      let glmContentValue = params.get('glm_content');

      if (glmContentValue !== partnerId) {
        params.set('glm_content', partnerId);
        parsedUrl.search = params.toString();
        return parsedUrl.href;
      }
    } else {
      return addQueryParam(url, 'glm_content', partnerId);
    }
  } else {
    if (params.has('utm_partnerid')) {
      let utmContentValue = params.get('utm_partnerid');

      if (utmContentValue !== partnerId) {
        params.set('utm_partnerid', partnerId);
        parsedUrl.search = params.toString();
        return parsedUrl.href;
      }
    } else {
      return addQueryParam(url, 'utm_partnerid', partnerId);
    }
  }

  return url;
}

export function mapContent(track: any, partnerId?: string) {
  let components = [];
  track.forEach((item) => {
    const { id } = item.sys.contentType.sys;
    switch (id) {
      case CONTENT_TYPES.HEADER_SUBHEADER_AND_TEXT: {
        components.push(mapTextBlock(item.fields, partnerId));
        break;
      }
      case CONTENT_TYPES.CARD_GROUP: {
        components.push(mapCardBlock(item.fields, partnerId));
        break;
      }
      case CONTENT_TYPES.EXTERNAL_VIDEO: {
        components.push({
          name: COMPONENT_NAMES.MICROSITE_VIDEO,
          data: {
            anchorId: item.fields.anchorId && item.fields.anchorId,
            title: item.fields.title && item.fields.title,
            video_url: item.fields.url,
          },
        });
        break;
      }
      case CONTENT_TYPES.CARD: {
        components.push(mapShowcaseCard(item.fields, partnerId));
        break;
      }
    }
  });
  return components;
}

export function mapSideMenu(item: any, partnerId?: string) {
  switch (item.sys.contentType.sys.id) {
    case CONTENT_TYPES.SIDE_MENU: {
      return mapSideNavigation(item.fields, partnerId);
    }
    case CONTENT_TYPES.TAB_CONTROLS_CONTAINER: {
      return { tabs: mapCtaTabs(item.fields, partnerId) };
    }
    default:
      return null;
  }
}

export function mapFooterCta(entryData: any, partnerId?: string) {
  return {
    heading: entryData.heading,
    tagline: entryData.tagline,
    leftColumnCtaText:
      entryData.leftColumnCtaText && entryData.leftColumnCtaText,
    leftColumnButtons:
      entryData.leftColumnButtons &&
      entryData.leftColumnButtons.map((button) => {
        return {
          text: button.fields.text,
          url: formatLink(button.fields.externalUrl, partnerId),
          dataGaName: button.fields.dataGaName
            ? button.fields.dataGaName
            : button.fields.text.toLowerCase(),
          dataGaLocation: button.fields.dataGaLocation
            ? button.fields.dataGaLocation
            : 'features',
        };
      }),
    rightColumnTaglines:
      entryData.rightColumnTaglines &&
      entryData.rightColumnTaglines.map((tag) => {
        return {
          header: tag.fields.header,
          text: tag.fields.text,
        };
      }),
  };
}

/****
 * Component helper functions
 ***/

const mapTextBlock = (entryData: any, partnerId?: string) => {
  return {
    name: COMPONENT_NAMES.MICROSITE_TEXT_BLOCK,
    data: {
      anchorId: entryData.anchorId && entryData.anchorId,
      header: entryData.header && entryData.header,
      subheader: entryData.subheader && entryData.subheader,
      text: entryData.text && entryData.text,
    },
  };
};

const mapCardBlock = (entryData: any, partnerId?: string) => {
  return {
    name: COMPONENT_NAMES.MICROSITE_CARD_BLOCK,
    data: {
      cards: entryData.card.map((cardEntry) => {
        return {
          icon: cardEntry.fields.iconName && cardEntry.fields.iconName,
          header: cardEntry.fields.title && cardEntry.fields.title,
          description:
            cardEntry.fields.description && cardEntry.fields.description,
        };
      }),
    },
  };
};

const mapShowcaseCard = (entryData: any, partnerId?: string) => {
  return {
    name: COMPONENT_NAMES.MICROSITE_SHOWCASE_CARD,
    data: {
      componentName: entryData.componentName && entryData.componentName,
      title: entryData.title && entryData.title,
      subtitle: entryData.subtitle && entryData.subtitle,
      description: entryData.description && entryData.description,
      list: entryData.list && [...entryData.list],
      iconName: entryData.iconName && entryData.iconName,
    },
  };
};

const mapSideNavigation = (ctfSideMenu: any, partnerId?: string) => {
  const data = ctfSideMenu.anchors
    .map((anchor: any) => anchor.fields)
    .map((anchor: any) => ({
      text: anchor.linkText,
      href: anchor.anchorLink,
      data_ga_name: anchor.dataGaName || anchor.linkText.toLowerCase(),
      data_ga_location: anchor.dataGaLocation || 'side navigation',
      nodes: anchor.nodes
        ? anchor.nodes.nodes.map((node: any) => ({
            text: node.text,
            href: node.href,
            data_ga_name: node.dataGaName || node.text.toLowerCase(),
            data_ga_location: `side navigation`,
          }))
        : [],
    }));

  return {
    ...ctfSideMenu.customFields,
    anchors: {
      data,
    },
    hyperlinks: {
      text: '',
      data: [],
    },
  };
};

const mapCtaTabs = (ctfTabs: any, partnerId?: string) => {
  const ctaTabs = {
    ...(ctfTabs.header && { heading: ctfTabs.header }),
    tabs: ctfTabs.tabs.map((item) => ({
      tab: {
        text: item.fields.tabButtonText,
        dataGaName: item.fields.tabGaName || item.fields.tabButtonText,
        dataGaLocation: item.fields.tabGaLocation || 'side navigation',
      },
      panel: {
        ...(item.fields.tabPanelContent &&
          item.fields.tabPanelContent.find(
            (entry) => entry.sys.contentType.sys.id === 'text',
          ) && {
            text: item.fields.tabPanelContent.find(
              (entry) => entry.sys.contentType.sys.id === 'text',
            ).fields.text,
          }),
        ...(item.fields.tabCta && {
          button: {
            text: item.fields.tabCta.fields.text,
            url: formatLink(item.fields.tabCta.fields.externalUrl, partnerId),
            dataGaName:
              item.fields.tabCta.fields.dataGaName ||
              item.fields.tabCta.fields.text.toLowerCase(),
          },
        }),
      },
    })),
  };
  return ctaTabs;
};
