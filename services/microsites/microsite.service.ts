import {
  mapFooterCta,
  mapContent,
  mapSideMenu,
  formatLink,
} from './microsite-helpers';

interface MicrositeItem {
  partnerName: string;
  partnerId: string;
  partnerLogo?: string;
  heading: string;
  navCtaText?: string;
  navCtaLink?: string;
  sideNavigation?: [];
  micrositeTrackSections: [];
  footerCta: [];
}

export function micrositeDataHelper(micrositeItems: MicrositeItem[]) {
  const {
    partnerName,
    partnerId,
    partnerLogo,
    heading,
    navCtaText,
    navCtaLink,
    sideNavigation,
    micrositeTrackSections,
    footerCta,
  } = micrositeItems;

  let page = {
    partner: {
      name: partnerName,
      logo: partnerLogo?.fields.file.url,
      alt: partnerLogo?.fields.alt || `${partnerName} logo`,
      id: partnerId,
    },
    navigation: {
      cta: {
        text: navCtaText,
        link: formatLink(navCtaLink, partnerId),
      },
    },
    content: {
      heading,
      body: {
        sideMenu: Object.assign(
          {},
          ...sideNavigation.map((item) => mapSideMenu(item, partnerId)),
        ),
        tracks: micrositeTrackSections.map((track) => {
          return {
            data: mapContent(track.fields.trackContentBlocks, partnerId),
          };
        }),
      },
    },
    footerCta: {
      name: 'MicrositeFooterCta',
      data: mapFooterCta(footerCta.fields, partnerId),
    },
  };

  return page;
}
