function formatId(input) {
  // Convert the string to lowercase and replace spaces with dashes
  const formattedString = input.toLowerCase().replace(/\s+/g, '-');

  // Replace '&' with 'and'
  const finalString = formattedString.replace(/&/g, 'and');

  return finalString;
}

export function techPartnerService(data: any[], searchData: any[]) {
  const pageData = {
    hero: {
      title: '',
      subtitle: '',
    },
    next_step: {
      title: '',
      subtitle: '',
      primary: {},
      secondary: {},
      image: {},
    },
    featured_partners: {
      header: '',
      partners: [],
    },
    all_partners: {
      header: '',
      platforms: [],
    },
    search: {
      placeholder: '',
    },
    navigation: {
      column_size: null,
      buttons: [],
    },
  };

  // Next Steps Variant
  const nextSteps = data.filter(
    (obj) => obj.sys.contentType.sys.id === 'nextSteps',
  );
  const nextStepsContent = nextSteps[0].fields;
  pageData.next_step = {
    title: nextStepsContent.heading,
    subtitle: nextStepsContent.tagline,
    primary: {
      text: nextStepsContent.primaryButton.fields.text,
      ga_name: nextStepsContent.primaryButton.fields.dataGaName,
      ga_location: nextStepsContent.primaryButton.fields.dataGaLocation,
      href: nextStepsContent.primaryButton.fields.externalUrl,
    },
    secondary: {
      text: nextStepsContent.secondaryButton.fields.text,
      ga_name: nextStepsContent.secondaryButton.fields.dataGaName,
      ga_location: nextStepsContent.secondaryButton.fields.dataGaLocation,
      href: nextStepsContent.secondaryButton.fields.externalUrl,
    },
    image: {
      url: nextStepsContent.image.fields.image.fields.file.url,
      alt: '',
    },
  };

  // Create Hero
  const hero = data.filter((obj) => obj.sys.contentType.sys.id === 'eventHero');
  pageData.hero = {
    title: hero[0].fields.title,
    subtitle: hero[0].fields.subheader,
  };

  // Create search section
  (pageData.search.placeholder = searchData.search.placeholder),
    (pageData.navigation.column_size = searchData.column_size),
    (pageData.navigation.buttons = searchData.navigation.buttons);

  // Create partner sections
  const headers = data.filter(
    (obj) => obj.sys.contentType.sys.id === 'headerAndText',
  );

  const blockGroups = data.filter(
    (obj) => obj.sys.contentType.sys.id === 'blockGroup',
  );

  const featuredPartners = blockGroups.flatMap((blocks) => {
    const title = blocks.fields.header; // we need this in order to add the category along for the ride

    return blocks.fields.blocks.filter((block) => {
      if (block.fields.featuredPartner === true) {
        block.header = title;
        return true;
      }
      return false;
    });
  });

  const createFeatured = featuredPartners.map((item) => {
    return {
      title: item.fields.applicationName,
      content: item.fields.content.toString(),
      image: item.fields.logo.fields.file.url,
      category: item.header, // this is the title passed from featuredPartners
      links: (item.fields.links || []).map(
        (link) =>
          link.fields && {
            url: link.fields.externalUrl,
            variation: link.fields.variation || 'tertiary',
            title: link.fields.text,
            ga_name: link.fields.dataGaName || null,
            ga_location: link.fields.dataGaLocation || 'body',
          },
      ),
    };
  });

  const createPlatforms = blockGroups.map((blocks, index) => ({
    header: index === 0 ? headers[1].fields.header : null,
    title: blocks.fields.header,
    id: formatId(blocks.fields.header),
    partners: blocks.fields.blocks.map((block) => ({
      title: block.fields.applicationName,
      id: formatId(block.fields.applicationName),
      image: block.fields.logo ? block.fields.logo.fields.file.url : null,
      category: blocks.fields.header,
      content: block.fields.content.toString(),
      links: (block.fields.links || []).map(
        (link) =>
          link.fields && {
            url: link.fields.externalUrl,
            variation: link.fields.variation || 'tertiary',
            title: link.fields.text,
            ga_name: link.fields.dataGaName || null,
            ga_location: link.fields.dataGaLocation || 'body',
          },
      ),
    })),
  }));

  // Featured
  pageData.featured_partners = {
    header: headers[0].fields.header,
    partners: [...createFeatured],
  };

  // All
  pageData.all_partners = {
    header: headers[1].fields.header,
    categories: [...createPlatforms],
  };

  return pageData;
}
