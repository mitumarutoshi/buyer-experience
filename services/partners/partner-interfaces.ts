interface Hero {
  title: string;
  body: {
    text: string;
  };
  image: {
    image_url: string;
    alt?: string;
  };
  primaryButton?: {
    url: string;
    newTab: string;
    variation: string;
    text: string;
    data_ga_name: string;
    data_ga_location?: string;
  };
  secondary_button?: {
    url: string;
    newTab: string;
    variation: string;
    text: string;
    data_ga_name: string;
    data_ga_location?: string;
  };
}

interface CopyMedia {
  data: {
    block: {
      header: string;
      text: string;
      hide_horizontal_rule: boolean;
      image?: {
        image_url: string;
      };
      video?: {
        video_url: string;
      };

      link_href: string;
      link_text: string;
      link_variant: string;
      link_newTab: string;
      link_data_ga_name: string;
      link_data_ga_location?: string;
      secondary_link_href: string;
      secondary_link_text: string;
      secondary_link_variant: string;
      secondary_link_newTab: string;
      secondary_link_data_ga_name: string;
      secondary_link_data_ga_location?: string;
    }[];
  };
}

interface PageData {
  hero: Hero;
  copy_media: CopyMedia;
  body: [];
  nextStepsVariant: null;
}
