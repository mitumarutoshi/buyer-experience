import { CONTENT_TYPES } from '~/common/content-types';
interface PressKitData {
  title?: string;
  description?: string;
  hero?: any;
  sections?: any;
  navigation?: any;
  nextSteps?: any;
}
function pressKitAssetBuilder(item) {
  const variantsBuild = [
    item?.fields?.png && {
      name: 'png',
      src:
        item?.fields?.png?.fields?.file?.url &&
        item?.fields?.png?.fields?.file?.url,
    },

    item?.fields?.jpg && {
      name: 'jpg',
      src:
        item?.fields?.jpg?.fields?.file?.url &&
        item?.fields?.jpg?.fields?.file?.url,
    },
    item?.fields?.svg && {
      name: 'svg',
      src:
        item?.fields?.svg?.fields?.file?.url &&
        item?.fields?.svg?.fields?.file?.url,
    },
    item?.fields?.eps && {
      name: 'eps',
      src:
        item?.fields?.eps?.fields?.file?.url &&
        item?.fields?.eps?.fields?.file?.url,
    },
    item?.fields?.pdf && {
      name: 'pdf',
      src:
        item?.fields?.pdf?.fields?.file?.url &&
        item?.fields?.pdf?.fields?.file?.url,
    },
  ];
  const variants = variantsBuild.filter((el) => {
    return el != null;
  });
  return {
    title: item.fields?.title && item.fields?.title,
    thumbnail:
      item?.fields?.thumbnail?.fields?.file?.url &&
      item?.fields?.thumbnail?.fields?.file?.url,
    variants,
  };
}
function pressCardBuilder(item) {
  return {
    title: item.title && item.title,
    subtitle: item.subtitle && item.subtitle,
    button: item.button && item.button.fields,
    // image: item.image
    image: item.image &&
      item?.image?.fields && {
        src:
          (item?.image?.fields?.file?.url && item?.image?.fields?.file?.url) ||
          '',
        alt: item?.image?.fields?.title && item?.image?.fields?.title,
      },
    cardLink: item.cardLink && item.cardLink,
    cardLinkDataGaName: item.cardLinkDataGaName && item.cardLinkDataGaName,
    cardLinkDataGaLocation:
      item.cardLinkDataGaLocation && item.cardLinkDataGaLocation,
  };
}
function sectionBuilder(section) {
  const items = section.card.map((item) => {
    if (item.sys.contentType.sys.id == 'card') {
      return pressCardBuilder(item.fields);
    } else if (item.sys.contentType.sys.id == 'pressKitAsset') {
      return pressKitAssetBuilder(item);
    }
  });

  return {
    header: section.header && section.header,
    id: section.componentName && section.componentName,
    description: section.description && section.description,
    image: section?.image?.fields && {
      src:
        (section?.image?.fields?.image?.fields?.file?.url &&
          section?.image?.fields?.image?.fields?.file?.url) ||
        '',
      alt: section?.image?.fields?.altText && section?.image?.fields?.altText,
    },
    cta: section?.cta?.fields && section?.cta?.fields,
    items,
  };
}
function menuBuilder(menu) {
  const items = menu.hyperlinks.map((link) => {
    return link.fields;
  });
  return { navigation: items };
}

function pageBuilder(page) {
  let result: PressKitData = {};
  result.sections = [];

  for (const section of page) {
    const id = section.sys.contentType?.sys.id;
    switch (id) {
      case CONTENT_TYPES.CARD_GROUP:
        result.sections.push(sectionBuilder(section.fields));
        break;
      case CONTENT_TYPES.HERO:
        result = { ...result, hero: { ...section.fields } };
        break;
      case CONTENT_TYPES.SIDE_MENU:
        result = { ...result, ...menuBuilder(section.fields) };
        break;
      case CONTENT_TYPES.NEXT_STEPS:
        result.nextSteps = section.fields.variant;
        break;
    }
  }

  return result;
}

function metadataHelper(data) {
  const seo = data[0].fields;
  return {
    title: seo.title,
    og_title: seo.title,
    description: seo.description,
    twitter_description: seo.description,
    image_title: seo?.ogImage && seo.ogImage.fields.image.fields.file.url,
    og_description: seo.description,
    og_image: seo?.ogImage && seo.ogImage.fields.image.fields.file.url,
    twitter_image: seo?.ogImage && seo.ogImage.fields.image.fields.file.url,
    noIndex: seo.noIndex,
  };
}

export function pressKitHelper(data) {
  const { pageContent, seoMetadata } = data;
  const cleanPagecontent = pageBuilder(pageContent);

  const metadata = metadataHelper(seoMetadata);
  const transformedData: PressKitData = {
    ...metadata,
    ...cleanPagecontent,
  };

  return transformedData;
}
