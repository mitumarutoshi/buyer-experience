import { Context } from '@nuxt/types';
import { getClient } from '~/plugins/contentful';
import { CONTENT_TYPES } from '~/common/content-types';
import { convertToCaseSensitiveLocale } from '~/common/util';
import { kebabCase } from 'lodash';
import { CtfEntry } from '~/models';

export class ContentfulService {
  private readonly $ctx: Context;

  constructor($context: Context) {
    this.$ctx = $context;
  }

  async getCustomPage(slug) {
    const { items } = await getClient().getEntries({
      content_type: CONTENT_TYPES.CUSTOM_PAGE,
      'fields.slug': slug,
      include: 10,
      locale: convertToCaseSensitiveLocale(this.$ctx.i18n.locale),
    });

    if (!items.length) {
      throw new Error('Page not found');
    }

    const [page] = items;

    return page;
  }

  async getCustomPagePreview(id, config) {
    const { items } = await getClient(config).getEntries({
      content_type: CONTENT_TYPES.CUSTOM_PAGE,
      'sys.id': id,
      include: 10,
      locale: convertToCaseSensitiveLocale(this.$ctx.i18n.locale),
    });

    if (!items.length) {
      throw new Error('Page preview not found');
    }

    const [page] = items;

    return page;
  }

  async getLandingChildren(landingSlug: string) {
    const { items } = await getClient().getEntries({
      content_type: CONTENT_TYPES.CUSTOM_PAGE,
      'fields.slug[match]': landingSlug,
      'fields.slug[ne]': landingSlug,
      order: '-fields.date',
      select: 'fields.header, fields.description, fields.slug, fields.date',
      include: 3,
      locale: convertToCaseSensitiveLocale(this.$ctx.i18n.locale),
    });

    return items;
  }

  mapSideNavigation(components: CtfEntry<any>[]) {
    return {
      anchors: {
        text: components.every((entry) => !entry.fields.header)
          ? ''
          : 'On this page',
        data: components
          .filter((entry) => entry.fields && entry.fields.header)
          .map((entry) => ({
            text: entry.fields.header,
            href: entry.fields.headerAnchorId
              ? `#${entry.fields.headerAnchorId}`
              : `#${kebabCase(entry.fields.header)}`,
            'data-ga-title': entry.fields.header.toLowerCase(),
            'data-ga-location': 'side-navigation',
          })),
      },
    };
  }
}
