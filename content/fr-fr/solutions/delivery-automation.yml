---
  title: Livraison logicielle automatisée avec GitLab
  description: "Pratiques d'automatisation DevOps : automatisation du code, des compilations, des tests et des releases"
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        title: Livraison logicielle automatisée
        subtitle: L'essentiel de l'automatisation pour des équipes plus productives, une efficacité opérationnelle optimale et une meilleure vélocité
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Commencer votre essai gratuit
          url: /free-trial/
        secondary_btn:
          text: Des questions ? Nous contacter
          url: /sales/
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Image\_: GitLab pour la livraison logicielle automatisée"
    - name: 'side-navigation-variant'
      links:
        - title: Présentation
          href: '#overview'
        - title: Capacités
          href: '#capabilities'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-intro'
              data:
                text:
                  highlight: La livraison logicielle automatisée avec GitLab
                  description: vous aidera à adopter facilement le Cloud natif, Kubernetes et le multi-cloud, à accélérer la vélocité en réduisant les échecs et à améliorer la productivité des développeurs en éliminant les tâches répétitives.
            - name: 'by-solution-benefits'
              data:
                title: Pourquoi la livraison logicielle automatisée ?
                video:
                  video_url: "https://player.vimeo.com/video/725654155"
                is_accordion: false
                items:
                  - icon:
                      name: increase
                      alt: Icône d'augmentation
                      variant: marketing
                    header: Mise à l'échelle de votre SDLC pour l'adoption d'une architecture Cloud native
                    text: Éliminez les clics, introduisez des contrôles et des contrepoids essentiels à l'adoption d'une architecture Cloud native.
                  - icon:
                      name: gitlab-release
                      alt: Icône de publication GitLab
                      variant: marketing
                    header: Chaque modification peut donner lieu à une release
                    text: Davantage de tests, des erreurs détectées en amont, moins de risques.
                  - icon:
                      name: collaboration
                      alt: Icône de collaboration
                      variant: marketing
                    header: Une expérience développeurs améliorée
                    text: Minimisez les tâches répétitives et concentrez-vous sur les tâches génératrices de valeur.
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-showcase'
              data:
                title: Automatisation de votre processus de livraison de logiciels
                description: Pour livrer des applications de meilleure qualité, les développeurs ont besoin d'un outil qui ne nécessite pas de maintenance constante. Ils ont besoin d'un outil de confiance.
                image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
                items:
                  - title: Gestion continue des modifications
                    text: Coordonnez, partagez et collaborez au sein de votre équipe de livraison de logiciels afin d'apporter plus rapidement une valeur ajoutée à votre activité.
                    list:
                      - Gestion du code source prête pour l'entreprise
                      - "Suivi de chaque modification\_: code pour l'application, l'infrastructure, les politiques, les configurations"
                      - "Contrôle de chaque modification : propriétaires du code, approbateurs, règles"
                      - Contrôle de version distribuée pour les équipes réparties sur des territoires géographiques différents
                    link:
                      text: En savoir plus
                      href: "/stages-devops-lifecycle/source-code-management/"
                      data_ga_name:
                      data_ga_location: body
                    icon:
                      name: gitlab-cd
                      alt: Icône de GitLab CD
                      variant: marketing
                    video: https://www.youtube.com/embed/JAgIEdYhj00?enablesjsapi=1&enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
                  - title: Intégration et vérification continues
                    text: Accélérez votre transformation numérique en créant des applications de haute qualité, mises à l'échelle.
                    list:
                      - Automatisation du code, de la compilation et des tests pour compiler et tester progressivement chaque modification
                      - Moins de risques en détectant les erreurs plus tôt
                      - Mise à l'échelle avec des compilations parallèles, des trains de fusion
                      - Collaboration entre différents projets avec un pipeline contenant plusieurs projets
                    link:
                      text: En savoir plus
                      href: "/solutions/continuous-integration/"
                      data_ga_name:
                      data_ga_location: body
                    icon:
                      name: continuous-integration
                      alt: Icône d'intégration continue
                      variant: marketing
                    video: https://www.youtube.com/embed/ljth1Q5oJoo?enablesjsapi=1&enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
                  - title: Environnements à la demande avec GitOps
                    text: Créez des environnements reproductibles et stables en minimisant le risque lié aux configurations d'infrastructure manuelles et aux clics.
                    list:
                      - Automatisez l'infrastructure pour une release plus rapide
                      - Remédiez plus rapidement aux erreurs
                      - Choisissez des configurations push ou pull
                      - Sécurisez l'accès au cluster Kubernetes pour éviter d'exposer votre cluster
                    link:
                      text: En savoir plus
                      href: "/solutions/gitops/"
                      data_ga_name:
                      data_ga_location: body
                    icon:
                      name: cog-code
                      alt: Icône de code dans un rouage
                      variant: marketing
                    video: https://www.youtube.com/embed/onFpj_wvbLM?enablesjsapi=1&enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
                  - title: Livraison continue
                    text: Automatisez votre processus de release d'applications pour une livraison de logiciels reproductible et à la demande.
                    list:
                      - Faites en sorte que chaque modification puisse donner lieu à une release
                      - Déployez progressivement les modifications afin de minimiser les perturbations
                      - Obtenez des feedbacks plus rapidement en testant les modifications sur un sous-ensemble d'utilisateurs
                    link:
                      text: En savoir plus
                      href: "/stages-devops-lifecycle/continuous-delivery/"
                      data_ga_name:
                      data_ga_location: body
                    icon:
                      name: continuous-delivery
                      alt: Icône de livraison continue
                      variant: marketing
                    video: https://www.youtube.com/embed/L0OFbZXs99U?enablesjsapi=1&enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
    - name: group-buttons
      data:
        header:
          text: Découvrez les autres fonctionnalités de GitLab qui permettent d'automatiser la livraison logicielle.
          link:
            text: Explorer plus de solutions
            href: /solutions/
        buttons:
          - text: Intégration continue
            icon_left: continuous-delivery
            href: /solutions/continuous-integration/
          - text: Sécurité logicielle continue
            icon_left: devsecops
            href: /solutions/continuous-software-security-assurance/
          - text: Gestion du code source
            icon_left: cog-code
            href: /stages-devops-lifecycle/source-code-management/
    - name: 'report-cta'
      data:
        layout: "dark"
        title: Rapports des analystes
        reports:
        - description: "GitLab reconnu comme unique Leader dans le rapport The Forrester Wave™, catégorie Plateformes intégrées de livraison de logiciels (2e\_trimestre\_2023)"
          url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
          link_text: Lire le rapport
        - description: "GitLab reconnu Leader dans le Gartner® Magic Quadrant™ 2023 pour les plateformes DevOps"
          url: /gartner-magic-quadrant/
          link_text: Lire le rapport
    - name: 'solutions-resource-cards'
      data:
        title: Ressources
        link:
          text: "En savoir plus"
        cards:
          - icon:
              name: ebook
              alt: Icône d'e-book
              variant: marketing
            event_type: "Présentation de la solution"
            header: "Guide pour la livraison automatisée de logiciels"
            link_text: "En savoir plus"
            fallback_image: /nuxt-images/blogimages/nvidia.jpg
            href: "https://learn.gitlab.com/automated-software-delivery/solution-brief-asd?lb_email={{lead.email address}}&utm_medium=other&utm_campaign=autosd&utm_content=autosdpage"
          - icon:
              name: ebook
              alt: Icône d'e-book
              variant: marketing
            event_type: "e-Book"
            header: "Mesurer le retour sur investissement de la livraison automatisée de logiciels"
            link_text: "Lire les considérations sur le RSI"
            fallback_image: /nuxt-images/blogimages/worldline-case-study-image.jpg
            href: "https://page.gitlab.com/autosd-roi.html"
          - icon:
              name: ebook
              alt: Icône d'e-book
              variant: marketing
            event_type: "e-Book"
            header: "Modernisez votre CI/CD"
            link_text: "En savoir plus"
            fallback_image: /nuxt-images/blogimages/modernize-cicd.jpg
            href: "/resources/ebook-fuel-growth-cicd/"
            data_ga_name: "Modernize your CI/CD"
            data_ga_location: "body"
          - icon:
              name: gitlab
              alt: Icône GitLab
              variant: marketing
            event_type: "Calculateur"
            header: "Combien coûte votre chaine d'outils\_?"
            link_text: "Calculer le RSI"
            href: "https://about.gitlab.com/calculator/roi"
            fallback_image: /nuxt-images/resources/resources_18.jpg
            data_ga_name: "Calculate ROI"
            data_ga_location: "body"
          - icon:
              name: ebook
              alt: Icône d'e-book
              variant: marketing
            event_type: "e-Book"
            header: "GitOps\_: guide pour débutants"
            link_text: "En savoir plus"
            href: "https://page.gitlab.com/resources-ebook-beginner-guide-gitops.html"
            fallback_image: /nuxt-images/blogimages/blog-performance-metrics.jpg
          - icon:
              name: ebook
              alt: Icône d'e-book
              variant: marketing
            event_type: "e-Book"
            header: "10\_raisons d'adopter GitOps dès aujourd'hui"
            link_text: "En savoir plus"
            href: "https://page.gitlab.com/gitops-enterprise-ebook.html"
            fallback_image: /nuxt-images/blogimages/gitops-image-unsplash.jpg
          - icon:
              name: ebook
              alt: Icône d'e-book
              variant: marketing
            event_type: "e-Book"
            header: "CI/CD à l'échelle"
            link_text: "En savoir plus"
            href: "/resources/scaled-ci-cd/"
            fallback_image: /nuxt-images/blogimages/modernize-cicd.jpg
            data_ga_name: "Scaled CI/CD"
            data_ga_location: "body"
          - icon:
              name: case-study
              alt: Icône étude de cas
              variant: marketing
            event_type: "Étude de cas"
            header: "Les bonnes pratiques GitOps"
            link_text: "Le témoignage de 3\_clients"
            href: "https://learn.gitlab.com/gitops-awereness-man-1/gitops_automation_success"
            fallback_image: /nuxt-images/blogimages/xcite_cover_image.jpg
            data_ga_name: "Scaled CI/CD"
            data_ga_location: "body"
          - icon:
              name: report
              alt: Icône de rapport
              variant: marketing
            event_type: "Rapport des analystes"
            header: "GitLab est un précurseur dans GigaOm Radar pour GitOps"
            link_text: "Lire le GigaOm Radar\_2022 pour GitOps"
            href: "https://page.gitlab.com/resources-report-gigaom-gitops-radar.html"
            fallback_image: /nuxt-images/blogimages/anwb_case_study_image_2.jpg
