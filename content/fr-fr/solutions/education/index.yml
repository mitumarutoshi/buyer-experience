---
  title: GitLab pour le secteur de l'éducation
  description: GitLab permet aux campus de se moderniser, d'innover et de se renforcer rapidement en matière de sécurité grâce à une plateforme tout-en-un où chacun peut contribuer.
  components:
    - name: 'solutions-hero'
      data:
        note:
          - GitLab pour le secteur de l'éducation
        title: L'avenir du développement logiciel commence ici
        subtitle: Donnez à tous les membres de votre établissement l'avantage de l'entreprise
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Essayer Ultimate gratuitement
          url: https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com/&glm_content=default-saas-trial

          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: Contacter le service commercial
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
          icon:
            variant: product
            name: chevron-lg-right
        image:
          image_url: /nuxt-images/solutions/education/education_hero.jpeg
          image_url_mobile: /nuxt-images/solutions/education/education_hero.jpeg
          alt: ""
          bordered: true
    - name: 'education-case-study-carousel'
      data:
        case_studies:
          - logo_url: /nuxt-images/logos/uw-logo.svg
            institution_name: University of Washington
            img_url: /nuxt-images/solutions/education/university_of_washington.jpg
            quote:
              quote_text: Au cours des deux dernières années, GitLab a transformé notre organisation à l'Université de Washington. Votre plateforme est fantastique !
              author: Aaron Timss
              author_title: Director of Information  Technology, Université de Washington
            case_study_url: /customers/uw/
            data_ga_name: University of Washington case study
            data_ga_location: case study carousel
          - logo_url: /nuxt-images/case-study-logos/logo_uni_surrey.svg
            institution_name: University of Surrey
            img_url: /nuxt-images/solutions/education/university_of_surrey.jpg
            quote:
              quote_text: Nous apprécions le fait que GitLab soit une plateforme qui offre une solution unique pour de nombreuses tâches. Il ne s'agit pas seulement d'un dépôt. La plateforme offre également de nombreuses fonctionnalités, notamment un wiki, GitLab Pages, des données d'analyse, une interface Web, des requêtes de fusion, un outil CI/CD et un espace commentaires sur les tickets. Vous pouvez ainsi collaborer sur les tickets et les requêtes de fusion. C'est une solution complète dans l'ensemble.
              author: Kosta Polyzos
              author_title: Senior Systems Administrator, University of Surrey
            case_study_url: /customers/university-of-surrey
            data_ga_name: University of Surrey case study
            data_ga_location: case study carousel
          - logo_url: /nuxt-images/logos/victoria-university-wellington-logo.svg
            institution_name: Te Herenga Waka—Victoria University of Wellington
            img_url: /nuxt-images/solutions/education/victoria.jpeg
            quote:
              quote_text: Lorsque j'ai entendu parler de GitLab Auto-géré, mon choix a été très clair. Seul GitLab répondait vraiment aux exigences que j'avais dans le cadre des cours de gestion de projets d'ingénierie. Cela et le fait que GitLab soit un seul et même produit.
              author: Dr James Quilty
              author_title: Director of Engineering, Te Herenga Waka—Victoria University of Wellington
            case_study_url: /customers/victoria_university
            data_ga_name: Victoria University of Wellington case study
            data_ga_location: case study carousel
          - logo_url: /nuxt-images/logos/deakin-university-logo.svg
            institution_name: Deakin University
            img_url: /nuxt-images/solutions/education/deakin-case-study.jpg
            quote:
              quote_text: L'une des raisons pour lesquelles nous avons adopté GitLab était le nombre de fonctions de sécurité prêtes à l'emploi qui nous ont permis de remplacer d'autres solutions et outils open source et, par conséquent, les ensembles de compétences qui les accompagnaient.
              author: Aaron Whitehand
              author_title: Director of Digital Enablement, Deakin University
            case_study_url: /customers/deakin-university
            data_ga_name: Deakin University case study
            data_ga_location: case study carousel
          - logo_url: /nuxt-images/logos/heriot-watt-logo.svg
            institution_name: Heriot Watt University
            img_url: /nuxt-images/solutions/education/heriot.jpeg
            quote:
              quote_text: GitLab a joué un rôle déterminant dans la préparation des étudiants à la pratique professionnelle, notamment en ce qui concerne l'apprentissage du contrôle de la source, des compilations et des meilleures pratiques en matière de développement Agile.
              author: Rob Stewart
              author_title: Assistant Professor Computer Science, Heriot Watt University
            case_study_url: /customers/heriot_watt_university
            data_ga_name: Heriot Watt University case study
            data_ga_location: case study carousel
          - logo_url: /nuxt-images/logos/dublin-city-university-logo.svg
            institution_name: Dublin City University
            img_url: /nuxt-images/solutions/education/dublin-city-uni.jpg
            quote:
              quote_text: "C'est un grand avantage pour DCU de pouvoir proposer GitLab dans le cadre du programme de formation afin de s'assurer que les étudiants sont à la pointe de la technologie. C'est une excellente préparation avant de rejoindre l'industrie."
              author: Jacob Byrne
              author_title: Étudiant de 4e année, Dublin City University
            case_study_url: /customers/dublin-city-university
            data_ga_name: Dublin City University case study
            data_ga_location: case study carousel
    - name: 'case-study-tabs'
      data:
        heading: Pourquoi choisir GitLab ?
        tabs:
          - tab: Établissements
            title: Aidez votre établissement d'enseignement (école, université, institut universitaire, école de codage ou tout autre type d'établissement scolaire) à livrer des logiciels plus rapidement et en toute sécurité.
            text: Avec la plateforme DevSecOps de GitLab, vous disposez de tout ce dont vous avez besoin pour rassembler les équipes afin de raccourcir les cycles, de réduire les coûts, de renforcer la sécurité et d'augmenter la productivité.
            cta:
              text: Contacter un expert
              url: /sales/
              data_ga_name: talk to an expert
              data_ga_location: schools tab
            cards:
              - title: Votre transformation numérique au pas de course
                icon_name: slp-digital-transformation
                text: |
                  Réussissez votre transformation numérique avec une application tout-en-un qui gère l'ensemble du cycle de livraison des logiciels. La plateforme DevSecOps de GitLab vous aidera à :

                  - Moderniser la façon dont vous créez et livrez des logiciels
                  - Augmenter l'efficacité opérationnelle
                  - Fournir de meilleurs produits plus rapidement
                  - Réduire les risques de sécurité et de conformité

                cta:
                  text: En savoir plus
                  url: /solutions/digital-transformation/
                  data_ga_name: digital transformation
                  data_ga_location: schools tab
              - title: Meilleure collaboration grâce à une plateforme unique
                icon_name: slp-collaboration-alt-4
                text: |
                  Aidez tous les membres de votre établissement (développeurs, responsables des opérations, de la sécurité et même les professeurs et étudiants) à travailler ensemble plus efficacement sur une plateforme DevSecOps conçue pour la collaboration entre les équipes distribuées. GitLab vous offre :

                  - Une planification de projet en toute transparence à l'aide de tickets, d'épopées, de tableaux, de jalons et bien plus encore
                  - Un contrôle de version ainsi qu'un historique des versions performants
                  - Des requêtes de fusion pour examiner et discuter du code (et des résultats des tests et analyses) avant de le fusionner
                cta:
                  text: En savoir plus
                  url: /platform/
                  data_ga_name: platform
                  data_ga_location: schools tab
              - title: Sécurité et conformité garanties
                icon_name: slp-release
                text: |
                  Sécurisez vos données, vos recherches, vos applications, les travaux de vos étudiants et tout le reste, tout en garantissant la conformité grâce à la plateforme DevSecOps de GitLab. Grâce à un ensemble complet de scanners de sécurité intégrés, à l'analyse des dépendances pour vous aider à créer un SBOM et à des pipelines conformes pour vous aider à automatiser les politiques de sécurité et de conformité, vous pouvez répondre à vos exigences sans sacrifier la vitesse.
                cta:
                  text: En savoir plus
                  url: /solutions/security-compliance/
                  data_ga_name: devsecops
                  data_ga_location: schools tab
          - tab: Enseignants
            title: Rejoignez les facultés du monde entier et donnez à vos étudiants l'avantage de l'entreprise en introduisant GitLab dans votre salle de classe et votre laboratoire de recherche.
            text: GitLab est une plateforme tout-en-un pour la gestion de projet, la collaboration, la gestion du contrôle de source, git, l'automatisation, la sécurité et bien plus encore. Parce qu'elle est facile à utiliser, flexible, et qu'elle regroupe tout en un seul endroit, la plateforme GitLab est le meilleur choix pour intégrer les pratiques du secteur sur le campus. Le programme GitLab pour l'éducation offre des licences GitLab gratuites pour l'enseignement, l'apprentissage et la recherche aux institutions éligibles dans le monde entier !
            cta:
              text: Vérifier mon éligibilité
              url: /solutions/education/join
              data_ga_name: get verified
              data_ga_location: teachers tab
            cards:
              - title: GitLab en classe
                icon_name: slp-user-laptop
                text: |
                  GitLab est utilisé dans des dizaines de disciplines, et pas seulement dans le domaine du codage. En introduisant GitLab dans votre salle de classe, non seulement les étudiants bénéficieront d'un sacré avantage en apprenant sur une plateforme de pointe, mais ils s'initieront également à la gestion de projet, la collaboration, le contrôle de version et les flux de travail opérationnels. GitLab peut être utilisé comme source unique de vérité pour les projets de bout en bout des étudiants, quelle que soit la discipline.

                  Pour les compétences propres à un domaine, les fonctionnalités DevSecOps de GitLab permettent aux étudiants d'apprendre le développement de logiciels, la gestion d'infrastructure, les technologies de l'information et la sécurité. Les tests de code, l'intégration continue, le développement continu et les tests de sécurité sont regroupés en un seul endroit. Fournissez à vos étudiants l'outil qui leur permettra d'aller plus loin dans leur apprentissage.
                cta:
                  text: Lire les résultats de l'enquête GitLab menée dans le secteur de l'éducation
                  url: /solutions/education/edu-survey
                  data_ga_name: gitlab education survey
                  data_ga_location: teachers tab
              - title: GitLab dans la recherche
                icon_name: slp-open-book
                text: |
                  GitLab transforme le processus de recherche scientifique dans toutes les disciplines en apportant le paradigme DevSecOps à la communauté scientifique. Les chercheurs constatent une amélioration en matière de transparence, de collaboration, de reproductibilité, d'obtention des résultats et d'intégrité des données lorsqu'ils incorporent GitLab dans leur processus de recherche. Des milliers d'articles scientifiques évalués par des pairs ont été publiés sur GitLab ou sur des données stockées dans GitLab.
                cta:
                  text: Voir des exemples de recherche
                  url: /blog/2022/02/15/devops-and-the-scientific-process-a-perfect-pairing/
                  data_ga_name: gitlab in research
                  data_ga_location: teachers tab
              - title: Invitez-nous sur votre campus
                icon_name: slp-institution
                text: |
                  Notre équipe serait ravie de venir vous aider, vous ou vos étudiants, à démarrer votre parcours DevSecOps. Nous proposons des ateliers, des conférences et nous pouvons discuter avec vos organisations étudiantes à propos de DevSecOps, GitLab et bien d'autres sujets.
                cta:
                  text: Programmer une visite
                  url: https://docs.google.com/forms/d/e/1FAIpQLSeGya0P_GOn9QXR0VrQYoy7uCSJ2x6IryrKEGBzcmQmcDZv1g/viewform
                  data_ga_name: invite us to your campus
                  data_ga_location: teachers tab
          - tab: Étudiants
            title: Prenez une longueur d'avance sur le marché du travail en vous formant dès maintenant aux outils de pointe du secteur. Les plus grandes entreprises du monde entier utilisent GitLab pour créer des logiciels, plus rapidement et de manière plus sécurisée, et vous pouvez en faire autant.
            text: Commencez à construire votre portfolio de compétences sur l'ensemble du cycle de vie DevSecOps avec GitLab pendant que vous êtes étudiant. Commencez modestement et apprenez au fur et à mesure. Nous sommes là pour vous aider.
            cta:
              text: Lire les témoignages des clients
              url: /customers/
              data_ga_name: read case studies
              data_ga_location: students tab
            cards:
              - title: Rejoignez le forum GitLab
                icon_name: slp-chat
                text: |
                  Venez vous présenter ! Nous avons créé une catégorie spéciale Éducation comme ressource pour les membres de notre programme. Considérez le forum comme votre ressource pour les questions et réponses à propos de GitLab !
                cta:
                  text: Accéder au forum GitLab
                  url: https://forum.gitlab.com/c/gitlab-for-education/37?_gl=1*8fqeup*_ga*MTc1MDg3NzQ5Ni4xNjU1NDAwMTk3*_ga_ENFH3X7M5Y*MTY3MTczOTQyMC4xOTQuMS4xNjcxNzQyMzI4LjAuMC4w
                  data_ga_name: gitlab forum
                  data_ga_location: students tab
              - title: Assistez à un événement meetup ou organisez-en un
                icon_name: slp-calendar-alt-3
                text: |
                  Découvrez nos meetups GitLab ! Un excellent moyen de rencontrer des professionnels dans votre région et d'en apprendre davantage sur DevSecOps. Vous ne trouvez pas d'événement meetup près de chez vous ? Organisez-en un au sein de votre communauté. Nous allons vous aider pour que cela soit super facile.
                cta:
                  text: Trouver un événement meetup local
                  url: /community/meetups
                  data_ga_name: meetups
                  data_ga_location: students tab
              - title: Participez à un hackathon
                icon_name: slp-idea-collaboration
                text: |
                  Tous les trimestres, nous organisons des hackathons au cours desquels vous pouvez apporter vos contributions, gagner des prix et nouer des contacts avec d'autres contributeurs. Les étudiants sont les bienvenus !
                cta:
                  text: S'inscrire à un hackathon
                  url: /community/hackathon/
                  data_ga_name: hackathon
                  data_ga_location: students tab
              - title: Premiers pas avec GitLab
                icon_name: slp-pencil
                text: |
                  Vous êtes novice sur GitLab ? Consultez nos ressources d'apprentissage pour commencer votre parcours DevSecOps.
                cta:
                  text: Ressources d'apprentissage
                  url: https://docs.gitlab.com/ee/tutorials/
                  data_ga_name: getting started
                  data_ga_location: students tab
              - title: Rejoignez-nous en tant que contributeur
                icon_name: slp-code
                text: |
                  Vous êtes étudiant et vous souhaitez acquérir de l'expérience ? Vous voulez développer vos compétences au sein d'un réseau de soutien ? Consultez notre programme dédié aux contributeurs de code pour commencer !
                cta:
                  text: Devenir contributeur
                  url: /community/contribute
                  data_ga_name: contribute
                  data_ga_location: students tab
    - name: 'education-feature-tiers'
      data:
        heading: Proposez GitLab sur votre campus
        teacher_tier:
          title: Enseignants
          text: Obtenez des licences gratuites de GitLab pour l'éducation et améliorez l'enseignement, l'apprentissage et la recherche sur votre campus*
          features:
            - item: Sièges illimités
            - item: Édition Ultimate
            - item: Toute méthode de déploiement
            - item: 50 000 unités de calcul
            - item: Soutien de la communauté
            - item: Utilisation en classe ou à des fins de recherche uniquement
          cta:
            text: Vérifier mon éligibilité
            url: /solutions/education/join/
            data_ga_name: get verified
            data_ga_location: education feature tiers
          disclaimer: '*ne doit pas servir à diriger, administrer ou faire fonctionner un établissement'
        schools_tier:
            title: Établissements
            text: Accédez à la plateforme DevSecOps complète pour l'ensemble de votre campus à un prix forfaitaire
            features:
              - item: Sièges illimités**
              - item: Édition Ultimate
              - item: Toute méthode de déploiement
              - item: 50 000 unités de calcul
              - item: Assistance prioritaire
              - item: Aucune restriction d'utilisation
            cta:
              text: Contacter le service commercial
              url: /sales/
              data_ga_name: contact sales
              data_ga_location: education feature tiers
            disclaimer: '**jusqu''à l''inscription des étudiants'
            callout_box:
              title: Ou créez votre propre forfait sur mesure
              features:
                - item: Juste les sièges dont vous avez besoin
                - item: N'importe quelle édition
                - item: Toute méthode de déploiement
                - item: Unités de calcul en fonction de l'édition sélectionnée
                - item: Assistance prioritaire
                - item: Aucune restriction d'utilisation
              cta:
                text: Bénéficiez de 20 % de réduction
                url: /sales/
                data_ga_name: get discount
                data_ga_location: education feature tiers
    - name: 'education-stats'
      data:
        header: Faites partie de la communauté GitLab
        stats:
          - spotlight_text: Plus de 1 000
            text: établissements d'enseignement font partie du programme GitLab pour l'éducation
          - spotlight_text: 3 millions
            text: de personnes (et ce n'est pas fini) utilisent GitLab dans des établissements d'enseignement
          - spotlight_text: '65'
            text: pays ont rejoint le programme GitLab pour l'éducation