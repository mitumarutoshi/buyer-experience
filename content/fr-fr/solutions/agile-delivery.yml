---
  title: Planification Agile avec GitLab
  description: Comment utiliser GitLab comme outil de gestion de projet Agile dans les processus Agile tels que Scrum, Kanban et Scaled Agile Framework (SAFe).
  report_cta:
    layout: "dark"
    title: Rapports d'analystes
    reports:
    - description: "GitLab reconnu comme unique leader dans The Forrester Wave™ dédié aux plateformes de livraison de logiciels intégrés, T2\_2023"
      url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
      link_text: Lire le rapport
    - description: "GitLab reconnu comme un leader dans le Gartner® Magic Quadrant™ 2023 dédié aux plateformes DevOps"
      url: /gartner-magic-quadrant/
      link_text: Lire le rapport
  components:
    - name: 'solutions-hero'
      data:
        title: Planification Agile
        subtitle: Planifiez et gérez vos projets, programmes et produits avec un support Agile intégré
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          text: Commencer votre essai gratuit
          url: /free-trial/
        secondary_btn:
          text: En savoir plus sur la tarification
          url: /pricing/
          data_ga_name: Learn about pricing
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Image\_: GitLab pour le secteur public"
    - name: 'copy-media'
      data:
        block:
          - header: Planification Agile avec GitLab
            text: |
              Les équipes de développement continuent d'accélérer la création de valeur en appliquant des méthodologies de projet itératives, graduelles et Lean, telles que Scrum, Kanban et Extreme Programming (XP). Les grandes sociétés ont adopté la méthodologie Agile à l'échelle de l'entreprise par le biais de divers cadriciels, y compris Scaled Agile Framework (SAFe), Spotify et Large Scale Scrum (LeSS). GitLab permet aux équipes d'appliquer des pratiques et des principes Agile afin d'organiser et de gérer leur travail, quelle que soit la méthodologie choisie.
            link_href: /demo/
            link_text: En savoir plus
            video:
              video_url: https://www.youtube.com/embed/wmtZKC8m2ew?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - header: Avantages de GitLab
            text: |
              Plateforme DevSecOps la plus complète, GitLab est :

              *   **Fluide :** GitLab favorise la collaboration et la visibilité pour les équipes Agile, de la planification au déploiement et au-delà, en proposant une expérience utilisateur unique et en mettant à disposition un ensemble commun d'outils
              *   **Intégrée :** gérez les projets au sein du système dans lequel vous travaillez
              *   **Évolutive :** organisez plusieurs équipes Agile pour obtenir une évolutivité Agile au niveau de l'entreprise
              *   **Flexible :** personnalisez les fonctionnalités prêtes à l'emploi en fonction des besoins de votre méthodologie, que vous utilisiez votre propre version d'Agile ou adoptiez un cadre formel
              *   **Intuitive :** consultez notre guide de [démarrage rapide](https://www.youtube.com/watch?v=VR2r1TJCDew){data-ga-name ="quick start" data-ga-location ="body"} pour en savoir plus sur la configuration des équipes Agile
            image:
              image_url: /nuxt-images/blogimages/cicd_pipeline_infograph.png
              alt: ""
          - header: Gestion des projets Agile
            inverted: true
            text: |
              GitLab permet une gestion de projet Lean et Agile, du suivi des tickets de base à la gestion de projets de type Scrum et Kanban. Que vous suiviez simplement quelques tickets ou que vous gériez le cycle de vie complet de DevSecOps au sein d'une équipe de développeurs, GitLab répond aux besoins de votre équipe.

              *   **Planifiez, assignez et suivez** à l'aide de [tickets](https://docs.gitlab.com/ee/user/project/issues/index.html){data-ga-name ="issues" data-ga-location ="body"}
              *   **Organisez le travail** avec des [labels](https://docs.gitlab.com/ee/user/project/labels.html){data-ga-name="labels" data-ga-location="body"}, des [itérations](https://docs.gitlab.com/ee/user/group/iterations/){data-ga-name="iterations" data-ga-location="body"} et des [jalons](https://docs.gitlab.com/ee/user/project/milestones/index.html#overview){data-ga-name="milestones" data-ga-location="body"}
              *   **Visualisez les tâches** sur les [tableaux de bord](https://docs.gitlab.com/ee/user/project/issue_board.html#issue-boards){data-ga-name="boards" data-ga-location="body"}
              *   **Associez les tâches à la sortie** à l'aide des [requêtes de fusion](https://docs.gitlab.com/ee/user/project/merge_requests/index.html){data-ga-name="body" data-ga-location="body"}
            video:
              video_url: https://www.youtube.com/embed/wmtZKC8m2ew?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - header: Gestion des programmes et des portefeuilles
            text: |
              Gardez de la visibilité et assurez l'alignement des employés et des projets sur les initiatives commerciales. Définissez et appliquez des politiques et des permissions, suivez les progrès et la vitesse de plusieurs projets et groupes, et hiérarchisez les initiatives pour générer le plus de valeur possible.

              *   **Organisez** les nouvelles initiatives et les nouveaux efforts commerciaux en [épopées](https://docs.gitlab.com/ee/user/group/epics/){data-ga-name ="epics" data-ga-location ="body"}
              *   **Alignez les équipes** et les projets dans des programmes, sans sacrifier la sécurité et la visibilité, en utilisant les sous-groupes
              *   **Planifiez** les [sous-épopées](https://docs.gitlab.com/ee/user/group/epics/manage_epics.html#multi-level-child-epics){data-ga-name = "subepics" data-ga-location = "body"} et les tickets liés aux itérations et aux jalons
              *   **Visualisez** la livraison de valeur à l'aide des feuilles de route, des [insights](https://docs.gitlab.com/ee/user/project/insights/){data-ga-name="insights" data-ga-location ="body"}, et de l'[analyse des chaînes de valeur](https://docs.gitlab.com/ee/user/group/value_stream_analytics/index.html){data-ga-name="value stream analytics" data-ga-location="body"}
            video:
              video_url: https://www.youtube.com/embed/VR2r1TJCDew?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - header: Scaled Agile Framework (SAFe) avec GitLab
            inverted: true
            text: |
              Découvrez comment votre organisation peut utiliser GitLab pour créer un cadriciel à l'aide de Scaled Agile Framework (SAFe). Explorez en détail la création de votre cadriciel Agile pour les équipes de développement, qui repose sur trois piliers : l'équipe, le programme et le portefeuille.
            video:
              video_url: https://www.youtube.com/embed/PmFFlTH2DQk?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
    - name: copy-media
      data:
        block:
          - header: Fonctionnalités
            miscellaneous: |
              *   **Tickets :** commencez par un [ticket](https://docs.gitlab.com/ee/user/project/issues/){data-ga-name="start with an issue" data-ga-location="body"} correspondant à une seule fonctionnalité offrant une valeur commerciale aux utilisateurs.
              *   **Tâches :** souvent, un cas d'utilisation est divisé en tâches individuelles. Pour mieux identifier ces tâches, vous pouvez créer une [liste](https://docs.gitlab.com/ee/user/markdown.html#task-lists){data-ga-name="task list" data-ga-location="body"} de tâches dans la description d'un ticket dans GitLab.
              *   **Tableaux des tickets :** ils sont tous regroupés au même endroit. [Suivez](https://docs.gitlab.com/ee/user/project/issues/#issues-per-project){data-ga-name = "track" data-ga-location = "body"} les tickets et communiquez les progrès sans passer d'un produit à l'autre. Une seule interface pour suivre vos tickets, de la création à l'achèvement.
              *   **Épopées :** gérez votre portefeuille de projets plus efficacement et facilement grâce au suivi de groupes de tickets sur le même thème, dans différents projets et jalons avec des [épopées](https://docs.gitlab.com/ee/user/group/epics/){data-ga-name ="milestones with epics" data-ga-location="body"}.
              *   **Jalons :** suivez les tickets et les requêtes de fusion créés pour atteindre un objectif plus large dans un certain délai avec les [jalons](https://docs.gitlab.com/ee/user/project/milestones/){data-ga-name ="gitlab milestones" data-ga-location="body"} de Gitlab.
              *   **Feuilles de route :** la date de début et/ou la date d'échéance peuvent être visualisées sous la forme d'une ligne chronologique. La [feuille de route](https://docs.gitlab.com/ee/user/group/roadmap/){data-ga-name = "roadmap" data-ga-location ="body"} des épopées permet de visualiser toutes les épopées qui font partie d'un groupe et/ou de ses sous-groupes.
              *   **Labels :** créez et assignez-les à des tickets individuels, ce qui vous permet de filtrer les listes de tickets en fonction d'un ou de plusieurs [labels](https://docs.gitlab.com/ee/user/project/labels.html#prioritize-labels){data-ga-name="multiple labels" data-ga-location="body"}.
              *   **Graphique d'avancement :** suivez le travail en temps réel et atténuez les risques au fur et à mesure qu'ils surviennent. Les graphiques d'[avancement](https://docs.gitlab.com/ee/user/project/milestones/burndown_and_burnup_charts.html#burndown-charts){data-ga-name="burndown" data-ga-location="body"} permettent aux équipes de visualiser le travail associé au sprint en cours au fur et à mesure qu'il est terminé.
              *   **Points et estimation :** indiquez l'effort estimé pour chaque ticket en assignant des attributs de [poids](https://docs.gitlab.com/ee/user/project/issues/issue_weight.html){data-ga-name ="weight" data-ga-location = "body"} et indiquez l'effort estimé
              *   **Collaboration :** la possibilité de [contribuer](https://docs.gitlab.com/ee/user/discussions/){data-ga-name ="contribute" data-ga-location ="body"} par le biais de commentaires est proposée dans l'ensemble de GitLab, c'est-à-dire dans les tickets, les épopées, les requêtes de fusion, les validations et plus encore !
              *   **Traçabilité :** alignez les tickets de votre équipe sur les [requêtes de fusion](https://docs.gitlab.com/ee/user/project/merge_requests/){data-ga-name ="merge requests" data-ga-location = "body"} suivantes. Cette méthode vous offre une traçabilité complète, de la création des tickets à la fin, une fois le pipeline associé terminé.
              *   **Wikis :** un système de documentation appelé [Wiki](https://docs.gitlab.com/ee/user/project/wiki/){data-ga-name ="wiki" data-ga-location ="body"}, si vous souhaitez conserver votre documentation dans le projet où réside votre code.
              *   **Cadriciels Agile pour entreprise :** les grandes sociétés ont adopté la méthodologie Agile à l'échelle de l'entreprise en utilisant divers cadriciels. GitLab peut prendre en charge [SAFe](https://www.scaledagileframework.com/), Spotify, Disciplined Agile Delivery et bien plus encore.
    - name: 'copy-media'
      data:
        block:
          - header: Une itération Agile avec GitLab
            subtitle: Témoignages d'utilisateurs → Tickets GitLab
            text: |
              Avec Agile, vous commencez souvent par un cas d'utilisation. Celui-ci capture une seule fonctionnalité offrant une valeur commerciale aux utilisateurs. Dans GitLab, un seul ticket au sein d'un projet sert cet objectif.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/issue-list.png
              alt: ""
          - subtitle: Tâche → Listes de tâches GitLab
            inverted: true
            text: |
              Souvent, un cas d'utilisation est divisé en tâches individuelles. Pour mieux les identifier, vous pouvez créer une liste de tâches dans la description d'un ticket dans GitLab.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/Tasks.png
              alt: ""
          - subtitle: Épopées → épopées GitLab
            text: |
              À l'inverse, certains adeptes de la méthode Agile privilégient un concept aux cas d'utilisation. Souvent appelé épopée, il correspond à un flux d'utilisation plus vaste composé de plusieurs fonctionnalités. Dans GitLab, une épopée contient également un titre et une description, un peu comme un ticket. Toutefois, elle vous permet d'attacher plusieurs tickets enfants pour indiquer cette hiérarchie.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/issue.png
              alt: ""
          - subtitle: Produits en attente → Listes de tickets et labels prioritaires sur GitLab
            inverted: true
            text: |
              Les propriétaires du produit ou d'unités commerciales créent généralement ces cas d'utilisation pour refléter les besoins de l'entreprise et des clients. Ils sont hiérarchisés dans un backlog de produits pour signaler l'urgence et l'ordre de développement souhaité. Le propriétaire du produit communique avec les parties prenantes pour déterminer les priorités et modifie constamment le backlog. Dans GitLab, des listes de tickets sont générées de façon dynamique. Les utilisateurs peuvent les consulter pour suivre leur backlog. Des labels peuvent être créés et assignés à des tickets individuels, ce qui vous permet de filtrer les listes de tickets en fonction d'un ou de plusieurs labels. Vous gagnez ainsi en flexibilité. Les labels prioritaires peuvent même être utilisés pour également classer les tickets au sein de ces listes.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/issue-board.png
              alt: ""
          - subtitle: Sprints → Jalons GitLab
            text: |
              Un sprint représente une période finie au cours de laquelle le travail doit être achevé. Il peut s'agir d'une ou de plusieurs semaines, ou peut-être d'un mois ou plus. Le propriétaire du produit et l'équipe de développement décident ensemble de la portée du sprint à venir. La fonctionnalité Jalons de GitLab prend en charge cette organisation : assignez aux jalons une date de début et une date d'échéance pour représenter la durée du sprint. L'équipe inclut ensuite les tickets au sprint en les assignant à ce jalon spécifique.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/Milestones.png
              alt: ""
          - subtitle: Points et estimation → poids des tickets GitLab
            inverted: true
            text: |
              Au cours de la même réunion, des cas d'utilisation sont communiqués et le niveau d'effort technique est estimé pour chaque cas d'utilisation qui entre dans le champ d'application. Dans GitLab, les tickets ont un attribut de poids, qui sert à indiquer l'effort estimé. Au cours de cette réunion (ou des suivantes), les cas d'utilisation sont décomposés en livrables techniques, documentant parfois les plans techniques et l'architecture. Dans GitLab, ces informations peuvent être documentées dans le ticket ou dans la description de la requête de fusion, car la collaboration technique a souvent lieu dans la requête de fusion. Au cours du sprint (jalon GitLab), les membres de l'équipe de développement travaillent sur les cas d'utilisation, un par un. Dans GitLab, les tickets ont des personnes assignées. Vous pouvez donc vous assigner un ticket pour indiquer que vous travaillez actuellement dessus. Nous vous recommandons de créer immédiatement une requête de fusion vide liée au ticket pour commencer le processus de collaboration technique, avant même d'écrire une seule ligne de code.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/weight.png
              alt: ""
          - subtitle: Tableau Agile → Tableaux des tickets de GitLab
            text: |
              Au cours du sprint, les tickets passent par différentes étapes, telles que Prêt pour le développement, Développement, Assurance qualité, Révision, Terminé, en fonction du flux de travail de votre organisation. Il s'agit généralement de colonnes dans un tableau Agile. Dans GitLab, les tableaux de tickets vous permettent de définir vos étapes et de déplacer les tickets de l'une à l'autre. L'équipe peut configurer le tableau par rapport au jalon et à d'autres attributs pertinents. Lors des points quotidiens, l'équipe regarde le tableau ensemble pour analyser le statut du sprint du point de vue du flux de travail.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/issue_board.png
              alt: ""
          - subtitle: Graphiques d'avancement → Graphiques d'avancement GitLab
            inverted: true
            text: |
              L'équipe de développement veut savoir si elle est en bonne voie en temps réel et atténuer les risques au fur et à mesure qu'ils surviennent. GitLab fournit des graphiques d'avancement, qui permettent à l'équipe de visualiser les progrès du travail associé au sprint en cours au fur et à mesure qu'il est terminé. Vers la fin du sprint, l'équipe de développement présente les fonctionnalités développées aux différentes parties prenantes. Avec GitLab, ce processus est simplifié par les applications de révision. Ainsi, même le code qui n'est pas encore en production, mais dans divers environnements de test, d'index ou d'UAT, peut être présenté. Les applications de révision et les fonctionnalités CI/CD sont intégrées à la requête de fusion elle-même. Ces mêmes outils sont utiles pour les développeurs et les rôles en charge de l'assurance qualité afin de maintenir la qualité du logiciel, que ce soit par le biais de tests automatisés avec CI/CD ou de tests manuels dans un environnement d'application de révision.
            image:
              image_url: /nuxt-images/solutions/agile-delivery/burndown-chart.png
              alt: ""
    - name: copy-resources
      data:
        hide_horizontal_rule: true
        block:
          - title: Ressources
            subtitle: Vous voulez passer à la prochaine étape ?
            text: |
              Obtenez de l'[aide sur l'utilisation de GitLab](/get-help/){data-ga-name="get help" data-ga-location="body"} si vous avez des questions
            resources:
              video:
                header: Vidéos
                links:
                  - text: Regardez notre tutoriel pour configurer des équipes Agile avec GitLab
                    link: https://youtu.be/VR2r1TJCDew
                  - text: Comment appliquer SAFe (Scaled Agile Framework) avec GitLab
                    link: https://youtu.be/PmFFlTH2DQk
                  - text: Fonctionnement des tableaux de tickets avec GitLab
                    link: https://youtu.be/CiolDtBIOA0
                  - text: Tableaux de tickets configurables
                    link: https://youtu.be/m5UTNCSqaDk
              blog:
                header: Blogs
                links:
                  - text: Comment utiliser GitLab pour Agile
                    link: /blog/2018/03/05/gitlab-for-agile-software-development/
                    data_ga_name: GitLab for Agile
                    data_ga_location: body
                  - text: 4 façons d'utiliser les tableaux
                    link: /blog/2018/08/02/4-ways-to-use-gitlab-issue-boards/
                    data_ga_name: 4 ways to use Boards
                    data_ga_location: body