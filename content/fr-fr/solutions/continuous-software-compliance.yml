---
  title: Conformité logicielle continue
  description: Intégrer la conformité pour moins de risques et plus d'efficacité
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        title: Conformité logicielle continue
        subtitle: Intégrer la conformité pour moins de risques et plus d'efficacité
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Commencer votre essai gratuit
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: Des questions? Nous contacter
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Image: gitLab pour une conformité logicielle continue"
    - name: 'by-solution-intro'
      data:
        text:
          highlight: La conformité logicielle ne se réduit plus à remplir quelques critères spécifiques.
          description: Les applications cloud-native présentent des surfaces d'attaque entièrement nouvelles via les conteneurs, les orchestrateurs, les API Web et d'autres infrastructures en tant que code. Ces nouvelles surfaces d'attaque, ainsi que les chaînes d'outils DevOps complexes, ont donné lieu à des attaques notoires de la chaîne d'approvisionnement en logiciels et ont conduit à de nouvelles exigences réglementaires. La conformité logicielle continue devient un moyen essentiel de gérer les risques inhérents aux applications cloud-native et à l'automatisation DevOps, au-delà de la simple réduction des failles de sécurité dans le code lui-même.
    - name: 'by-solution-benefits'
      data:
        title: Conformité. Sécurité. Simplifiée.
        image:
          image_url: "/nuxt-images/resources/resources_11.jpeg"
          alt: GitLab pour le secteur public
        is_accordion: true
        items:
          - header: Confiance à chaque validation
            icon:
              name: release
              alt: Icône de bouclier avec une coche
              variant: marketing
            text: La conformité aux licences et les analyses de sécurité sont automatiquement effectuées à chaque modification de code validée.
          - header: Pipelines de conformité pour assurer l'application
            icon:
              name: pipeline-alt
              alt: Icône de pipeline
              variant: marketing
            text: Veillez à ce que les politiques importantes soient appliquées, qu'il s'agisse de contrôles réglementaires standard ou de votre propre cadre politique.
          - header: Simplifier les audits
            icon:
              name: magnifying-glass-code
              alt: Icône Code loupe
              variant: marketing
            text: Les politiques automatisées simplifient les audits grâce à la visibilité et à la traçabilité.
          - header: Tout au même endroit
            icon:
              name: devsecops
              alt: Icône DevSecOps
              variant: marketing
            text: Avec GitLab, vous avez une seule application à gérer. Il n'y a pas de silos et rien n'est perdu en cours de route.
    - name: 'by-solution-value-prop'
      data:
        title: Une unique plateforme DevOps pour la conformité
        cards:
          - title: Contrôles intégrés
            description: La conformité des logiciels peut s'avérer difficile lorsqu'elle est déconnectée du processus de développement des logiciels. Les organisations ont besoin d'un programme de conformité qui soit intégré, et non greffé, à leurs flux de travail et processus existants. Pour en savoir plus, téléchargez le <a href="https://page.gitlab.com/resources-ebook-modernsoftwarefactory.html" data-ga-name="Guide to Software Supply Chain Security" data-ga-location="body">Guide de la sécurité de la chaîne d’approvisionnement des logiciels</a>
            icon:
              name: visibility
              alt: Icône visibilité
              variant: marketing
          - title: Automatisation des politiques
            description: Les garde-fous de conformité permettent un développement rapide des logiciels tout en réduisant les risques de non-conformité et de retard des projets. L'audit est simplifié en ayant tout au même endroit.
            icon:
              name: continuous-integration
              alt: Icône d'intégration continue
              variant: marketing
          - title: Décalage de la conformité à gauche
            description: Comme pour les failles de sécurité, il est plus efficace de trouver et de corriger les failles de conformité le plus tôt possible. Veiller à ce que la conformité soit intégrée dans le développement permet à la conformité de se décaler également à gauche.
            icon:
              name: less-risk
              alt: Icône moins de risque
              variant: marketing
          - title: Cadres de conformité
            description: Appliquez facilement des règles de conformité communes à un projet à l'aide d'un label.
            list:
              - text: <a href="https://about.gitlab.com/blog/2023/08/17/meet-regulatory-standards-with-gitlab/" data-ga-name="PCI Compliance" data-ga-location="body" >Conformité PCI</a>
              - text: <a href="https://about.gitlab.com/solutions/hipaa-compliance/" data-ga-name="HIPAA Compliance" data-ga-location="body" >Conformité HIPAA</a>
              - text: <a href="https://about.gitlab.com/solutions/financial-services-regulatory-compliance/" data-ga-name="Financial Services Regulatory Compliance" data-ga-location="body" >Conformité réglementaire des services financiers</a>
              - text: <a href="/privacy/privacy-compliance/" data-ga-name="GDPR" data-ga-location="body" >RGPD</a>
              - text: <a href="https://about.gitlab.com/solutions/iec-62304/" data-ga-name="IEC 62304:2006" data-ga-location="body" >CEI 62304:2006</a>
              - text: <a href="https://about.gitlab.com/solutions/iso-13485/" data-ga-name="ISO 13485:2016" data-ga-location="body" >ISO 13485:2016</a>
              - text: <a href="https://about.gitlab.com/solutions/iso-26262/" data-ga-name="ISO 26262-6:2018" data-ga-location="body" >ISO 26262-6:2018</a>
              - text: Votre propre cadre politique
            icon:
              name: web-alt
              alt: Icône Web
              variant: marketing
    - name: 'by-industry-solutions-block'
      data:
        subtitle: Simplifier la conformité logicielle continue
        sub_description: "Les fonctionnalités de gestion de la conformité de GitLab visent à créer une expérience simple, conviviale et aussi peu contraignante que possible en vous permettant de définir, d'appliquer et de rendre compte des politiques et des cadres de conformité."
        white_bg: true
        sub_image: /nuxt-images/solutions/showcase.png
        solutions:
          - title: Gestion des politiques
            description: Définissez des règles et des politiques pour respecter les cadres de conformité et les contrôles communs.
            icon:
              name: clipboard-checks
              alt: Icône de vérification du presse-papiers
              variant: marketing
            link_text: En savoir plus
            link_url: https://docs.gitlab.com/ee/administration/compliance.html
            data_ga_name: common controls
            data_ga_location: solutions block
          - title: Automatisation du flux de travail conforme
            icon:
              name: automated-code
              alt: Icône de code automatisé
              variant: marketing
            description:  L'automatisation de la conformité vous aide à appliquer les règles et politiques définies et la séparation des tâches tout en réduisant le risque global pour l'entreprise.
            link_text: En savoir plus
            link_url: https://docs.gitlab.com/ee/administration/compliance.html#compliant-workflow-automation
            data_ga_name: Compliant Workflow Automation
            data_ga_location: solutions block
          - title: Gestion de l'audit
            icon:
              name: magnifying-glass-code
              alt: Icône Code loupe
              variant: marketing
            description: Enregistrez les activités tout au long de votre automatisation DevOps pour identifier les incidents et prouver le respect des règles de conformité et des politiques définies. Grâce à une plateforme unique et à l'absence de silos dans la chaîne d'outils, la visibilité est meilleure.
            link_text: En savoir plus
            link_url: https://docs.gitlab.com/ee/administration/compliance.html#audit-management
            data_ga_name: Audit management
            data_ga_location: solutions block
          - title: Tests de sécurité et gestion des vulnérabilités
            description: Garantissez l'analyse de sécurité et la conformité des licences pour chaque modification de code et permettez aux ingénieurs DevOps et aux professionnels de la sécurité de suivre et de gérer les vulnérabilités.
            icon:
              name: approve-dismiss
              alt: Icône approuver refuser
              variant: marketing
            link_text: En savoir plus
            link_url: https://about.gitlab.com/solutions/security-compliance/
            data_ga_name: Security testing and vulnerability management
            data_ga_location: solutions block
          - title: Sécurité de la chaîne d'approvisionnement des logiciels
            description: Gérez les surfaces d'attaque de bout en bout des applications cloud-native et de l'automatisation DevOps, au-delà des tests traditionnels de sécurité des applications.
            link_text: En savoir plus
            icon:
              name: release
              alt: Icône du bouclier de vérification
              variant: marketing
            link_url: https://about.gitlab.com/direction/supply-chain/
            data_ga_name: offline environment
            data_ga_location: solutions block
    - name: 'solutions-resource-cards'
      data:
        title: Ressources
        column_size: 4
        link:
          text: "En savoir plus"
        cards:
          - icon:
              name: video
              alt: Icône vidéo
              variant: marketing
            event_type: "Vidéo"
            header: "Pipelines conformes"
            link_text: "Regarder maintenant"
            image: "/nuxt-images/solutions/compliance/compliant-pipelines.jpeg"
            href: "https://www.youtube.com/embed/jKA_e_jimoI"
            data_ga_name: "Compliant pipelines"
            data_ga_location: resource cards
          - icon:
              name: blog
              alt: Icône de blog
              variant: marketing
            event_type: "Blog"
            header: "Trois choses que vous ne savez peut-être pas sur la sécurité de GitLab"
            link_text: "En savoir plus"
            href: "https://about.gitlab.com/blog/2021/11/23/three-things-you-might-not-know-about-gitlab-security/"
            image: "/nuxt-images/resources/resources_1.jpeg"
            data_ga_name: "Three things you may not know about GitLab security"
            data_ga_location: resource cards
          - icon:
              name: webcast
              alt: Icône Webcast
              variant: marketing
            event_type: "Webinaire"
            header: "Sécuriser votre chaîne d'approvisionnement des logiciels"
            link_text: "En savoir plus"
            href: "https://www.youtube.com/watch?v=dZfS4Wt5ZRE"
            fallback_image: "/nuxt-images/resources/fallback/img-fallback-cards-gitops.png"
            data_ga_name: "Secure your software supply chain"
            data_ga_location: resource cards
          - icon:
              name: ebook
              alt: Icône e-book
              variant: marketing
            event_type: "E-book"
            header: "Sécurité de la chaîne d'approvisionnement des logiciels"
            link_text: "En savoir plus"
            href: "https://learn.gitlab.com/devsecops-aware/software-supply-chain-security-ebook"
            fallback_image: "/nuxt-images/resources/fallback/img-fallback-cards-gitlab.png"
            data_ga_name: "Software Supply Chain Security"
            data_ga_location: resource cards
          - icon:
              name: case-study
              alt: Icône étude de cas
              variant: marketing
            event_type: "Étude de cas"
            header: "Étude de cas Chorus"
            link_text: "En savoir plus"
            image: "/nuxt-images/resources/resources_2.jpeg"
            href: "/customers/chorus/"
            data_ga_name: "Chorus case study"
            data_ga_location: resource cards
