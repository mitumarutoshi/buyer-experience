---
  title: Contact GitLab Sales
  description: "Contact the GitLab's Sales team if you want to see a custom demo, get help finding the right plan or find answers to frequently asks questions."
  components:
    - name: crop-infinity-hero
      data:
        title: Talk to an Expert
        subtitle: Want to see a custom demo or get help finding the right plan? We'd love to chat.
    - name: sales-form
      data:
        form:
          formId: 1415
          form_header: ''
          datalayer: sales
          form_required_text: Please complete all fields.
        card:
          title: Looking for technical help?
          description: Please contact customer support by submitting a case on our GitLab Support page.
          link:
            text: Contact Support
            href: https://support.gitlab.com/
            ga_name: Support
            ga_location: body
        quotes_carousel:
          quotes:
            - title_img:
                url: /nuxt-images/logos/nvidia-logo-horizontal.png
                alt: Nvidia Logo
              quote: |
                Without GitLab, we'd be wasting engineering time with lots of individual little servers being managed around the world. We would probably have a lot more headaches and still be suffering with scalability problems.
              author: Patrick Herlihy, Configuration Management Specialist, NVIDIA
              url: /customers/nvidia/
              data_ga_name: nvidia
              data_ga_location: body
            - title_img:
                url: /nuxt-images/logos/logo_ticketmaster_2_color.svg
                alt: Ticketmaster logo
              quote: |
                Things were looking pretty scrappy for our CI pipeline only a few months ago. Now it is a whole different ball game. If your team is looking for a way to breathe fresh life into a legacy CI pipeline, I suggest taking a look at GitLab CI. It has been a real game changer for our mobile team at Ticketmaster.
              author: Jeff Kelsey, Lead Android Engineer, Ticketmaster
              url: /blog/2017/06/07/continuous-integration-ticketmaster/
              data_ga_name: ticketmaster case study
              data_ga_location: body
            - title_img:
                url: /nuxt-images/logos/logo_goldman_sachs-color.svg
                alt: Goldman Sachs logo
              quote: |
                GitLab has allowed us to dramatically increase the velocity of development in our Engineering Division. We believe GitLab’s dedication to helping enterprises rapidly and effectively bring software to market will help other companies achieve the same sort of efficiencies we have seen inside Goldman Sachs. We now see some teams running and merging 1000+ CI feature branch builds a day!
              author: Andrew Knight, Managing Director, Goldman Sachs
              url: /customers/goldman-sachs/
              data_ga_name: goldman sachs case study
              data_ga_location: body
            - title_img:
                url: /nuxt-images/logos/equinix-logo.svg
                alt: Equinix logo
              quote: |
                Moving into a system like GitLab will help any organization or enterprise to get into DevOps methodology and continuously improve the deployment workflows to achieve quality, agility, and self-serviceability.
              author: Bala Kannan, Senior Software Engineer, Equinix
              url: /customers/equinix/
              data_ga_name: equinix case study
              data_ga_location: body
            - title_img:
                url: /nuxt-images/logos/logo_siemens_color.svg
                alt: Siemens Logo
              quote: |
                GitLab is a great product and is one of the friendliest and healthiest open source communities.
              author: Alexis Reigel, Senior Software Engineer, Siemens
              url: /blog/2018/12/18/contributor-post-siemens/
              data_ga_name: siemens case study
              data_ga_location: body
            - title_img:
                url: /nuxt-images/logos/usarmy.svg
                alt: us army Logo
              quote: |
                Instead of having to teach people 10 different things, they just learn one thing, which makes it much easier in the long run.
              author: JChris Apsey, Captain, U.S. Army Cyber School
              url: /customers/us_army_cyber_school/
              data_ga_name: us army cyber school case study
              data_ga_location: body
            - title_img:
                url: /nuxt-images/logos/logoradiofrance.svg
                alt: Radio france Logo
              quote: |
                That was the main goal that we had - to reunify multiple tools into a single one and make it really easy for developers to deploy to production. We were at 10 per day before the migration. Now with GitLab, we do 50 deployments per day in production, which is way more efficient than when we had to switch between GitLab and Jenkins.
              author: Julien Vey, Operational Excellence Manager, Radio France
              url: /customers/radiofrance/
              data_ga_name: radio france case study
              data_ga_location: body
            - title_img:
                url: /nuxt-images/logos/hackerone-logo.png
                alt: hackerone Logo
              quote: |
                GitLab is helping us catch security flaws early and it's integrated it into the developer's flow. An engineer can push code to GitLab CI, get that immediate feedback from one of many cascading audit steps and see if there's a security vulnerability built in there, and even build their own new step that might test a very specific security issue.
              author: Mitch Trale, Head of Infrastructure, HackerOne
              url: /customers/hackerone/
              data_ga_name: hacker one case study
              data_ga_location: body
            - title_img:
                url: /nuxt-images/logos/worldline-logo.svg
                alt: worldline Logo
              quote: |
                GitLab is the backbone of our development environment. Today we have 2,500 people working on that for us daily.
              author: Antoine Neveux, Software Engineering- Kazan Team, Worldline
              url: /customers/worldline/
              data_ga_name: worldline case study
              data_ga_location: body
            - title_img:
                url: /nuxt-images/logos/uw-logo.svg
                alt: University of Washington logo
              quote: |
                Over the past two years, GitLab has been transformational for our organization here at UW. Your platform is fantastic!
              author: Aaron Timss, Director of Information Technology, CSE
              url: /customers/uw/
              data_ga_name: university of washington case study
              data_ga_location: body
  faq:
    header: Frequently asked questions
    groups:
      - header: "License and Subscription"
        questions:
          - question: I already have an account, how do I upgrade?
            answer: >-
              Head over to [https://customers.gitlab.com](https://customers.gitlab.com){data-ga-name="customers" data-ga-location="body"}, choose the plan that is right for you.
          - question: Can I add more users to my subscription?
            answer: >-
              Yes. You have a few options. You can add users to your subscription any time during the subscription period. You can log in to your account via the [GitLab Customers Portal](https://customers.gitlab.com){data-ga-name="gitlab customers portal" data-ga-location="body"} and add more seats or [contact sales](/sales/){data-ga-name="contact sales" data-ga-location="body"} for a quote. In either case, the cost will be prorated from the date of quote/purchase through the end of the subscription period. You may also pay for the additional licenses per our true-up model.
          - question: How will I be charged for add-on users?
            answer: |
              If you have [quarterly subscription reconciliation](https://docs.gitlab.com/ee/subscriptions/quarterly_reconciliation.html){ data-ga-name="reconciliation" data-ga-location="body"} enabled (default option for new and renewing subscriptions after Aug 1, 2021), users added during a quarter will only be charged for the remaining quarters of their subscription term as opposed to the full annual subscription fee(s) with annual true-ups. For example, if you add 50 users to your subscription during the third quarter of your subscription term, the 50 users will only be charged for the fourth quarter of your subscription term as opposed to all four quarters as per annual true-ups.

              If you do not have quarterly subscription reconciliation enabled, add-on users will be charged annual true-ups for the full term during which they were added. For example, if you have 100 active users today, you should purchase a 100 user subscription. Suppose that when you renew next year you have 300 active users (200 extra users). When you renew you pay for a 300 user subscription and you also pay the full annual fee for the 200 users that you added during the year.
          - question: What happens when my subscription is about to expire or has expired?
            answer: >-
              You will receive a new license that you will need to upload to your GitLab instance. This can be done by following [these instructions](https://docs.gitlab.com/ee/user/admin_area/license.html){ data-ga-name="licence" data-ga-location="body"}.
          - question: What happens if I decide not to renew my subscription?
            answer: >-
              14 days after the end of your subscription, your key will no longer work and GitLab Enterprise Edition will not be functional anymore. You will be able to downgrade to GitLab Community Edition, which is free to use.
          - question: Can I acquire a mix of licenses?
            answer: >-
              No, all users in the group need to be on the same plan.
          - question: How does the license key work?
            answer: >-
              The license key is a static file which, upon uploading, allows GitLab Enterprise Edition to run. During license upload we check that the active users on your GitLab Enterprise Edition instance doesn't exceed the new number of users. During the licensed period you may add as many users as you want. The license key will expire after one year for GitLab subscribers.

      - header: "Payments and Pricing"
        questions:
          - question: What is a user?
            answer: >-
              User means each individual end-user (person or machine) of Customer and/or its Affiliates (including, without limitation, employees, agents, and consultants thereof) with access to the Licensed Materials hereunder.
          - question: Is the listed pricing all inclusive?
            answer: >-
              The listed prices may be subject to applicable local and withholding taxes. Pricing may vary when purchased through a partner or reseller.
          - question: What features are included in GitLab self-managed and SaaS across the pricing plans?
            answer: >-
              You can find an up to date list on the [features page](/features/){ data-ga-name="features page" data-ga-location="body"}.
          - question: Can I import my projects from another provider?
            answer: >-
              Yes. You can import your projects from most of the existing providers, including GitHub and Bitbucket. [See our documentation](https://docs.gitlab.com/ee/user/project/import/index.html){ data-ga-name="see our documentation" data-ga-location="body"} for all your import options.
          - question: Do you have special pricing for open source projects, educational institutions, or startups?
            answer: >-
              Yes! We provide free Ultimate licenses, along with 50K compute minutes/month, to qualifying open source projects, educational institutions, and startups. Find out more by visiting our [GitLab for Open Source](/solutions/open-source/){ data-ga-name="open source" data-ga-location="body"}, [GitLab for Education](/solutions/education/){ data-ga-name="education" data-ga-location="body"}, and [GitLab for Startups](/solutions/startups/){ data-ga-name="startups" data-ga-location="body"} program pages.
          - question: How does GitLab determine what future features fall into given tiers?
            answer: >-
              On this page we represent our [capabilities](/company/pricing/#capabilities){ data-ga-name="capabilities" data-ga-location="body"} and those are meant to be filters on our [buyer-based open core](/company/pricing/#buyer-based-tiering-clarification){ data-ga-name="open core" data-ga-location="body"} pricing model. You can learn more about how we make tiering decisions on our [pricing handbook](/handbook/ceo/pricing){ data-ga-name="pricing decisions" data-ga-location="body"} page.

      - header: Features and Benefits
        questions:
          - question: What are the differences between Free, Premium, and Ultimate plans?
            answer: >-
              All of the features and benefits of the different GitLab offerings can be found on the [feature comparison pages](/features/){ data-ga-name="feature comparison pages" data-ga-location="body"}. Read more about whether [Premium](/pricing/premium){ data-ga-name="premium" data-ga-location="body"} and [Ultimate](/pricing/ultimate){ data-ga-name="ultimate" data-ga-location="body"} are the right tiers for you.
          - question: What is the difference between GitLab and other DevOps solutions?
            answer: >-
              You can see all the differences between GitLab and other popular DevOps solutions on our [competitive comparison pages](/devops-tools/){ data-ga-name="devops tools" data-ga-location="body"}.

      - header: "GitLab SaaS"
        questions:
          - question: Where is SaaS hosted?
            answer: >-
              Currently we are hosted on the Google Cloud Platform in the USA
          - question: What features are not available on GitLab SaaS?
            answer: >-
              Some features are unique to self-managed and do not apply to SaaS. You can find an up to date list on the [features page](/features/){data-ga-name="features page saas" data-ga-location="body"}.

      - header: "Storage and Transfer Limits"
        questions:
          - question: Do the storage and transfer limits apply to self-managed?
            answer: >-
              No. There are no application limits on the amount of storage and transfer for self-managed instances. The administrators are responsible for the underlying infrastructure costs.
          - question: What happens if I exceed my storage and transfer limits?
            answer: >-
              The limits are currently soft limits only, GitLab will not enforce these limits automatically as the product does not show the usage of all Storage and Transfer yet. If you exceed the limits, GitLab may reach out and work with you to reduce your usage or purchase additional add-ons. In the future, the limits will be automatically enforced. This will be announced separately.
          - question: Where can I see my storage and transfer usage?
            answer: >-
              Storage usage can be viewed on the group's Settings->Usage Quota page. Transfer and container registry usage will be added at a later date.
          - question: What counts towards my storage limit?
            answer: >-
              Storage limits are applied at the top-level namespace. All storage consumed by projects within a namespace is counted, including the git repositories, LFS, package registries, and artifacts.
          - question: Can additional storage and transfer be purchased?
            answer: >-
              Additional storage and transfer can be purchased as an add-on. Please follow the process described in our [documentation](https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#purchase-more-storage){data-ga-name="documentation" data-ga-location="body"}.

      - header: "Compute minutes"
        questions:
          - question: What are compute minutes?
            answer: >-
              Compute minutes are the execution time (in minutes) for your pipelines on our shared runners. Execution on your own runners will not increase your compute minutes count and is unlimited.
          - question: What happens if I reach my minutes limit?
            answer: >-
              If you reach your limits, you can [manage your compute usage](/pricing/faq-consumption-cicd/#managing-your-cicd-minutes-usage){data-ga-name="manage minutes usage" data-ga-location="body"},
              [purchase additional compute minutes](https://docs.gitlab.com/ee/subscriptions/gitlab_com/#purchase-additional-ci-minutes){data-ga-name="purchase additional minutes" data-ga-location="body"}, or
              upgrade your account to Premium or Ultimate. Your own runners can still be used even if you reach your limits.
          - question: Does the minute limit apply to all runners?
            answer: >-
              No. We will only restrict your minutes for our shared runners (SaaS only). If you have a
              [specific runner setup for your projects](https://docs.gitlab.com/runner/){data-ga-name="runner" data-ga-location="body"}, there is no limit to your build time on GitLab SaaS.
          - question: Do plans increase the minutes limit depending on the number of users in that group?
            answer: >-
              No. The limit will be applied to a group, no matter the number of users in that group.
          - question: Why do I need to enter credit/debit card details for free compute minutes?
            answer: >-
              There has been a massive uptick in abuse of free compute minutes available on GitLab.com to mine cryptocurrencies - which creates intermittent performance issues for GitLab.com users. To discourage such abuse, credit/debit card details are required if you choose to use GitLab.com shared runners. Credit/debit card details are not required if you bring your own runner or disable shared runners. When you provide the card, it will be verified with a one-dollar authorization transaction. No charge will be made and no money will transfer. Learn more [here](/blog/2021/05/17/prevent-crypto-mining-abuse/){data-ga-name="crypto mining" data-ga-location="body"}
          - question: Is there a different compute minutes limit for public projects?
            answer: >-
              Yes. Public projects created after 2021-07-17 will have an allocation of [compute minutes](https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#ci-pipeline-minutes){data-ga-name="compute minutes" data-ga-location="body"} as follows: Free tier - 50,000 minutes, Premium tier - 1,250,000 minutes, Ultimate tier - 6,250,000.



