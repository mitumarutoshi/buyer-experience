---
  title: Why GitLab Ultimate?
  description: Achieve organization wide security, compliance, and planning with GitLab Ultimate
  side_menu:
    anchors:
      text: On this page
      data:
      - text: Overview
        href: "#overview"
        data_ga_name: overview
        data_ga_location: side-navigation
        nodes:
        - text: Summary
          href: "#wu-summary"
          data_ga_name: summary
          data_ga_location: side-navigation
        - text: Key solutions
          href: "#wu-key-solutions"
          data_ga_name: key-solutions
          data_ga_location: side-navigation
        - text: Customer case studies
          href: "#wu-customer-case-studies"
          data_ga_name: customer-case-studies
          data_ga_location: side-navigation
      - text: ROI calculator
        href: "#wu-roi-calculator"
        data_ga_name: roi-calculator
        data_ga_location: side-navigation
      - text: Ultimate features
        href: "#wu-ultimate-features"
        data_ga_name: ultimate-features
        data_ga_location: side-navigation
        nodes:
        - text: Advanced security testing
          href: "#wu-advanced-security-testing"
          data_ga_name: advanced-security-testing
          data_ga_location: side-navigation
        - text: Security risk mitigation
          href: "#wu-security-risk-mitigation"
          data_ga_name: security-risk-mitigation
          data_ga_location: side-navigation
        - text: Compliance
          href: "#wu-compliance"
          data_ga_name: compliance
          data_ga_location: side-navigation
        - text: Portfolio management
          href: "#wu-portfolio-management"
          data_ga_name: portfolio-management
          data_ga_location: side-navigation
        - text: Value stream management
          href: "#wu-value-stream-management"
          data_ga_name: value-stream-management
          data_ga_location: side-navigation
        - text: Free guest users
          href: "#wu-free-guest-users"
          data_ga_name: free-guest-users
          data_ga_location: side-navigation
        - text: Other Ultimate features
          href: "#wu-other-ultimate-features"
          data_ga_name: other-ultimate-features
          data_ga_location: side-navigation
      - text: Get in touch
        href: "#get-in-touch"
        data_ga_name: get-in-touch
        data_ga_location: side-navigation
    hyperlinks:
      text: Next Steps
      data:
      - text: Buy Ultimate now
        href: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a&test=capabilities
        variant: primary
        icon: false
        data_ga_name: buy-ultimate-now
        data_ga_location: side-navigation
      - text: Learn about Premium
        href: /pricing/premium/
        variant: secondary
        icon: true
        data_ga_name: learn-about-premium
        data_ga_location: side-navigation
  components:
  - name: plan-summary
    data:
      id: wu-summary
      title: Why Ultimate?
      subtitle: Ideal for organization-wide security, compliance, and planning
      text: "Available in both SaaS and self-managed deployment options, GitLab Ultimate adds:"
      list: 
        - Advanced security capabilities
        - Priority support
        - Security risk mitigation
        - Live upgrade assistance
        - Compliance
        - Portfolio management 
        - Value stream management
        - Customer Success Manager for eligible customers
      cta: 
        text: Compare all features
        url: /pricing/feature-comparison/
      footnote: GitLab Ultimate also allows for free guest user licenses to improve your license usage for users with minimal interaction with the system. 
      buttons:
        - variant: primary
          icon: false
          text: Buy Ultimate now
          url: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a&test=capabilities
          data_ga_name: learn-about-ultimate
          data_ga_location: mobile-body
        - variant: secondary
          icon: true
          text: Learn about Premium
          url: /pricing/premium/
          data_ga_name: learn-about-premium
          data_ga_location: mobile-body
  - name: guest-calculator
    data:
      id: wu-guest-calculator
      title: Calculate the cost for your organization
      input_title: GitLab offers unlimited free guest users on Ultimate plans
      caption: |
        *All plans billed annually. The listed prices may be subject to applicable local and withholding taxes. Pricing may vary when purchased through a partner or reseller. See our [Pricing Page](/pricing/){data-ga-name="pricing" data-ga-location="guest calculator"} for more details.
      seats:
        title: Number of GitLab seats
        tooltip: GitLab seats are people with reporter, developer, maintainer or owner access. Guest users do not consume a seat in GitLab Ultimate. Learn more about seat usage <a href="https://docs.gitlab.com/ee/subscriptions/gitlab_com/#how-seat-usage-is-determined">here</a>.
      guests:
        title: Number of guest users
        tooltip: Guest users can create and assign issues, view certain analytics, optionally view code, and more. Learn more <a href="https://docs.gitlab.com/ee/user/permissions.html#project-members-permissions">here</a>.
      premium:
        title: Premium monthly cost*
        link: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a&test=capabilities
        data_ga_name: buy premium
        data_ga_location: guest calculator
        button: Buy Premium
      ultimate:
        title: Ultimate monthly cost*
        link: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a&test=capabilities
        data_ga_name: buy ultimate
        data_ga_location: guest calculator
        button: Buy Ultimate

  - name: 'by-solution-value-prop'
    data:
      id: wu-key-solutions
      light_background: true
      small_margin: true
      large_card_on_bottom: true
      title: GitLab Ultimate helps you
      cards:
        - icon:
            name: increase
            alt: Increase icon
            variant: marketing
          title: Increase Operational Efficiencies
          description:	GitLab Ultimate provides a single, scalable interface for organization wide DevSecOps, reducing handoffs across tools and teams - thereby improving efficiencies.
        - icon:
            name: speed-alt
            variant: marketing
            alt: Speed Icon
          title: Deliver Better Products Faster
          description: With end to end Value Stream Management and Portfolio Management, GitLab Ultimate allow for greater visibility and transparency across projects - helping to eliminate bottlenecks and deliver products faster.
        - icon:
            name: lock-alt-5
            alt: Lock icon
            variant: marketing
          title: Reduce Security and Compliance Risk
          description: GitLab Ultimate introduces built-in security testing, compliance and preventive security for cloud native applications helping you manage security risk and achieve regulatory compliance.
  - name: 'education-case-study-carousel'
    data:
      id: 'wp-customer-case-studies'
      case_studies:
        - logo_url: /nuxt-images/logos/carfax-logo.png
          text: CARFAX improves security, cuts pipeline management and costs with GitLab
          img_url: /nuxt-images/blogimages/carfax-banner.jpeg
          case_study_url: /customers/carfax/
          data_ga_name: carfax case study
          data_ga_location: case study carousel
        - logo_url: /nuxt-images/logos/lockheed-martin.png
          img_url: /nuxt-images/blogimages/lockheed-martin-cover-2.jpg
          text: Lockheed Martin saves time, money, and tech muscle with GitLab
          case_study_url: /customers/lockheed-martin/
          data_ga_name: lockheed-martin case study
          data_ga_location: case study carousel
        - logo_url: /nuxt-images/home/dl-telekom.png
          img_url: /nuxt-images/blogimages/deutsche-telekom-cover.jpg
          text: Deutsche Telekom drives DevSecOps transformation with GitLab Ultimate
          case_study_url: /customers/deutsche-telekom
          data_ga_name: deutsche-telekom case study
          data_ga_location: case study carousel
        - logo_url: /nuxt-images/home/logo_iron_mountain_mono.svg
          img_url: /nuxt-images/blogimages/iron-mountain-2.png
          text: Iron Mountain drives DevOps evolution with GitLab Ultimate
          case_study_url: /customers/iron-mountain/
          data_ga_name: iron-mountain case study
          data_ga_location: case study carousel
        - logo_url: /nuxt-images/logos/dunelm.svg
          img_url: /nuxt-images/blogimages/dunelm.png
          text: 'Dunelm “shifts left”: U.K. homewares leader moves security to front of cycle, boosts cloud move'
          case_study_url: /customers/dunelm
          data_ga_name: dunelm case study
          data_ga_location: case study carousel
        - logo_url: /nuxt-images/logos/hackerone-logo.png
          img_url: /nuxt-images/blogimages/hackerone-cover-photo.jpg
          text: HackerOne achieves 5x faster deployments with GitLab’s integrated security
          case_study_url: /customers/hackerone
          data_ga_name: hackerone case study
          data_ga_location: case study carousel
  - name: roi-calculator-block
    data:
      id: wu-roi-calculator
      header:
        gradient_line: true
        title:
          text: ROI calculator
          anchor: wu-roi-calculator
        subtitle: How much is your toolchain costing you?
      calc_data_src: calculator/index
  features_block:
    id: wu-ultimate-features
    tier: ultimate
    header:
      gradient_line: true
      title:
        text: Ultimate features
        anchor: wu-ultimate-features
        button:
          text: Compare all features
          data_ga_name: Compare all features
          data_ga_location: body
          url: /pricing/feature-comparison/
    pricing_themes:
      - id: wu-advanced-security-testing
        theme: Advanced security testing
        text: protects the integrity of your software supply chain with built in security testing.
        link:
          text: Learn more
          data_ga_name: Ultimate features learn more
          data_ga_location: body
          url: /solutions/security-compliance/
      - id: wu-security-risk-mitigation
        theme: Security risk mitigation
        text: helps you manage your organization's security policies, alerts, and approval rules.
        link:
          text: Learn more
          data_ga_name: Ultimate features learn more
          data_ga_location: body
          url: /solutions/security-compliance/
      - id: wu-compliance
        theme: Compliance
        text: ensures your code, deployments, and environments comply with changing regulations and emerging risks.
        link:
          text: Learn more
          data_ga_name: Ultimate features learn more
          data_ga_location: body
          url: /solutions/compliance/
      - id: wu-portfolio-management
        theme: Portfolio management
        text: allows you to manage large scale organization wide projects.
        link:
          text: Learn more
          data_ga_name: Ultimate features learn more
          data_ga_location: body
          url: /solutions/portfolio-management/
      - id: wu-value-stream-management
        theme: Value stream management
        text: measures and manages the business value of your DevSecOps lifecycle.
        link:
          text: Learn more
          data_ga_name: Ultimate features learn more
          data_ga_location: body
          url: /solutions/value-stream-management/
      - id: wu-free-guest-users
        theme: Free guest users
      - id: wu-other-ultimate-features
        text: Ultimate features unrelated to a theme
