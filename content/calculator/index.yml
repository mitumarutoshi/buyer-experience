---
  title: Return on Investment Calculator
  description: Here you can find information on the GitLab Categories ROI Calculator. View more here and see the value!
  heading: How much is your toolchain costing you?
  cost_roi:
    people: How many people are using and maintaining your tool chain?
    users: Number of users
    maintainers: Number of DevSecOps tools maintainers
    nextStep: Continue to next step
    spendPerYear: Approximately, what is your spend per year (in USD) on these capabilities?
    sourceCodeManagement: Source Code Management
    agileProjectManagement: Agile Project Management
    continuousIntegration: Continuous Integration
    apm: Agile Portfolio Management
    continuousDelivery: Continuous Delivery
    applicationSecurity: Application Security
    registries: Registries
    savings: See your savings
    restart: Restart Calculator
  time_roi:
    people: How many people are using and maintaining your tool chain?
    users: Number of users
    maintainers: Number of DevSecOps tools maintainers
    nextStep: Continue to next step
    howManyDaysToProduction: How many days does it take for an application update (code commit) to make it to production?
    daysToProduction: Days to production
    hoursPerWeek: Hours per week
    developmentConfigurationHours : How many hours per week do your developers spend configuring/integrating different tools in your DevOps toolchain?
    developmentSecurityHours: How many hours per week do your developers spend on fixing security vulnerabilities before production?
    savings: See your savings
    restart: Restart Calculator
  tooltips:
    numberOfUsers: How many employees are using your current toolset?
    numberOfMaintainers: |
      How many employees install, integrate and manage your toolset? An average annual salary of USD $97,000 is assumed source: <a href="https://www.glassdoor.co.in/Salaries/us-software-developer-salary-SRCH_IL.0,2_IN1_KO3,21.htm">glassdoor</a>
    sourceCodeManagement: Annual spend (in USD) for source code management tools including asset version control, branching, and code reviews. (E.g., tools like GitHub, BitBucket)
    continuousIntegration: Annual spend (in USD) for continuous integration tools including build, test, and validation. (E.g., tools like Jenkins, CircleCI)
    continuousDelivery: Annual spend (in USD) for continuous delivery tools used for safe deployments to staging/production environments. (E.g., tools like Harness, ArgoCD)
    applicationSecurity: Annual spend (in USD) for application security tools including static and dynamic testing, amongst others. (E.g., tools like Snyk, Veracode, or Sonarcube)
    agileProjectManagement: Annual spend (in USD) for agile project management tools used for planning within a team/project (E.g., tools like Jira, Trello, or Asana)
    agilePortfolioManagement: Annual spend (in USD) for agile portfolio management tools used for planning across multiple teams/projects. (E.g., tools like Planview Enterprise One, Jira Align)
    registries: Annual spend (in USD) for container and package registries. (E.g., tools like Artifactory, Docker Hub)
  currentSpendTooltip: Current spend includes the costs of tools and maintainers
  cost_results:
    heading: Your toolchain is currently costing you
    monetary_sign: $
    premium_description: GitLab Premium may be a great choice for your company to enhance team productivity and collaboration.
    disclaimer: Results are based on similar sized organizations reported savings. The results are purely an estimate and subject to change based on various factors that went into the calculation.
    youCouldSave: You could save
    ultimateSwitch: annually by switching to GitLab Ultimate!
    ultimate_description: GitLab Ultimate is the best choice for your company to achieve organization wide security, compliance, and planning.
    current_spend: Your current spend
    vs: Vs.
    gitlab_ultimate: GitLab Ultimate
    free_trial: Get free trial
    buy_ultimate: Buy Ultimate now
    results_disclaimer: The results are purely an estimate and subject to change based on various factors that went into the calculation.
    premiumSwitch: annually by switching to GitLab Premium!
    gitlab_premium: GitLab Premium
    buy_premium: Buy Premium now
  time_results: 
    toolchain_cost: Today you spend
    hoursPerProject: hours per project
    hours: hours
    save: Save
    switching: By switching to
    efficiency: Increase efficiency by
    premium: GitLab Premium
    ultimate: GitLab Ultimate
    results: Results are based on similar sized organizations reported savings. The results are purely an estimate and subject to change based on various factors that went into the calculation.
    tooltip: Calculated based on the GitLab-commissioned Forrester Consulting Total Economic Impact™ study in 2022.
    ultimateButton: 
      text: Learn about Ultimate
      href: /pricing/ultimate/
      variant: primary
      data_ga_name: ultimate learn more
      data_ga_location: calculator body with time calculator selected
    premiumButton: 
      text: Learn about Premium
      href: /pricing/premium/
      variant: primary
      data_ga_name: premium learn more
      data_ga_location: calculator body with time calculator selected
    salesButton: 
      text: Contact Sales
      href: /sales
      variant: tertiary
      data_ga_name: sales
      data_ga_location: calculator body with time calculator selected
  marketoForm:
    contactUs: Contact us for a closer look at your savings
    requiredFields: All fields are required
    submissionReceived: Submission received!
  premium_features:
    header: 'GitLab Premium includes:'
    button:
      text: Learn more about Premium
      url: '/pricing/premium/'
      data_ga_location: premium roi results
      data_ga_name: learn more about premium
      listHeading: "GitLab Premium includes:"
      freeTrial: Get free trial
      learnMore: Learn more about Premium
    list:
      - text: Faster code reviews
        information:
          - text: Multiple approvers in code review
          - text: Code Owners
          - text: Code Review Analytics
          - text: Merge request reviews
          - text: Code Quality Reports
          - text: Merged results pipelines
          - text: Comments in Review Apps
      - text: Advanced CI/CD
        information:
          - text: CI/CD Pipelines Dashboard
          - text: Merge Trains
          - text: Multi-project pipeline graphs
          - text: CI/CD for external repo
          - text: GitOps deployment management
          - text: Environments Dashboard
          - text: Group file templates
          - text: Robust deploy and rollback bundle
      - text: Enterprise Agile Delivery
        information:
          - text: Roadmaps
          - text: Multiple Issue Assignees
          - text: Single level Epics
          - text: Issue Board Milestone Lists
          - text: Scoped Labels
          - text: Promote Issue to Epic
          - text: Multiple Group Issue Board
          - text: Issue Dependencies
      - text: Release controls
        information:
          - text: Approval rules for code review
          - text: Required Merge Request Approvals
          - text: Merge Request Dependencies
          - text: Push rules
          - text: Restrict push and merge access
          - text: Protected Environments
          - text: Lock project membership to group
          - text: Geolocation-aware DNS
      - text: Self-managed reliability
        information:
          - text: Disaster Recovery
          - text: Maintenance mode
          - text: Distributed cloning with GitLab Geo
          - text: Fault-tolerant Git storage with Gitaly
          - text: Support for Scaled Architectures
          - text: Container Registry geographic replication
          - text: Database load balancing for PostgreSQL
          - text: Log forwarding
      - text: 10,000 compute minutes per month
      - text: Support
  ultimate_features:
    header: 'GitLab Ultimate includes:'
    button:
      text: Learn more about Ultimate
      url: '/pricing/ultimate/'
      data_ga_location: premium roi results
      data_ga_name: learn more about ultimate
    list:
      - text: Advanced security testing
        information:
          - text: Dynamic Application Security Testing
          - text: Security Dashboards
          - text: Vulnerability Management
          - text: Container Scanning
          - text: Dependency Scanning
          - text: Vulnerability Reports
          - text: API Fuzz Testing
          - text: Vulnerability Database
      - text: Vulnerability management
        information:
          - text: Security Approvals
          - text: Security Policies
      - text: Compliance pipelines
        information:
          - text: Audit events report
          - text: Interface for audit events
          - text: License Compliance
          - text: Compliance report
          - text: Quality Management
          - text: Requirements Management
          - text: Require a Jira issue before merging code
          - text: Custom compliance frameworks
      - text: Portfolio management
        information:
          - text: Epic Boards
          - text: Multi-level Epics
          - text: Issue and Epic Health Reporting
          - text: Planning hierarchy
          - text: Portfolio-level Roadmaps
          - text: Bulk Edit Epics
      - text: Value stream management
        information:
          - text: Insights
          - text: DORA-4 metric - Deployment frequency
          - text: Dora-4 metric - Lead time for changes
      - text: 50,000 compute minutes per month
      - text: Support
      - text: Free guest users
  time_card:
    img_url: /nuxt-images/blogimages/hackerone-cover-photo.jpg
    text: See how HackerOne achieves 5x faster deployments by switching to GitLab's integrated security
    cta:
      text: Read customer case study
      url: /customers/hackerone/
      data_ga_name: HackerOne case study
      data_ga_location: calculator body with time calculator selected
  cards:
  - icon:
      name: money
      alt: Money Icon
      variant: marketing
    title: Learn more about GitLab pricing plans
    link_text: Go to pricing page
    link_url: /pricing
    data_ga_name: learn more about gitlab pricing plans
    data_ga_location: roi calculator
  - icon:
      name: cloud-thin
      alt: Cloud Thin Icon
      variant: marketing
    title: Purchase GitLab through Cloud Marketplaces
    link_text: Contact sales
    link_url: /sales
    data_ga_name: purchase gitlab through cloud marketplaces
    data_ga_location: roi calculator