---
  title: InnerSource mit GitLab
  description: InnerSource ist die Anwendung bewährter Methoden und die Etablierung einer Open-Source-Kultur in deinem Unternehmen. Erfahre mehr!
  template: 'industry'
  no_gradient: true
  nextstep_variant: 5
  components:
    - name: 'solutions-hero'
      data:
        title: InnerSource mit GitLab
        subtitle: Das integrierte Entwicklerportal nutzt bewährte Methoden und Prozesse, um eine effektivere Arbeit und Zusammenarbeit zu ermöglichen.
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Kostenlose Testversion starten
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        image:
          image_url: /nuxt-images/solutions/stock3.png
          image_url_mobile: /nuxt-images/solutions/stock3.png
          alt: "Bild: GitLab für den öffentlichen Sektor"
          bordered: true
    - name: 'side-navigation-variant'
      links:
        - title: Vorteile
          href: '#benefits'
        - title: Leistungen
          href: '#capabilities'
        - title: Preise
          href: '#pricing'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'benefits'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-benefits'
              data:
                title: Gründe, die für InnerSource sprechen
                subtitle: |
                  Leistungsstarke Entwicklungs- und Engineering-Teams übernehmen Open-Source-Prozesse und -Kulturen, um die Erfahrung der Entwickler(innen) zu verbessern und die Zusammenarbeit zu optimieren. Teams verwenden GitLab als internes Entwicklungsportal, um:
                link:
                    text: Erfahre mehr über InnerSource
                    href: https://about.gitlab.com/topics/version-control/what-is-innersource/
                    ga_name: innersource topic
                    ga_location: benefits
                is_accordion: true
                items:
                  - icon:
                      name: speed-alt-2
                      alt: Icon speed-alt-2
                      variant: marketing
                    header: Bessere Software schneller zu entwickeln
                    text: Mit Unit-Tests, Code-Abdeckung, kontinuierlicher Integration und eingebauter End-to-End-Sicherheit wird Code früher abgesichert und verbessert.
                  - icon:
                      name: docs-alt
                      alt: Icon documents
                      variant: marketing
                    header: Umfassende Dokumentation zu erstellen
                    text: Der Code wird in MRs mit Kommentaren sowie Änderungsvorschlägen und in Wikis, Tickets und READMEs für den Prozess besser dokumentiert.
                  - icon:
                      name: stopwatch
                      alt: stopwatch Icon
                      variant: marketing
                    header: Eine höhere Effizienz zu erzielen
                    text: Code, Architektur und Assets sind team- und unternehmensweit auffindbar und verfügbar.
                  - icon:
                      name: user-collaboration
                      alt: user-collaboration Icon
                      variant: marketing
                    header: Zusammenarbeit zu verbessern
                    text: Weniger Reibungsverluste bei Code-Reviews, Kommunikation und Beiträgen in und zwischen Teams im gesamten Unternehmen – auf einer einzigen Plattform.
                  - icon:
                      name: user-laptop
                      alt: user-laptop Icon
                      variant: marketing
                    header: Die Erfahrung der Teammitglieder zu verbessern
                    text: Überflüssige Strukturen werden aufgelöst, die Produktivität und Zufriedenheit steigt und damit auch die Bindung an das Team und die Einstellung neuer Mitarbeiter. 
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'solutions-by-solution-list'
              data:
                title: InnerSource mit GitLab
                icon: slp-check-circle-alt
                header: "GitLab bietet ein optimiertes und angenehmes Entwicklererlebnis und ermöglicht es so Softwareentwicklungsteams, die Qualität zu verbessern, die Effizienz zu steigern, die Zusammenarbeit zu verbessern und Innovationen zu beschleunigen.\_ GitLab optimiert diese Erfahrung durch den Ansatz einer einzigartigen DevSecOps-Plattform mit einem integrierten internen Entwicklerportal, das Folgendes bietet:"
                items: 
                  - Prozess- und Produktdokumentation in anpassbaren READMEs und Runbooks
                  - Zentralisierte, kollaborative Code-Reviews mit [Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests/){data-ga-name="merge request" data-ga-location="capabilities"}
                  - Wissensmanagement über [Wikis](https://docs.gitlab.com/ee/user/project/wiki/){data-ga-name="wikis" data-ga-location="capabilities"}
                  - Ideenfindung, Kommunikation und Updates über [Tickets](https://docs.gitlab.com/ee/user/project/issues){data-ga-name="issues" data-ga-location="capabilities"} und [Thread-Diskussionen](https://docs.gitlab.com/ee/user/discussions/){data-ga-name="threaded discussions" data-ga-location="capabilities"}
                  - Umfassende API-Dokumentation
                  - Visualisierung der Testabdeckungsergebnisse
                  - Leistungsstarke Web-IDE basierend auf VS-Code, die direkt in der Benutzeroberfläche enthalten ist
                  - Native CLI
                footer: Dies alles befindet sich auf derselben Plattform, auf der Teams von der Planung bis hin zur Produktion arbeiten.
                cta:
                  text: Erfahre, wie du InnerSource freischalten kannst
                  video: https://www.youtube.com/embed/ZS1mCpBHXaI
                  ga_name: innersource unlock
                  ga_location: capabilities
                  
        - name: div
          id: 'pricing'
          slot_enabled: true
          slot_content:
            - name: 'tier-block'
              class: 'slp-mt-96'
              id: 'pricing'
              data:
                header: Welcher Tarif ist der richtige für dich?
                cta:
                  url: /pricing/
                  text: Erfahre mehr über die Preise
                  data_ga_name: pricing learn more
                  data_ga_location: pricing
                  aria_label: Preise
                tiers:
                  - id: free
                    title: Forfait Gratuit
                    items:
                      - SAST und Erkennung von Geheimnissen
                      - Ergebnisse in JSON-Datei
                  - id: premium
                    title: Premium
                    items:
                      - Alle Funktionen von Free und zusätzlich
                      - Genehmigungen und Schutzmaßnahmen
                    link:
                      href: /pricing/premium/
                      text: Mehr über Premium
                      data_ga_name: premium learn more
                      data_ga_location: pricing
                      aria_label: Premium Tarif
                  - id: ultimate
                    title: Ultimate
                    items:
                      - Alle Funktionen von Premium und zusätzlich
                      - DAST, API-Fuzzing, Lizenzkonformität und mehr
                      - SBOM und Abhängigkeitslisten
                      - Verwalten von Sicherheitslücken
                      - Konformitätsmanagement
                    link:
                      href: /pricing/ultimate
                      text: Mehr über Ultimate
                      data_ga_name: ultimate learn more
                      data_ga_location: pricing
                      aria_label: Ultimate Tarif
                    cta:
                      href: /free-trial/
                      text: Ultimate kostenlos testen
                      data_ga_name: try ultimate
                      data_ga_location: pricing
                      aria_label: Ultimate Tarif
      