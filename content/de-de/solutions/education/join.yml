---
  title: JNimm am Programm GitLab für Bildungseinrichtungen teil
  description: Mit GitLab für Bildungseinrichtungen bringen wir DevOps in einen Unterrichtsraum in deiner Nähe. Bewirb dich jetzt, um in die Welt von DevOps einzutauchen!
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab für Bildungseinrichtungen
        subtitle: DevOps für deinen Campus
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          url: '#application'
          text: Nimm am Programm GitLab für Bildungseinrichtungen teil
          data_ga_name: join education program
          data_ga_location: header
          icon:
            name: arrow-down
            variant: product
        image:
          image_url: /nuxt-images/solutions/education.jpeg
          alt: "Bild: GitLab für Bildungseinrichtungen"
          rounded: true
    - name: 'side-navigation-variant'
      links:
        - title: Anforderungen
          href: '#requirements'
        - title: Anwendung
          href: '#application'
        - title: Verlängerung
          href: '#renewal'
        - title: Häufig gestellte Fragen (FAQ)
          href: '#frequently-asked-questions'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'requirements'
          slot_enabled: true
          slot_content:
            - name: copy-media
              data:
                block:
                  - header: Anforderungen
                    aos_animation: fade-up
                    aos_duration: 500
                    hide_horizontal_rule: true
                    metadata:
                      id_tag: requirements
                    miscellaneous: |
                      ##### In order to be accepted into the GitLab for Education Program, each educational institution must meet the following requirements.

                      * **Akkreditierung:** Die Bildungseinrichtung muss von einer autorisierten Behörde auf lokaler, staatlicher, landesweiter, bundesstaatlicher oder nationaler Ebene akkreditiert sein. [Mehr erfahren](/handbook/marketing/community-relations/community-programs/education-program/#gitlab-for-education-program-requirements){data-ga-name="accredited" data-ga-location="body"}.
                      * **Hauptzweck: Unterricht** Hauptzweck der Bildungseinrichtung muss der Unterricht der angemeldeten Studierenden sein.
                      * **Gewährung eines Abschlusses:** Die Bildungseinrichtung garantiert aktiv Abschlüsse wie einen Bachelor- oder Masterabschluss oder vergleichbare Abschlüsse (z. B. Associate Degree).
                      * **Gemeinnützigkeit:** Die Bildungseinrichtung darf nicht gewinnorientiert sein. Gewinnorientierte Bildungseinrichtungen sind nicht zur Teilnahme berechtigt.

                      ##### GitLab for Education Licenses can only be used for

                      * **Nutzung zu Lehrzwecken:** GitLab darf nur für Aktivitäten eingesetzt werden, die direkt mit der Ausbildung, dem Unterricht oder der Weiterentwicklung von Studierenden in Zusammenhang stehen, einschließlich des akademischen Unterrichts der Bildungseinrichtung, oder
                      * **Nutzung für nichtkommerzielle akademische Forschung:** GitLab darf nur für die Durchführung von nicht gewinnorientierten Forschungsprojekten, aus denen keine Ergebnisse, Arbeiten, Dienste oder Daten für die kommerzielle Nutzung und die Erzielung von Einnahmen entstehen, eingesetzt werden. Forschung, die auf Antrag und zugunsten eines Dritten durchgeführt wird, ist im Rahmen der GitLab-Lizenz für Bildungseinrichtungen nicht zulässig.
                      * **Es ist nicht erlaubt, Bildungseinrichtungen mit einer GitLab-Lizenz für Bildungseinrichtungen zu führen, zu verwalten oder zu betreiben.** GitLab bietet akademische Rabatte und Sonderpreise für die campusweite Nutzung. [Mehr erfahren](/solutions/education/campus/){data-ga-name= "campus pricing" data-ga-location="body"}.

                      * **Hinweis:** Derzeit sind Bildungseinrichtungen mit Schülern unter 13 Jahren nicht für die Nutzung von GitLab SaaS berechtigt. Solche Bildungseinrichtungen müssen eine selbstverwaltete GitLab-Lizenz erwerben.

                      ##### Applicants must

                      * **Lehrkörper oder Personal:** Nur in Vollzeit an der Bildungseinrichtung beschäftigtes Lehrpersonal oder Personal kann sich bewerben. Wir können keine Lizenzen direkt für Studierende erstellen.
                      * **E-Mail-Domain:** Die Bewerber(innen) müssen sich mit der E-Mail-Domain ihrer jeweiligen Bildungseinrichtung bewerben. Persönliche E-Mail-Domains werden nicht akzeptiert.

                      ##### Country of Origin

                      * GitLab, Inc. stellt keine Lizenzen an Bildungseinrichtungen in China aus. Weitere Informationen über Lizenzen zu Bildungszwecken in China erhältst du von [JiHu] (mailto:ychen@gitlab.cn). [Mehr Informationen zu JiHu](/blog/2021/03/18/gitlab-licensed-technology-to-new-independent-chinese-company/){data-ga-name="more about JiHu" data-ga-location="body"}.

                      #### GitLab for Education Agreement

                      * Mit ihrer Teilnahme unterliegen alle Programmmitglieder der [Programmvereinbarung von GitLab für Bildungseinrichtungen](/handbook/legal/education-agreement/){data-ga-name="education agreement" data-ga-location="body"}.

                      #### Program Benefits

                      * Unbegrenzte Plätze pro Lizenz für unsere erstklassigen Funktionen (SaaS oder selbstverwaltet). Die Anzahl der Plätze entspricht der Anzahl der Benutzer(innen), die diese Lizenz im nächsten Jahr verwenden werden.
                      * Die Anzahl der Plätze und die Art der Lizenz (SaaS oder selbstverwaltet) kein zum Zeitpunkt der Verlängerung oder auf Anfrage geändert werden.
                      * Der GitLab-Support ist nicht in der Lizenz für Bildungseinrichtungen enthalten.
                      * Im Abonnement sind 50.000 CI-Runner-Minuten enthalten. ([Weitere Minuten müssen dazugekauft werden](https://docs.gitlab.com/ee/subscriptions/#purchasing-additional-ci-minutes){data-ga-name="additional minutes" data-ga-location="body"}).

        - name: 'div'
          id: 'application'
          slot_enabled: true
          slot_content:
            - name: copy-form
              data:
                form:
                  external_form:
                    url: https://offers.sheerid.com/gitlab/university/teacher/
                    width: 800
                    height: 1300
                header: Anwendung
                metadata:
                  id_tag: application
                form_header: Antragsformular
                datalayer: sales

                content: |
                  ## Bewerbungsprozess
                  * Fülle das Antragsformular auf der rechten Seite aus. Bitte gib möglichst genaue und umfassende Informationen an.
                  * GitLab arbeitet mit dem Trusted Partner SheerID zusammen, der verifizieren wird, ob du derzeit Lehrende(r), Dozent(in) oder Mitarbeiter(in) einer qualifizierten Bildungseinrichtung bist.

                  ## Was zu erwarten ist
                  Wenn du das Antragsformular ausgefüllt hast und du die Anforderungen erfüllst, erhältst du eine Verifizierungs-E-Mail mit Anweisungen zum Abrufen deiner Lizenz. Bitte befolge die Anweisungen sorgfältig.

                  ## Hilfe und Unterstützung
                  Wenn du beim Abrufen deiner Lizenz aus dem Kundenportal Probleme hast, öffne bitte im [GitLab-Support-Portal](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293) ein Ticket und wähle „Probleme mit Lizenz und Verlängerung“ aus.

        - name: 'div'
          id: 'renewal'
          slot_enabled: true
          slot_content:
            - name: copy
              data:
                aos_animation: fade-up
                aos_duration: 500
                block:
                  - header: Verlängerung
                    metadata:
                      id_tag: renewal
                    subtitle: Verlängerung beantragen
                    column_size: 10
                    text: |
                      GitLab-Lizenzen für Bildungseinrichtungen müssen jährlich verlängert werden. Die Anforderungen des Programms können sich von Zeit zu Zeit ändern und wir müssen sicherstellen, dass bestehende Mitglieder diese auch weiterhin erfüllen.

                      Gehe vor der Verlängerung folgendermaßen vor:

                      * Überprüfe deine Berechtigungen. Die gleiche Person, die das Abonnement für die Bildungseinrichtung im GitLab-Kundenportal erstellt hat, muss auch die Verlängerung des Abonnements beantragen.
                      * Wenn eine andere Person die Verlängerung beantragen soll, muss der/die aktuelle Eigentümer(in) die [Inhaberschaft des Kundenportal-Kontos übertragen](https://docs.gitlab.com/ee/subscriptions/#change-account-owner-information). Wenn der/die aktuelle Eigentümer(in) die Inhaberschaft nicht mehr übertragen kann, öffne bitte ein [Support-Ticket](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293), um den/die Eigentümer(in) des Abonnements zu ändern.


                      Bei der erstmaligen Beantragung des Programms und der Verlängerung einer bestehenden Mitgliedschaft ist das gleiche Formular auszufüllen.

                      Wenn du das Antragsformular ausgefüllt hast und du die Anforderungen erfüllst, erhältst du eine Verifizierungs-E-Mail mit Anweisungen zum Abrufen deiner Lizenz. Bitte befolge die Anweisungen sorgfältig.

        - name: 'div'
          id: 'frequently-asked-questions'
          slot_enabled: true
          slot_content:
            - name: faq
              data:
                metadata:
                  id_tag: frequently-asked-questions
                aos_animation: fade-up
                aos_duration: 500
                header: Häufig gestellte Fragen (FAQ)
                groups:
                  -
                    questions:
                      - question: Kann man zu Forschungszwecken eine Lizenz für Bildungseinrichtungen erhalten?
                        answer: |
                          Ja, wenn die Bildungseinrichtung für eine solche Lizenz berechtigt ist und die Forschung nichtkommerziellen Zwecken dient. Weitere Informationen findest du in unseren Programmbedingungen.
                      - question: Können wir mehrere selbstverwaltete Instanzen mit demselben Lizenzschlüssel ausführen?
                        answer: |
                          Ja. Es ist möglich, mehrere selbstverwaltete Instanzen mit demselben Lizenzschlüssel zu aktivieren.
                      - question: Warum stellt GitLab keine kostenlosen Lizenzen direkt für die Studierenden zur Verfügung?
                        answer: Unser GitLab-Programm für Bildungseinrichtungen richtet sich direkt an die Bildungseinrichtung (d. h. auf Enterprise-Ebene) und nicht an Einzelpersonen. Wir bieten eine unbegrenzte Anzahl an Plätzen und Top-Tier-Lizenzen, sodass alle Studierende einer Bildungseinrichtung auf GitLab zugreifen können. Wir verstehen, dass einige Studierende gerne eine eigene GitLab-Lizenz hätten. Derzeit besteht allerdings logistisch nicht die Möglichkeit, individuelle Lizenzen auszustellen. Wir empfehlen allen Studierenden, eine(n) Sponsor(in) unter dem Lehrpersonal oder dem allgemeinen Personal zu finden, der/die sich für das Programm bewirbt. Studierende können auch unser kostenloses Abonnement für GitLab.com nutzen oder unsere kostenlosen, selbstverwalteten Angebote herunterladen. Du kannst auch eine 30-tägige Testversion beantragen, wenn du unsere erweiterten Funktionen ausprobieren möchtest.
                      - question: Wie kann ich die Sichtbarkeit unserer Projekte verwalten?
                        answer: |
                          Wenn du ein Mitglied der übergeordneten Gruppe in GitLab bist, hast du automatisch Zugriff auf alle abhängigen Elemente. GitLab unterstützt keine Untergruppen, die restriktiver sind als ihre übergeordneten Gruppen. Wenn du Teil einer Untergruppe bist, bedeutet das jedoch nicht, dass du Zugriff auf die übergeordnete Gruppe hast.
                          \
                          Die einfachste Möglichkeit ist, alle zu Mitgliedern der jeweiligen Untergruppe zu machen und in der obersten Gruppe nur Administrator(inn)en zu haben.
                      - question: Kann diese Lizenz in der IT-Abteilung für IT-Services genutzt werden?
                        answer: |
                          Nein, die professionelle Nutzung durch IT-Expert(inn)en oder die administrative Nutzung für die Bildungseinrichtung sind nicht zulässig. Die GitLab-Lizenz für Bildungseinrichtungen darf nur zu Lehr- und Forschungszwecken verwendet werden. Bitte wende dich an unser Vertriebsteam, wenn du GitLab im IT-Bereich professionell nutzen möchtest.
                      - question: Können Studierende unsere GitLab-Instanz nach ihrem Abschluss weiter nutzen?
                        answer: |
                          GitLab-Lizenzen für Bildungseinrichtungen werden direkt für die jeweilige Bildungseinrichtung ausgestellt. Daher müssen Studierende eine eigene Lizenz erwerben, wenn sie keinen Zugriff mehr über ihre Bildungseinrichtung haben.
                      - question: Sind Änderungen an der Endbenutzer-Lizenzvereinbarung möglich?
                        answer: |
                          Derzeit können wir keine Änderungen an unseren Nutzungsvereinbarungen vornehmen. Bitte sende eine E-Mail an education@gitlab.com, wenn du weitere Fragen hast.
                      - question: Ist es möglich, Benutzer(innen) per LDAP über SSL zu authentifizieren?
                        answer: |
                          Dies ist nur in unserer selbstverwalteten Version möglich. Der Server benötigt nicht unbedingt eine statische IP, da ein DNS-Name für den LDAP-Server verwendete werden kann.
                      - question: Ist es möglich, die Anzahl der Plätze in Zukunft zu erhöhen?
                        answer: |
                          Wenn du die Anzahl der Plätze für deine bestehende Lizenz erhöhen möchtest, sende bitte eine E-Mail an education@gitlab.com. Wir erstellen dann ein Ergänzungsangebot für die zusätzlichen Plätze.
                      - question: Wer wird im Abonnement gezählt?
                        answer: |
                          Weitere Informationen dazu findest du in unseren FAQs zu Lizenzen und Abonnements.
                      - question: Wie erhalte ich Unterstützung?
                        answer: |
                          Im [Support für Community-Programme](https://docs.gitlab.com/ee/subscriptions/#support-for-community-programs) erfährst du, wo du Unterstützung erhältst. Bitte beachte, dass der Support für GitLab-Lizenzen für Bildungseinrichtungen nicht mehr separat erworben werden kann. Stattdessen haben qualifizierte Bildungseinrichtungen die Möglichkeit, ein [GitLab-Abonnement für den Campus](/solutions/education/campus/) zu erwerben.
