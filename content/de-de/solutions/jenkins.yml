---
  title: Jenkins-Integration von GitLab
  description: Mit der Jenkins-Integration von GitLab kannst du dein Projekt mühelos für den Build mit Jenkins einrichten und GitLab gibt die Ergebnisse direkt in der GitLab-Benutzeroberfläche aus.
  components:
    - name: 'solutions-hero'
      data:
        title: Jenkins-Integration von GitLab
        subtitle: Löse einen Jenkins-Build für jeden Push zu deinen GitLab-Projekten aus
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          url: https://docs.gitlab.com/ee/integration/jenkins.html
          text: Dokumentation
          data_ga_name: jenkins integration
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Bild: Jenkins-Integration von GitLab"
    - name: copy
      data:
        block:
          - header: Übersicht
            id: overview
            text: |
              GitLab ist eine voll ausgestattete Softwareentwicklungsplattform, die neben anderen leistungsstarken [Funktionen](/features/){data-ga-name="features" data-ga-location="body"} eine integrierte [GitLab CI/CD](/solutions/continuous-integration/){data-ga-name="CI/CD" data-ga-location="body"} enthält, mit der du deine Anwendungen erstellen, testen und bereitstellen kannst, ohne externe CI/CD-Tools integrieren zu müssen.

              Viele Unternehmen nutzen jedoch [Jenkins](https://jenkins.io/) für ihre Bereitstellungsprozesse und benötigen eine Möglichkeit, Jenkins in GitLab zu integrieren, bevor sie zu GitLab CI/CD wechseln können. Andere müssen Jenkins zum Erstellen und Bereitstellen ihrer Anwendungen verwenden, weil sie die bestehende Infrastruktur für ihre aktuellen Projekte nicht ändern können, aber sie möchten GitLab für alle anderen Funktionen nutzen.

              Mit der Jenkins-Integration von GitLab kannst du dein Projekt mühelos so einrichten, dass es mit Jenkins gebaut wird. Die Ergebnisse werden von GitLab direkt in der Benutzeroberfläche von GitLab ausgegeben.
    - name: copy-media
      data:
        block:
          - header: So funktioniert es
            id: how-it-works
            inverted: true
            text: |
              * **Jenkins-Ergebnisse in GitLab Merge Requests anzeigen:** Wenn du die Jenkins-Integration von GitLab für dein Projekt einrichtest, löst jeder Push zu deinem Projekt einen Build auf der externen Jenkins-Installation aus. GitLab gibt den Pipeline-Status (erfolgreich oder fehlgeschlagen) direkt im Merge Request-Widget und in der Pipeline-Liste deines Projekts aus.
              * **Schneller Zugriff auf deine Build-Protokolle:** Wann immer du dein Build-Protokoll überprüfen willst, klickst du einfach auf das Ergebnissymbol und GitLab bringt dich zu deiner [Pipeline](https://docs.gitlab.com/ee/ci/pipelines/index.html){data-ga-name="pipeline" data-ga-location="body"} auf der Benutzeroberfläche von Jenkins.
            icon:
              name: checklist
              alt: Checkliste
              variant: marketing
              hex_color: '#F43012'
          - header: Vorteile
            id: benefits
            text: |
              * **Einfache und schnelle Konfiguration:** Jenkins lässt sich ganz einfach in die [GitLab Enterprise Edition](/pricing/){data-ga-name="pricing" data-ga-location="body"} integrieren – und zwar direkt in den Integrationseinstellungen deines Projekts. Sobald du den Dienst aktiviert hast, um die Authentifizierung von GitLab mit deinem Jenkins-Server zu konfigurieren, und Jenkins weiß, wie es mit GitLab interagieren kann, ist es sofort einsatzbereit.
              * **Deinen Workflow mit Hilfe von GitLab pflegen:** Auch wenn Jenkins deine Builds ausführt, kannst du alles andere mit GitLab erledigen, von der Diskussion neuer Ideen bis hin zur Inbetriebnahme. Die Jenkins-Schnittstelle wird nur benötigt, wenn du weitere Details erhalten möchtest, zum Beispiel im Falle eines Fehlers.
            icon:
              name: clock-alt
              alt: Uhr
              variant: marketing
              hex_color: '#F43012'
