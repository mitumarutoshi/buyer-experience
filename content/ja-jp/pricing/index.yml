---
  title: 各プランと価格について
  description: "単一のアプリケーションでソフトウェアを開発、保護、および操作する機能を組み合わせた、GitLabのDevSecOpsプラットフォームの価格をご覧ください。"
  schema_faq:
    - question: すでにアカウントを持っていますが、アップグレードするにはどうすればよいですか？
      answer: >-
        [GitLabカスタマーポータル](https://customers.gitlab.com)にアクセスして、最適なプランをお選びください。
    - question: サブスクリプションにさらにユーザーを追加できますか？
      answer: >-
        はい。 追加するにはいくつかのオプションがあります。 サブスクリプション期間中はいつでもユーザーをサブスクリプションに追加できます。[GitLabカスタマーポータル](https://customers.gitlab.com)からアカウントにログインして、ライセンス数を追加するか、[セールスチームに問い合わせ](/sales/)から見積もり依頼が可能です。いずれの場合も、費用は見積もり/購入日からサブスクリプション期間の終了日まで日割り計算されます。 また、当社のTrue-upモデルに従い、追加のライセンス料金を支払う方法もございます。
    - question: アドオンユーザーの料金はどのように請求されますか？
      answer: |
        [四半期ごとに調整できるサブスクリプション](https://docs.gitlab.com/ee/subscriptions/quarterly_reconciliation.html)が有効になっている場合(2021年8月1日以降にサブスクリプションを新規購入および更新する場合はデフォルトオプション)、四半期中に追加されたユーザーには、過去1年間遡った料金を請求されるTrue-upモデルとは異なり、サブスクリプション期間の残りの四半期分のみが請求されます。 たとえば、サブスクリプション期間の第3四半期中に50ユーザーを追加した場合、50ユーザーはサブスクリプション期間の第4四半期のみが課金されます。過去1年分の料金を請求されるTrue-upモデルは適応されません。

        四半期ごとに調整できるサブスクリプションが有効になっていない場合は、契約更新時に、アドオンユーザーの過去1年分遡ったTrue-up分が請求されます。 たとえば、現在100人のアクティブユーザーがいる場合は、100ユーザーのサブスクリプションを購入する必要があります。 次回の更新時に、300人のアクティブユーザー (200人の追加ユーザー) がいるとします。その場合の更新時には、300ユーザーのサブスクリプションの料金（次年度更新分）にプラスして、前年に使用した追加分の200ユーザー分の料金全額(追加遡り請求)もあわせて支払うことになります。
    - question: サブスクリプションの有効期限が近づいている、または有効期限が切れた場合はどうなりますか？
      answer: >-
        アクティベーションコードを使ってGitLab有料プランをアクティベートした場合、サブスクリプションを更新すると、次回の [サブスクリプションデータ同期](https://docs.gitlab.com/ee/subscriptions/self_managed/#subscription-data-synchronization)中に新しいサブスクリプションの契約期間がGitLab Self-Managedインスタンスに同期されます。ライセンスキーを使用した場合、新しいライセンスが送信されるので、GitLabインスタンスにアップロードしてください。[これらの手順](https://docs.gitlab.com/ee/user/admin_area/license.html){:data-ga-name="licence"}{:data-ga-location="body"}に従って行ってください。
    - question: サブスクリプションを更新しない場合はどうなりますか？
      answer: >-
        サブスクリプションの終了から14日後、キーが使えなくなり、GitLab Enterpriseエディションは機能しなくなります。フリーで使用できるGitLab Communityエディションにダウングレードできます。
    - question: 異なるプランのライセンスを組み合わせることはできますか？
      answer: >-
        SaaS版のグループ内、またはSelf-Managedインスタンスのユーザーは、すべて同じプランに入っている必要があります。
    - question: ライセンスキーはどのように機能しますか？
      answer: >-
        ライセンスキーはアップロード時にGitLab Enterpriseエディションを実行できる静的ファイルです。 ライセンスのアップロード中に、GitLab Enterpriseエディションインスタンスのアクティブユーザー数が新規ユーザー数を超えていないことを確認します。ライセンス期間中は、必要な数のユーザーを追加できます。 ライセンスキーの有効期限は、GitLab契約者の場合1年になります。2022年現在、ライセンスキーはGitLab有料プランをアクティベートする旧式の方法であり、ほとんどの有料サブスクリプションでアクティベーションコードに置き換えられています。
    - question: アクティベーションコードとは?
      answer: >-
        アクティベーションコードは、Cloud Licensingを使用してSelf-ManagedのサブスクリプションをアクティベートするためのGitLabの手法を指します。これにより、サブスクリプションの利用がよりスムーズになります。これは、バージョン14.1以降のすべてのお客様に必須となります。 Cloud Licensingの詳細については、[Cloud Licensingとは?](https://about.gitlab.com/pricing/licensing-faq/cloud-licensing/)および[アクティベーションコードでアクティベーションするには?](https://docs.gitlab.com/ee/user/admin_area/license.html)を参照してください。
    - question: ユーザーとは何ですか?
      answer: >-
        ユーザーとは、お客様および/またはその関連会社 (従業員、代理人、およびコンサルタントを含むがこれらに限定されない) の個々のエンドユーザー (人または機械) のことで、本契約に基づくライセンス化された素材にアクセスできます。
    - question: 記載されている料金にはすべて含まれていますか？
      answer: >-
        表示されている料金には、地方税および源泉徴収税が適用される場合があります。 パートナーまたは再販業者を通じて購入すると、価格が変わる場合があります。
    - question: 価格プラン全体で、GitLabのSelf-ManagedとSaaSにはどのような機能が含まれていますか？
      answer: >-
        [機能ページ](/features/)に最新のリストがあります。
    - question: 他のプロバイダーからプロジェクトをインポートできますか？
      answer: >-
        はい。 GitHubやBitbucketなど、ほとんどの既存のプロバイダーからプロジェクトをインポートできます。 インポートに関するオプションについては、[ドキュメント](https://docs.gitlab.com/ee/user/project/import/index.html)を参照してください。
    - question: オープンソースプロジェクト、教育機関、またはスタートアップ用に特別価格を適用できますか？
      answer: >-
        はい。特定の条件を満たすオープンソースプロジェクト、教育機関、およびスタートアップに対して、月50Kユニットのコンピューティング/月と無料のUltimateライセンスを提供します。詳細については、[GitLab for Open Source](/solutions/open-source/)、[GitLab for Education](/solutions/education/)、および[GitLab for Startups](/solutions/startups/)の各プログラムページをご覧ください。
    - question: GitLabは、今後リリースされる新機能を、どのランクへ分類するのかをどのように決定していますか？
      answer: >-
        [こちらのページ](/company/pricing/#capabilities)で機能について説明しており、これらは[購入者ベースのオープンコア](/company/pricing/#buyer-based-tiering-clarification)価格モデルにおいて、特定の条件や基準を満たすかどうかを判断する際に活用されています。ランク付けの決定方法についての詳細は、[価格設定ハンドブックページ](/handbook/ceo/pricing){data-ga-name="pricing decisions"}{data-ga-location="body"}をご覧ください。
    - question: Free、Premium、Ultimateプランの違いは何ですか?
      answer: >-
        [機能比較ページ](/features/)で、さまざまなGitLab製品のあらゆる機能やメリットを確認できます。[Premium](/pricing/premium)と[Ultimate](/pricing/ultimate)をご覧いただき、それぞれの違いをご確認ください。
    - question: GitLabと他のDevSecOpsソリューションの違いは何ですか?
      answer: >-
        [競合比較ページ](/devops-tools/)で、GitLabと他の一般的なDevSecOpsソリューションの違いをすべて確認できます。
    - question: サポートには何が含まれますか?
      answer: >-
        有料プランの場合、サポートリクエストのSLAの時間は、リクエスト自体の[サポートの影響](/support/#defitions-of-support-impact)により異なります。 緊急事態(重大度度1)レベルのものは24時間年中無休のサポートを受けられますが、他の重要度のものは平日24時間のサポートになります。 サポート時間の詳細については、[GitLabグローバルサポート時間の定義](/support/#definitions-of-gitlab-global-support-hours)および[サポートの優先地域が選択されている場合のサポート時間への影響](/support/#effect-on-support-hours-if-a-preferred-region-for-support-is-chosen)をご覧ください。
    - question: SaaSはどこにホストされていますか?
      answer: >-
        現在、米国のGoogle Cloud Platformでホストされています。
    - question: GitLab SaaSで利用できない機能は何ですか?
      answer: >-
        一部の機能はSelf-Managed専用で、SaaSには適用されません。[機能ページ](/features/)に最新のリストがあります。
    - question: ストレージと転送の制限はSelf-Managed版に適用されますか？
      answer: >-
        いいえ。Self-Managedインスタンスのストレージと転送の量に制限は適用されません。基盤となるインフラストラクチャのコストは、管理者が負担します。
    - question: ストレージと転送の制限はいつ適用されますか？
      answer: |-
        GitLab SaaSのフリープランでは、[ストレージ管理の改善](https://gitlab.com/gitlab-org/gitlab/-/issues/375296)が利用可能になるまでは、ストレージの制限は適用されません。 これらの制限の適用は段階的に行われる予定です。対象となるユーザーには、ストレージ制限が適用される少なくとも60日前にアプリ内で通知します。詳細は [効率的なGitLab SaaSフリー版に関するFAQ](/pricing/faq-efficient-free-tier/)をご確認ください。

        GitLab SaaSの有料プランを現在ご利用いただいているお客様には、ストレージ制限はまだ適用されません。GitLabは、ストレージ制限が適用される前に、お客様が[ストレージ使用量を表示および管理](https://gitlab.com/gitlab-org/gitlab/-/issues/375296)できるように、今後数か月以内に追加機能をリリースします。

        ストレージ制限はサブスクリプション規約に追加されました。制限を超えている既存のお客様は、更新の少なくとも60日前にアプリ内で通知されます。新規および既存のお客様は、ストレージ制限がトップレベルのグループに適用される前に、新規の条件に同意する必要があります。詳細は [GitLab SaaS有償版のストレージとデータ転送制限に関するFAQ](/pricing/faq-paid-storage-transfer/)で確認できます。

        転送制限は、GitLabから別途通知があるまで適用されません。

    - question: ストレージと転送の制限を超えた場合はどうなりますか?
      answer: |-
        ストレージ制限が適用された後、ストレージ制限を超えるトップレベルグループは読み取りアクセス権を継続して使用できますが、新しいデータを書き込むことはできません。これは、リポジトリ、LFS、パッケージ、レジストリを含むすべてのストレージタイプに適用されます。
    - question: ストレージと転送の使用状況はどこで確認できますか？
      answer: >-
        ストレージの使用状況は、グループの「設定」→「使用量割り当て」で確認できます。 転送の使用量については、後日追加される予定です。
    - question: ストレージ制限には何が含まれますか？
      answer: >-
        ストレージ制限は、トップレベルグループに適用されます。Gitリポジトリ、LFS、パッケージレジストリ、アーティファクトを含む、トップレベルグループとそのサブグループ内のプロジェクトが使用するすべてのストレージが含まれます。
    - question: 転送制限には何が含まれますか？
      answer: |-
        転送とは、GitLab.com SaaSから出力されるデータ量を指します。ただし、次の場合を除きます。

        ** GitLab.com SaaS共有Runner
        ** ウェブインターフェイス
        ** 有料プランのみ：Self-Managed Runnerの転送とデプロイ。これは、CI_JOB_TOKENまたはDEPLOY_TOKENで認証された転送によって決定されます

        転送制限は、コードリポジトリを扱うエンドユーザー、およびコンテナとパッケージの公開配布に適用されます。
        将来的にデータ転送を可能にする機能が追加された場合、そのデータ転送も使用量制限の対象となります。
    - question: 追加のストレージと転送を購入できますか？
      answer: |
        追加のストレージと転送はアドオンとして購入できます。 [ドキュメント](https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#purchase-more-storage)に記載されている手順に従ってください。

          各ストレージと転送アドオンを購入すると、毎月10GiBのストレージと20GiBの転送が利用できます。
          ** 15GiBのストレージが必要な場合は、年間で120ドルを支払う必要があります。
          ** 毎月25GiBの転送が必要な場合は、年間で120ドルを支払います。
          ** 毎月15GiBのストレージと25GiBの転送の両方が必要な場合でも、年間で支払う金額は120ドルです。
          ** 5つのアドオンを購入した場合は、毎月50GiBのストレージと100GiBの転送が利用可能です。
    - question: コンピューティング時間（分）とは何ですか?
      answer: >-
        コンピューティング時間（分）は、共有Runnerでのパイプライ実行中に使用されます。ご自身のRunnerで実行した場合はコンピューティング時間は使用されず、制限はありません。
    - question: コンピューティング時間 (分) をすべて使用した場合はどうなりますか？
      answer: >-
        すべてのコンピューティング時間 (分) を使用した場合は、[コンピューティング時間 (分) の使用を管理](/pricing/faq-consumption-cicd/#managing-your-cicd-minutes-usage)するか、[追加のコンピューティング時間 (分) を購入](https://docs.gitlab.com/ee/subscriptions/gitlab_com/#purchase-additional-compute-minutes)する、もしくはアカウントをPremiumまたはUltimateにアップグレードしてください。また、制限に達した場合でも、ご自身のRunnerはそのまま使用できます。
    - question: コンピューティング分数のクォータはすべてのRunnerに適用されますか?
      answer: >-
        いいえ。共有Runnerのみに制限されています(SaaSのみ)。 [プロジェクトに対する特定のRunner設定](https://docs.gitlab.com/runner/)がある場合は、GitLab SaaSでのビルド時間に制限は適用されません。
    - question: コンピューティング時間 (分) のクォータは、グループ内のユーザー数またはサブスクリプション内のユーザー数に応じて増加しますか？
      answer: >-
        いいえ。クォータは、グループ内やサブスクリプション内のユーザー数に関係なく、グループに適用されます。
    - question: 無料のコンピューティング時間 (分) なのにクレジットカードやデビットカード情報を入力する必要があるのはなぜですか？
      answer: >-
        暗号通貨をマイニングするためにGitLab.com SaaSで利用可能な無料のコンピューティング時間 (分) の不正利用が大幅に増加しています。その結果として断続的なパフォーマンス問題が生じ、GitLab.com SaaSユーザーに影響しています。このような不正利用を防ぐために、GitLab.com SaaSの共有Runnerを使用する場合は、クレジットカードまたはデビットカードの情報をご入力いただく必要があります。ご自身のRunnerを使用する場合や、共有Runnerを無効にする場合は、クレジットカードやデビットカードの詳細は必要ありません。カードを入力すると、1ドルの認証トランザクションで確認されます。 料金はかかりませんし、送金されることもありません。詳細は[こちら](/blog/2021/05/17/prevent-crypto-mining-abuse/){data-ga-name="crypto mining"}{data-ga-location="body"}をご覧ください。
    - question: パブリックプロジェクトでは、コンピューティング分数の制限は異なりますか?
      answer: >-
        はい。2021年7月17日以降に作成されたパブリックプロジェクトには、次のように[コンピューティング分数](https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#ci-pipeline-minutes)が割り当てられます: Freeプラン - 50,000分、Premiumプラン - 1,250,000分、Ultimateプラン - 6,250,000分。
    - question: 自前のGitLab CI Runnerを利用することはできますか?
      answer: >-
        はい。すべてのプランで自前のGitLab CI Runnerをご利用いただけます。
    - question: GitLab.com SaaSのFreeプランの5ユーザー制限とは何ですか?
      answer: 5ユーザー制限は、プライベート表示レベルのトップレベルグループでGitLab.com SaaS版の無料プランのユーザーに対してのみ適用されます。これらの変更は、パブリックトップレベルグループのGitLab.com SaaS版のFreeプランユーザー、有料プラン、Self-Managed版のFreeプラン、および[コミュニティプログラム](https://about.gitlab.com/community/)(GitLab for Open Source、GitLab for Education、GitLab for Startupsのユーザーを含む)には適用されません。詳細は[こちら](https://docs.gitlab.com/ee/user/free_user_limit.html)をご覧ください。
  schema_org: '{"@context": "https://schema.org/","@type": "Product","name": "DevOpsプラットフォーム", "description": "DevOpsプラットフォームは、単一のアプリケーションでソフトウェアの開発、保護、運用を行える機能を兼ね備えているため、プロダクトマネージャーからオペレーション担当者まで、ソフトウェア開発プロセスに関わるすべての人々がシームレスに連携して、ソフトウェアを迅速にリリースすることができます。", "audience": "オペレーション担当者、セキュリティ、テスター、プロダクトマネージャー、プロダクト設計者、経理、法務チーム", "sku": "", "mpn": "", "brand": { "@type": "Corporation", "name": "GitLab","logo": "https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo.png" }, "offers": [{ "@type": "Offer", "name": "DevOpsプラットフォーム - Freeプラン", "Price": "0.00", "priceCurrency": "USD" },{ "@type": "Offer", "name": "DevOpsプラットフォーム - Premiumプラン - 年次請求", "Price": "285.36", "priceCurrency": "USD" },{ "@type": "Offer", "name": "DevOpsプラットフォーム - Ultimateプラン - 年次請求", "Price": "1188.00", "priceCurrency": "USD" } ]}'
  heading: AI搭載のDevSecOpsプラットフォームを利用する
  modal_content:
    header: ご希望のホスティング設定を選択
    saas:
      header: GitLab SaaS
      subtitle: (当社がホスティング)
      experience_notice: 技術的なセットアップは必要ありません。
      description: GitLabをご自分でダウンロードしてインストールしたくない場合。
    self_managed:
      header: GitLab Self-Managed
      subtitle: (お客様がホスティング)
      experience_notice: Linuxの経験が必要です。
      description: お使いのインフラストラクチャまたはパブリッククラウド環境にGitLabをダウンロードしてインストールします。
  free_trial_cta:
    title: ご質問はありますか？私たちにお任せください。
    subtitle: 私たちがお手伝いします。すぐに専門家に相談する。
  feature_comparison:
    header: すべての機能を比較
    rows:
      - text: 1か月あたりのコンピューティング時間（分）
        values:
          free:
            desktop: 400 (コンピューティング時間)
            mobile: "400"
          premium:
            desktop: 10,000 (コンピューティング時間)
            mobile: 10,000
          ultimate:
            desktop: 50,000 (コンピューティング時間)
            mobile: 50,000
      - text: ストレージ
        values:
          free:
            desktop: 5GiB
            mobile: 5GiB
          premium:
            desktop: 50GiB
            mobile: 50GiB
          ultimate:
            desktop: 250GiB
            mobile: 250GiB
      - text: 1か月あたりの転送
        values:
          free:
            desktop: 10GiB
            mobile: 10GiB
          premium:
            desktop: 100GiB
            mobile: 100GiB
          ultimate:
            desktop: 500GiB
            mobile: 500GiB
      - text: オープンソース - MITライセンス
        tier_level: 0
      - text: 自前のGitLab CI/CD Runneの使用
        tier_level: 0
      - text: 無料の静的ウェブサイトの利用
        tier_level: 0
      - text: より速いコードレビュー
        link: https://about.gitlab.com/solutions/code-reviews/
        tier_level: 1
      - text: 高度なCI/CD
        link: https://about.gitlab.com/solutions/continuous-integration/
        tier_level: 1
      - text: エンタープライズアジャイル計画
        link: https://about.gitlab.com/solutions/agile-delivery/
        tier_level: 1
      - text: リリースコントロール
        tier_level: 1
      - text: Self-Managedの信頼性
        tier_level: 1
      - text: サポート
        tier_level: 1
      - text: 高度なセキュリティテスト
        link: https://about.gitlab.com/solutions/security-compliance/
        tier_level: 2
      - text: セキュリティリスクの軽減
        tier_level: 2
      - text: コンプライアンス
        link: https://about.gitlab.com/solutions/compliance/
        tier_level: 2
      - text: ポートフォリオ管理
        link: https://about.gitlab.com/solutions/portfolio-management/
        tier_level: 2
      - text: バリューストリーム管理
        link: https://about.gitlab.com/solutions/value-stream-management/
        tier_level: 2
      - text: 無料のゲストユーザー
        tier_level: 2
  plans_by_type:
    - name: SaaS
      id: saas
      label: Cloud
      description:
        text: 当社がホストしますので、技術的なセットアップは不要です。
      tiers:
        - id: free
          level: 0
          title: Free
          description: 個人ユーザー向けの基本的な機能
          monthly_price: 0
          price_text: "1ユーザー/月"
          billing_type: "クレジットカードは不要"
          button:
            text: 無料トライアルを入手する
            url: https://gitlab.com/-/trial_registrations/new?_gl=1%2A1c025i%2A_ga%2AMTYyOTY3MzIyLjE2ODcyNzM0MTI.%2A_ga_ENFH3X7M5Y%2AMTY4NzI3MzQxMS4xLjAuMTY4NzI3MzQxNC4wLjAuMA
            data_ga_name: sign up
            data_ga_location: saas pricing
          links:
            hosted:
              url: https://gitlab.com/-/trial_registrations/new
              cta_text: SaaSの無料トライアル
            self_managed:
              url: /install/
              cta_text: Self-Managedのインストール
          button_copy: 無料トライアルを入手する
          table_button_copy: 無料トライアルを開始する
          data_ga_name: free-modal
          data_ga_location: pricing-tier
          features_id:
          features:
            header: "Freeの機能: "
            list:
              - text: 5GiBのストレージ
                links:
                  - url: "#do-the-storage-and-transfer-limits-apply-to-self-managed"
                    text: "[1]"
                    data_ga_name: "storage and transfer limits"
                    data_ga_location: body
              - text: 毎月10GiBの転送
                links:
                  - url: "#do-the-storage-and-transfer-limits-apply-to-self-managed"
                    text: "[2]"
                    data_ga_name: "storage and transfer limits"
                    data_ga_location: body
              - text: 毎月400分のコンピューティング時間
                links:
                  - url: "#why-do-i-need-to-enter-credit-debit-card-details-for-free-compute-minutes"
                    text: "[3]"
                    data_ga_name: "why enter credit debit"
                    data_ga_location: body
              - text: トップレベルグループあたり5ユーザー
                links:
                  - url: "#what-is-the-5-user-limit-on-the-gitlab-com-saas-free-tier"
                    text: "[4]"
                    data_ga_name: "what-is-the-5-user-limit-on-the-gitlab-com-saas-free-tier"
                    data_ga_location: body
        - id: silver-premium
          level: 2
          title: Premium
          description: チームの生産性と連携を強化
          pill: "スモールビジネスに最適"
          monthly_price: 29
          annual_price: 348
          price_text: 1ユーザー/月
          billing_type: 年額$348(米ドル)課金
          button:
            text: GitLab Premiumを購入
            url: https://gitlab.com/-/subscriptions/new?plan_id=2c92a00d76f0d5060176f2fb0a5029ff&test=capabilities
            data_ga_name: buy premium
            data_ga_location: saas pricing
          links:
            hosted:
              url: https://gitlab.com/-/subscriptions/new?plan_id=2c92a00d76f0d5060176f2fb0a5029ff&test=capabilities
              cta_text: SaaSの購入
            self_managed:
              url: https://customers.gitlab.com/subscriptions/new?plan_id=2c92a01176f0d50a0176f3043c4d4a53&test=capabilities
              cta_text: Self-Managedの購入
          button_copy: GitLab Premiumの購入
          table_button_copy: Premiumを購入
          data_ga_location: pricing-tier
          data_ga_name: premium-modal
          features_id: gitlab_premium
          features:
            header: "Freeプランのすべての機能に加えて、以下が含まれます:"
            learn_more:
              text: Premiumの詳細
              url: '/pricing/premium/'
              data_ga_location: saas pricing
              data_ga_name: premium learn more
            list:
              - text: コードの所有権と保護ブランチ
                information:
                - text: コードレビューのための承認ルール
                - text: コードレビューでの複数の承認者
                - text: 保護ブランチ
                - text: プッシュルール
                - text: コードレビュー分析
                - text: コード品質レポート
              - text: 承認ルールを使用したマージリクエスト
                information:
                  - text: 必須の承認者
                  - text: マージリクエスト依存関係
                  - text: マージアクセスの制限
                  - text: マージリクエストのレビュー
              - text: エンタープライズアジャイル計画
                information:
                  - text: 単一レベルのエピック
                  - text: ロードマップ
                  - text: 複数の課題担当者
                  - text: イシューのエピックへのプロモート
                  - text: イシューボードのマイルストーンリスト
                  - text: スコープラベル
                  - text: マルチグループイシューボード
              - text: 高度なCI/CD
                information:
                  - text: CI/CDパイプラインダッシュボード
                  - text: 外部リポジトリ用CI/CD
                  - text: マージトレイン
                  - text: 外部テンプレート
              - text: エンタープライズユーザーとインシデント管理
                information:
                  - text: カスタムコンプライアンスフレームワーク
                  - text: LDAP同期構成と複数のLDAPサーバー
                  - text: サービス品質保証契約のカウントダウンタイマーとアクション
                  - text: SLAエスカレーションポリシーとスケジュール管理
                  - text: データベースのロードバランシングとジオログ
              - text: サポート
              - text: 50GiBのストレージ
                links:
                  - url: "#when-are-storage-and-transfer-limits-applicable"
                    text: "[1]"
                    data_ga_name: "storage and transfer limits"
                    data_ga_location: body
              - text: 毎月100GiBの転送
                links:
                  - url: "#when-are-storage-and-transfer-limits-applicable"
                    text: "[2]"
                    data_ga_name: "storage and transfer limits"
                    data_ga_location: body
              - text: 毎月10,000分のコンピューティング時間
        - id: gold-ultimate
          level: 3
          title: Ultimate
          description: 組織全体のセキュリティ、コンプライアンス、およびプランニング機能を完備
          subtitle: より迅速なソフトウェア提供を実現したいエンタープライズ向け
          blurb: 組織全体でのセキュリティ、法令遵守、計画が求められる、ビジネスの中核を担うソフトウェア開発向けのプランです
          pill: "エンタープライズに最適"
          monthly_price: 99
          annual_price: 1188
          price_text: 1ユーザー/月
          billing_type: 年額$1,188(米ドル)課金
          links:
            hosted:
              url: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a&test=capabilities
              cta_text: SaaSを購入
            self_managed:
              url: https://customers.gitlab.com/subscriptions/new?plan_id=2c92a00c76f0c6c20176f2f9328b33c9&test=capabilities
              cta_text: Self-Managedを購入
          button_copy: GitLab Ultimateを購入
          table_button_copy: Ultimateを購入
          data_ga_location: pricing-tier
          data_ga_name: ultimate-modal
          button:
            text: GitLab Ultimateを購入
            url: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff76f0d5250176f2f8c86f305a&test=capabilities
            data_ga_name: buy ultimate
            data_ga_location: saas pricing
          additonal_button:
            url: /sales/
            text: GitLabへのお問い合わせ
            data_ga_name: sales
            data_ga_location: saas pricing
          features_id: gitlab_ultimate
          features:
            header: "Premiumプランのすべての機能に加えて、以下が含まれます:"
            learn_more:
              text: Ultimateの詳細
              data_ga_location: saas pricing
              data_ga_name: ultimate learn more
              url: '/pricing/ultimate/'
            list:
              - text: 推奨のレビュアー
              - text: 動的アプリケーションセキュリティテスト
              - text: セキュリティダッシュボード
                information:
                  - text: プロジェクトとグループ別にすべてのスキャナの傾向を表示
                  - text: セキュリティアラート
              - text: 脆弱性管理
              - text: 依存関係スキャン
              - text: コンテナスキャン
                links:
                  - url: "#features-and-benefits"
                    text: "[1]"
                    data_ga_name: "features and benefits"
                    data_ga_location: body
              - text: 静的アプリケーションセキュリティテスト
                links:
                  - url: "#features-and-benefits"
                    text: "[2]"
                    data_ga_name: "features and benefits"
                    data_ga_location: body
              - text: 複数階層のエピック
              - text: バリューストリーム管理
                information:
                  - text: 監査イベントストリーム
                  - text: カスタマイズ可能なバリューストリームとインサイトレポート
                  - text: DORA -4メトリクス
              - text: 250GiBのストレージ
                links:
                  - url: "#when-are-storage-and-transfer-limits-applicable"
                    text: "[3]"
                    data_ga_name: "storage and transfer limits"
                    data_ga_location: body
              - text: 毎月500GiBの転送
                links:
                  - url: "#when-are-storage-and-transfer-limits-applicable"
                    text: "[4]"
                    data_ga_name: "ストレージと転送の制限"
                    data_ga_location: body
              - text: 毎月50,000分のコンピューティング時間
              - text: フリーゲストユーザー
    - name: Self_Managed
      id: self-managed
      label: Self Managed
      description:
        text: GitLabをホスティングし、ご自身の設定でインストールします。
        tooltip: Linuxの経験が必要です。
      tiers:
        - id: free
          level: 0
          title: Free
          description: 個人ユーザー向けのベーシックな機能
          monthly_price: 0
          price_text: "1ユーザー/月"
          billing_type: "クレジットカードは不要"
          button:
            text: 無料トライアルを入手する
            url: /install/
            data_ga_name: sign up
            data_ga_location: self managed pricing
          features_id:
          features:
            header: "無料の機能:"
            list:
              - text: GitLabのデプロイを完全に制御
              - text: ご自身のストレージとRunnerをご利用ください
        - id: silver-premium
          level: 2
          title: Premium
          description: チームの生産性と連携を強化
          pill: "スモールビジネスに最適"
          monthly_price: 29
          annual_price: 348
          price_text: 1ユーザー/月
          billing_type: 年額$348(米ドル)課金
          button:
            text: GitLab Premiumを購入
            url: https://customers.gitlab.com/subscriptions/new?plan_id=2c92a01176f0d50a0176f3043c4d4a53&test=capabilities
            data_ga_name: buy premium
            data_ga_location: self managed pricing
          features_id: gitlab_premium
          features:
            header: "Freeプランのすべての機能に加えて、以下が含まれます:"
            learn_more:
              text: Premiumの詳細
              url: '/pricing/premium/'
              data_ga_location: self managed pricing
              data_ga_name: premium learn more
            list:
              - text: コードの所有権と保護ブランチ
                information:
                - text: コードレビューのための承認ルール
                - text: コードレビューでの複数の承認者
                - text: 保護ブランチ
                - text: プッシュルール
                - text: コードレビュー分析
                - text: コード品質レポート
              - text: 承認ルールを使用したマージリクエスト
                information:
                  - text: 必須の承認者
                  - text: マージリクエスト依存関係
                  - text: マージアクセスの制限
                  - text: マージリクエストのレビュー
              - text: エンタープライズアジャイル計画
                information:
                  - text: 単一レベルのエピック
                  - text: ロードマップ
                  - text: 複数のイシュー担当者
                  - text: イシューのエピックへのプロモート
                  - text: イシューボードのマイルストーンリスト
                  - text: スコープラベル
                  - text: マルチグループイシューボード
              - text: 高度なCI/CD
                information:
                  - text: CI/CDパイプラインダッシュボード
                  - text: 外部リポジトリ用CI/CD
                  - text: マージトレイン
                  - text: 外部テンプレート
              - text: エンタープライズユーザーとインシデント管理
                information:
                  - text: カスタムコンプライアンスフレームワーク
                  - text: LDAP同期構成と複数のLDAPサーバー
                  - text: サービス品質保証契約のカウントダウンタイマーとアクション
                  - text: SLAエスカレーションポリシーとスケジュール管理
                  - text: データベースのロードバランシングとジオログ
              - text: サポート
        - id: gold-ultimate
          level: 3
          title: Ultimate
          description: 組織全体のセキュリティ、コンプライアンス、およびプランニング機能を完備
          subtitle: より迅速なソフトウェアのデリバリーを実現したいエンタープライズ向け
          blurb: 組織全体でのセキュリティ、法令遵守、計画が求められる、ビジネスの中核を担うソフトウェア開発向けのプランです
          pill: "エンタープライズに最適"
          monthly_price: 99
          annual_price: 1188
          price_text: 1ユーザー/月
          billing_type: 年額$1188(米ドル)課金
          button:
            text: GitLab Ultimateを購入
            url: https://customers.gitlab.com/subscriptions/new?plan_id=2c92a00c76f0c6c20176f2f9328b33c9&test=capabilities
            data_ga_name: buy ultimate
            data_ga_location: self managed pricing
          additonal_button:
            url: /sales/
            text: GitLabへのお問い合わせ
            data_ga_name: sales
            data_ga_location: self managed pricing
          features_id: gitlab_ultimate
          features:
            header: "Premiumプランのすべての機能に加えて、以下が含まれます:"
            learn_more:
              text: Ultimateの詳細
              data_ga_location: self managed pricing
              data_ga_name: ultimate learn more
              url: '/pricing/ultimate/'
            list:
              - text: 推奨のレビュアー
              - text: 動的アプリケーションセキュリティテスト
              - text: セキュリティダッシュボード
                information:
                  - text: プロジェクトとグループ別にすべてのスキャナの傾向を表示
                  - text: セキュリティアラート
              - text: 脆弱性管理
              - text: 依存関係スキャン
              - text: コンテナのスキャン
              - text: 静的アプリケーションセキュリティテスト
              - text: マルチレベルのエピック
              - text: バリューストリーム管理
                information:
                  - text: 監査イベントストリーム
                  - text: カスタマイズ可能なバリューストリームとインサイトレポート
                  - text: DORA -4メトリクス
              - text: 無料ゲストユーザー
    - name: Dedicated
      id: dedicated
      label: Dedicated
      description:
        text: シングルテナント SaaS利用によるフルマネージド
        tooltip: 最低購入数 1,000
      tiers:
        - id: dedicated
          title: GitLab Dedicated
          description: データの分離、保存、保護が必要なエンタープライズ向け
          pill: "エンタープライズ向け"
          features_id: gitlab_dedicated
          features:
            header: "Ultimateプランのすべての機能 ＋"
            button:
              text: Contact us for pricing
              data_ga_location: dedicated pricing
              data_ga_name: dedicated pricing
              url: '/sales/'
            learn_more:
              text: Dedicatedに関して詳しく見る
              data_ga_location: dedicated pricing
              data_ga_name: dedicated learn more
              url: '/dedicated/'
            list:
              - text: GitLabによるフルマネージド
              - text: ご希望のリージョンにデータ保存
              - text: データとソースコードを外部から完全に分離
              - text: 独自の暗号鍵を持ち込み可能
              - text: 高い信頼性と堅牢なセキュリティ
              - text: 定期的なアップグレードによる新機能の利用やパフォーマンスの向上が可能
  dedicated_banner:
    badge_text: New
    eyebrow_headline: 1,000ライセンスを確約
    headline: GitLab Dedicated
    blurb: データの分離、レジデンシー、保護を備えたシングルテナントSaaSソリューション
    button:
      text: 価格についてお問い合わせ
      url: /sales/
      data_ga_name: "価格ついてお問い合わせください"
      data_ga_location: body
    footnote: '* Self-Managedではご利用になれません'
    features:
      header: "Ultimateプランのすべての機能に加えて、以下が含まれます:"
      list:
        - text: GitLabがすべて管理
        - text: 選択した範囲でのデータレジデンシー
        - text: 完全なデータとソースコードの分離
        - text: ご自分のキーを利用した暗号化
        - text: エンタープライズグレードのセキュリティ
        - text: 標準アップグレードケイデンス
  agile_banner:
    badge_text: New
    eyebrow_headline: アジャイルプランニングの50ライセンスを確約
    headline: GitLabエンタープライズ アジャイルプランニング
    blurb: GitLab Ultimateのお客様向けに アジャイルプランニング機能を追加したライセンスをご利用いただけます
    button:
      text: 価格についてお問い合わせ
      url: /sales/
      data_ga_name: "contact us for pricing"
      data_ga_location: body
    features:
      header: "エンタープライズ向けのアジャイルプランニングソリューション："
      list:
        - text: Jiraの代替
        - text: ソフトウェア開発ライフサイクルに関わる全メンバーのための、1つの計画ワークフロー
        - text: ベロシティやインパクトを測定するバリューストリーム分析
        - text: 組織全体を可視化するエグゼクティブダッシュボード
        - text: GitLab Ultimateのお客様向けの、スタンドアロンのエンタープライズアジャイルプランニングライセンス
