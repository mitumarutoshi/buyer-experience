---
  title: 公共部門向けGitLab
  description: ソーシャルコーディング、継続的インテグレーション、リリースの自動化によって、開発が加速し、ソフトウェアの品質が向上して、ミッションの目標を達成できることが証明されています。詳細をご確認ください!
  twitter_image: /nuxt-images/open-graph/gitlab-blog-cover.png
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        title: 公共部門向けGitLab
        subtitle: DevSecOpsプラットフォームによって、ミッションを迅速に達成できます。
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: 無料トライアルを開始
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: ご質問があれば お問い合わせください
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
        image:
          image_url: /nuxt-images/solutions/public-sector.jpg
          image_url_mobile: /nuxt-images/solutions/public-sector.jpg
          alt: "画像: 公共部門向けGitLab"
          bordered: true
    - name: 'by-industry-intro'
      data:
        logos:
          - name: ワシントン大学のロゴ
            image: /nuxt-images/logos/uw-logo.svg
            url: https://about.gitlab.com/customers/uw/
            aria_label: ワシントン大学の顧客事例へのリンク
          - name: ロッキード・マーティンのロゴ
            image: /nuxt-images/logos/lockheed-martin.png
            url: https://about.gitlab.com/customers/lockheed-martin/
            aria_label: ロッキード・マーティンの顧客事例へのリンク
          - name: クック郡のロゴ
            image: /nuxt-images/logos/cookcounty-logo.svg
            url: https://about.gitlab.com/customers/cook-county/
            aria_label: クック郡の顧客事例へのリンク
          - name: サリー大学のロゴ
            image: /nuxt-images/case-study-logos/logo_uni_surrey.svg
            url: https://about.gitlab.com/customers/university-of-surrey/
            aria_label: サリー大学の顧客事例へのリンク
          - name: ビクトリア大学ウェリントンのロゴ
            image: /nuxt-images/logos/victoria-university-wellington-logo.svg
            url: https://about.gitlab.com/customers/victoria_university/
            aria_label: ビクトリア大学ウェリントンの顧客事例へのリンク
    - name: 'side-navigation-variant'
      links:
        - title: 概要
          href: '#overview'
        - title: お客様の声
          href: '#testimonials'
        - title: 機能
          href: '#capabilities'
        - title: メリット
          href: '#benefits'
        - title: 事例
          href: '#case-studies'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-benefits'
              data:
                title: Security. Efficiency. Control.
                image:
                  image_url: "/nuxt-images/solutions/benefits/by-solution-benefits-public-sector.jpeg"
                  alt: 共同作業の画像
                is_accordion: true
                items:
                  - icon:
                      name: devsecops
                      alt: DevSecOpsアイコン
                      variant: marketing
                    header: セキュリティとコンプライアンスのリスクを軽減
                    text: DevSecOpsライフサイクル全体にわたって一貫したガードレールを実施しながら、プロセスの早い段階でセキュリティとコンプライアンスの欠陥を発見します。
                    link_text: DevSecOpsの詳細はこちら
                    link_url: /solutions/security-compliance/
                    ga_name: reduce security learn more
                    ga_location: benefits
                  - icon:
                      name: repo-code
                      alt: コードリポジトリのアイコン
                      variant: marketing
                    header: リソースを解放
                    text: 共同作業とイノベーションの妨げとなる脆弱で複雑なDIYツールチェーンを排除することで、より良い民間のエクスペリエンスを提供し、グローバルな脅威を封じ込めます。
                    link_text: GitLabを選ぶ理由
                    link_url: /why-gitlab/
                    ga_name: free up learn more
                    ga_location: benefits
                  - icon:
                      name: digital-transformation
                      alt: デジタルトランスフォーメーションのアイコン
                      variant: marketing
                    header: 簡素化しながらモダン化
                    text: クラウドネイティブアプリケーションと、それらが依存するインフラストラクチャの固有のニーズを満たすように設計された、DevSecOpsプラットフォームをご利用ください。
                    link_text: GitLabのプラットフォームアプローチの詳細はこちら
                    link_url: /solutions/devops-platform/
        - name: 'div'
          id: 'testimonials'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-quotes-carousel'
              data:
                header: |
                  政府機関に信頼され
                  <br />
                  開発者に愛用される
                quotes:
                  - title_img:
                      url: /nuxt-images/logos/usarmy.svg
                      alt: 米国陸軍
                    quote: 10個のことを教える代わりに、1つのことを学んでもらうだけで済むので、長い目で見ればはるかに負担が軽くなります。
                    author: Chris Apsey氏
                    position: アメリカ陸軍大尉
                    ga_carousel: public-sector us army
                  - title_img:
                      url: /nuxt-images/logos/uw-logo.svg
                      alt: ワシントン大学のロゴ
                    quote: この2年間で、GitLabはここワシントン大学の組織に変革をもたらしました。GitLabのプラットフォームは素晴らしいです!
                    author: Aaron Timss氏
                    position: CSE、情報ディレクター
                    ga_carousel: public-sector university of washington
                  - title_img:
                      url: /nuxt-images/logos/cookcounty-logo.svg
                      alt: クック郡のロゴ
                    quote: GitLabを使用すると、リサーチを作成して、オフィスでみんなに見せることができます。リサーチの変更を提案された場合、バージョン管理を心配したり作業を保存したりすることなく、これらの変更を行うことができます。単一のリポジトリのおかげで、実際の作業にもっと集中できるようになり、仕事の仕組みについてあまり考えなくて済むようになりました。
                    author: Robert Ross氏
                    position: 最高データ責任者
                    ga_carousel: public-sector cook county
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-solutions-block'
              data:
                subtitle: 公共部門向けの最も包括的なDevSecOpsプラットフォーム
                sub_description: '安全で堅牢なソースコード管理(SCM)、継続的インテグレーション(CI)、継続的デリバリー(CD)、継続的なソフトウェアセキュリティとコンプライアンスを含むDevSecOpsプラットフォームから始まり、GitLabは次のような独自のニーズにも対応します。'
                white_bg: true
                sub_image: /nuxt-images/solutions/public-sector/showcase-pubsec.svg
                alt: メリットの画像
                solutions:
                  - title: SBOM
                    description: 既知の脆弱性など、使用されている依存関係の主な詳細について、プロジェクトのソフトウェア部品表を確認しましょう。
                    icon:
                      name: less-risk
                      alt: 低リスクアイコン
                      variant: marketing
                    link_text: 詳細はこちら
                    link_url: https://docs.gitlab.com/ee/user/application_security/dependency_list/
                    data_ga_name: SBOM
                    data_ga_location: solutions block
                  - title: ゼロトラスト
                    description: GitLabがどのようにゼロトラストの原則を守り、ベストプラクティスを実証しているかをご覧ください。
                    icon:
                      name: monitor-pipeline
                      alt: モニターとパイプラインのアイコン
                      variant: marketing
                    link_text: 詳細はこちら
                    link_url: /handbook/security/
                    data_ga_name: zero trust
                    data_ga_location: solutions block
                  - title: 脆弱性管理
                    description: ソフトウェアの脆弱性をパイプライン内、プロジェクト内、プロジェクトグループ内、およびグループ間ですべて1か所で管理できます。
                    icon:
                      name: shield-check
                      alt: チェックマーク付きの盾アイコン
                      variant: marketing
                    link_text: 詳細はこちら
                    link_url: https://docs.gitlab.com/ee/user/application_security/vulnerability_report/
                    data_ga_name: vulnerability management
                    data_ga_location: solutions block
                  - title: ファジングテスト
                    description: GitLabを使用すると、一連の包括的なスキャナーと一緒に、パイプラインにファジングテストを追加できます。ファジングテストは、予期しない動作が引き起こされるよう、アプリケーションに実装されたバージョンにランダムな入力を送信します。この動作によって、対処すべきセキュリティとロジック上の欠陥を明らかにします。
                    icon:
                      name: monitor-test
                      alt: モニターとテストのアイコン
                      variant: marketing
                    link_text: 詳細はこちら
                    link_url: https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/
                    data_ga_name: fuzz testing
                    data_ga_location: solutions block
                  - title: オフライン環境
                    description: インターネットから切断されている場合でも、ほとんどのGitLabセキュリティスキャナーを実行できます。
                    icon:
                      name: gitlab-monitor-alt
                      alt: GitLabロゴが表示されたモニターのアイコン
                      variant: marketing
                    link_text: 詳細はこちら
                    link_url: https://docs.gitlab.com/ee/user/application_security/offline_deployments/
                    data_ga_name: offline environment
                    data_ga_location: solutions block
                  - title: コンプライアンスのための一般的な制御
                    description: 職務分掌、保護ブランチ、プッシュルールなどの一般的なポリシーを自動化して実施します。
                    icon:
                      name: shield-check
                      alt: チェックマーク付きの盾アイコン
                      variant: marketing
                    link_text: 詳細はこちら
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html
                    data_ga_name: common controls for compliance
                    data_ga_location: solutions block
                  - title: コンプライアンスパイプライン
                    description: 必要なセキュリティスキャンが回避されないように、パイプラインスキャンの設定を行います。
                    icon:
                      name: shield-check
                      alt: チェックマーク付きの盾アイコン
                      variant: marketing
                    link_text: 詳細はこちら
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html
                    data_ga_name: compliance
                    data_ga_location: solutions block
                  - title: 上流工程から下流工程まで
                    description: さまざまな開発チーム間の共同作業を可能にします。
                    icon:
                      name: cog-code
                      alt: 歯車とコードのアイコン
                      variant: marketing
                    link_text: 詳細はこちら
                    link_url: https://www.youtube.com/watch?v=HSfDTslLRT8/
                    data_ga_name: low to high
                    data_ga_location: solutions block
                  - title: オンプレミス、セルフホスト、またはSaaS
                    description: GitLabはすべての環境で動作します。何を選ぶかはあなた次第です。
                    icon:
                      name: gitlab-monitor-alt
                      alt: GitLabロゴが表示されたモニターのアイコン
                      variant: marketing
                    link_text: 詳細はこちら
                    link_url: /pricing/
                    data_ga_name: on-prem self-hosted or saas
                    data_ga_location: solutions block
                  - title: 強化されたコンテナイメージ
                    description: DoDに準拠して強化されたコンテナイメージによって、リスクプロファイルを最小化し、より安全なアプリケーションを迅速にデプロイでき、継続的なプロセスの運用権限(ATO)をサポートします。また、Iron Bankでも受け入れられます。
                    icon:
                      name: lock-cog
                      alt: 鍵と歯車のアイコン
                      variant: marketing
                    link_text: 詳細はこちら
                    link_url: /press/releases/2020-07-01-gitlab-announces-hardened-container-image-in-support-of-the-us-department-of-defense-enterprise-devsecops-initiative/
                    data_ga_name: hardened container image
                    data_ga_location: solutions block
        - name: 'div'
          id: 'benefits'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-value-prop'
              data:
                title: 公共部門固有のニーズに対応
                cards:
                  - title: NISTの安全なソフトウェア開発フレームワーク(SSDF)
                    description: GitLabはNISTのガイダンスに沿っており、CIOがソフトウェアサプライチェーンのセキュリティに必要なアクションを実装して、その機関を積極的に守れるよう支援します。どのように<a href="/blog/2022/03/29/comply-with-nist-secure-supply-chain-framework-with-gitlab/" data-ga-name="gitlab meets nist" data-ga-location="value prop">GitLabがNIST SSDF 1.1のガイダンスに準拠しているかについて詳しくはこちら</a>をご覧ください。
                    icon:
                      name: less-risk
                      alt: 低リスクアイコン
                      variant: marketing
                  - title: DI2Eの代替手段
                    description: DI2E (防衛インテリジェンス情報エンタープライズ)へのアクセスが取り消され、各機関はDevSecOpsモデル全体を再考することを余儀なくされています。GitLabはDI2Eの確実な代替手段であり、単一のアプリケーションであるため、調達を簡素化します。
                    icon:
                      name: devsecops
                      alt: DevSecOpsアイコン
                      variant: marketing
                  - title: サプライチェーンの可視化と制御
                    description: GitLabのDevSecOpsプラットフォームは、エンドツーエンドの可視性とトレーサビリティを簡素化する、単一の<a href="/press/releases/2020-07-01-gitlab-announces-hardened-container-image-in-support-of-the-us-department-of-defense-enterprise-devsecops-initiative/" data-ga-name="hardened" data-ga-location="value prop">強化された</a>アプリケーションとして提供されます。セキュリティおよびコンプライアンスポリシーは、すべてのDevSecOpsプロセスで一貫して管理され、実施されます。
                    icon:
                      name: eye-magnifying-glass
                      alt: 虫眼鏡アイコン
                      variant: marketing
                  - title: オンプレミス、セルフホスト、またはSaaS
                    description: 何を選ぶかはあなた次第です。
                    icon:
                      name: monitor-web-app
                      alt: モニターとウェブアプリのアイコン
                      variant: marketing
        - name: 'div'
          id: 'case-studies'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-case-studies'
              data:
                title: お客様が実感しているメリット
                link:
                  text: すべてのケーススタディ
                  link: /customers/
                rows:
                  - title: 米国陸軍サイバースクール
                    subtitle: 米国陸軍サイバースクールがGitLabで「コードとしてのコースウェア」を作成した方法
                    image:
                      url: /nuxt-images/blogimages/us-army-cyber-school.jpeg
                      alt: 無線機で話す兵士
                    button:
                      href: /customers/us_army_cyber_school/
                      text: 詳細はこちら
                      data_ga_name: us army learn more
                      data_ga_location: case studies
                  - title: クック郡の査定員事務所
                    subtitle: シカゴ州クック郡が透明性を実現し、バージョン管理を用いて経済データを評価した方法
                    image:
                      url: /nuxt-images/blogimages/cookcounty.jpg
                      alt: 上空から見た家
                    button:
                      href: /customers/cook-county/
                      text: 詳細はこちら
                      data_ga_name: cook country learn more
                      data_ga_location: case studies
                  - title: ワシントン大学
                    subtitle: Paul G. Allen Center for Computer Science & Engineeringは、10,000件を超えるプロジェクトを容易に管理できる制御性と柔軟性を確保
                    image:
                      url: /nuxt-images/blogimages/uw-case-study-image.png
                      alt: ワシントン大学キャンパス
                    button:
                      href: /customers/uw/
                      text: 詳細はこちら
                      data_ga_name: uw learn more
                      data_ga_location: case studies
    - name: 'by-solution-link'
      data:
        title: 'GitLabイベント'
        description: "イベントに参加して、チームがセキュリティとコンプライアンスを強化しながら、ソフトウェアを迅速かつ効率的に提供する方法を学びましょう。2022年は多数のイベントに参加するほか、主催および運営予定です。皆さまのご参加をお待ちしています!"
        link: /events/
        image: /nuxt-images/events/google-cloud-next/pubsec-event-link.jpeg
        alt: プレゼンテーション中の聴衆
        icon: calendar-alt-2