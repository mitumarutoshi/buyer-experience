---
  title: スタートアップ向けGitLabプログラムに参加する
  description: 今すぐ申し込んで、スタートアップ向けGitLabプログラムにご参加ください。GitLabチームからGitLabの開始方法についてご案内します。
  image_alt: スタートアップ向けGitLab
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        note:
          - スタートアップ向けGitLabプログラム
        title: Software. Faster.
        subtitle: サイクルタイムを短縮し、より少ない労力でデプロイの頻度を増やせます。ソフトウェア開発ライフサイクル全体にわたって単一のアプリケーションを使用することで、チームはより優れたソフトウェアを迅速に出荷することに専念できます。
        aos_animation: fade-down
        aos_duration: 500
        title_variant: 'display1'
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          url: '#application-form'
          text: 今すぐ申し込む
          icon:
            name: arrow-down
            variant: product
        image:
          image_url: /nuxt-images/solutions/startups_join.jpeg
          alt: "画像：オープンソース団体向けGitLab"
          bordered: true
          position: left
    - name: 'startups-intro'
      data:
        title: "ビジネスを加速させるよう構築されたプログラムにより さらに多くのスタートアップにご利用いただけるようになりました。"
        subtitle: |

          スタートアップは、より少ないコストでより多くのことを成し遂げながら、指数関数的成長を遂げる必要があります。GitLabを使用すれば、革新的な製品をビルドして新しい顧客にアプローチし、収益目標を達成できます。

          **対象となる\*スタートアップは、GitLab Ultimateを1年間無料で利用できます。**

    - name: 'education-case-study-carousel'
      data:
        customers_cta: true
        cta:
          href: '/customers/'
          text: その他の事例を表示
          data_ga_name: customers
        logo_in_card: true
        case_studies:
          - logo_url: "/nuxt-images/logos/chorus-color.svg"
            institution_name: ビクトリア大学ウェリントン（テヘレンガワカ校）
            img_url: /nuxt-images/blogimages/Chorus_case_study.png
            quote:
              quote_text: GitLabを使用すれば、プロダクトエンジニアの作業が楽になり、プロダクトエンジニアと関わる人も簡単にやり取りできるようになります
              author: Russell Levy
              author_title: Chorus.ai社共同創設者兼CTO
            case_study_url: /customers/chorus/
            data_ga_name: Victoria University of Wellington case study
            data_ga_location: case study carousel

    - name: startups-overview
      data:
        header: スタートアップ向けGitLabプログラムの対象者
        description: スタートアップ向けGitLabのプランは、企業の成長ステージに合わせてカスタマイズされています。
        blocks:
          - header: シードステージのスタートアップ
            offers:
              - list:
                - |
                  プレシードまたはシードステージ。最大
                  500万ドルの資金調達。
                - 資金は外部から調達されている必要があります。
                - 新規のお客様のみ。
              - title: "1年目には、以下をご利用いただけます。"
                list:
                  - 最大20シートの無料Ultimateライセンス（SaaSまたはSelf-Managed）。
                  - サポートは含まれていませんが、ご心配なく。GitLabは使いやすさを重視して設計されています。
              - title: "2年目には、以下をご利用いただけます。"
                list:
                  - すべてのプランで50%割引。最大20人のユーザー。
          - header: アーリーステージのスタートアップ
            offers:
              - list:
                - |
                  現在、シリーズAまたはシリーズB。最大
                  2,000万ドルの資金調達。
                - 資金は外部から調達されている必要があります。
                - 新規のお客様のみ。
              - title: "1年目には、以下をご利用いただけます。"
                list:
                  - すべてのプランで50%割引。最大20人のユーザー。
              - title: "2年目には、以下をご利用いただけます。"
                list:
                  - すべてのプランで25%割引。最大20人のユーザー。
        information:
          header: その他の要件と情報
          list:
            - 申請後、資格を確認するために追加情報の提供をお願いする場合があります。
            - スタートアップ向けGitLabプログラムに承認されると、すべてのプログラムメンバーは[GitLabサブスクリプション契約](/handbook/legal/subscription-agreement/)の対象となります。
            - セルフホストインスタンスの場合、ユーザーは使用状況と分析機能を有効にする必要があります。
            - スタートアップ向けGitLabプログラムは、GitLabの独自の裁量で提供され、変更される可能性があります。
            - ご質問があれば、[startups@gitlab.com](mailto:startups@gitlab.com)までお問い合わせください。
    - name: 'open-source-form-section'
      data:
        id: application-form
        title: お申し込みフォーム
        blocks:
          - content: |
              今すぐ申し込んで、スタートアップ向けGitLabプログラムにご参加ください。GitLabチームから、GitLabの開始方法についてご案内します。GitLabを活用してどのような革新的な製品を構築されるのかを楽しみにしています。
        form:
          required_text: すべてのフィールドを入力してください
          formId: 4012
          form_header: ''
          datalayer: startups
    - name: 'startups-link'
      data:
        header: スタートアップ向けGitLabに参加する理由
        description: シード資金調達からプラスのキャッシュフロー、大規模な多国籍企業への成長まで、どのような目標をお持ちでも、GitLabはあなたのビジネスの成長に合わせて一緒に成長していきます。
        button:
          href: /solutions/startups
          text: 詳細はこちら
        image: /nuxt-images/solutions/startups_hero.jpeg
        alt: プレゼンテーション時の群衆
        icon: calendar-alt-2
    - name: faq
      data:
        header: よくある質問
        show_all_button: true
        background: true
        texts:
          show: すべて展開
          hide: すべて折りたたむ
        groups:
        - header: ""
          questions:
            - question: 自己資金によるスタートアップの場合、申請できますか？
              answer: >-
                このプログラムは現在、所定の要件を満たし、外部から資金提供を受けた企業のみご利用いただけます。次のイテレーションでは、他のポートフォリオの評価を検討します。
            - question: 外部資金はどのように検証されますか？
              answer: >-
                適格性を判断するために、GitLabがお申し込み時のデータを検証します。申請者は、資金調達状況を証明するために、Crunchbase、Pitchbookのプロフィール、財務諸表などの合理的な証拠を提供する必要があります。
            - question: 1年後はどうなりますか？
              answer: |
                2年目にご利用いただける価格設定については、資金調達パラメータにおける御社の位置をご参照ください。チームメンバーから事前に連絡し、準備のためのガイダンスを提供し、準備時間を設けます。
            - question: 誰がサブスクリプションにカウントされますか？
              answer: >-
                詳しくは、[ライセンスとサブスクリプションに関するよくある質問](/pricing/licensing-faq/#who-gets-counted-in-the-subscription)をご覧ください。
            - question: 使用開始後にシート数を増やせますか？
              answer: >-
                既存のライセンスのシート数を増やしたい場合は、[startups@gitlab.com](mailto: startups@gitlab.com)までメールでお問い合わせください。追加シート用のアドオンの見積もりをご用意します。