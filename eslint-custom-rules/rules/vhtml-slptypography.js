/* eslint-disable */
module.exports = {
  create(context) {
    const invalidTags = ['p', 'h1', 'h2', 'h3', 'h4', 'h5'];
    return context.parserServices.defineTemplateBodyVisitor({
      "VAttribute[directive=true][key.name.name='html']"(node) {
        if (node.parent.parent.name === 'slptypography') {
          node.key.parent.parent.attributes.forEach((attribute) => {
            if (
              attribute.value.parent.key.name == 'tag' &&
              invalidTags.includes(attribute.value.value)
            ) {
              context.report({
                node: node,
                message: 'Do not render Markdown in a SlpTypography component.',
              });
            }
            return;
          });
        }
        return;
      },
    });
  },
};
