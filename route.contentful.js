// File used to generate routes for content files living in Contentful
// This is done to generate those routes during yarn generate
import { createClient } from 'contentful';
import { CONTENT_TYPES } from './common/content-types';
import { getClient } from './plugins/contentful';

const client = createClient({
  space: process.env.CTF_SPACE_ID,
  accessToken: process.env.CTF_CDA_ACCESS_TOKEN,
  host: 'cdn.contentful.com',
  retryLimit: 1000,
  logHandler: (level, data) => {
    if (data.includes('Rate limit')) {
      return;
    }
  },
});

export async function fetchPages() {
  try {
    let totalPages = 0;
    let pages = [];
    do {
      const response = await client.withAllLocales.getEntries({
        content_type: CONTENT_TYPES.PAGE,
        select: 'fields.slug, fields.description, fields.pageContent',
        limit: 1000,
      });
      totalPages = response.total;

      const filteredResponse = response.items.filter(
        (page) => page.fields.pageContent && page.fields.pageContent['en-US'],
      );

      const baseSlugs = filteredResponse.map(
        (page) => `/${page.fields.slug['en-US']}`,
      );

      const localizedSlugs = [];
      filteredResponse.forEach((page) => {
        const locales = Object.keys(page.fields.description);
        locales.forEach((locale) => {
          if (locale !== 'en-US' && page.fields.slug['en-US'] !== '/') {
            localizedSlugs.push(
              `/${locale.toLowerCase()}/${page.fields.slug['en-US']}`,
            );
          }
        });
      });

      pages = [...pages, ...baseSlugs, ...localizedSlugs];
    } while (pages.length < totalPages);
    const result = pages.map((page) => page.replace('//', '/'));
    return result;
  } catch (error) {
    // eslint-disable-next-line
    console.error('Error fetching page routes from Contentful:', error);
    throw error;
  }
}

export async function fetchCustomerRoutes() {
  try {
    const caseStudies = await client.getEntries({
      content_type: CONTENT_TYPES.CASE_STUDY,
      select: 'fields.slug',
      limit: 1000,
    });

    return caseStudies.items.map(
      (caseStudy) => `/customers/${caseStudy.fields.slug}`,
    );
  } catch (error) {
    // eslint-disable-next-line
    console.error('Error fetching customer routes from Contentful:', error);
    throw error;
  }
}

export async function fetchTopicsRoutes() {
  try {
    const topics = await client.getEntries({
      content_type: CONTENT_TYPES.TOPICS,
      select: 'fields.slug',
      limit: 1000,
    });

    return topics.items.map((topic) => `${topic.fields.slug}`);
  } catch (error) {
    // eslint-disable-next-line
    console.error('Error fetching topics routes from Contentful:', error);
    throw error;
  }
}

function getCustomPages(limit, skip) {
  return getClient().getEntries({
    content_type: CONTENT_TYPES.CUSTOM_PAGE,
    select: 'fields.slug',
    limit,
    skip,
  });
}

export async function fetchCustomRoutes() {
  try {
    let skip = 0;
    const limit = 100;
    const { items, total } = await getCustomPages(limit, skip);
    let allEntries = [...items];

    if (total > 0) {
      while (skip < total) {
        skip += limit;
        const { items: batchEntries } = await getCustomPages(limit, skip);
        allEntries = [...allEntries, ...batchEntries];
      }

      // eslint-disable-next-line
      console.log(
        `Custom page routes fetching completed: ${allEntries.length} routes have been generated`,
      );

      return allEntries.map((entry) => `${entry.fields.slug}`);
    }
    return [];
  } catch (error) {
    // eslint-disable-next-line
    console.error('Error fetching custom pages routes from Contentful:', error);
    throw error;
  }
}

export async function fetchEventRoutes() {
  try {
    const eventPages = await client.getEntries({
      content_type: CONTENT_TYPES.EVENT,
      select: ['fields.slug', 'fields.eventType'],
      limit: 1000,
    });

    return eventPages.items
      .filter((event) => event.fields && event.fields.slug)
      .map(({ fields }) => {
        const { eventType, slug } = fields;
        let route = `/events/${fields.slug}`;

        if (
          eventType === 'World Tour' ||
          eventType === 'World Tour Executive'
        ) {
          route = slug.includes('executive')
            ? `/events/devsecops-world-tour${fields.slug}`
            : `/events/devsecops-world-tour/${fields.slug}`;
        }

        return route;
      });
  } catch (error) {
    // eslint-disable-next-line
    console.error('Error fetching event routes from Contentful:', error);
    throw error;
  }
}

export async function fetchMicrositeRoutes() {
  try {
    const microsites = await client.getEntries({
      content_type: CONTENT_TYPES.MICROSITE,
      select: 'fields.slug',
    });

    return microsites.items.map((site) => `/partner/${site.fields.slug}`);
  } catch (error) {
    // eslint-disable-next-line
    console.error('Error fetching microsite routes from Contentful:', error);
    throw error;
  }
}

export default [
  '/analysts',
  '/customers/',
  '/company/',
  '/company/all-remote/',
  '/company/contact/',
  '/company/preference-center/',
  '/company/visiting/',
  // '/community/beta/',
  '/dedicated/',
  '/environmental-social-governance/',
  '/free-trial/',
  '/free-trial/devsecops/',
  '/gartner-magic-quadrant/',
  '/get-started/continuous-integration/',
  '/get-started/build-business-case/',
  '/get-started/enterprise/',
  '/get-started/small-business/',
  '/install/ce-or-ee/',
  '/security/',
  '/solutions/',
  '/solutions/slack/',
  '/solutions/kubernetes/',
  '/solutions/github/',
  '/solutions/jenkins/',
  '/solutions/jira/',
  '/solutions/faster-software-delivery/',
  '/solutions/analytics-and-insights/',
  '/solutions/cloud-native/',
  '/solutions/code-suggestions/',
  '/solutions/education/',
  '/solutions/education/edu-survey/',
  '/solutions/education/join/',
  '/solutions/gitlab-duo-pro/sales/',
  '/solutions/devops-platform/',
  '/solutions/digital-transformation/',
  '/topics/',
  '/support',
  '/support/us-government-support',
  '/support/statement-of-support',
  '/support/providing-large-files',
  '/support/general-policies',
  '/support/customer-satisfaction',
  '/support/definitions',
  '/support/scheduling-upgrade-assistance',
  '/support/enhanced-support-offerings',
  '/support/sensitive-information',
  '/support/managing-support-contacts',
  '/support/portal',
  '/support/gitlab-com-policies',
  '/jobs/all-jobs',
];
