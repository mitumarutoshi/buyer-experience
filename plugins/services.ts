import { Context } from '@nuxt/types';
import { AiTransparencyCenterService } from '~/services/ai-transparency-center.service';
import { GitlabTechnicalDemoSeriesService } from '../services/gitlab-technical-demo-series.service';
import {
  GetStartedService,
  DevSecOpsService,
  DedicatedService,
  SolutionsService,
  FreeTrialService,
  SolutionsEducationService,
  SecurityService,
  InstallService,
  ContentfulService,
  IntegrationsService,
  EnvironmentalSocialGovernanceService,
  EventsService,
  SummitEventService,
  GartnerMagicQuadrantService,
  WorldTourEventsService,
  PricingService,
  CustomerSuccessManagementService,
} from '../services';

export default ($ctx: Context, inject: any) => {
  const devSecOpsService = new DevSecOpsService($ctx);
  const getStartedService = new GetStartedService($ctx);
  const dedicatedService = new DedicatedService($ctx);
  const freeTrialService = new FreeTrialService($ctx);
  const securityService = new SecurityService($ctx);
  const solutionsService = new SolutionsService($ctx);
  const solutionsEducationService = new SolutionsEducationService($ctx);
  const installService = new InstallService($ctx);
  const contentfulService = new ContentfulService($ctx);
  const integrationsService = new IntegrationsService($ctx);
  const environmentalSocialGovernanceService =
    new EnvironmentalSocialGovernanceService($ctx);
  const eventsService = new EventsService($ctx);
  const summitEventService = new SummitEventService($ctx);
  const gartnerMagicQuadrant = new GartnerMagicQuadrantService($ctx);
  const worldTourEventsService = new WorldTourEventsService($ctx);
  const gitlabTechnicalDemoSeriesService = new GitlabTechnicalDemoSeriesService(
    $ctx,
  );
  const customerSuccessManagementService = new CustomerSuccessManagementService(
    $ctx,
  );
  const aiTransparencyCenterService = new AiTransparencyCenterService($ctx);
  const pricingService = new PricingService($ctx);

  inject('devSecOpsService', devSecOpsService);
  inject('getStartedService', getStartedService);
  inject('dedicatedService', dedicatedService);
  inject('freeTrialService', freeTrialService);
  inject('securityService', securityService);
  inject('solutionsService', solutionsService);
  inject('solutionsEducationService', solutionsEducationService);
  inject('installService', installService);
  inject('contentfulService', contentfulService);
  inject('integrationsService', integrationsService);
  inject(
    'environmentalSocialGovernanceService',
    environmentalSocialGovernanceService,
  );
  inject('eventsService', eventsService);
  inject('summitEventService', summitEventService);
  inject('gartnerMagicQuadrantService', gartnerMagicQuadrant);
  inject('worldTourEventsService', worldTourEventsService);
  inject('gitlabTechnicalDemoSeriesService', gitlabTechnicalDemoSeriesService);
  inject('aiTransparencyCenterService', aiTransparencyCenterService);
  inject('pricingService', pricingService);
  inject('customerSuccessManagementService', customerSuccessManagementService);
};
