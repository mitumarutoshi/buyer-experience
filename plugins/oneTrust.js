// OptanonWrapper is the OneTrust callback function,
// which fires when OneTrust initializes.

window.OptanonWrapper = function () {
  const getBody = document.querySelector('body');
  const getBanner = document.querySelector('#onetrust-banner-sdk');
  const bannerAcceptBtn = document.querySelector(
    '#onetrust-accept-btn-handler',
  );
  const bannerRejectBtn = document.querySelector(
    '#onetrust-reject-all-handler',
  );
  const pcAllowAllBtn = document.querySelector(
    '#accept-recommended-btn-handler',
  );
  const pcSaveBtn = document.querySelector('.save-preference-btn-handler');

  function removeClassFromBody() {
    getBody.classList.remove('noScroll');
  }

  if (getBanner) {
    getBody.classList.add('noScroll');
    if (bannerAcceptBtn) {
      bannerAcceptBtn.addEventListener('click', () => removeClassFromBody());
    }
    if (bannerRejectBtn) {
      bannerRejectBtn.addEventListener('click', () => removeClassFromBody());
    }
    if (pcAllowAllBtn) {
      pcAllowAllBtn.addEventListener('click', () => removeClassFromBody());
    }
    if (pcSaveBtn) {
      pcSaveBtn.addEventListener('click', () => removeClassFromBody());
    }
  }
};
