import Vue from 'vue';
import { COMPONENT_NAMES, USER_SEGMENTS } from '../common/constants';

export const UserSegmentsMixin = Vue.extend({
  data() {
    return {
      segmentCustomersIndex: 0,
      segmentCasesIndex: 0,
      employeeCount: 0,
    };
  },
  computed: {
    getSegmentFromQueryParms() {
      const segment = this.$route.query.segment;
      if (!segment) return USER_SEGMENTS.DEFAULT;

      // Look for passed segments in list of valid segments
      const isValidSegment = Object.values(USER_SEGMENTS).includes(segment);

      // Return the default segment in case an invalid segment was passed
      if (!isValidSegment) {
        console.error(new Error(`Invalid segment: ${segment}`));
        return USER_SEGMENTS.DEFAULT;
      }

      return segment;
    },
  },
  beforeMount() {
    this.getData();
  },
  methods: {
    async getData() {
      const response = await fetch(
        'https://epsilon.6sense.com/v3/company/details',
        {
          method: 'GET',
          headers: {
            Authorization: `Token 715ff02645f8e09a174845255d544dac8c178c02`,
          },
        },
      );

      const segment = await response.json();

      this.employeeCount = Number(segment.company?.employee_count);

      return Promise.resolve(this.employeeCount);
    },
    defineSegment() {
      // If there is a segment `s` queryParam, we return the given segment
      const querySegment = this.getSegmentFromQueryParms;

      if (querySegment !== USER_SEGMENTS.DEFAULT) {
        return querySegment;
      }

      // If no queryParam avaliable, we compute `6sense` employeeCount for determing segment
      if (this.employeeCount < 100) {
        return USER_SEGMENTS.SMALL_BUSINESS;
      }

      if (this.employeeCount >= 100 && this.employeeCount < 1000) {
        return USER_SEGMENTS.MEDIUM;
      }

      if (this.employeeCount >= 1000) {
        return USER_SEGMENTS.ENTERPRISE;
      }

      // If no employeeCount is given, we return the default segment
      return USER_SEGMENTS.DEFAULT;
    },
    setSegment(data: Array<any>, component: string) {
      // For this to work, data has to be an array of objects,
      // and each object must contain an attribute `segment` { segment: 'USER_SEGMENTS' }

      if (component === COMPONENT_NAMES.CUSTOMER_LOGOS) {
        // Segment CustomersLogo component
        this.segmentCustomersIndex = data?.findIndex(
          (value) => value.segment == this.defineSegment(),
        );
        return;
      }

      if (component === COMPONENT_NAMES.TABBED_CASE_STUDIES) {
        // Segment CaseStudies component
        this.segmentCasesIndex = data?.findIndex(
          (value) => value.segment == this.defineSegment(),
        );
        return;
      }

      // ... Add more components for segmentation here if needed
    },
  },
});
