import Vue from 'vue';
import { init, refresh } from 'aos';

export const AosAnimationsMixin = Vue.extend({
  beforeMount() {
    init({
      once: true,
    });
  },
  mounted() {
    refresh();
  },
});
