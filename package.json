{
  "name": "gitlab-core-marketing-site",
  "version": "3.10.0",
  "private": true,
  "author": "GitLab",
  "scripts": {
    "dev": "nuxt dev",
    "build": "nuxt build",
    "start": "nuxt start",
    "generate": "nuxt generate --fail-on-error",
    "lighthouse": "lhci",
    "lint:js": "eslint --ext \".js,.vue\" --ignore-path .gitignore .",
    "lint:prettier": "prettier --check .",
    "lint": "yarn lint:js && yarn lint:prettier",
    "lintfix": "prettier --write --list-different . && yarn lint:js --fix",
    "postinstall": "husky install && yarn sync-data",
    "test": "jest",
    "sync-data": "./scripts/sync-data.sh",
    "find-broken-links": "node ./scripts/find-broken-links.mjs",
    "get-week": "node ./scripts/get-week.mjs"
  },
  "dependencies": {
    "@braid/vue-formulate": "^2.5.2",
    "@contentful/live-preview": "^2.11.13",
    "@nuxt/content": "^1.15.1",
    "@nuxtjs/component-cache": "^1.1.6",
    "@nuxtjs/dotenv": "^1.4.2",
    "@nuxtjs/gtm": "^2.4.0",
    "@nuxtjs/i18n": "^7.3.1",
    "@nuxtjs/markdownit": "^2.0.0",
    "@nuxtjs/sitemap": "^2.4.0",
    "@semantic-release/changelog": "^6.0.1",
    "@semantic-release/commit-analyzer": "^9.0.2",
    "@semantic-release/git": "^10.0.1",
    "@semantic-release/gitlab": "^9.5.0",
    "@semantic-release/npm": "^9.0.1",
    "@semantic-release/release-notes-generator": "^10.0.3",
    "@typeform/embed": "^2.12.0",
    "aos": "^2.3.4",
    "be-navigation": "6.4.1",
    "contentful": "^10.4.2",
    "contentful-management": "^11.6.1",
    "conventional-changelog-conventionalcommits": "^5.0.0",
    "core-js": "^3.15.1",
    "floating-vue": "^1.0.0-beta.14",
    "ipx": "^0.9.10",
    "js-yaml": "^4.1.0",
    "launchdarkly-js-client-sdk": "^3.1.4",
    "lodash": "^4.17.21",
    "markdown-it-anchor": "5.0.1",
    "markdown-it-attrs": "^2.3.2",
    "markdown-it-multimd-table": "^4.1.3",
    "nuxt": "^2.16.3",
    "semantic-release": "^19.0.5",
    "slippers-ui": "2.1.55",
    "vue-autosuggest": "^2.2.0",
    "vue-mermaid-string": "^2.2.5",
    "vue-pluralize": "^0.0.2",
    "vue-slick-carousel": "^1.0.6"
  },
  "devDependencies": {
    "@lhci/cli": "0.11.0",
    "@nuxt/image": "^0.7.1",
    "@nuxt/types": "^2.12.2",
    "@nuxt/typescript-build": "^3.0.1",
    "@nuxtjs/eslint-config-typescript": "^12.1.0",
    "@nuxtjs/eslint-module": "^3.0.2",
    "@nuxtjs/google-fonts": "^3.0.0-0",
    "@nuxtjs/style-resources": "^1.2.1",
    "@types/aos": "^3.0.4",
    "@types/lodash": "^4.14.200",
    "@types/webpack-env": "^1.16.3",
    "@typescript-eslint/eslint-plugin": "^6.9.1",
    "@typescript-eslint/parser": "^6.9.1",
    "axios": "^1.1.3",
    "dotenv": "^16.0.3",
    "eslint": "^8.52.0",
    "eslint-config-prettier": "^9.0.0",
    "eslint-plugin-custom-rules": "file:eslint-custom-rules",
    "eslint-plugin-nuxt": "^4.0.0",
    "eslint-plugin-vue": "^9.18.1",
    "husky": "^7.0.4",
    "jest": "^28.1.1",
    "linkinator": "^4.0.3",
    "prettier": "^3.0.3",
    "sass": "^1.38.1",
    "sass-loader": "10",
    "typescript": "^5.2.2",
    "vue-eslint-parser": "^9.3.2"
  },
  "release": {
    "branches": [
      "main"
    ],
    "plugins": [
      [
        "@semantic-release/commit-analyzer",
        {
          "releaseRules": [
            {
              "breaking": true,
              "release": "major"
            },
            {
              "type": "feat",
              "release": "minor"
            },
            {
              "type": "refactor",
              "release": "patch"
            },
            {
              "type": "docs",
              "release": "patch"
            },
            {
              "type": "test",
              "release": "patch"
            },
            {
              "type": "style",
              "release": "patch"
            },
            {
              "type": "perf",
              "release": "patch"
            },
            {
              "type": "build",
              "release": "patch"
            },
            {
              "type": "content",
              "release": "patch"
            },
            {
              "type": "chore",
              "release": false
            },
            {
              "type:": "i18n",
              "release": "patch"
            }
          ]
        }
      ],
      [
        "@semantic-release/release-notes-generator",
        {
          "preset": "conventionalcommits",
          "presetConfig": {
            "types": [
              {
                "type": "feat",
                "section": "🚀 Features",
                "hidden": false
              },
              {
                "type": "fix",
                "section": "🐜 Bug Fixes",
                "hidden": false
              },
              {
                "type": "docs",
                "section": "📝 Documentation",
                "hidden": false
              },
              {
                "type": "style",
                "section": "🎨 Styling",
                "hidden": false
              },
              {
                "type": "refactor",
                "section": "♻️ Refactors",
                "hidden": false
              },
              {
                "type": "perf",
                "section": "⚡ Performance Improvements",
                "hidden": false
              },
              {
                "type": "test",
                "section": "🧪 Tests",
                "hidden": false
              },
              {
                "type": "content",
                "section": "🗂️ Content change",
                "hidden": false
              },
              {
                "type": "i18n",
                "section": "🌍 i18n change",
                "hidden": false
              },
              {
                "type": "chore",
                "hidden": true
              }
            ]
          }
        }
      ],
      "@semantic-release/npm",
      [
        "@semantic-release/changelog",
        {
          "changelogFile": "CHANGELOG.md"
        }
      ],
      [
        "@semantic-release/git",
        {
          "assets": [
            "CHANGELOG.md",
            "package.json"
          ],
          "message": "Release: ${nextRelease.version} [skip ci]\n\n${nextRelease.notes}"
        }
      ]
    ],
    "preset": "conventionalcommits"
  }
}
