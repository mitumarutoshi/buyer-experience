## [3.10.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v3.9.0...v3.10.0) (2024-04-05)


### 🌍 i18n change

* /ja-jp Update pricing page ([8b93a9b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8b93a9bfc4a8d1efe64c2afcda5ccb065461e2f8))
* Resolve "Japanese Pricing Page: Ultimate and dedicated tiers" ([a0a4b6e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a0a4b6ebf86888085b499c35f9b99a44ac689b67)), closes [#3551](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3551)


### ⚡ Performance Improvements

* Move Lighthouse job to a schedule instead of running in every pipeline ([0654498](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0654498c76efab868c08d84b4719ed584b6b5583))


### 🎨 Styling

* **/gitlab-duo:** post-launch follow-up updates ([939df80](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/939df80033b4138628caacebf274255dfe604362))
* fix styling of form header ([84f9fa8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/84f9fa8f7d3052c11cb188715b5c45fba9921cb8))


### 🚀 Features

* **404 Checker:** Add the ability to create an issue and a slack message with the list of broken links ([a108cdf](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a108cdf51d64a8c8f68e3416b99517ec44cc6720))
* create gitlab duo pro self-managed trial signup page ([8bf4556](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8bf4556d66b7b76d8722430bff5c40ca2830568e))
* Create new GitLab Duo Add-on cards for pricing page ([27d4c3a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/27d4c3a11ed32a74b5aae95fdfc94df374468838))
* gitlab duo flow updates ([3db8214](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3db821449aea1dbdc5ae4b102a525f9cb7f67914))
* gitlab-duo page iteration ([7265c1a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7265c1a3788b38a37f2b400726d9e3f8f0956657))
* Resolve "add "no need to rip and replace" to StartUps and SMB" ([048859d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/048859dd38f9e2e4ed59930c73378ce226071f65)), closes [#3557](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3557)


### 🗂️ Content change

* Add CTA to confidential page ([e414b56](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e414b56fcb91e07afe2197a7a7cd40e3b7ea82ad)), closes [#3378](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3378)
* **gitlab duo:** chat and vulnerability text updates ([89e3909](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/89e3909370b69b4f1071a11c3e31185a90a787b4))
* **gitlab duo:** typo updates ([807691b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/807691b8c4251513b6771f11b7582d18cfe6e63c))
* remove seat limit verbiage ([54d29a2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/54d29a28b5c35417a305c8d841e91c5750941af1))
* update create stage description ([713d164](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/713d164ce6f9c03c8b8ee213be9f2d8153e5f2d0))


### 🐜 Bug Fixes

* add patch to privacy page to mitigate ux issue from old broken link ([4ecdcef](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4ecdcef8e260161ee1a69eec841cdb98036da976))
* Comment out lighthouse job ([09e4f23](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/09e4f23d5c43183ad68088f62ed1509ca158e0f7)), closes [#3567](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3567)
* content: update create stage description ([d582732](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d5827321d0bfa0b9754d90f3a8a3fdac7276f0b6))
* feat(404 Checker): Add the ability to create an issue and a slack message with the list of broken links ([39f1716](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/39f1716182a5bb41b374fcd4e97e588f375a841b)), closes [#3480](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3480)
* JF-TEST ([d33a390](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d33a3909b353a587be551bd17ddfbd3d8ded8fc1))
* new custom page on /faster-together ([ea138b7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ea138b7c110ae790845fa0e26eedb99b51c59f97)), closes [#3563](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3563)
* perf: Move Lighthouse job to a schedule instead of running in every pipeline ([4518577](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4518577eced6178d1a44314c0818e677e3b4db99)), closes [#3550](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3550)
* **platform:** category + feature table link behavior ([3c8a580](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3c8a580c7a7393d1e458fb452979a0aaf78beb73))
* Remove broken customer links ([191403f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/191403f2d91ca3a54e5382906f1ac23f1435faf7))
* remove english content still existing in BE ([85ca81e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/85ca81e035feb1ebcd2aa8473b0a08b4a87667ab)), closes [#3526](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3526)
* Remove the use of Shared to describe GitLab SaaS Runners ([27f6efa](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/27f6efa67d7c61cb50fd498d507c790a12ba33e6))
* remove video auto play ([0929184](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0929184afcf5a0d6a36a58f64e2a33ba8dd836a1))
* Resolve "Enable live previews for Events pages" ([91f55e2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/91f55e2a4b469d8dc1a7cca1c1811d586f09e139)), closes [#3501](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3501)
* Resolve "Find and fix broken auto-generated anchor links" ([1ed22fe](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1ed22fedf83efc9c30568b4d66cd6a760b47b7ae))
* show subscription info text ([c67e6d8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c67e6d8f708fece85e41f77eb168f909176525a4))
* **sitemap:** exclude routes listed as noIndex in the CMS ([548c862](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/548c8620c089c7ffaf8854e53b148adc6322a598))
* Update issue/mr templates ([a9f1235](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a9f123574534f3e836ccf3774a74fc2d7ea61f0f)), closes [#3326](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3326)
* Update platform page categories table to provide valid links ([7a7ed3f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7a7ed3f5bc96317e56c98d341f955b79755e0789))

## [3.9.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v3.8.0...v3.9.0) (2024-03-22)


### ⚡ Performance Improvements

* **Caching:** Use GitLab caching to cache our install pipeline stage ([4eecddc](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4eecddc37c6e13d59e9ef87e6af494ced40d78bc))
* **Caching:** Use GitLab caching to cache our install pipeline stage ([19373f1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/19373f1803e72928996b2d19102e212448a938de))


### 🐜 Bug Fixes

* Adding data attributes to homepage buttons ([9ea5078](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9ea5078895e35948e0bd643925aa9ea0ea0bacd3))
* **bug:** security/compliance/ side navigation ([00971c8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/00971c8138c7c9853b097d1ce4f4b0a0f340994a))
* chore: Add comment to the "convertToCaseSensitiveLocale" method ([77b2dc6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/77b2dc633b2fc58d0de9acf8e370399a46206601))
* fix: Fixing hreflang issue ([9555796](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/95557968ced8af9c2dea2954d3d80824a2f48101)), closes [#3516](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3516)
* fix(bug): security/compliance/ side navigation ([076b771](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/076b77131a0408ea25e178d0e76f15ff12fefa1b))
* fix(performance): Features page performance issues ([eaa99f6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/eaa99f68d10e3cf8a4e6505cabbdcb558e9fef60))
* fix(Privacy): Tables not rendering correctly in privacy page ([68d1018](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/68d10188d163c7c4e64b232ce12b5fedfc83f8e6)), closes [#3554](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3554)
* Fixing hreflang issue ([2f19130](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2f19130e69ec3aab46c29aa946082862676cd43b))
* perf(Caching): Use GitLab caching to cache our install pipeline stage ([9714fb4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9714fb4bf3ce8f6b9c37f671816fe9fcda8fa901)), closes [#3477](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3477)
* perf(Caching): Use GitLab caching to cache our install pipeline stage ([a24baf1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a24baf1d65ace8763db55dbceb504213ba985dd7)), closes [#3477](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3477)
* **performance:** Features page performance issues ([755c8f7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/755c8f79c7b1daecd912741b2f32c86cf418268b))
* **Privacy:** Tables not rendering correctly in privacy page ([e1f42c5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e1f42c55248e3b75ea80d0a984b9e578805b6951))
* Revert "feat: Targeted Builds - Buyer Experience" ([e48b6f6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e48b6f6950c584f7e42fc1a2a02e25fb5e3c937b))
* Revert "fix: perf(Caching): Use GitLab caching to cache our install pipeline stage" ([f45ce0a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f45ce0a4ae6c80ee359c84a78f47d96ff94f4bf8))


### 🚀 Features

* add 6sense personalization foundations to homepage ([0bef4c0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0bef4c032dfb4145bd2d1976532a1fe3cbaf499b))
* Add LaunchDarkly AB test to sales button and form ([36fe9e5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/36fe9e56f04b14b360ce89cf6619d35ff2ccf174))
* **partner microsite:** analytics and feedback iteration ([4032617](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/40326176483dad392f6485ef09f9aff6f7033c25))
* **platform:** platform page design and content iteration ([dfb9e41](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/dfb9e411192996f8cbeb8393f821b3ce292edc1d))
* Refactor software faster page ([5ec12cb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5ec12cb7f0569ad3a0cb3f19067d74c8cb35ff58))
* Resolve "Add solution and industry drop down filter to GitLab Events page." ([fea42c0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fea42c027226425cc4ccb800a602412fdd1960f2))
* Resolve "Add solution and industry drop down filter to GitLab Events page." ([3be01f5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3be01f5e044e6008db573e6933d5ebf5ad67baa5))
* Targeted Builds - Buyer Experience ([b9d6181](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b9d618198b0466f71957aeb438f5e76334814292)), closes [#3523](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3523)


### 🎨 Styling

* **homepage:** add fade animation to hide personalization client-side content flicker to logos ([a1e6221](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a1e6221161a2590c7563f5e77d9dee072fbac3fb))

## [3.8.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v3.7.0...v3.8.0) (2024-03-08)


### 📝 Documentation

* improve buyer experience project developer documentation ([b7d36c9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b7d36c9bd5c87fdd4413b8c4b1b651a5549e3503))


### 🎨 Styling

* update section spacing on company page ([eb8856b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/eb8856b6de71d01215f5b43290c38f3e2bd83a01))


### 🌍 i18n change

* Resolve "[ENG] Pricing L10N to Contentful: localization for faq, next_steps, and addons components" ([6f09acf](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6f09acf74b88f1fcb55c62f0e33f6f532464f223))


### ♻️ Refactors

* adjust timing of calls and handle null values to prevent jobs page pipeline error ([ec12581](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ec12581665657b57b8da845d5fa8a40b28dd61ae))
* adjust timing of calls and handle null values to prevent jobs page pipeline error ([24361c5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/24361c56abe218e4e09dc0be98351ab5787a40db))
* **Dedicated:** Dedicated Contentful Improvements ([227b814](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/227b8148484996102ba8f331afc8a00cd2a32672))
* **Dedicated:** Dedicated Contentful Improvements ([f68ac2d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f68ac2df1a693f3f7bebd7a5bdb9678fc0d84814))
* **layouts:** utilize mixins to make our page layouts consistent ([d6b045a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d6b045ae1b813df84a8c1382479a3ccfcebfcc47))
* remove hardcoded strings from custom page marketo form ([bc4f847](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/bc4f847e01b6513fe2306322e1cc030d8a92aa73))


### 🚀 Features

* Add CSM landing page to Buyer Experience ([72efbe3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/72efbe32ddc79fbef9c7ce8fae59c2dcf0dbee1d))
* add live preview page for partner microsites ([b538e3e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b538e3ebacb7cf294606fe410fc19bea9a393bfa))
* create microsite page components ([e3d3e88](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e3d3e886daae54e79a4d8a91bff310ae1ead9066))
* **custom page:** create marketo form component and content type ([a4ababe](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a4ababe41ab8bbcde4f502642c94eb0e5d4c3307))
* microsite mvc1 - set up route generation and static page elements ([3471bc9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3471bc9e6ac26911f46413687aaf8512df15cefd))
* **navattic-demo:** adding ai demo to solutions/code-suggestions/ ([612076e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/612076e559a3bf7643ba09ab4efefc4af852c0b8))
* **navattic-demo:** Adding ci demo to startups component ([440c834](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/440c834a7f4429a95c26bbb9a5ae49e8ba7bfa5d))
* **navattic-demo:** Adding navattic demo to solutions page ([c32d1e2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c32d1e2cb2383cd5d02852d9802496bb6ac2fe23))
* **navattic-demos:** Adding demos to homepage ([a1ac9b0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a1ac9b0119ab349f6c0f61c5f2dfeb431db58081))
* **navattic-demos:** Ci demo addition solutions/security-compliance/ ([c4c66c3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c4c66c3d18b42ebe8faa01c74ff5214d683bf47b))
* **navattic:** Ci demo addition to security compliance page ([9ee879d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9ee879d502fce7ae87e878f062134aa4042617c7))
* **navattic:** Ci demo addition to security compliance page ([cc5237a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cc5237af0a7eff89c5f9a5aaba3035e6ae67efa2))
* pricing page to cms - faq, next_steps, and addons components migration ([d5e7999](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d5e79991082faaf9922ce9c05d468dc48e0481cb))
* Resolve "Update about.gitlab.com/ social meta tags to "The most-comprehensive AI-powered DevSecOps platform"" ([3897569](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/389756921040401585e126d6244cad0a2e16c5cd)), closes [#3284](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3284)
* Ship AI Trust Center to about.gitlab.com ([67c76fb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/67c76fb10a985b1e33489f4901fb7d2330666196))


### 🐜 Bug Fixes

* "Pricing page links to Code Reviews solution page in the old design" ([88b7bb2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/88b7bb2538cb03d7fd562426400e72696826b23a))
* **AI Transparency Center:** fix AI Transparency Center bugs ([4a7b7f7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4a7b7f7815c1348805af4f3e14d2d83a1818405a))
* Bug: Remove broken FAQ from DIB and Summit ([44ef898](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/44ef898da30bed0b98c3f4f899f9e5dddb5185de)), closes [#3515](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3515)
* console error for Code Suggestions page ([edd68c0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/edd68c0c33776882e4a631a67f6587c05975938c))
* feat: Ship AI Trust Center to about.gitlab.com ([00c7977](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/00c797702efe8280d3fe6a2df6836fb8db3ec7d9)), closes [#3499](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3499)
* feat(navattic-demo): adding ai demo to solutions/code-suggestions/ ([75c5ce3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/75c5ce368f22ba285e4de6777743b57b361b48e3))
* feat(navattic-demo): Adding ci demo to startups component ([77a8351](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/77a8351d95d78c49f5ded54c2c6fac58ed91b488))
* feat(navattic-demo): Adding navattic demo to solutions page ([1aad1fa](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1aad1faaf10c924ac84907253c4c6e5a881be13f))
* feat(navattic-demos): Adding demos to homepage ([8c95144](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8c951449df90fe4181efff3dec7b123e282ead3f))
* feat(navattic-demos): Ci demo addition solutions/security-compliance/ ([1cdeaf1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1cdeaf1b6aae78af87e2b3e94433c3bd31c6cd74)), closes [#3382](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3382)
* feat(navattic): Ci demo addition to security compliance page ([403c2b5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/403c2b50e1c9b879cd853b5d0080939272c1875a)), closes [#3382](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3382)
* feat(navattic): Ci demo addition to security compliance page ([001dce4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/001dce4912321bde5994980c5bc76fc822e6b50a)), closes [#3382](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3382)
* Fix metadata that was not working on custom pages ([7a49be0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7a49be07e74e02995f418cbddd8c2f37e9252b20))
* Fix small pricing issues ([89b2755](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/89b2755d6b311e1bd24d4afe9ac0a8d8f5ff3417))
* fix: console error for Code Suggestions page ([53f37d5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/53f37d58e35d0967411e33f0b9434973a43e0a09))
* fix: Fix metadata that was not working on custom pages ([5d223c9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5d223c950356fc81fd906b7ddc3562a4d89f711f)), closes [#3521](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3521)
* fix: fixing warning ([efb1b26](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/efb1b260e1bec3f2f0976b7f152372ff39e84743))
* fix: removing console.log from homepage.service.ts ([a2a284b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a2a284bc5576c6397101b500a238547c4459613e))
* fix: Removing mvc routes ([04a7a60](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/04a7a606567529b4297e74a713e08fdac199bfb3))
* fix(AI Transparency Center): fix AI Transparency Center bugs ([ab9d245](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ab9d245d7539c1d2df355c9cb48277c86087fbc0))
* fix(gtag): Gtag Default Consent Order ([97232d7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/97232d74f26995a99305784e5b2149313affbc4e))
* fix(null-check): Add optional chain to prevent page from breaking ([ca76f92](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ca76f9265f26224ed62c3406f13c53488d221a37))
* fixing warning ([83bc115](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/83bc115dbe7d358eb69c4b9f34479da69fe8e43e))
* **gtag:** Gtag Default Consent Order ([05b6ac5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/05b6ac5505c6db24ea2a44375bb760ff270424ae))
* Have default templates set by md file in repo ([e0a5020](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e0a50205dd58073fb8eca715ea3e444cb6bcf5e6))
* **null-check:** Add optional chain to prevent page from breaking ([5d141c8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5d141c8bab0fd298ec343076e9c8fbab58d2ee22))
* remove security - cap page route ([a4c9438](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a4c9438090717c56be2f6cf1ba6e531258af47ef))
* Removed FAQ about upcoming renewal ([d3a9c81](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d3a9c814e9cf2f53c5282135de2befd20039de21))
* removing console.log from homepage.service.ts ([b4e7a58](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b4e7a587be40eecf825fa966acb02b5da4c84524))
* Removing mvc routes ([69dabc4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/69dabc45a470d2f2059eedad0308212167e20677))
* Resolve "Add scrolling to the stage in the /features page" ([8ad23b6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8ad23b672ee310d061acc98071e13deba05f3906))
* Resolve "Bug: CSS formatting ignoring the Work phone number on /sales form" ([d91849a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d91849a8939e4a40c58d192973abfe610e3fa2ab))
* Resolve "Home & Platform Page Insights" ([a011240](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a01124091a58ec3c11e20e11881aa700b0a2e5e5))
* Resolve "MVC 2: Fix 404'ing links raised by our scheduled 404 checker job" ([4e206fd](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4e206fddcfcbe4c11c49734c4b25c24e924364a1)), closes [#3479](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3479)
* Update file nuxt.config.js ([f351a40](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f351a409f68917dfdfe0217fbbbfc40b4d0c4f24))

## [3.7.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v3.6.0...v3.7.0) (2024-02-23)


### 🌍 i18n change

* free trial devsecops page refactor and l10n migration ([8ef7509](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8ef7509c85554522a4492ffde9a28c42b85bd21f))
* Resolve "[ENG] Small Business L10N to Contentful" ([de50f6a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/de50f6aae1e5f98be58fcbdccd719f834c34fafa))
* update free trial navigation to use content from CMS ([ecff6b9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ecff6b9bbfd819b0f881037b4872bc23da387f58))


### ♻️ Refactors

* **Events:** Improve CMS "Events" pages ([cf3c3b6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cf3c3b621cbb5c94ca6c6a632b611c19fc31bcd6))
* **Events:** Improve CMS "Events" pages ([e44ab44](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e44ab44fc75c5efa3adfbcaab4e0cc80b456ddf6))
* **Solutions:** Nonprofit service deprecation ([6a7cafc](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6a7cafc8c9d6bf71485f505dfd0e8125a406f20f))


### 🐜 Bug Fixes

* Add id missing to solutions components ([8453bb7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8453bb7a8a117f40a34a55194183de26fb29932c))
* Add new event card content type for events landing page ([dd770b3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/dd770b3f424c0cca3962b5501a33b493df97a987)), closes [#3293](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3293)
* Add rip and replace messaging back to solutions page ([914fdbc](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/914fdbc4ebd6d98d95e249166ad6ea97d8fa3a42)), closes [#3465](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3465)
* Add scheduled job to delete stopped review app environments ([04c94d6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/04c94d6c006f808823a6220877407b1bb90eea99)), closes [#1492](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1492)
* bump slippers to 2.1.52 ([c98a19d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c98a19d430ed5b6fec18b6fea876076a600e7dcc))
* Create basic solution template for Contentful markdown fields ([93e1982](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/93e19820d38ee3ebbafdeb81ee46fc1680bbc955)), closes [#3325](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3325)
* feat: "Community Beta" landing Page ([b515283](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b5152836f94fbae2c96a5c4a4418ef413923951f)), closes [#3232](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3232)
* feat: Gartner Magic Quadrant migration to CMS ([98cfc45](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/98cfc45f56cc34dca02ffe851d813d319518418a)), closes [#2987](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2987)
* feat: Resolve "Fix GitLab Duo animation order and timing" ([ec386e5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ec386e5e0dbea0f393ca79aae878d80c814d2643)), closes [#3222](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3222)
* feat(gtag): Updated gtag default consent logic ([4900c77](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4900c77c417d5891e8a5fdf29b42cc42ef6815bb))
* fix: Add id missing to solutions components ([2d6b8a9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2d6b8a9a61b4cefc2d96ee10990fda607c5b8865))
* fix: remove unwanted log ([376f9df](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/376f9df0b402b0b7c3361d5f8b08208f2cbb3872))
* Fixes Recent News cards order ([692c36d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/692c36db0ffebf093c4bf01fcd122638793d979e))
* refactor(Solutions): Nonprofit service deprecation ([99fc0f8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/99fc0f83c586934b30cdfe988939bb350a0b4109)), closes [#3384](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3384)
* remove unwanted log ([30dcf23](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/30dcf231cb4d72a964e026b3f3ddf88b2bc331ce))
* removed old homepage localization files ([f581516](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f5815160508839be1c91e12eaaae3e34c30088d7))
* Resolve "[ENG] Install L10N to Contentful" ([b5f31f7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b5f31f76d1b1a2c1ee9105d3a94a1e7c120b3ad4)), closes [#3201](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3201)
* Resolve "[ENG] New Homepage - Add Analytics and Insights to DevSecOps section" ([e87aaf2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e87aaf292e169da59519fae2a4215da3553be3cb)), closes [#2741](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2741)
* Resolve "[ENG] Padding issue /sales" ([e8fe905](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e8fe9051658746bdd3c50cc89ba252a52fc05410))
* Resolve "[UX & ENG] Add link to GitLab Duo Pro sales landing page on GitLab Duo landing page" ([677e5c7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/677e5c7a68e26f7ec11deb12a841cec2963c90a9)), closes [#3417](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3417)
* Resolve "Bug - Platform page: Fix Nasdaq video" ([2577623](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2577623a36fc41fbcd752667ba582c0ef09bba40)), closes [#3442](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3442)
* Resolve "hreflink mixin believes bottom CTAs on /install are localized when they are not" ([352b2dc](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/352b2dc591e4410f35136916015173e06d37eb6c)), closes [#3471](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3471)
* Resolve "Unparsable structured data - Incorrect value type" ([c2ab0c8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c2ab0c877702c8d75f13f05799658965c96a1ca6)), closes [#3423](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3423)
* Small follow up JA-JP pricing LQA ([e93f536](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e93f5361d2ee1b91aa06c197e55d21fe8b0218c7))
* Update Merge Request template ([23dcb3f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/23dcb3fbd4e7f388873bf652cf7a4139f6b268ba))
* updates press page to fetch data from press child pages ([26d26b1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/26d26b153c51db926838a9e70d028d493c807349))


### 🚀 Features

* "Community Beta" landing Page ([7ffd156](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7ffd156ab34619ed9c972ead7c8369f0c3fcdf77))
* Add contentful information to triggered pipelines ([3433adb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3433adbdd1e1112dd206162f1e189412d9e75f04))
* Add new event card content type for events landing page ([0460b66](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0460b66c75f225816851f9068c856bebedf390f6))
* configure nuxt to generate fr/de/ja routes via contentful ([cdffa10](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cdffa103ac617431b4c7c0d6196cfd3d760c5502))
* Gartner Magic Quadrant migration to CMS ([377b042](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/377b04283d7b30854e1cf94c1b461398e9c3ced5))
* **gtag:** Updated gtag default consent logic ([3c84b57](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3c84b57d88aba77c0de2264204c188cbf472932a))
* migrate /jobs page to buyer experience and cms ([260e895](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/260e8952981c93aafbf6d7348e4e1ba092a2daf1))
* Resolve "[ENG] All Get Started L10N to Contentful" ([f55a4e8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f55a4e8089ce557f2e2621175803d12cecb33f04))
* Resolve "[ENG] Contentful deployment iteration" ([1d8ebd0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1d8ebd08ee173870a0cc6c64b7d17d58023223b2)), closes [#3407](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3407)
* Resolve "[ENG] Press page to pull press releases from Contentful" ([a3b5627](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a3b5627e42b3c8d76200a278e3d2044ea2692b9c))
* Resolve "Fix GitLab Duo animation order and timing" ([80fc5b2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/80fc5b24de8c2a45d204f5ad009fa880708b0158))
* Resolve "Technical Demo Series page - Iteration" ([ea04109](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ea0410954f57541c87202f477371e8cd8801fa0a))
* Revert Pricing ja-jp ([92b40c7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/92b40c78e24ab8511c04364c7b25068fda8ec33c)), closes [#3385](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3385)
* update slippers version - adjust body type site-wide to improve readability ([973e39a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/973e39a39d451a8a5f6eb59e263d6d9ecf9e2850))

## [3.6.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v3.5.0...v3.6.0) (2024-02-09)


### 🌍 i18n change

* **embedded vimeo videos:** Show de/fr/ja captions on Vimeo videos by default if on locale-specific page and captions are available ([0e5b4ec](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0e5b4ec3a0689922983e57096ee1004f3eaefa30))


### ♻️ Refactors

* **subtitles:** show english subtitles by default ([aa57be6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/aa57be634dc94769af4c3a472ef0a335cd5db741))
* **subtitles:** show english subtitles by default ([f19dc0e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f19dc0e588b0135de12c65e2256e354a089e801e))


### 🚀 Features

* Add cropping logic to e-group and board of director headshots ([a7f3b04](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a7f3b04170d74706abf5185e781bfcdbcbe9eacc))
* Iterate on Technology Partners Child Pages" ([9efbf4b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9efbf4b368da51d1433dedb95fe16e0a6918f443))
* **l10n:** translating submission text for startups join form ([820f701](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/820f70103652fe1fe27b344bbd91d47dc999d4ea))
* Migrate all-jobs page to Buyer Experience / CMS ([8d33c2e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8d33c2e6aa0b1bf69b65823d8b724ddbec1c3442))
* **navattic-demo:** Adding CI Demo to security and compliance page ([603f6c5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/603f6c503ce2bce60abe2ca500d703c6583129a6))
* **navattic-demo:** Adds Ci Demo to continuous-integration page ([617726a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/617726a21da71c256b00ddfb060eb5a35aa3f0c3))
* Resolve "GitLab Duo Pro Pricing Change - Jan 31 " ([84dda1a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/84dda1a757ef158b9dbe890639b88baee5a583a4))
* **solutions:** Resolve "[ENG] Value Stream Management solution page" ([49e6bdc](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/49e6bdcd0be1064a3ed21bffcf167c5ca8e9264c))


### 🐜 Bug Fixes

* Add additional CI check ([59a1873](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/59a1873992c95c5e67dead4343046914be6105d6))
* Add manual review app job for preview api ([c70b580](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c70b580393243c449fdf5c0c3ac0775db842463a)), closes [#3194](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3194)
* Add marketing links to table ([8471b63](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8471b631adda96ab2b06c715dbaba1dee5bcb833))
* Add validation to our /board-of-directors/ and /e-group/ pages ([39a9a2d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/39a9a2d3f5b7c633f96f1bdda7068480d29e6d70)), closes [#3394](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3394)
* bug: fixing undefined string on solutions/secuirty-compliance ([0effae8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0effae820d2317ad970e6907b216146ff53ed44e)), closes [#3413](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3413)
* chore(services): Resolve "[ENG] Startup Contentful Improvements - MVC1 deprecate startups.service.ts" ([5f1e6dd](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5f1e6dd95ccdd571abe27fc658e1a343b7c16a72)), closes [#3383](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3383)
* feat: Add cropping logic to e-group and board of director headshots ([6639fd3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6639fd3384c90e2dd6d7ca42a420876ce0f57d0d)), closes [#3199](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3199)
* feat: Resolve "GitLab Duo Pro Pricing Change - Jan 31 " ([f9962a6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f9962a6cee7c9f11ef76c52495bb71aa31033a1b)), closes [#3375](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3375)
* feat(l10n): translating submission text for startups join form ([2477a51](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2477a51f6f8458150de232005fd6edc587f987cf)), closes [#3389](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3389)
* feat(navattic-demo): Adding CI Demo to security and compliance page ([5687e32](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5687e327c1a20abb313044379becd4ae6842b0c8))
* feat(navattic-demo): Adds Ci Demo to continuous-integration page ([613f69b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/613f69b2620dad903a70af18f15176d01779fae4))
* feat(navattic-demo):Adding demo component to solutions industry template ([3b02d39](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3b02d39e713948276207525ed3d561d05968d7d8))
* feat(solutions): Resolve "[ENG] Value Stream Management solution page" ([277ec9b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/277ec9b399fd97de348fe2cd42f45b8524ad8fa2)), closes [#3373](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3373)
* Fix some Events pages that were not working due to missing SEO entry. ([7429d99](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7429d99fb6ad13e3c1a6f47347c902d05a20e964))
* fix tech partners hydration error ([ccafe27](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ccafe272bda80c1ed97ae486885502729c2fd865))
* Fix video modal in solutions hero ([ef2e2a6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ef2e2a6472d9ae55622affc97a5abe50a3e38222))
* fix: Fix some Events pages that were not working due to missing SEO entry. ([ac23879](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ac23879dabceac8337e7517f4614a76e85fb3a86))
* fix: make install page hero description a conditional element ([a96130b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a96130b850c2fade3a877f8715444f0c37d6ef2d))
* fix: Missing canonical paths in /solutions/* and /press/releases/* ([27c4013](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/27c401390f9a47d141104baeec2e249a6d467964)), closes [#3329](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3329)
* fix(navattic-demo): Update ga location ([90c4826](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/90c48264b7a0850ec97f2dea6776d57e7c3c1da1))
* fix(solutions): Add video thumbnail property to mapSolutionsHero ([e46e96b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e46e96b8a868a076d09deaa62f433b37ca41f2c4))
* Free Trial L10N to Contentful ([b7386d3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b7386d3da142b56c4259fbebd26e49b6c110c539)), closes [#3129](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3129)
* hotfix: Fix video modal in solutions hero ([25b2de8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/25b2de883d7762aefda75672567df22696d16ee7))
* make install page hero description a conditional element ([b8cf530](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b8cf53075f50f63e570b638f4283c41869aebe0e))
* Missing canonical paths in /solutions/* and /press/releases/* ([d4ac294](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d4ac294aeed20b11e26a9aa0bd82762b192c87c9))
* Move /calculator/roi/ to /calculator/ ([b1af4c3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b1af4c3dddd1f2e1b2b8a59a718ddf2ccab73019))
* **navattic-demo:** Update ga location ([27e6851](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/27e6851b5c3209769a9712044a1ddeebaa145750))
* Remove localized duo content locally and fetch from Contentful ([118e6f3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/118e6f36641d050cbc1bb5b627c6f844805e17c5)), closes [#3200](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3200)
* remove solutions content yaml file ([ecf4a65](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ecf4a65dc4107a7bf8147da9365b62071da6a1ed))
* **solutions:** Add video thumbnail property to mapSolutionsHero ([4b6053b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4b6053baf78af15c2e00b419a17267abe179da85))
* Update badges on free trial devsecops page ([05e8609](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/05e8609a672c46cb13f5025138c67b38224405d4))
* Update link to roi calculator in premium and ultimate pages ([46ed6c9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/46ed6c971f25d98c52f9291b4ac0dd27541c23b8))

## [3.5.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v3.4.0...v3.5.0) (2024-01-26)


### 📝 Documentation

* **project readme:** clarify use of required environment variables to see in-draft or published content locally ([1f68dac](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1f68daca88ddd10c0c7f1ca30918f33cf61442fc))


### 🗂️ Content change

* Add LibreFoodPantry logo to open source partners page ([8ea2b94](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8ea2b94c68f78d8607646df19be57793f1bc44ce))


### 🚀 Features

* Atlassian page updates ([15e6184](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/15e61848a8a51acfb42d79c78af2cb82b49a5c8d))
* create ci demo component ([6966be3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6966be3aab3d926a954f70bd207ba2ce62a348cf))
* Gpc onetrust ([4f4eec8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4f4eec85a8b3d48c549f4703b726e295623cc8a6))
* **l10n:** Japanese devsecops video ([af47ef0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/af47ef0ba4e24259a9d7e986b8e624136ca2fb87))
* **l10n:** Startups/join form in german ([a68ae19](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a68ae194b2b5707f36c15f0850a3c8010e48a5b0))
* **l10n:** Translating startups/join to french ([8aaaac4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8aaaac40f63da3ad09f99b5b38402ff04d5f2b00))
* Make next step a boolean for custom pages ([513ae34](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/513ae34c6d46f02278212f6a059f3bc7de655b47))
* **navattic-demo:** update ga ([f0137fb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f0137fb9fdd78639e7d50d709ce44dc573431175))
* **page:** changing code suggestions to gitlab pro duo ([7442bb6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7442bb62bb546191a8d846be52c44b54831a7e6b))
* Resolve "[ENG] Platform page 'Read the report' L10N" ([6aaf8b3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6aaf8b36cd348729a5cfb4ba0607e9b49c65cef2))
* Resolve "[Eng] Refactor Resources page to handle new content" ([4ad08c3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4ad08c36924c13c57fadfad2839e82e2960373c3))
* Resolve "Include Chat as part of the Duo Pro add-on" ([71e5310](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/71e5310975fbd3b622493ca8374df029770debfa))
* Resolve "Set up live preview for custom template" ([6bbd6dd](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6bbd6ddb3673a38fb4a8808aa93c37773fb5e49e))
* **solutions:** Resolve "[ENG] /solutions/open-source/partners migration" ([1de6ad7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1de6ad70ba649b444504fdfeab9f406fca8ddc11))
* Updates localized pricing page addons content ([c78f7ba](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c78f7ba4a196aba513fe1a15c557c97da52045e1))


### 🐜 Bug Fixes

* Add details to the compliance message in govern page ([20bd762](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/20bd762287dd4d29d21ded538ed5b6d44f78e806))
* feat: create ci demo component ([2c9579d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2c9579dd1eda84bc9e275c9615b2a01c6e2971fc))
* feat(l10n): Startups/join form in german ([dbd8726](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/dbd87264c26119c3cb0630548abfb4bd5d8fc650)), closes [#3369](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3369)
* feat(navattic-demo): update ga ([7461e4c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7461e4c2ee1197b1d54a64fdebafe6798932dd16))
* Find and remove .html instances in slug names for Press Releases pages ([2f84a6f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2f84a6fda73e717d0b3506f9258939c3b057497f))
* Fix "localized-href" mixin bugs ([c645cb0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c645cb0160981210e3fbcb0522b7a4b2b5b0d9ef))
* Fix Solutions pages returning 404 ([77946d5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/77946d5f175849bbe0809c08ff17a9d9c2dc2b81))
* fix: Find and remove .html instances in slug names for Press Releases pages ([6b9b452](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6b9b452319fa904a743d1ab54e62183124ba78b5)), closes [#3290](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3290)
* **pricing:** Add overflow clip pricing template ([3d25e78](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3d25e785721503a11b47d3aec8c6bdeb7a5f913d))
* Resolve "[ENG] /solutions/education/* pages migration" ([90b9377](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/90b9377c43fb46de2e775ec2947847a1c3d52eaf)), closes [#2925](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2925)
* **seo:** remove custom-page-preview from search engine indexing ([b576a3c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b576a3cb272427844221e3c8c609e0ee7f1dce73))
* **solutions:** Add role attribute to template section for overflow fix ([31156b5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/31156b58cddae7d3e6a81fd3a56854d7b89cb560))
* URL Update Request on About.Gitlab.com/Learn ([12d1d36](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/12d1d36df7daef87cceb7dfc5b62fba1426373ea))

## [3.4.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v3.3.0...v3.4.0) (2024-01-12)


### 🗂️ Content change

* **VSA:** update value streams assessment to be value stream workshop ([5119956](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5119956fddb73089f6ba6e9799f5658733efc14a))


### 🌍 i18n change

* Migrate Company L10N to Contentful" ([6a56b03](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6a56b03942dfa12e3455f0d3087575cad63a5fc6))
* migrate Sales L10N to Contentful ([0baa6c3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0baa6c312573815582643132bb38be0518ef706f))


### 🐜 Bug Fixes

* Add new support url to routes ([2594b73](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2594b73aabc84b87015b86a87fc55e13acc2945e))
* broken localized /solutions/startups pages ([250114a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/250114a73f8e82920520bf2ddb2346d3910c9dd5))
* **gtag - legal:** Updated denied section of gtag script ([3ec5898](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3ec58986f94b2c17058efb35ef2134f22bbc95ef))
* hardcode select solutions routes to route file to prevent 404s ([c073158](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c073158acf8d92ac552c531af93cde7401711407))
* patch services page ([271ff5d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/271ff5dfb89cb3b47f9500dcb7dbc3dfa204a1df))
* Resolve "[ENG] Migrate missing news" ([e338b67](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e338b6751629f4f8bf0dfbe96df7c149946967f9))
* **sitemap:** sitemap now takes in all dynamic slug routes and routes to exclude will work ([21175f3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/21175f378ec2eb9190fa2a801b8ff7d062530c3d))
* Update file join.yml with new handbook links ([ba7ba1d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ba7ba1d38abd0fb711476a24c4e1ed74f776f058))


### 🚀 Features

* Add script to fail pipeline if essential routes are not generated ([0b68b40](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0b68b405732ebb31c9741373ad71de7fbbc1ab0d))
* Add script to fail pipeline if essential routes are not generated ([c0ae08b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c0ae08b1708649a002f7f7bde6d7be6301411fc8))
* Contentful Live previews for /support/* & /customers/* ([00b3766](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/00b3766a251a9f9e827d279cddf4989348093363))
* **Contentful:** Contentful "Custom Page" template ([ef402fa](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ef402fa8d3c5598b78eee87f0c7bc40ded7f8313))
* **Custom Pages:** Improve header ID generation ([47d8ecc](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/47d8eccb5435ca135abb87356ab5f9537acedefe))
* **customers:** Add embeded video customer case content page ([a20d01c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a20d01c11ea6ce695f487e14470e58a19bc70d45))
* **customers:** Add embeded video customer case content page ([cded9a8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cded9a87734a973195d3479ecd5ac99cc8a85198))
* Environmental Social Governance CMS migration ([ad8f0d1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ad8f0d1233a6f11960090a061681e0f539fe7619))
* **footer:** Add contentful entry url to footer - phase 1 ([ec89251](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ec892519751b98b717ff755f9145759e033ca3e7))
* **l10n:** Localizing Japanese startups/join page ([9ad9f43](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9ad9f43eb2449aa313d781319052e01810f32143))
* **l10n:** Update ja-jp devsecops video ([d1824c3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d1824c3676aa28df5f7488951b419ea71c591f47))
* Resolve "[ENG] /press migrate to BE and Contentful" ([13f5f1a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/13f5f1abbbca1c7aa3833c02e360a9553f5023c9))
* Resolve "[ENG] /press/recent-news/ migrate to BE and Contentful" ([633c61e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/633c61ef64be21727c8235b458d30581c27415d0))
* Resolve "[ENG] Privacy L10N to Contentful" ([f256c11](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f256c119da1ec5351ca383a4cfa554e25678b38b))
* Resolve "[ENG] Update lint rules to fire error on console.log" ([3b1ee65](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3b1ee65c1a7b1fba98ebe340425b51741f67dc7c))
* **solutions:** [ENG] /solutions/open-source/ landing page migration ([3772366](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3772366e522d1619789224d678e7898f235065da))
* **solutions:** [ENG] /solutions/open-source/ landing page migration ([bc1d0d2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/bc1d0d27d61bb732004db61c3ba29fbf9a5b8e94))
* **solutions:** [ENG] /solutions/open-source/join migration ([311f179](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/311f1792b48a9870c40699d1b058872dc1b3cb75))
* **solutions:** [ENG] /solutions/open-source/join migration ([e095cd6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e095cd6052c52c68dfd3337b224fc3d9c9bb4f6b))
* Update hero on software faster page ([901ab88](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/901ab8863e84634fcdcae1aafc678c0fe8f916ff))

## [3.3.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v3.2.0...v3.3.0) (2023-12-15)


### 🐜 Bug Fixes

* Delete page building startups/index ([a09b9dd](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a09b9dd2f341d84e58181103a7d234f14691b906))
* improve error handling at page level - throw build errors on critical page building failures ([7f1d0c6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7f1d0c61bd2f85b65ef3be5e0d6c343e6a017347))
* move small business mapping logic to separate file + fix cms live preview for page ([5e0073d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5e0073d9f34be0f8c2a1ba863abc5fdb0d5f1fec))
* Resolve "[ENG] Bug - Why GitLab side nav turns dark on scroll" ([7578935](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/75789351f3038e32885955696bf48459795f26de))
* Resolve "[ENG] Bug - Why GitLab side nav turns dark on scroll" ([2909f17](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2909f173c12bbc353addce69d91a91828c554d89))
* Resolve "[ENG] Pricing page - Fix Agile add on CTA chevron position" ([11915e2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/11915e2b77ecda00c1677126c97d00ffb8bb120c))
* **ROI:** Fix ROI calculator critical bugs ([fb09bf3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fb09bf3fef773652cf20ed7011064e237f016103))
* **small business:** add old layout back in to account for non-updated translation files ([d376c53](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d376c53f577a03416a2ca5fb5a5324effdab2156))
* **summit layout:** pass new cms content prop to minimized footer ([c1e7f60](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c1e7f60b08c14cd2f11feb5302e779470eacbb7f))


### 🌍 i18n change

* **Homepage:** Homepage L10N to Contentful - Round 2 ([f77b88c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f77b88ca3e9569c4dd4cc4efc8d180c2d30810f4))
* migrate Software Faster translations to Contentful ([6587896](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/658789693b7dc124a60de3dd416f04b3cfe5ef32))
* **partners:** migrate L10N content to CMS ([0319d91](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0319d91558f29e17248e40b81ef4c7a1f31ab7ca))


### 🚀 Features

* [ENG] Impressum & Mentions legales L10N to Contentful ([7ea41c5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7ea41c5573a54756ef9d4c893c7f5d7718cf4ea3))
* add Gitlab Technical demo series page ([e85bdee](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e85bdeec1db0fe6ded45863fec6b5621742b2f7e)), closes [#2743](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2743)
* **category/stage table:** pull from categories yml file to compile content for platform features table ([522abaf](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/522abaf75c311e2473b41b4a03cb4caf7391c85e))
* Create Ci demo component & add demo to small business page ([9b36505](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9b36505aa7dab35c98be558ed284821e42eed736))
* Cs banner ([7fab969](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7fab96914fbef1a8f3c6a58b56eac3866c622a2c))
* **Install:** Install CMS Migration ([cb8773c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cb8773c8173d04550bc25323b814345695a3d9e8))
* **Integrations:** "Integrations" page migration to Contentful ([a7c2487](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a7c248777342ce12902024467fc44d2d56d87aca))
* **nav:** Update navigation package version - 6.2.0 ([b2ceb7c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b2ceb7c397122bbd9788bf72592e1a1494edc39d))
* Removes A/B test adds localization placeholder text ([45c1ae8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/45c1ae87f7bdac1ed71c12ce93f3aec67377dd28))
* Resolve "[ENG] ROI Calculator: Efficiency calculation and design update" ([83d444a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/83d444a7fecf789185be0b7cf0febcc2a91eb729))
* Resolve "[UX & ENG] MTM - Commercial Solutions - Update the Startups page experience" ([18b0de6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/18b0de6a187283fedd3d9c59e40280b36c88a6e0))
* **solutions:** Redesign CI/CD Solutions Page to fit modern design (with updated copy) ([48a1b2f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/48a1b2f77b98ceb81d39438898e0ad07a17a28b3))

## [3.2.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v3.1.0...v3.2.0) (2023-12-01)


### ♻️ Refactors

* add footer entry id to api call as a safety precaution ([45ae346](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/45ae34673aa68b9a7ba3cecc2429fe01a213f0c7))


### 🎨 Styling

* **partners:** add padding to add space where quote component was removed ([36a49f1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/36a49f1b1a0c818a23445a36edfa6769a7576574))


### 🌍 i18n change

* **Homepage:** Homepage L10N to Contentful ([0d66b84](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0d66b840b95f7200c4b99dc31820cca9490ec9d4))
* **Homepage:** Homepage L10N to Contentful ([6aec702](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6aec7025df7ef743ce7d309167795c7fff763802))
* migrate enterprise translations to contentful ([c38e6c3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c38e6c3fb61a1382e6a5d4b1f29ac3acaf4fea9e))
* **preference center:** migrate communication preference center translations to CMS ([23754d4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/23754d4710664d9fddd3f16fda8da9b926b576e2))


### 🐜 Bug Fixes

* [ENG] Homepage - Hero - Replace last slide with a card for wide viewports ([ce7b51a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ce7b51a894b6491f55323027cdb8145355a25c02))
* Adds missing overflow property to solutions template ([acac30a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/acac30a3618a61c5c21830fb596bc76d82de2eae))
* Adds missing overflow property to solutions template ([c96086a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c96086a63dd39bf9c3f6b7f4c175ec99e56287fe))
* Changing education institution ([b69fe44](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b69fe443157ab4efa2b6d102bc3788239a8653ad))
* **Company:** Fix Company page missing localized content ([737b51f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/737b51fad6f96bc54200a85fb1cb58fb3808ea3c))
* Fix Missing localized content ([ef200f7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ef200f700d7558a3065a02c9917718a313d68184))
* **GitLab Duo:** GitLab Duo Metadata fix ([11a26dc](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/11a26dcfe217d741187154242ec87c1db0ffece6))
* **solutions:** hotfix for select 404ing solutions pages ([5c295ac](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5c295ac247ab7863e51ebe31b51d493af7237e1a))
* Update file name for nonprofit-join page ([d587f5a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d587f5a57f32249507ff755713857d89ec5d9ba8))


### 🚀 Features

* Contentful Live Preview for Buyer Experience ([0870afe](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0870afebb88612ac00ba45d1dc09ccf6293e2651))
* create 404 page route as a new fallback for errors ([27ad30b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/27ad30b7b7bee79ce3508e4eb9da7a41a40a23c7))
* **localization:** Update file /ja-jp/get-started/small-business.yml ([07f7bbb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/07f7bbb677e24108014e286a1e33f7fc8150f5a2))
* migrate /community/ to BE and contentful ([eb1656d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/eb1656d1d453ff88ec77d6ea23737bdc0576924f)), closes [#3147](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3147)
* migrate all-remote and visiting pages to contentful ([b752b14](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b752b14a38f1ee696f19aff07d565d011b290304))
* nav release 6.1.0 + wired up minimized footer with CMS ([3bc7604](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3bc7604d3d5d94c256176c402ca0c78bd83bf522))
* Premium/Ultimate page refresh ([372e847](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/372e847c4f59b842dba4989d39228d018310d0ea)), closes [#2954](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2954)
* Resolve "[ENG] /solutions/nonprofit/join migration" ([ec50767](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ec5076763a898fe66dadf512945d3a5d49d04a64))
* Resolve "[ENG] Analytics and Insights page MVC2" ([925bbde](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/925bbdeada6f608549e36ab0496d7c8e37d1e6fe))
* Resolve "[ENG] Analytics and Insights page MVC2" ([c0aad1f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c0aad1f1917db3e1ce5f8d3a3832a41335c1f1d3))
* Resolve "[ENG] Update LaunchDarkly js SDK to newest version" ([cd25926](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cd25926f238d2b9dc78f1e9e162d7ec9e804ae51))
* **small business:** small business page refresh ([01de84c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/01de84ccbab03b9f44af3a3d8ca1f21fb37e530e))

## [3.1.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v3.0.0...v3.1.0) (2023-11-17)


### 🐜 Bug Fixes

* Add correct case study link on Why GitLab page ([5206392](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5206392d6660779804ec72e561571a3994b18659))
* **Features:** Fix missing icons on /features page ([d1ad9c3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d1ad9c32bb85245067d602118ab226deb620c7f6))
* Find and replace free trial link to  -/trial_registrations/new ([cb826ed](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cb826edbcddecc97f489f6969f64cf5651517f60)), closes [#3060](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3060)
* Move /why-gitlab/ to to Contentful ([9df0e62](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9df0e620aa3a0f6378b893c3c096b482b0d10b6f)), closes [#3043](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3043)
* Resources for /solutions/agile-delivery/ ([fff0183](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fff0183ae74e35e3d6e30e0b1c8f9fada87cf114))


### 🚀 Features

* **localization:** Localizing the continuous-integration page ([3c3d4c7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3c3d4c79e81896195505ca9cf98557c55772fa75))
* **localization:** Noriko adds translations of enterprise agile planning part in the pricing page. ([cb98b6e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cb98b6e00b85634fed81f354a8e673af31ea6edf))
* **localization:** Noriko edits Japanese strings on the free-trial devsecops page ([e9c82db](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e9c82db8e7f76c4fed4bca42b3fe547e0c084990))
* Migrate /company/ pages to cms ([4b95cca](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4b95ccafc37488517074fb5d0d733459eb882a64))
* Refresh /solutions/agile-delivery/ ([8794b3e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8794b3e8c647effe7f83f8898d7460e7d0a07b58)), closes [#3086](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3086)
* Resolve "[ENG] /solutions/nonprofit migration" ([4f0c04a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4f0c04a81916511a1702a1c4bf3d74cc4b9ebda2))
* Resolve "[ENG] /teamops migration" ([3462591](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/34625910df021d2da1d64be17af947c960cffc11))
* Updates to Atlassian page ([df00016](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/df00016a1e9a22ea81c8661cdc25a50ab415715f))

## [3.0.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v2.11.0...v3.0.0) (2023-11-13)


### ⚠ BREAKING CHANGES

* Contentful implementation

### 🗂️ Content change

* **localization:** restore Norikos edits to homepage ([6d14466](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6d14466e6d823a3977f2457c0363a04d6adbc193))
* **localization:** restore norkios changes to pricing ([762c6dd](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/762c6dd14bd3355eeb5dbecab38a9a08a3c1a703))
* remove a portion of yml files from pages already migrated to cms ([ba7ca78](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ba7ca7858818352e1fdc9c13d364f1e9c65dc829))


### 🐜 Bug Fixes

* Add forms back ([4eaf24c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4eaf24c01c344cb8d896ca8de146917b9c4bbbea))
* **metadata:** twitter and og_image seo fields are incorrect on events and case studies ([fbedacf](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fbedacffc96b4a5fc6265ec7e0f1374e9359e33f))
* Missing Goldman Sachs logo on translated /finance ([6644710](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/66447108bcfecd9c92bc516f2b554882f34256e3))
* Update file route.contentful.js with get-started pages ([4a776cc](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4a776cc2606764f310d39c61ace369959d36e6d2))
* **why-gitlab-hero component:** solve for empty H1 elements on solutions pages ([8806f7c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8806f7ceb1c482e2fcfbc8ccfa45f5b8666a4014))


### 🚀 Features

* **Component:** [ENG] MTM - ENT Solutions - "no need to rip & replace" messaging" ([55338c6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/55338c62f44cdb4e58104eb96b29da75b7cae947))
* Contentful CMS Notes ([daffaf0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/daffaf01091e4859c9d392048292960c48f7e100))
* **localization:** Updating japanese on gartner magic quadrant ([69d15e7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/69d15e7be910d098198f3582bad9ea92be656d70))
* Migrate Stages DevOps Lifecycle pages to Contentful ([79570e2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/79570e2032e7f3d5e175a0226c3362ac9ee14e6f)), closes [#2968](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2968)
* Resolve "Solutions Startups Join related to [ENG]  /solutions/*4 pages migration" ([51400fc](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/51400fc358a7e92f13ff40018482018685be73fb))
* update case study entries to allow for seo metadata entry references ([7560f2e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7560f2e40f88496590178132a33b2de4b58f4bf6))


### ♻️ Refactors

* fetch small-business page by id; pre-work for [#3214](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3214) ([ffd6e86](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ffd6e8620953c6d9feaa9f3c1e00c36bdbb050bb))

## [2.11.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v2.10.0...v2.11.0) (2023-11-03)


### 🗂️ Content change

* Add SBOM package to Security CAP ([acc7300](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/acc7300de79432b35a1fe8b4ade917435b74e79b))
* Adds new workshop to events page ([63d25a2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/63d25a2215c4e8a58f4b9092213f6de19a40d46a))
* Changing 'Start up license' to 'nonprofit license' ([6776215](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6776215f401ccc6c59576a4c1c50c71aa9ca3f68))
* Close registration for DevSecOps world tour DC executive event ([e49560c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e49560c8ba7ddd63037edba2876764123aeb9468))
* **french legales:** typo fixes ([9181ef4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9181ef4a070b6a97dab5c90f4d1977102631f53a))
* Migrate World Tour and Summit pages to Contentful ([663dfd8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/663dfd80f63c15560012b8bb514b7a72c3ae0d59))
* New event added - Advanced ci/cd workshop to event website ([c4d55d4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c4d55d4e7ca8256be3d06261ebd02af93df9c5f8))
* **privacy:** update hero text ([5302eec](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5302eecee24c15ef36a8098750e5c7805397dc83))
* typo fix on gitlab for nonprofits page ([9632c29](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9632c2999a83e90f2c0d88838e4d0d19fb8e8ede))


### 🐜 Bug Fixes

* AWS re:Invent ([8ccc82f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8ccc82f702a2a6dc246c8b6de95309e40860e1c0))
* AWS re:Invent ([5e2eef1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5e2eef1bc76b97d5050bb9f9aa5b06e0b7abdd96))
* Landing Page KubeCon NA 2023 image fix ([efb09d5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/efb09d53feb7a2b85379344d0c5bb0e29fc6a717))
* refactor how software faster page consumes data ([d675b63](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d675b63fcd47f12ddc27437b42b79a31cc93052c))
* Refactor HrefLang method ([7229d39](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7229d391da27b6108fb1040fdbc1e81b4367e6ce))
* Removed tab in developer survey ([32d8098](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/32d80989a156002ff1cda5db4747b93985de8b25)), closes [#3027](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3027)
* topics contentful regression ([9576819](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9576819b93dc0400455a62fc71ebac664b84f8ec))


### 🚀 Features

* Adding mabl partner ([ce4f3a4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ce4f3a45385e480856bd83e7cb4c8868a7827c6f))
* Atlassian EOL page updates ([b2f4a62](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b2f4a6267be9472a9bcf9565e9094aba1078a38f))
* **Get Started:** Migration of Get Started landing page into Contentful ([ec84867](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ec8486786fb5943425a8f2b04906b1a77a2ebf24))
* **localization:** Noriko adds more changes in the pricing page ([f837575](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f837575ea4dc4c294bcd4c08a31855f26b318fea))
* **localization:** Noriko adds more changes to the startups page. ([4a494e6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4a494e60b8520e0ce7d8d7f8c54ce9f1c844466c))
* **localization:** Noriko edits Japanese translations of devsecops page. ([52f410f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/52f410fc06dfd4703eb5990a7286a04d478db417))
* **localization:** Noriko translates Gartner Magic Quadrant  page ([cf7fe61](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cf7fe61aabf38b5a2cbdca8b5667a6e836e86c2c))
* **localization:** solutions lqa  Japanese ([ece9dc4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ece9dc451e32a3033b566dc0606f3a1b888ecb5f))
* **localization:** Updating homepage translations ([432b31a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/432b31ab58aa07ba192e0449d8223d642c62ea24))
* **localization:** Updating japanese text in Next Steps alt ([32a409a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/32a409a82b7f742e5535e225739e5f56f7e91508))
* **localization:** updating pricing page ([43e2322](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/43e232234ab4c6c192933694a6cb40554274d4d6))
* migrate /partners/benefits page to contentful ([9643799](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9643799e90f2603acfb547379b996d6b6af33d3f))
* migrate /sales to contentful ([03efc42](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/03efc42b4987381e9c1923c8113d98af6413b5d1)), closes [#3036](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3036)
* migrate /services to contentful ([0beb0f4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0beb0f40c6e38e64f7834b8d1087062be21465db)), closes [#3037](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3037)
* migrate /technology-partners to contentful ([9624141](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9624141c07b2dd9536c184925bcb82848be52d39))
* migrate /update to contentful ([6921b03](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6921b035f06fb331e3b4c06d08219bbd37567851)), closes [#3038](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/3038)
* migrate demo page to contentful ([42ff97e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/42ff97edde2f047846cbca2ecc811f9cc930f51b))
* migrate get-help to contentful ([588e97f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/588e97f00afca579bd9628ec7842af5009e6f274))
* migrate partners page to contentful ([fdaae78](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fdaae78d523de58f6941e49637302df04ee6acf4))
* migrate privacy page to contentful ([8d5c682](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8d5c682caa71e1b340a5c71f13062b0a0b9e6b58))
* release nav package 6.0.4 ([fc5fd6d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fc5fd6d40605c514d1ab368ebea7735bf3eb27e1))
* Resolve "[ENG] /analysts migration" ([4a35562](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4a3556240bdf6e20929862ed96cf4512a427f27b))
* Resolve "[ENG] /security migration" ([3071342](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/30713420d590be2d94159d1b46309d497afcbba2))
* Resolve "[ENG] /solutions/ analytics-and-insights" ([f900e4f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f900e4f233abfd81f3d7ec9d4f7af29c1b621371))
* Resolve "[ENG] /solutions/ analytics-and-insights" ([eb899a5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/eb899a59a399f82aaa5308dac0a3435873d13d6c))
* Resolve "[ENG] /solutions/ startups migration" ([ba9b787](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ba9b7870f1adc423535c112d470a3e418736d0ba))
* standard events template updates ([177c007](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/177c0076e218fc710d792df0428ac9e6d121b247))
* Update meta tags for /devsecops /why-gitlab /move-to-gitlab-from-atlassian ([382a821](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/382a821595569121240ae077c89610819bd8ca1a))

## [2.10.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v2.9.0...v2.10.0) (2023-10-20)


### 🚀 Features

* Atlassian EOL Landing Page ([35fcd80](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/35fcd80d7f1aa40883c000654664ab148b13172b))
* Create /why-gitlab/ Move current /why-gitlab/ to /devsecops/ ([04815ac](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/04815ac00cb8dbcafdf965d84751877efd01b0e7))
* DevSecOps World Tour City Page | DC updates ([5472591](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5472591d8838fcda82e45924e19b7b8c6553c043))
* DevSecOps World Tour City Pages | Paris updates ([8e6689e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8e6689e5e207b7b25ff0a67e03b92883c954574c))
* Enterprise page migration ([1f3068f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1f3068f76642f3ef5caa98a19313160894d4b49d))
* **localization:** Noriko edit Gartner title on solutions/delivery-automation page ([a0a9be2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a0a9be2fafaf45926f00d59542497adebc79abc1))
* **localization:** Noriko edit Gartner title on the gitlab-duo page ([708ffcc](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/708ffccf488a8d56b2a01748f6a14dae8107a4cb))
* **localization:** Noriko edits about.gitlab.com/ja-jp/pricing/ page. ([a5238dc](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a5238dc8a43a81bc08131f18fb16c4739daea0c8))
* **localization:** Noriko edits Gartner article on the solutions/agile-delivery page ([a250f6b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a250f6bcc6b6c153b54593afe2fe148a1b7f132a))
* **localization:** Noriko edits Gartner title on solutions_security-compliance page ([7199e2b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7199e2bb49789ba2e6234951ff5ceb93064bbef4))
* **localization:** Noriko edits Japanese strings on the pricing page ([6446482](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/64464826ce8352f9d17797f8f93a332e5bcdb434))
* **localization:** Noriko edits sales page part 2. ([837a230](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/837a23080e85900eef57759edf75a0367d74fe53))
* **localization:** Noriko makes corrections on the solutions_gitops page to reflect gartner's request ([af4ed36](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/af4ed36dece51d694f0351653ef1385eb5228fa8))
* **localization:** Noriko makes corrections on the value-stream-management page to reflect gartner's request ([7153b19](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7153b194b09b9f0c260f0e52e5ec8712fe23da14))
* **localization:** Noriko updates Gartner article on the platform page ([189d998](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/189d9989e9967b9a6baf1cbd2ce92f9e67136b35))
* **localization:** Noriko updates Gartner article title on source-code-management-page ([bd016d0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/bd016d027ee8e9ba5186a0fe35b87e26014dc0e2))
* Migrate e-group and board pages to CMS ([cf54520](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cf545200aebf207a7ffb9b39d201328f47429f31))
* navigation update ([d73404c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d73404c13d94cd921d75804931382968e48da778))
* Resolve "[ENG] /gitlab-duo migration" ([adb7821](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/adb78210dd55906c0247e06113434853d33052ef))
* Resolve "[ENG] /partners/technology-partners/sub pages migration" ([8daf599](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8daf59971e25d9160e06063190cfb59da543d4c2))
* Restores events landing page ([6886f02](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6886f02fb35a446fb1e730ec7519d12624f938ee))
* Small business migration ([f0620a5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f0620a5593898913a67a0765a00993dcb7591b73))
* Updates pricing page features for our Ultimate tier ([2203e25](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2203e25cc966940bf35a93641d20d879fe99a468))
* Why GitLab tweaks ([c2337b5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c2337b5bc2a0cbecea76fc381cf09ede6c6a525d))


### 🐜 Bug Fixes

* Fixes time for World Tour Paris agenda ([549fe5d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/549fe5d66a040ef2d484f90049e40a1aff3c343c))
* Remove hreflang from Impressum/Mentions légales ([2cd7b1a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2cd7b1aae40e936f9f21e65fe32fdbdf676f3d8b))
* Resolve "[ENG][I18N] Add hreflang attribute to GitLab Duo page" ([01231e3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/01231e35c1235a44b0d85d7bafcb3f2457a8451f))
* Why GitLab seo/tracking ([60dada5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/60dada5b9ada7523610c534cf0b7d04cd602a0cf))


### 🗂️ Content change

* Adding GitLab Connect Long Beach to the events page. ([0308f1b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0308f1b68abff25fc8b87f1b94d535fc07fe5462))
* Fix old link to gitlab-foss ([63a190d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/63a190dd03ee2958ea80f8681dd3ae0335a161f1))
* Fixing typo from 'yout trial' to 'your trial' ([693afdc](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/693afdc907d826baef5082c523954fe70eb0538e))
* Fixing typos in FAQ section ([844f31a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/844f31acee3529b1f6862d6100ca5b63d6a32fb9))
* New Event added to event page: Blackhat Europe 2023 ([59b374b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/59b374bbc28c01151a9dfea28d6824e93f5ff2a8))
* Update de-de internal links on homepage ([184cd3d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/184cd3db2b8c73b037ec6c3f916a1a3152546370))
* Update event date for next hackathon ([fddabc3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fddabc3ea801ceb87bfe73d824c09d9f26fb21ba))
* **why-gitlab:** changing 'enterprise' step to 'planning' ([4602bac](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4602bac99ac609f475ecb3fb641e6f1edbe17297))

## [2.9.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v2.8.0...v2.9.0) (2023-10-06)


### 🌍 i18n change

* Update home page and pricing page ([e090dff](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e090dff78d54de3af17397d0bdcf7b826d7803ec)), closes [#2675](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2675)


### 🐜 Bug Fixes

* Adds error handling for asyncData in /install/ce-or-ee/ page ([3ff1afb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3ff1afbb8536e62209a35b44f82ed20a714c1914))
* Adds error handling for asyncData in /install/ce-or-ee/ page ([d1277d4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d1277d495d93270c9ee7cdfee9a631f5ded0d146))
* **events:** Update speaker for Dallas DevSecOps World Tour page ([9224582](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9224582a2c37cb1c70fe490cf8c879fca636b70d))


### 🗂️ Content change

* Adding 2 webcasts to GitLab Events page ([582b574](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/582b574e6ca6aa529d50605d0d9693a7dd43d67e))
* Adds new conference to events page ([a97a058](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a97a058ea92dfb5d1186e1368c7a81fea5ec7dc4))
* Created new event website entry - Software Supply Chain Security Workshop ([b7bd4b5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b7bd4b5cc038aba3d0f0ce7579cb9b38e5018791))
* migrate english privacy page over to buyer experience from www ([7375962](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7375962b9f53dc067eb6b95717f4891780872f0f)), closes [#2890](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2890)
* New event added to website - CLoud Expo Europe Paris ([863ccb3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/863ccb3b191ad653ec37f4821cf31d41ecd5171e))
* Radovan Bacovic: Added PyCon Sweden event ([14a8597](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/14a8597959fa78e8eee347f4f1252c167eacd905))
* **seo:** semantic fixes and seo improvements to select topics pages ([d724a0d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d724a0d7e5f02ba2c44090d62c0dd0a40afa0adc))
* Update filter on features page ([ef44d7d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ef44d7d6f37cabe919a58da28c6d282a9b63b41d))


### 🚀 Features

* [Buyer Experience] - Navigation release 6.0.0 ([cb52f87](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cb52f876c8ac94f1bbad53e6883614e4a69a9749))
* /dedicated migration ([d7cc668](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d7cc668ef18753f70c9ee2788edd9e1ea9362b6f))
* creating Nonprofit join page ([8a0e6b9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8a0e6b93aaccbecdb16e13be27815d386ed01d78))
* **localization:** Copywriter changes to Japanese platform page ([10f4595](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/10f45955aab335cb7ef36eed33fc5f7d73804a8d))
* **localization:** Copywriter edits to Enterprise page ([a543107](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a54310789bd276a2a71d68be7f051a04ad4c60a3))
* **localization:** Ja/jp copywriter edits to homepage 5/5 ([bc953f4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/bc953f42ab52fe1037c504a41e4f9efe1bd73676))
* **localization:** Ja/jp copywriter edits to sales page ([3a3e4a3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3a3e4a363938b554009f615959b4bdb6b40d62c3))
* **localization:** Noriko changes some strings on homepage ([e53cf5c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e53cf5cd1ab2d20e602a7ea3ff481d91c18fee91))
* **localization:** Noriko changes some strings on the sales page. ([b20a0c4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b20a0c40f5bdd318ad64319a01daf0afa4e8e778))
* **localization:** Noriko edits "about.gitlab.com/ja-jp/software-faster" page. ([2d5e12d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2d5e12d550fe7b580d9cabf293d7e9f498e1882b))
* **localization:** Noriko edits Japanese strings on the small business page. ([65612d2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/65612d2a537ed18d46b6682df608cd62c2926f16))
* **localization:** Noriko edits Sales page. ([1ec9338](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1ec9338c115ad839275e2fed1169014898751b5d))
* **localization:** Noriko makes corrections on the homepage Gartner analysts card ([134575e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/134575e255184a0de7930d504b95e92717ae24e8))
* **localization:** Noriko’s 2nd LQA on the top page ([0b178e7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0b178e7d8c3fe375616496a27c0072b4a9bba7ec))
* migrate /customers to contentful ([a9ad254](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a9ad254ff899991636bec897ec04817d86f54a07)), closes [#2863](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2863)
* migrate software-faster page to contentful ([7f14daa](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7f14daaea359f03f741867d810e21797daa2e881))
* migrate terms page to contentful ([56fac00](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/56fac001c4606a9a52789fda94ee959f8c87a671))
* Removes LaunchDarkly experiment from Homepage Hero ([48dd84b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/48dd84b2d739b9264440562e2f17aaf0a96e29d3))
* Resolve "[ENG] Add new GitLab Duo logo to the Duo landing page" ([eec84a3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/eec84a3d43f3c540e7c6b071aa464787e8b8bf19))
* Resolve "[ENG] Solutions landing page" ([3c66aff](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3c66aff088858e0de35628c18e0d606862e2d42e))
* Resolves "[ENG] /solutions/code-suggestions migration" ([0c15d9c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0c15d9c84249bbf6255e4b79690ec38ce69c5c85))
* Update dallas registration form ([1c363e9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1c363e933a7eadfa2bcc9726f19ed9a2bec24e44))
* Updates World Tour DC speakers ([ba7acf2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ba7acf2fd2befa7943398d85236838272c73f35b))

## [2.8.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v2.7.0...v2.8.0) (2023-09-22)


### ⚡ Performance Improvements

* **navigation:** add navigation skeleton to mitigate content layout shift ([42a9484](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/42a948404a3ca96bd7d23bc919c3dec26e0eadf9))


### 🌍 i18n change

* LQA updates ([1da69fb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1da69fbf71610f50163dea8ea83179e7cd15d52b))


### 🐜 Bug Fixes

* **events:** Update date in executive DC page ([29d050d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/29d050dfdae4915ea62cad2f85c78d30dccccecf))
* Fix title for SEO metadata in Resolve "[ENG] install/ce-or-ee migrate to Contentful" ([7d5a3e0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7d5a3e08d707d518bb833acf9563386c305bac09))
* **navigation:** fixes minimal footer view source button ([018673c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/018673cbefeae94afa6e2bd4ea47e86ccca040ae))
* Resolve "Localized pages company culture  missing content" ([1a12c84](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1a12c84ee9c6c653d2c1ebe03bf5b5ca103fbbb4))
* Resolve Follow-up from "Resolve "[ENG] Solutions Landing page / Analytics and Insights" ([ce744be](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ce744be5c19ce764f3902006ed69a43b76047129))
* Resolve Follow-up from "Resolve "[ENG] Solutions Landing page / Analytics and Insights" ([ee420e7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ee420e7965f2b14f26929ece2e2b58823881146a))
* update link on licensing faq to have anchor link work properly ([11e248a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/11e248a33302336cacb82c08d498bc5a2372e0ea))


### 🗂️ Content change

* add testifysec to apps.yml ([2671eae](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2671eae42e853406a89b44b9faf7d63c6b35c44b))
* Added new analyst 451 research report in resources ([9b4c656](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9b4c656ab881a83882b1968481cc78f7b8338969))
* Fix typo on Nonprofit page ([5a07161](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5a07161e2f0b30f8128bff27c7fce04364110b29))
* New conference added to events page ([53d1b1b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/53d1b1b58ccc4c1ed74a2e13fb9ac5f0f5a56754))
* Update pricing page i18n ([4fefa20](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4fefa20e775cd9d00d0d1548b5aba25ef13ada89))
* Update security & compliance solution page resources ([2a427f1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2a427f14897d1a34d4cf4d75c9796d0cd346b29c))
* Update solutions links ([0ca4383](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0ca4383e73bedec22b637a1dc164ae33e25ca95d))
* Updating one of the informational bullet listed under the fireside chat question ([2dc9fd8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2dc9fd8556c9a4e5b3c181b46538929b2ca0ff24))


### 🚀 Features

* Create and implement "Take GitLab for a Spin" entries ([dc412ea](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/dc412ea631ff37a45fdb64e69e1faf47f3af9c0c))
* **devsecops world tour:** Resolve "[ENG] DevSecOps World Tour Executive Experience Page | DC" ([129927f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/129927f37692e109bfe9c65bc257a3be971ad840))
* **localization:** Updating Japanese translations of homepage ([e3a7409](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e3a7409d4c197d55fed8e359f321e37fd266345a))
* **localization:** Updating Japanese translations of Platform oage ([f4457ea](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f4457eaa6188af496abd550204d0263939b1ddde))
* LQA Marketing Site Solutions - September ([d1df1b5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d1df1b5f0cc14885504cfe5d02cf81775652ff3d))
* nav release 5.0.3 ([616f60c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/616f60c53121e1cf9b74d271779fa50482a73fcf))
* Resolve "[ENG] install/ce-or-ee migrate to Contentful" ([87f6ea7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/87f6ea79eb0c338e034e20d76286a4cd39610a70))
* Update platform page ([355c7e1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/355c7e12e4841aa24c3977ab01e3f74e14c887c2))


### ♻️ Refactors

* Optimizing Software Faster hero ([d501e11](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d501e11e4ca32bba6e7b6d41739c4f6fdaed6a8f))

## [2.7.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v2.6.0...v2.7.0) (2023-09-08)


### 🐜 Bug Fixes

* force the cache flush by adding comment to page template ([347e9e6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/347e9e6fc3f058b2d3b1a6b4ab8d9ac8244c7bf5))
* Update noindex to add nofollow ([2ae88b5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2ae88b567eb4ca7865ba82aab63525b7f0968f04))


### 🌍 i18n change

* **free trial:** remove hardcoded free trial button text ([fbd3d0e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fbd3d0e694edb4322b11d356a149d1785edebbce))
* **free trial:** remove hardcoded free trial button text ([d38e132](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d38e13254d5e8a40bf3ee9bd923184e45f6e2ca4))


### 🗂️ Content change

* Add Data Science Conference Europe 2023 to events ([a42367d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a42367d9fa18b8b44e77582b8fb66ea4bf89ac65))
* Add gitops i18n files ([be071ca](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/be071caf613cdd77db331868e10dfa57a63b0ecd))
* Added new event details - GitHub to GitLab Migration Workshop ([6a1cd58](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6a1cd58b9cb17c3004a2a4070cbf86030346e99b))
* Fix minor grammar, errors, naming on the pricing page ([4546687](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4546687f27e751b50cb2b27f99e47fbebb4cda97))
* Resolve "[ENG][I18N] Pricing content sync - August 14th, 2023" ([8019eb3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8019eb3070805d08c79edb6b0a9b7ce33326f9c3))
* Resolve "VSA content update" ([467c157](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/467c1572a202a95099b25933e667d811c20e7df9)), closes [#2814](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2814)
* Summit page content update ([31df8a1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/31df8a16ed3c63877e5a82a79d046b7f6210de0a))
* update copy ([24313c8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/24313c876ca2862a9347ec559123162e635b1c9e))
* update speaker information in Berlin and Paris ([662b3ee](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/662b3ee75e024539297fe0a54df4a609f99b5971))
* why gitlab  ([ff4220c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ff4220c2a32ef8888c120786fecf14f9056bee38)), closes [#2817](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2817)


### 🚀 Features

* Add dallas exec page ([bf60e58](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/bf60e5840c2643f033bf9e874c2a170ca7df498d))
* Add NY Executive DSOWT page ([946d7f7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/946d7f73ffeab65449fdf2c74fbdd6e757aaca08))
* Change LighthouseCI to start measuring mobile instead of desktop ([e30fdcf](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e30fdcf7414c31dbb05a423b85d8c4f31913e403))
* DevSecOps Survey - AI Tab ([b3afe10](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b3afe10406529c43e75405bf227f2fa26e6510b3))
* non-profit landing page ([94c9352](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/94c935242201ba64eb26d021228d9a545b7484de))
* Resolve "[ENG] Platform page iteration" ([d7b4bbd](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d7b4bbdccded9ebc94e8e14c033ed59ad0ee0a3b))

## [2.6.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v2.5.0...v2.6.0) (2023-08-25)


### 📝 Documentation

* Update docs for semantic changelog commits ([3f81bfd](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3f81bfde4906d3df8fdf173a97a6521c5f5dfa2f))


### 🌍 i18n change

* Add translations to Solutions - Kubernetes page ([ff4bf5c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ff4bf5c8222cb1e9283841164ef0baf903cff9d8))
* localize all solutions pages ([4febf52](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4febf52e459f30ca590f1035f4273f8b2ca3bf58))
* localize all solutions pages ([c9d1507](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c9d1507d2be6a3d8144543294bc3708d72707f24))
* pricing content sync ([b80d469](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b80d469028cba5d3ac84c01fbd3bbb5a855ee319))


### ⚡ Performance Improvements

* **homepage:** preload above-the-fold images and lazy load below; optimize navigation image ([69cf18c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/69cf18cf32f99fb21a1da4c3d012bc95023eb2a4))


### 🚀 Features

* GitOps page refresh ([305bed1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/305bed1344f5e83b8e3f6e9b55aa54c66c650eab))
* Implement Contentful's JS module into BE ([7e1bddf](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7e1bddf41ee3d0035d1106ba602e5da50b1a5edc))
* **localization prep:** Refactoring calculator roi components ([2f4aa9f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2f4aa9f68bbb92f29877d57a187ec0ed9c6a5204))
* push minor version of navigation ([1447954](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1447954a35c404b163ecf39c36459e7df4fabd3c))
* remove personalization from navigation ([14f7a60](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/14f7a608ba91c1383052143f90609a3afc8b9e59))
* Resolve "[ENG] Add video to hero section /dedicated" ([479d379](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/479d379c7ed10cef8e1ef1fd95dbe9ae803a33bf))
* Resolve "[ENG] Change URL for CI/CD solutions page" ([bf6bc0a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/bf6bc0a341b3cc7dfdfa21a6cda91b2f36dd61a2))
* Resolve "[ENG] Solutions Landing page / Analytics and Insights" ([9ffce34](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9ffce34bafd429c6adfc7ac700db4fab8960f6d9))


### 🐜 Bug Fixes

* add href links to i18n GitLab Duo feature cards ([ef19fb9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ef19fb92f0e5e13759bd22e019fa911dfb0f9a5d))
* Anchor links don't work for i18n /pricing ([a8ede17](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a8ede172de29824e683339e61852bab6beb4d56b))
* GA4: gtag code revision ([ab3644c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ab3644cf97ac8ccb38bf232626278d0597ce8ae6))
* Resolve "Resolve Functional bugs" ([01d7d3c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/01d7d3cdd2b2a3768f518fcd882cae4d4636f755)), closes [#2719](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2719)
* Resolve i18n data tracking ([a2394f3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a2394f3c0d2ce0313fdd64c7b85c2db0a7d4dd20)), closes [#2718](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2718)
* Update sitemap with excluded paths ([b3bb037](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b3bb037f7b4f8d938af506fb06233b05a4b514c2))


### 🗂️ Content change

* add closing speaker to Mountain View ([e99536d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e99536dba00053aeabd91b23f09e3166b29f47ea))
* Add text below forms on DSOWT exec pages ([bc25227](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/bc25227ad7f88359696c7196167a4223babcd5ec))
* Add two upcoming webcast events ([c863f58](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c863f5896a9f09f8535b168b1ebb4e19083f947c))
* Added link to NY event page in world tour landing page ([9b6aad8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9b6aad81423e1a7a927097ea274e4c74f660afa6))
* Adding DevOpsDays DC to the events page. ([2a48760](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2a48760dfc36e622ad548a8c5e35bc9602bd8038))
* Adding in Compliance Tech Demo into events page ([13d3a9b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/13d3a9b8e3bae2cb4895d48db767d45cb513a9e3))
* Adds new workshop to events page ([036c65f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/036c65fafb5df6bb2809265108f54180ac40435e))
* bump nav to 4.1.5 ([75aa46c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/75aa46cdd054f5df1f7c3ae2334e2794cf5eeee1))
* Deleting GBI CISO Evening Gathering Los Angeles as he event was canceled. ([64ed927](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/64ed927e019f530d3458da05d7ce6e0431acadb6))
* NYC follow up updates ([ae9c241](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ae9c241d37b34676f625d5d741a4ce3edd77594c))
* **services:** update services page content ([e39efe5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e39efe599626b4773e02e352ce452abfad7f8bd9))
* update Bringing GitLab to work description ([19da4fe](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/19da4fe9dabf915fdcb5dbba3b7214dc664fe1c8))
* Update file apps.yml ([06c4df6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/06c4df6160538bcba617bdb08f29361b2e105a85))
* Update link on GCN page ([7f471db](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7f471db4840f27cac6083f6a12e4deb19fc3bca4))
* Update location devsecops world tour new york ([0bc0177](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0bc01772225a7dc2e5ec27de244a3c073a8203ca))
* Update speaker sessions on Google Cloud Next page ([bb6fea8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/bb6fea8801361d4e5a16f508aec39c19a1106194))
* update terms of use page ([6f0afe4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6f0afe43d206edf044f6a5b7491431863c0ce8cb))
* Updating Events page with Q3 APAC activities ([1b2060d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1b2060d5a225fb08dd0976261f5145f2fc5b2554))
* version and lock examples for installation ([80dd7f6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/80dd7f6009e1d34529a26a1e1bfffba2a6297d3c))

## [2.5.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v2.4.0...v2.5.0) (2023-08-11)


### 📝 Documentation

* **/index redirects:** Add "/index" sufffixed pages redirect docs ([195bd7c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/195bd7c11a773dcb043c411a7d3d7d982540644f))


### 🌍 i18n change

* add translations for all-remote company page ([db49f8f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/db49f8f1e52b43cd9ebe6f8ddfcfa4950f9dca81))
* add translations for education - join page ([900c9b1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/900c9b186b23cbc99543f18013ac29f62c5f5213))
* add translations for education - join page ([9e6502c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9e6502cd39a9b028582ff5069eec143e5af5112e))
* add translations for why gitlab ([81df187](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/81df1878b3aebaa60dfae7f5c18d49406b6b72b4))
* add translations for why gitlab ([8141d04](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8141d04c22b1663550f65270e6d338b2996eeb33))


### 🐜 Bug Fixes

* Fixes hero image for Irvine DevSecOps World Tour page ([858207f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/858207f331bd6d9e79ba1046ab5834a46cac7515))
* **Ot test:** Revert Adding OT to terms ([26fd71e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/26fd71eced12df735791a1a13582c30fde938198))
* **partners:** anchor link functions broken by id that started with number ([b7b4939](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b7b493953f82fffdb15baf7c5c75d34d0c76d4b4))
* Remove trailing backticks in README ([9842bc9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9842bc97bc360d425fc187d9a296497aca85ed9a))
* Resolve "[ENG] Updating DevSecOps World Tour Palo Alto to DevSecOps World Tour Mountain View" ([fe53499](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fe5349923338e5de2511f8c3115a2372154b2c36))
* revert recent static href localization update ([94cbe03](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/94cbe033267dad8c5356537456042744103ad09f))
* **software-faster:** Removes unavailable customer case and make featured content clickable ([fffbc32](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fffbc3279d2f23ae892f62083ed0b5ed19c9d762))
* **software-faster:** Removes unavailable customer case and make featured content clickable ([66ffd21](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/66ffd2110e0bebcd61b95e5b8bef3b8311501272))
* Update exec event pages noindex meta tag ([d68e891](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d68e891723667f4c6283860507c95c4f596f7ed6))


### 🚀 Features

* [ENG] /customers/paessler to migrate to BE ([ef85c6b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ef85c6b1e42deb10a20a755e6497ed4eaf953685))
* Add exec form registration to DSOWT pages ([ef8e34c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ef8e34c84f04f83f02de5440c5f83c80c8d05b1a))
* add germany impressum page ([65291ef](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/65291efa6aa3ab4c100beca135b41679dc7630ff))
* Add mentions legales page ([e24fe26](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e24fe263168e927125ed1ea7380e6d0606e382bb))
* **adding page:** "[ENG] DevSecOps World Tour City Pages | Palo Alto" ([6220c74](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6220c746fd1eb534d28fb66181874eaca0bc21e3))
* AI in DevOps topics page ([7a1b000](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7a1b00034c7c70ab1379cca8a76c78113d7b31ad))
* **customers:** Add the last mile case study ([1d9abb8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1d9abb8e46ae1410e4dd3b1eae5816c610612b52))
* **customers:** Add the last mile case study ([4b7f587](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4b7f5874a5eeb53b9a572267bce525ae5ef887c8))
* **customers:** Push dataLayer in customers/all filters ([44c1409](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/44c14099627e4bc4962b2c59ffaef38f8531d9e8))
* **event world tour page:** "[ENG] Updating DevSecOps World Tour Palo Alto to DevSecOps World Tour Mountain View" ([24c3b31](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/24c3b3124bdd6a2a771540575aa4dc7cd5cadf03))
* **form:** Add fallback error message ([aee14d8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/aee14d887477b2de9e9129555eacd9d4515b0de2))
* Migrate Keytrade bank to BEx ([7c0259c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7c0259caa8e59dd546c21441263ca9c646750e1f))
* Resolve "[ENG] /customers/bi_worldwide to migrate to BE" ([07a1a06](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/07a1a066dfed40e8928c9d71588836c3a103779f))
* Resolve "[ENG] /customers/duncan-aviation to migrate to BE" ([b0d8d24](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b0d8d24b73fe000eb7bef75fd37483cbc9576d2d))
* Resolve "[ENG] /customers/ibeo_automotive to migrate to BE" ([2eba4f5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2eba4f516af80ea28d410cbea6f87288869ca38c))
* Resolve "[ENG] /customers/sopra_steria to migrate to BE" ([dca3f89](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/dca3f89248da61fef32d458e73c0c87a5b061dde))
* Resolve "[ENG] /customers/trendyol to migrate to BE" ([2177359](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/21773597bd5a9e54a07f967b06cfc28576ae556b))
* Resolve "[ENG] DevSecOps World Tour City Pages | Irvine" ([bb09fbd](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/bb09fbd5d41b9bd8ac72dedb9be891597e77ef63))
* Resolve "[ENG] Google Cloud Next '23 - Event Landing Page" ([5e41996](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5e4199698910499a84c9fb971bab51e928183884))
* Resolve "Add class for full background within a contained section" ([b02c5c8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b02c5c8890c6464cfa9def2fa5eb291f1f8decb7))
* SAST vs DAST topic page ([aff4b72](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/aff4b7200d8d178c5d4d2b98a25a5cd074fff828))
* Update GitLab on Google Cloud Landing Page ([d8609c2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d8609c25a0529833bf72aad2b97d2475cbf42830))


### 🗂️ Content change

* add aikido as technology partner ([51bf3b5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/51bf3b57d5f17bee5101eb17a8a2eeb0e3472744))
* add bearer to apps.yml ([902fa74](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/902fa7407e44dcdc680b177a7621bf1677af446b))
* add bearer to apps.yml ([8e92c67](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8e92c671d29c6509d3bb59ab0974221f05f61b60))
* Add meetup to events page ([4ebda5e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4ebda5e190d4eef79faf3e57d8dc7f869f1d1a00))
* Adding in Intro to Security Tech Demo to events page ([e923758](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e92375866f69b089a66d99b7f88b39226f053236))
* Adds 2 new events to the events page ([f1a8814](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f1a8814c20d78913a742135fd49589b768e4fd0e))
* Adds new event to events page ([05437de](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/05437dea5cde6c6d9234708ac4811aba3989ba0d))
* Adds new workshop to events page ([f0d449e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f0d449e197fa0dcec4e12c3f9d362f2fdd092946))
* Adds new workshop to events page ([812113f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/812113fd1de1441dd69a1f168d9164f1ecf6b344))
* Change Technical Account Manager to Customer Success Manager in Why Ultimate page ([e2aade5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e2aade579f9786764e9773c312b906d335bd1e5c))
* **events:** add google cloud next 23 event ([dba513a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/dba513a797dfcb8b3282349c44fbfe47efd71873))
* Removing AWS Community Day as the event has been cancelled. ([108ac49](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/108ac4923c7c3f258fc5333e90cc8101268adcd2))
* **resources:** quarterly release webcast ([66933c2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/66933c2db5d73d01003710dd44d473ba28aaa200))
* Restaurant Name was misspelled. ([cde1fa1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cde1fa1aa534c4f3aac759b22d4fce5c8cc68a01))
* Update hackathon event dates ([4542ac8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4542ac87f15fa8b8a2e41a503c0676f31a6733dc))
* Update location for Google event ([d085ba2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d085ba234e4ea1f9282f6dbca41adbe15c7e0ebd))
* update melbourne speakers ([76a333f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/76a333fbabfd303f32c882c6c5083ce3789443f8))

## [2.4.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v2.3.0...v2.4.0) (2023-07-28)


### ♻️ Refactors

* Move esg page ([53d13d0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/53d13d0d3794dd05f66a96472317803703579011))
* Resolve "[ENG] Bug fix extend background color across section on Dedicated page" ([565fde4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/565fde4fc85d6ccc62cf06e14cacff4c403783b7))


### 🐜 Bug Fixes

* **company page:** Fixing buttons and image ([efe48e4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/efe48e4a2d33d50d5a8e897a7b4aaf76eece4fcd))
* **devops platform topics:** markdown formatting; html hydration; side nav links ([0c4054c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0c4054cc33c13c89fba792c39760ce3679671df9))
* ga-attributes translation bug ([b4ad253](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b4ad2530701f63dba9b1b0be53937640ec8c1df6))
* ignore anchor links in link localization function ([683070a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/683070ac52e6f061023ae598666f913d98e61306))
* LQA2 Marketing Site - German ([14e4c41](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/14e4c41abf1c3a3eab1e3ec91c5326d74a918ed2))
* misdirected hyperlink and update tracking codes ([225f00c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/225f00c6e02c74c00dba9d5ea9565e416c06516f))
* **typo:** lowercase 'L' in 'FLow' ([075db59](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/075db596656284bf3b70084a521cdaf0d32f52c0))


### 🌍 i18n change

* add translations for code suggestions page ([92597df](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/92597df3f8f5b9dd3c068fc089016cbcb44a43b4))
* add translations for code suggestions page ([e57a43b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e57a43b22ee5bb3dc22a209352b81554bdf4a60c))
* add translations for free trial - devsecops page ([657aa4b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/657aa4b75758ce8070fb72ee838f669b207f1445))
* localize company page ([46f6cbb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/46f6cbb56578d1b91704b0eaab7335bb48c00967))
* localize partners page ([e9ab309](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e9ab309df5b9be14a3f9ca36da01c2e1b5b5a800))
* Pushing translated pricing page live ([1409e36](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1409e3605e49f6e988dbfb5a3d5f54492f72bd43))


### 🗂️ Content change

* Add 1password to dev tools ([842896f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/842896f259a66cee16d27be8a5b283d834d3acf0))
* Add DrupalCon Lille 2023 to the event calendar ([38e5e5b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/38e5e5b2a8c04d578b012f6f4069c2cfe9114a80))
* Add event OutSystems Product Design Unwrapped ([e1b43fb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e1b43fb43de141f3460db78fff4d5537af59b24f))
* add informational disclaimer to localized privacy and cookies pages ([42dad60](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/42dad607024452a95683a801d607e3915cd786fd))
* add speaker to melbourne page ([0577870](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0577870994f50a322859145462d8cb3bf0dd43b8))
* Added DevSecOps business success ebook to Resources page ([f5edf3d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f5edf3d2fa552c273d5f1aa295c21613e72794e2))
* Added new Event AWS PubSec Rome ([184fbce](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/184fbce765bccdae24a1305c27c27abb9b6c3cf2))
* Adding Annual CISO Summit Half Moon Bay to the events page. ([04e1231](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/04e1231ae34ae829ce23f7a892b4e90ca7462101))
* Adding AWS Community Day Bay Area to the events page. ([23833ec](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/23833ecbe237b87c4480df874165fceae234ae0a))
* Adding GBI CISO Evening Gathering Los Angeles to the events page. ([7a53901](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7a539015db847ce5f9f8e9bcd4fe97e946f53352))
* Fix icon build errors and slippers upgrade ([98c0837](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/98c0837ecea1390446a426418861ee831e71ee19))
* Fix malformed URL link ([7705f25](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7705f25a0f2aecb79713bfa17360aa564e3fefef))
* Update 1password logo ([e184152](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e1841522001d5099b9c5f270a7490fb0c1570b15))
* Update abstract for the Welcome and Product keynote ([029940d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/029940dedb8543c077a69a1f11e7ba52e569d847))
* Updated the open graph image for our Software Faster page ([a807332](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a807332f6290af1648ae6ea0683240259183d819))
* Updated the open graph image for our Software Faster page ([ee65d3c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ee65d3cc3bec2420c29478f91f08c38728376887))
* Updates based on recent Plan features ([ad1b818](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ad1b8185c71950d85e8401fc545cb5db00ec0f32))


### 🚀 Features

* Add french localized company preference center, fix option spacing ([c0c634b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c0c634b7686eabf0bce64e26a9b7946e1067564d))
* **customer cases:** Add inventx customer case ([7a178ec](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7a178ecbe53ffc59eea04056627d286ad1c3c73b))
* **customer cases:** Add inventx customer case ([d18ea72](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d18ea72f4fa42de2c08e7da9a50e99e3c82d1dc4))
* Esg landing page ([e4f708a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e4f708a4e31506d27353a5f8af9220df2983cc95))
* **i18n:** Resolve "[ENG][I18N] GitLab Duo" ([4d05907](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4d05907436a03367e3e5d89c8ef4941afe8914ea))
* Navigation release 4.1.3 ([aa61654](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/aa61654f814d8c07efc776ff46073d75d8aa2974))
* Remove form from 2022 survey page ([1be325c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1be325c7c03362d59267eb367c5c599f11dc3091))
* Resolve "[ENG] /customers/keytradebank to migrate to BE" ([678e549](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/678e549d048593f3d000ca8c89679efa4c8f4768))
* Resolve "[ENG] Add multi step form to Melbourne World Tour page" ([e9c3e25](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e9c3e255d1e4a628a13f54a84a9a9f139072b493))
* Resolve "[ENG] Bug fix header spacing for education page hero" ([c3e08b5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c3e08b52b41a2232379d87e227a48a0e091da090))
* Resolve "Landing Page: Value Stream Assessment" ([04f874b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/04f874b9ad6280a562e4fe08dd47c4bcd333c16a))
* Run Google Lighthouse on every main branch commit ([fff1eeb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fff1eebc039d88a8b64635eb5769c5cd6f0abae4))
* Update root lang property based on locale ([5cf3557](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5cf3557587f7f81aaaa0f950e9f8de0a9fa292f4))

## [2.3.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v2.2.0...v2.3.0) (2023-07-14)


### 🐜 Bug Fixes

* Bug missing container /solutions/continuous-software-compliance/ ([50d8843](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/50d8843ff38c71680b321b6c41245765d524a0cc))
* **code suggestions:** Fix bracket colors ([af968e3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/af968e3643263d6a94186a97da612638981d3375))
* **customers:** Fix glympse logo ([706290b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/706290bec452c3420bccc61fe4dca4142023b1d0))
* Fix View Page Source and Edit Links on English Homepage ([9816c8c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9816c8ca913f5f5573af1f5d013332615f58c6ea))
* Incorrect font stack loads for 1 sec to user ([154e55a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/154e55afe966d1f70b3a1b0d4ef0d127bb896b19))
* Resolve "[ENG] Topics page card incorrect URLs" ([65782ff](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/65782ff12b0bccc213f6d08750706fca41d07a3e))


### ♻️ Refactors

* Remove unused grid space from customer logo carousel ([a50f5ba](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a50f5ba7e27058c7de27c78cb082aede9acf9f4d))
* Resolve "[ENG] Fix customers logos with horizontal blank space" ([4f4bced](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4f4bced7d104d1d25d9d3955146b1e09643859bf))


### 🚀 Features

* add integrations page ([a75673a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a75673a7f7662fcbfffcbdf156c91a1b7e17ccc1))
* **gitlab duo:** Add links to features cards and update Header ([b6f0038](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b6f0038451924295b00e79234cf6e1ba698d5603))
* **gitlab duo:** Add links to features cards and update Header ([001a960](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/001a960d58e8da69697634305a4ab9f8734f5ab6))
* Resolve "[ENG] /customers/jasper-solutions to migrate to BE" ([5766d42](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5766d423c0bb2ec40c45bc8d18aa3912e2d4d2e7))
* Resolve "[ENG] /customers/knowbe4 to migrate to BE" ([0736406](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/07364064f9282935f32b7a4902b2f19ed2dcfab2))
* Resolve "[ENG] /customers/parimatch to migrate to BE" ([4765eff](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4765effb156ed164429d07d593ce8b33c1600162))


### 🌍 i18n change

* localize privacy and cookie pages ([5d3e66c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5d3e66cb3471212423e9ee14abf8d17d6fabaa5b))
* Localized Company Preference Center - German, Japanese ([cf99582](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cf995822a8ab54f1e5a719a2f8b9e2311f774f2d))
* LQA2 - French pages - batch 2 ([6674495](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/66744953818b5934e4ebb5b567f7232527b8df86))
* LQA2 Marketing Site - French ([81416e0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/81416e03cd7a31790cb39aeb77aba981582cfb50))
* Static links for i18n in Buyer Experience ([d4d3e1e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d4d3e1ed38e98fe1a16e4e7a33624c295731d955))
* Static links for i18n in Buyer Experience ([d1543dc](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d1543dc64745a6a2ecd9820740762f0e52741035))


### 🗂️ Content change

* add AI Google Cloud fireside chat to events page ([ddf6561](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ddf6561a68d036bcb86888e22dd64aa7c85f8161))
* Add AlmaLinux and EL 9 package to install page ([0d64b24](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0d64b2447ce0a50b82f37491eeda8b2c1706ca62))
* Adding DevOps Connect with NADOG Irvine and fixing the NADOG Denver description and title ([c757e36](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c757e36b73233651854e2970fe054a07f6ad71c2))
* Adding ELC Annual SF Conference to the events page. ([ad9238c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ad9238c424d99843e1f6f7a761625c89aa9ffd55))
* Adding in GitOps tech demo to events page ([866e8b0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/866e8b09877773294bce100c1f110b73c087a505))
* Adding NADOG Denver to the events page. ([31b090e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/31b090eea8e83cd6b59ac9486aff05998817bc18))
* Adding Phoenix DevOps Connect with NADOG to the events page. ([88bb846](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/88bb8460b9093357079be2f4955b0f33733f39b3))
* Change title and description on innersource solutions page ([cd7a4cf](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cd7a4cf4da1da006b150d5bc297a31fb45a45d54))
* Fix helm chart registry Learn More link ([67288fa](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/67288fa630a6ab2d2331386eb0829400d4dc150d))
* Remove Git LFS for Package Stage ([f86b2da](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f86b2daa2e122d167e7175b6515f2e9ea91cea48))
* Update duplicate meta tag for open source partners ([4a947b2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4a947b2d9b461794f7d207ce8901c6f9a374d1f1))
* Update Events page with APAC Q3 Events ([d05849a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d05849ad3da2cd81594d4e671acab3211d76b180))
* Update image for Kate ([b67b42d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b67b42d3f56de4520e79327071bbd19be1d52479))
* Update join Gitlab for Startups meta tags ([bdbd7a7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/bdbd7a7871465cdb2fbc23399003d136c25a19e2))
* Update link to google cloud marketplace ([5ed5363](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5ed536348ad3126d8a77ac4b404b0195c622b38c))
* Update meta tags on customers ([a7052a2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a7052a2dde94e3247a017ba2fa03154cc3fcb686))
* update speaker ([0603e96](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0603e961b8f4e69fda1e902241dbfb5efa5baf00))
* update speakers for london page ([60192cf](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/60192cf71aca96b0d0945e8579406b7be8af2779))

## [2.2.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v2.1.0...v2.2.0) (2023-06-30)


### 🌍 i18n change

* **homepage:** human translated german, french, and japanese homepage content ([a77063a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a77063aef1da64aa9a8005b684c09dc40fc546b5))
* Middleware to redirect all BE links to a localized page ([4a1139f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4a1139f13327b3ce8e87068f9068d0750c3143fd))
* Middleware to redirect all BE links to a localized page ([06f7b9a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/06f7b9a4e5e590926d5398809e9779d041a0b094))


### 🗂️ Content change

* Add legacy testing agreement ([216694c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/216694c25e7e919b228ad1d9f114d464decbc501))
* Added new workshop event - AI in DevSecOps ([31d27b1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/31d27b1255b08de760fa98d86be4cd3c1ba1717c))
* GitLab Duo and Homepage Update ([77c6370](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/77c63709174b02eca14c050de6371eadd64356b3))
* Removed from Resources: Gartner 2021 Market Guide for DevOps Value Stream Delivery Platforms ([4b7fff8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4b7fff8f4c38cd6acd137f37d3573783c6be2e9f))
* Removing expired analyst reports ([a466714](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a466714d426a7fbcb30183a374709ab0228f38b7))
* Updates to startup program terms ([7c459c9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7c459c997021935cccff7c3e420d60d62e4d8b7f))


### 🚀 Features

* Add devops assessment page ([cc08061](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cc080610f20a605bf4ae042e215ab5de0e3d4ac9))
* How-to Videos Landing page ([cc0e860](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cc0e86018f55068e4315fa9c13f4d7059e362764))
* **localization:** Localize install page tier2 ([9a144de](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9a144def345d70e63f18e11f2166f53be7c226fe))
* Resolve "[ENG] & [UX] MVC2 Gartner landing page updates" ([bf80fe2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/bf80fe22d568e4517e33a8452fb321c9e07e07bc))
* Resolve "[ENG] Add Gartner link and copy to new Forrester CTA" ([264c87d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/264c87d25b5068fe1b4e016ce5a2529e13e60e24))
* **solutions:** Add Code Suggestions page and component ([2cf3f9c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2cf3f9c7521297064adba0ede1610bd39a9550e7))
* Update modals to close when clicking outside modal ([871fc91](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/871fc91397b54fe3d6b025e6cbedc1b79b50281e))


### 🐜 Bug Fixes

* Add alt tags to images on the home page ([020f210](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/020f2101580a9e5e1d0a7038d81398fe10797fb3))
* **customers:** Fix max width on customers logo ([4a5c17d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4a5c17d3bb96a8ba205a5bf8d7526f436b681fb4))
* Resolve "[ENG] GitLab Duo page horizontal scroll bug" ([f024149](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f0241490f3bfd66244a8b190dfbe0533578d6bc5))
* **topics:** remove link icon from page sections that do not have a header ([fc46a34](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fc46a340c7f914583f0f0b41dbabd42b9baa4bc9))

## [2.1.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v2.0.0...v2.1.0) (2023-06-16)


### 🌍 i18n change

* Add french human translated content for software faster page ([094a08d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/094a08dc5cef5aee63746bfaedbd3b5b37a87b39))
* add translated page titles to solutions - startups page ([5e12394](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5e12394e055e217d63b626b6c35434acb54c40c3))
* **content:** German Enterprise page - Human translation review ([1082c04](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1082c04ea399edbe7bd177d87e724ffa4fef8928))
* **content:** German Platform page - Human translation review ([f5d68b3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f5d68b3b17620a4c96452047a6eb836dcc95ea02))
* **content:** German Small Business page - Human translation review ([ee9563d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ee9563dab336bed8ddeade2febe58f1cae93ccef))
* **content:** Japanese DevOps Platform Topics page - Human translation review ([f9cae43](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f9cae4370c0a454204bb57160ac8784d0a529d24))
* **content:** Japanese Enterprise page - Human translation review ([b31a8f2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b31a8f20dbb7835616c2492dfe5cf58e418dd4a3))
* **content:** Japanese Platform page - Human translation review ([a9eab20](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a9eab20fbfe53b6990ab6b0c7bf32b323c66c615))
* **content:** Japanese Small Business page - Human translation review ([1a3d2f6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1a3d2f6e61ca06f4fd6304485fb58510b921f111))
* **content:** Japanese Software Faster page - Human translation review ([5dcc362](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5dcc362fb229039b0e1ce931a6ab7d61d6356588))
* **content:** Japanese TeamOps page - Human translation review ([9bc5bd8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9bc5bd8f10d58d1ffb2fb0d405617f7a0afb77d0))
* **Enterprise:** French Enterprise page - Human translation review ([abfac1f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/abfac1f01ddf72765aa1b77223cfcef2dbd95a1d))
* Resolve "French Get Started - Enterprise page - Human translation review" ([cdcde9c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cdcde9cf76a42da73cc6478d23f84c04cac3c62c))
* Resolve "German DevOps Platform Topics page - Human translation review" ([aa83c37](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/aa83c37b0493b213cd1c596c5d103b7a9262e278))
* Resolve "German Get Started - Enterprise page - Human translation review" ([dc440b3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/dc440b39a5541d22a295700c705456788be56310))
* Resolve "Japanese Get Started - Enterprise page - Human translation review" ([a81c778](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a81c778ac1b961bb7abd4a78f13114e9edb4adcb))
* Update french devops-platform topic page with human translated content ([8ee137b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8ee137b86ed7c68ffa851159ab0ea52329e38813))
* Update french platform page with human translated content ([64a79fd](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/64a79fd0e932ddf105cb33fc547957fffd479b4b))
* Update french small business content with human translated content ([53a44bc](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/53a44bc9ed77ba595eb09475c3e728cd7e722cb3))


### 🚀 Features

* Add code-suggestions component ([f7d984b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f7d984bcf0e22b0d38d36e1604cff92f03fd6410))
* **events:** city form closing option ([b99b754](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b99b75462ac7449bb8422e57001011f6463f7e96)), closes [#2552](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2552)
* Gartner Banner & 3.3.7 Navigation Release ([0506a57](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0506a57e777041cf6902638c85d4629b94447247)), closes [#2525](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2525)
* Navigation release 4.0.1 ([4bdfd6e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4bdfd6e83a86e338a9fec1b82e023faef73b65b1))
* New get-started/CI page ([ef2cd8c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ef2cd8ced11aa0d7522fcc2ac6818a820a1a5d43))
* Resolve "[ENG] /customers/curve to migrate to BE" ([fc16a29](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fc16a29ff864c0fc6d45c05ee34652177614007c))
* Resolve "[ENG] New Solutions page InnerSource" ([7898f61](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7898f61dde5b4859f6c85029af789a865559a0a4))
* Update dedicated page ([4ad04c3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4ad04c327b9471a1686201da7661390d0630e602))
* Update topics dynamic card logic to include subdirectories and add trailing slash to paths ([2b72587](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2b725876cc89fd24fb3d3eb09e9e59d084a9c5c7))
* World tour region grouping ([8af8f6f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8af8f6f340316431d9a857680fe82a5d1b39edbb))


### ♻️ Refactors

* **meta:** Hreflang attribute for english pages ([24a3ae7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/24a3ae7e939f77c23026ef5c3c933233995b8646))
* Resolve "[ENG] Refactor InnerSource page" ([086fe09](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/086fe09ff35f5a4dc0f2d4d1fb98d46c6d1d3307))


### 🗂️ Content change

* [ENG] /topics landing page links missing trailing slash ([75f6a1d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/75f6a1d552575108614f30800db9cf427a8f97c2))
* Add Gartner DevOps Platforms report to Resources page ([d95691d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d95691d40bca62a2a1f701c59a947f04edee2784))
* Adding AWS Summit São Paulo 2023 to the events page. ([9a1a73b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9a1a73bcebc351f2fb0b36b658a9c79b4abf382d))
* Adding Brazil Making the case for CI/CD in your Organization to the events page. ([5efc6a1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5efc6a1dd1ba331089c0a4e4bb87d2e11c7a794e))
* Adding GitLab Connect East Bay to the events page. ([cfd14e4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cfd14e494f5b0348627dab231732a0bc636d9a28))
* Adding Presentando el caso de CI/CD en tu organización to the events page. ([d05e023](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d05e02370e3759e9afafaf59416db7b1b32e42ed))
* DevSecOps Survey Q2 Updates ([1173cb3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1173cb30b142352a60267c948b510c93d2a069f4))
* **events:** Radovan Bacovic added Crunch Data Conference details ([4357d8c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4357d8c8d0c6ffcccfa4aecbe0743700a40d7dbd))
* fix internal redirects on homepage ([a1d213d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a1d213d7ef9abeb263f19ffbb881377c03dfa965))
* Fix invalid links to documentation on self-managed renewal ([c7820d0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c7820d03824ef349a82c2df2f5cc63c1b41b60e3))
* **open-source:** add logo for new partner Colmena ([d5ef314](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d5ef3144ab2ba6b63b445c8abf5830d91b34860d))
* Removing code suggestions from pricing page ([0f07647](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0f07647d0ddb31d823ff037a3ed24230985bcbea))
* Removing LATAM Making the case for CI/CD in your Organization from the events page. ([32425a8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/32425a82da92eddacb7fd767bd342dc15f65e451))
* **resources:** Add Gartner MQ AST report to Resources ([7ac0dbd](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7ac0dbd281d766c926ea66ee6930f5480e4c9371))
* **solutions:** Add Eclipse Foundation to open source partner page ([51d7c21](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/51d7c219ff6b25f6c5b32ddd0f072d1e516ef3f4))
* udpate start time for GitLab 16 ([1dff5e6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1dff5e61d91fc2f688d47ddf3631b4386fc56068))
* Update AWS re:inforce lightning talks ([9790c8a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9790c8ad2c9695e8bf774f83c56d811fec1fec78))
* Update broken handbook hyperlinks ([b074494](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b07449421f59dcb169692f536a4c701faad68b9c))
* update meta image ([2694695](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2694695e5c880566eeddbfb27f19efc9c8bacf63))
* update meta image ([32bc3ff](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/32bc3ff352d7461fe28ed82b5d7d7d9147360707))
* Updating the Chicago page's Keynote session to say Chicago and not Atlanta ([44a1fd4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/44a1fd40547e3d352c29e86b0662baa29c0cd6e3))


### 🐜 Bug Fixes

* for new ci page link ([0defadc](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0defadc748be3afb3b37f36a982ea8c5e486b11e))
* gitlab infomercial video not loading on enterprise page ([8cffc41](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8cffc41f28e6f53a649ef405152afd08281545cc))
* platform page de-de and ja-jp report cta ([428177e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/428177e9caf874cba087374010e2f97cce6fc83e))

## [2.0.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v1.10.0...v2.0.0) (2023-06-02)


### ⚠ BREAKING CHANGES

* **Localization:** Localized Marketing Site Release

### ⚡ Performance Improvements

* Resolve "[ENG] Add lazy loading to large payload pages" ([64684dc](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/64684dc485b98c229030d75be1bc22c9663ded0b))


### ♻️ Refactors

* Resolve "Remove Competitive Pages" ([f89ee11](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f89ee1166812ad903083ae5f3419ed96b43fe834))


### 🌍 i18n change

* i18n pages built that aren't actually i18n ([33817aa](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/33817aaecf064a5cfc17204f8251be13ca62d11e))
* Resolve "German Sales page - Human translation review" ([7ba4c2e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7ba4c2e5e6e46e78247ff078a811f05f6e721856))


### 🐜 Bug Fixes

* Better aligned headshots to avoid cropping in world tour city pages ([b20da97](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b20da97efbd91aa645ac41776bca6b768f9f42fd))
* **calculators:** retain campaign params when using calculator ([c61224a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c61224aec7b4703c298ab2b22f21707339056470))
* **case studies:** prevent logo svgs from rendering too small ([ea3fe5b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ea3fe5ba44e39697ee1e7105b672c70829da65f0))
* **homepage:** Fix Resources cards responsive styles in tablet ([4eeea44](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4eeea44cbd282bf216e95afc9ebe4777c8975460))
* **homepage:** Fix Resources cards responsive styles in tablet ([ad85d01](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ad85d015f6174a1bcfa017cd4e010e91f346b020))
* Loading marketo script on layout to prevent reference errors ([755b66c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/755b66c5d7818fa2e1195ea609f50183c8a98dda))
* remove references to MGA ([736b15f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/736b15feef185dc93466c17190e1905a2fcea07c))
* remove references to MGA ([072e325](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/072e325cf2d59fbb2f2e9b9f0e04bd581c5295d5))
* Resolve "[ENG] JS Bug MktoForms2 is not defined on all pages without a form on them" ([0d9910a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0d9910ae1ff674c355d4a4df86b3615acf0e93db))
* Revert marketo fix attempt on layout tempalte ([37a58da](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/37a58da004b3997645e0097fad0bbc846ba81980))
* **search:** sitesearchresult event ([4275b8b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4275b8b18a9cdc963c55809b05de6c12b023f1be))
* **search:** sitesearchresult event tracking ([a1dfbf1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a1dfbf12958b073eff78efb6cdd93ad2e9cc3ba4)), closes [#2464](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2464)
* set Marketo Forms 2 Cross Domain request proxy in default layout ([9624af8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9624af8fe8d10f7f930c90f13a5063b1ccc9d1be))


### 🚀 Features

* "AWS re:inforce event page" ([dfa3a49](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/dfa3a49f0dcc9e1f3e5494972f212a2eb2b6c37d))
* **case studies:** migrate BAB page to buyer experience ([fdc901b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fdc901bf466f90a9257df3672c3d7f230a7370b1))
* **case studies:** migrate dublin city university page to buyer experience ([58322ea](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/58322ea6477b346e4716d235c5626dab60c41a01))
* **case studies:** migrate everymatrix page to buyer experience ([be44c0c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/be44c0c717040487a8182d9ce27086e129a43053))
* **case studies:** migrate hotjar page to buyer experience ([3484c6e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3484c6e70c1ed31d14adcb0fa709ff70aa6f835d))
* **case studies:** migrate kiwi page to buyer experience ([e7fbc7b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e7fbc7b5924b64719ea89e13d5d3d76ea1b09869))
* **case studies:** migrate mpei page to buyer experience ([66a0c42](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/66a0c429fdec40f008d62dbc3af7930605a8b8d4))
* **case studies:** migrate new10 page to buyer experience ([29f48d4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/29f48d45708793ffcc848f517cc56da62814539a))
* **case studies:** migrate radiofrance page to buyer experience ([5174fcb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5174fcba65f7eeb3c484f7e640060f9b2eeaecd9))
* **customers:** migrate /customers/european-space-agency to BE ([584df51](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/584df517f2441f9b12c12be60819a87082cda9da)), closes [#2163](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2163)
* **customers:** migrate /customers/surf to BE ([e69bfa9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e69bfa93428d28cbc73c8e1061f72745cf2c3e3e)), closes [#2153](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2153)
* Deutsche Telekom case study ([aba3228](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/aba3228b1b6734e4c42db4189bb680199314e1f5))
* **Localization:** Localized Marketing Site Release ([86525c4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/86525c4fb1d021de13a84a9fb195bc97fc815147))
* Navigation release v4.0.0 ([27a98db](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/27a98db8488726885b29b4a739990eec450f7363))
* **nav:** navigation release 3.3.5 - be ([8ad0829](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8ad082993e10de56f6d41dda9f0817f8f8c337f2))
* **open-source:** Add Manjaro to open source partner roster ([e8e3ffa](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e8e3ffa516e95dfad889b233c8c83542762aa0a6))
* Remove hover states from ai cards ([08b6b8f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/08b6b8fbdac8a2a4fa1401e851b26058ba385619))
* Resolve "[ENG] /customers/anchormen to migrate to BE" ([2b7a909](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2b7a909aad6a731c7da8fc4006fe8e7137bf4044))
* Resolve "[ENG] /customers/bgs to migrate to BE" ([51fe9e3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/51fe9e3624f8aeb1ff89882cec3804a531c5b7a4))
* Resolve "[ENG] /customers/chorus to migrate to BE" ([bd3e03c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/bd3e03c77d07eefe89dd4724a1b88f6c69465ba8))
* Resolve "[ENG] Add CTA in hero /customers" ([a25b6d2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a25b6d22dd141db27a87662b5526d35bcb22b224))
* **solutions:** Remove hover states from ai cards ([abee4c9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/abee4c9e754d85f561ed2aed21f1a5b9ecda3356)), closes [#2489](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2489)
* **topics:** migrate 18 topics pages to buyer experience ([6ccd167](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6ccd1679e616ad060aec51f4a401bd275267866f))
* **topics:** migrate gitlab flow topics page to buyer experience ([313e0c0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/313e0c0554c3409120914340a25a94a6d7841979))
* University of Cambridge case study ([b9f7f51](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b9f7f51549b85824823274c9bcaff64d70292da5))


### 🗂️ Content change

* add ciso to egroup ([5a929cb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5a929cbd6acbabffb74cf49a926e68a266a47455))
* **event:** Add Hack in the Box event ([520c108](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/520c108fcb15c958417b15b3f81909560ed4a67e))
* **event:** Radovan Bacovic added an event (Budapest Data Forum) ([aeaef62](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/aeaef62e98ac8591c472298c4e235097f009fdf8))
* **homepage:** Add home pill ([4279cd6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4279cd6323989953dfb08e16b13219db16079500))
* Localized  Small Business Get Started page - German, Japanese, French ([f7f4a26](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f7f4a2642f0d5d80076cd4c32c0751189bce71c2))
* Moving analytics-request issue template under MS&A ([0f3dc03](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0f3dc038bb6ea8cf6d934f6aafe507fbcb994cee))
* **partners:** add resmo to technology partners page ([b04bed7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b04bed7ec048cf031892e6bd3af502f87c8b7c7d))
* Resolve "[ENG] Marketing messaging - replace "innovate" & "modernize" buzzwords with updated language" ([34a3c73](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/34a3c73fc1d7c9319dd48cfcdb9983ea979ffaf2))
* Swap resource image ([c0c1f83](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c0c1f83608eb9a13b86a410ecd788502ec7abac2))
* update Justin's title ([f4705f1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f4705f1377ce429b9f7a979f1c3b9f03dff9fed4))
* Updates analyst relations contact info ([371bb72](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/371bb72f2c8bc2c01664a1d19725c292922b4f41))

## [1.10.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v1.9.0...v1.10.0) (2023-05-19)


### 🎨 Styling

* **branding:** Rename CI minutes to units of compute ([3a1fb45](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3a1fb459c151a290d621ace9c9a99b5d600ffb54))
* **branding:** Updating to units of compute ([359c694](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/359c694974e3352fe12e988757bcb800a370b309))


### ⚡ Performance Improvements

* Resolve "[ENG] Identify and replace large images from BE" ([79abe3c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/79abe3cdcd1927d9351c35e0785c04365ab16605))


### ♻️ Refactors

* Integrate schema markup generator functions + docs ([f4a7f09](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f4a7f0963935cee35ae50661188f70bfcbcccc1a))
* Remove AB test from navigation component in Buyer Experience ([b1049cd](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b1049cd0f3238d3609279d5f5a6d3e362cb8e239))
* **schemas:** clean up schema generator functions + create schema documentation readme file ([68ef058](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/68ef058b90fd860a4d2082cf930d6e2b663b4a0e))


### 🗂️ Content change

* Add AI-powered to homepage here ([56039f9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/56039f9ae9ee905619b396612b7516a33e1c3f4d))
* Add audit events to compare features page ([32a25cf](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/32a25cfa328bc73d4c02357d56189be1083b3d7e))
* Add Lockheed quote to homepage ([a35d83f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a35d83fc3a39a2a22026a9fa0d60fd8b4c3b6875))
* Add sourcegraph to technology partners page ([4d03171](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4d031719c255fa40b11bab29c7b2b5909b78b32a))
* Added 4 AWS conferences and DOES Europe ([877899a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/877899af758825f20a3f7d483d69ae4282d141a9))
* Adding GitLab Connect San Diego to the events page. ([599b4a9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/599b4a91634c3f4e3de3a0b644ac0b7c2b3d643e))
* Adding GitLab GitOps Hands-on Workshop to the events page. ([a4ab209](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a4ab2096e63e78e3bbb26775cb28d3fb2a4c6593))
* **Free Trial:** Update localized free-trial page - German, Japanese, French ([70f7e94](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/70f7e940774a7d6f680a71d4673b1f24e8d506ee))
* Further info on grace period subscription expiry ([8970c65](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8970c65236c95a335eeb84d9c6747c0ab8ff8ba6))
* **open-source:** Add logo for partner The Good Docs Project ([11dc62b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/11dc62b4238bdb52f250564b246f8ade2cd6d05a))
* **open-source:** Refresh Kali Linux partner logo ([fbf8a24](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fbf8a24a45890f96e4302577717323ed6b62a92f))
* **Sales:** Update localized sales page - German, Japanese, French ([a89783e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a89783ea9d0d8bff1cf2d4305b7835e20545870d))
* **small business:** content updates ([21e098d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/21e098dcff7cc0df55b604d3336056304c6517e7))
* **Topics:** Add datePublished and dateModified schema attributes to topic pages ([9d9b35b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9d9b35bc619c6ef663f59150dbecff4a1ff01bee))
* Update Atlanta Address ([8b86187](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8b861875c506f0c5fca3caf43b84563f6099db27))
* Update homepage featured card to add AI ([9ecedaf](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9ecedaffd5baeb9c5c7bf0511685933be5f50203))
* Update solutions security compliance link ([bcf5294](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/bcf529475407ee1610b4b51ac8c243a05274f9d5))


### 🐜 Bug Fixes

* Add datalayer event to submit on /startups/join ([74fa0a9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/74fa0a93edcb67193aabacb5b4f0ec41c99cb0e9)), closes [#2286](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2286)
* Fix hreflang attribute for localized pages ([b516d4e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b516d4e97916ed2a22ca80650d480bec33c35ad8))
* **get-started:** faq show and hide all button bug ([f7a11e7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f7a11e75a1782f47d78c7a6913aa6e34a7ed0bf1))
* GTM tracking with solutions-video-modal.vue ([ecfa82e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ecfa82e324d524f44d8a29b23d42fb0d3039fc0e)), closes [#2287](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2287)
* Homepage logo resizing ([380ffa3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/380ffa3e51418b631a1f76809d63b3f162b9970a))
* **links:** Update buy links on why-gitlab ([1597840](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/15978403c2f1c30f32848a61dd0d9526f34b5004))
* **platform:** Update localized platform page hero image ([ad75841](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ad75841a45e62db13c587104d8c5398aa50f69e8))
* **redirects:** Fix the issue When a URL has params but does not have a trailing slash JS breaks ([34445aa](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/34445aa7a3add1d9d856bc02853eb8efe301d28f))
* Resolve "[ENG] /solutions/open-partners disabled component causing SEO errors" ([f15b4bf](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f15b4bff5c1780c43730c8a666170fb638feaa51))
* Resolve "[ENG] BUG Goldman Sachs logo customer case study" ([7ac7842](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7ac7842d072602973d8a9f86c001b278680c17d8))
* Resolve "[ENG] BUG Goldman Sachs logo customer case study" ([dedfd84](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/dedfd84b5c826ae1898e6f5e6c4a3c68eb28da08))
* **solutions:** update href on solutions card ([b6797f5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b6797f527d78180f24a8ed3ec06ed37b6f5e420a))
* spacing under searchbar ([50fb17c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/50fb17ca72a980e27abcee99c32ae532e74db7e6))
* **topics:** Fix japanese translation link topics devops platform ([b833521](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b833521a052eedfe433f780df9c32be2f0bed4c2))
* **what is gitlab video:** replace with working youtube link ([1f9abbd](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1f9abbd22d9710bedc19d7c353c468e83f3b3967))


### 🚀 Features

* Added dev survey hero animation to ai page hero ([05b1014](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/05b10147f24640ecf7159ad89ffef5fdea149e26))
* Ai animation fix ([f2f8f54](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f2f8f5490e47065e930a58b54f5f29f9a3e1c024))
* Buyer Experience: Navigation release v3.3.3 ([9deba5a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9deba5a4c91083d6ee61d66110f5bf8ddb6faf3d))
* **case studies:** migrate lely page to buyer experience ([eb18fd6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/eb18fd68c46e6e4929483a8080260041bcb2831d))
* **case study:** migrate hemmersbach page to buyer experience ([4aa1720](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4aa172049c6e504d8dcb7350dcdc2d293ae31321))
* **customers:** Adds LM case study ([b373a17](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b373a1703a4bd21b1d198bc93164b2dd75b93c36))
* **customers:** migrate us_army_cyber_school to BE ([e9c192e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e9c192edb53e63be2ed77feb2fb25c644089f932)), closes [#2109](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2109)
* GitLab 16 Launch Virtual Event Landing Page ([406a124](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/406a1241274db9b6833fe2bc9c0c1346d857e266))
* Hiding Ai illustration initially to prevent it being fully displayed before animating ([a363732](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a363732b06176934ff1ff04c40da110f5240bf2a))
* **i18n:** Added get-started  ([e5e626c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e5e626cf495c54c2023c950c553967c26cf7c16e)), closes [#2370](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2370)
* **localization:** Install page tier 1 ([d92620e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d92620e2d9158192430385cfd01a667f05fba5b9))
* **nav:** release 3.3.4 ([ab77c93](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ab77c9383553a1207e01db55abedc8b6b267081e))
* Resolve "[ENG] /customers/cern to migrate to BE" ([67abca0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/67abca057a0842e9bf5979288ac489a7b3982139))
* Resolve "[ENG] New AI/ML solutions page" ([0467be3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0467be397e279e00afb2ee6fba09f0c53322c0af))
* **Solutions:** Localized Startups solution page - German,  Japanese, French ([9de2c08](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9de2c08a13f6f1b94593b6ee54ec39e9f1061b55))
* Update pubsec page with Lockheed Martin link ([7e6d99d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7e6d99d421d084509011957b4f54c387c9bd19fb))
* Updates to case study template ([4a560c0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4a560c01948ccb402b4dd96decea3061ea209bdc))

## [1.9.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v1.8.0...v1.9.0) (2023-05-05)


### 🎨 Styling

* **topics:** read time pill on /topics child pages ([0eb5bf1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0eb5bf1c89caa91cda351b699ad6646f5b475239)), closes [#2321](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2321)


### 🚀 Features

* **case studies:** migrate anwb page to buyer experience ([a007b83](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a007b83a9617e0ba91a226907f4cafaaef891b7b))
* **case studies:** migrate paessler-prtg page to buyer experience ([fc8b88c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fc8b88c14393618c8df6e17a658598fe76bc04ed))
* **case studies:** migrate potato london page to buyer experience ([b413695](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b4136951e4906e4f092b1448602115f0305fdd42))
* **case study:** migrate chefkoch page to buyer experience ([4070217](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4070217bbf29eefb2b6a18d36e27e3e140eb89db))
* **case study:** migrate goldman sachs page to buyer experience ([1294c1a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1294c1af27bf8c261bda6b50c168ef8327882ae8))
* **case study:** migrate hilti page to buyer experience ([9dceb37](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9dceb37bb1f9bae5e251183b3310ae9badae46f9))
* **case study:** migrate nebulaworks page to buyer experience ([25b1515](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/25b1515ecc179f0935ae7cad496442d6c22d9720))
* **case study:** migrate ow2 page to buyer experience ([e183435](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e183435708eb948d36a5ea3ff01381bc9000cb00))
* **case study:** migrate regenhu page to buyer experience ([d16af2a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d16af2ae7a078d3b117194e72e5e5c7273c94638))
* **case study:** migrate the zebra page to buyer experience ([83a064d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/83a064ddcbb12f4a13f6b75784aba19a7a451e4b))
* **case study:** migrate weave page to buyer experience ([78ba08c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/78ba08ced06efda29355c6f34e6c1d31b5ea5773))
* **case study:** migrate zoopla page to buyer experience ([2595f73](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2595f739bfcc46271dbc7b6ede384bb141af6d59))
* **customers:** Add trek10 case study ([40a94ee](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/40a94ee1e0fac1a49cffce714eb8d7963ddc1ce0)), closes [#2158](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2158)
* **Customers:** Airbus migration into Buyer Experience ([986fad3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/986fad313aa6eb73972e60fb2c129ac3032a5061))
* **Customers:** Drupalassociation migration into Buyer Experience ([259145c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/259145cb183160f4d48e96a50aab819985a37666))
* **Customers:** Fanatics migration into Buyer Experience ([0e5eae8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0e5eae858e8215bfc5fadc7be995f296774bbd18))
* **customers:** migrate /customers/axway to BE ([5a77c22](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5a77c22dd85f79ef1d4a22adaa97b357014067d9)), closes [#2159](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2159)
* **customers:** Migrate fullsave customer case study ([bc1714f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/bc1714f87f18f2e1fe18271a2d42db3cb46a07be)), closes [#2128](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2128)
* **Customers:** Moneyfarm migration into Buyer Experience ([530a240](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/530a24031c0ebd4f6e0fabcd21121dc820400644))
* **free trial landing:** mvc3 - dynamic hero content based on params ([94b65de](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/94b65de3cf6b39d58d268debd14524a6f0ac8538))
* Innovation outside of R&D is not within GitLab, let's make it specific. ([995e209](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/995e209e3b35e3365d4df0c49a33523a272630b4))
* Innovation outside of R&D is not within GitLab, let's make it specific. ([32119d6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/32119d65c6ec18ab42025c68726814e0fb505015))
* Localized asset on enterprise page ([9341d68](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9341d68226e9b1a2b7ed642e5ff3f91141a2c509))
* migrate /customers/deakin-university to BE ([11eb399](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/11eb3993425e2d303833d9eecae79a34971339e6)), closes [#2129](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2129)
* migrate /customers/dunelm to BE ([95707a6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/95707a61fb52fd7aab574b46b47f24644acd5cea)), closes [#2131](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2131)
* migrate /customers/haven-technologies to BE ([e225a46](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e225a460e1f824dacb4450a19fe67f2ff5a73442)), closes [#2130](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2130)
* Migrate /topics/ci-cd/continuous-integration-agile to BE ([0af092c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0af092c94f2219d0d67840e2a878949850bd2682))
* Migrate /topics/ci-cd/continuous-integration-agile to BE ([50d1496](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/50d1496f04ef51c091fcf89ccf0137aa7b0fa429))
* Migrate /topics/version-control/what-is-code-review to BE ([7147f39](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7147f395c993026330c669c4fc99f7ed84230862))
* Migrate /topics/version-control/what-is-code-review to BE ([471ca2e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/471ca2eed8bf72836718518e57765c013a7f2daf))
* Migrate /topics/version-control/what-is-gitlab-flow to BE ([da28eec](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/da28eec1ce7eac9533fb59988270fae4f639da34))
* Migrate /topics/version-control/what-is-gitlab-flow to BE ([c6c4d4c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c6c4d4c1cb28f3ad264be28e61a5c847c026c27f))
* Migrate /topics/version-control/what-is-innersource to BE ([96c1e6e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/96c1e6e9668d30d2c82d71c6ee951ef061909830))
* Migrate /topics/version-control/what-is-innersource to BE ([6116d8c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6116d8cf06f9415d969b5695c5147c63f65bca67))
* Migrate company preference center into BE ([2b3966c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2b3966c86b7b5a14dfc1a69df3496d8c7f93d7a7))
* Nav release BEx: Version 3.3.2 ([c4e6c23](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c4e6c2369d6d22d81ca98658047cf41d52634766))
* Navigation AB test with black gleam free trial button variant ([24a9d69](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/24a9d690f43add19ac3ac026b72affaba74a4e59))
* **platform:** Localized Platform page ([28b7b73](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/28b7b739d035a3a9d5bcbd5ba3edf5c134546268))
* Refresh /why-gitlab/ ([d067a80](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d067a805408b459a3dbd9c725b81e22456c37861))
* **Software Faster:** Localization of the landing page ([b235098](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b235098b2bd734c28f236e1464b15c17c8a430e3))
* Solutions landing page MVC2 ([d793615](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d79361532f503f1c0f1ca6faccaeca5e8a203dc7)), closes [#2238](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2238)
* **Topics:** Migrate "/topics/ci-cd/continuous-integration-server/" to Buyer Experience ([3d55230](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3d55230853ead2cc9324398288a9656f880c3369))


### 🗂️ Content change

* Brand Landing Page update to update Developer Survey to 2023 ([9605f68](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9605f68bf8f025e9249aba77a36ef7dc6603c4b8))
* **events:** atlanta follow up ([c8d7638](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c8d7638b38928c04feb89d52a3c97b03dc68e723)), closes [#2345](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2345)
* **pricing:** Added multiple namespace FAQ ([bff8bf8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/bff8bf870dd597817603c4e2e3220166c79a22ed))
* Update FY24Q2 hackathon dates ([a07aaef](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a07aaef9d5b18a53aabb00b6b15f2d9f25d1daed))


### 🐜 Bug Fixes

* Add chrome build image to lighthouse job ([d50b9e4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d50b9e40de0f3ad50b3f98721045ffb0035b2e08))
* breadcrumb URL in world tour Atlanta page ([864587f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/864587fd9764e1160b398d2eb33ecd0331ea8de1))
* copy-media.vue regression on icons ([a4f5867](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a4f5867d98a22f15748287f78808aebb6b77f701)), closes [#2342](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2342)
* **customers:** load more link issue ([e76e8c0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e76e8c0fe3a883f0b9e015bd4e13c0c7ffe7259d))
* Fix image platform translations ([7a6d041](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7a6d0416483d8d90d84f541934aed3b5fd57c99b))
* Fixing population bug on GitLab vs GitHub ([827e517](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/827e517d6a1c7f3bd93eecf517f3795dd4063cb5))
* Resolve "Small Business Video" in resources block ([3267a5d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3267a5d6a57b99263e862269e75a0621794592af)), closes [#2296](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2296)
* **why-gitlab:** data props, cta link ([7bcdb77](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7bcdb77e37f2669f76dc270f740a17234c99c7b9)), closes [#2322](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2322)

## [1.8.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v1.7.0...v1.8.0) (2023-04-21)


### ♻️ Refactors

* **Software Faster:** Content and hero refresh ([4c9d180](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4c9d1806957291e8acf684a19290bb0299e069ab))


### 🎨 Styling

* **buttons:** moving partner's cta buttons ([0b95b2d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0b95b2d5c0a0d050e5823ce423e51cde1de7b7b3))
* Remove Offset property from solutions components ([7996d09](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7996d09e8446033887f8ea40cca23ce8da29afb8)), closes [#2216](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2216)


### 🚀 Features

* Add Google Lighthouse Job ([a2e7cbc](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a2e7cbc0065201540e4014027512bfb3e3e9f326)), closes [#1990](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1990)
* Apply template to Topics Devops Resources page ([c6b55a5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c6b55a55de30143c20a59f1b452320c9b291e63d))
* Apply updated template to all old topics pages ([fd22ff9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fd22ff9b25164180dc8ea5f9cfa1853eb66c5133))
* **case study:** migrate hackerone to buyer experience ([b0270a5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b0270a523b042ca2222c24c7e326be0d09721048))
* **case study:** migrate nvidia page to buyer experience ([d30d5bf](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d30d5bfa4cc96c3ac26392c27880dd1f1c814f86))
* **case study:** migrate siemens to buyer experience ([2b34066](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2b340660511472ab534be86b6570d045ed74e42d))
* **Company:** Company Vision - migrate to BE ([682e913](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/682e913ee2ecbaf57e5b899caa8702c0ee7099db))
* **customers:** Migrated Iron Mountain customer page ([5d04c26](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5d04c26ece7769b3b5131f68442f19a003421782))
* devsecops survey landing page ([79ac7a9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/79ac7a9b14cc9d925052a762c478995e44f7233c))
* i18n Small business page for french, german, and japanese ([db0116e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/db0116eb7cab38ee3a831131ca73739afb09af42))
* **Localization:** Localized DevOps Platform Topics page ([d782a9b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d782a9bbddb0618e8381f5e036bf6137a6086986))
* Migrate customer case study template from www ([d343235](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d343235f5cca9c3b9bff2e8a05c5f9cc22baa151)), closes [#1979](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1979)
* **migration:** Migrate board of directors page to Buyer Experience ([94a332f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/94a332f57b3c1b3774362155dd101bd513c8c58d))
* **migration:** Migrate egroup page to buyer experience ([a4f0a79](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a4f0a7939d259506c7df173ab90fd024666f4dfe))
* **migration:** Migrating company/contact to BE ([4896321](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/48963212bb00aba2c18582f8d2b3b2125156b90a))
* **nav:** Nav 3.3.1 Release ([24e9361](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/24e936160a8a9a2b1dc5b54aea4a50c4c6852b55))
* **topics:** migrate agile-methodology topics page to buyer experience ([6b131a2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6b131a24ffeb15e9c9da4ea3d229e9fd672ee870))
* **topics:** migrate agile-ppm topics page to buyer experience ([0d75972](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0d75972e6541ddad3dc8942d7cb9254083d10e82))
* **topics:** migrate ci-cd choose continuous integration tool topics page to buyer experience ([a6e20e7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a6e20e76fde0979326af30bc86fdf4ff4eaa51af))
* **topics:** migrate continuous delivery topics page to buyer experience ([a6bdfc8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a6bdfc873febb015d917809b13378ccf37542d7f))
* **topics:** migrate devsecops - what is developer first security page to buyer experience ([fa3a232](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fa3a232884c917b090e3ed656b9c024ecbe74e31))
* **topics:** migrate devsecops - what is fuzz testing topics page to buyer experience ([5b1d80b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5b1d80ba2293e9f42ef89a87197b5b6b51850d95))
* **topics:** migrate single application topics page to buyer experience ([1bb89ef](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1bb89ef73b0d57bde37632807d890ad59c6115d4))
* **topics:** migrate version control - centralized version control system topics page to buyer experience ([8a5d1c4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8a5d1c4103669ea8d9218a10cb4959b5906b31af))
* **topics:** migrate version control - what is git workflow page to buyer experience ([f67cea6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f67cea6086a1b108e29f770d65847438cff5b6b1))
* Update Topics landing page ([022302c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/022302c2827d4fc49e6147483c91b53b3ef8699e)), closes [#2091](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2091)


### 🐜 Bug Fixes

* **case study template:** change slptypography tag render to resolve html hydration issues ([c07e5de](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c07e5deb3536e9bb5b6aa79285a9bc8c72d283fe))
* **case study:** icon overflow ([17a7d9c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/17a7d9ca823c499d852bc1bd7d866965dce518dd))
* **guest calculator:** add tooltip links ([e21477c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e21477c0c9d436bea1421ecbddbcb04098de20f6))
* set FAQ jump down button as optional ([7e1001b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7e1001be6d2e5ecda246f3657edabdd80fa52067))
* svg animations on survey page ([8d0c2c3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8d0c2c31a9df74a5386b59ef4f0333b873a10714))
* topics landing page URL ([01ccd01](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/01ccd0170d7193d50890fdeea71fa2b02d1f54d5))
* topics landing page URL fix ([66e1912](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/66e191226fcdfe5db380589add6c248009b5b293))


### 🗂️ Content change

* Add The Open Group to open source partners ([03a63d9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/03a63d9b907a6e40d5817e2a7e5a8103e10f1714))
* customers parent nvidia link ([9883a29](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9883a29406b6351dcd2a42c7726f8a1dc1ae9a52)), closes [#2240](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2240)
* Fix typo on solutions/education ([5f6352c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5f6352cf1a8db80a5fb8e253d98d9743826e0282))
* KubeCon EU 2023 Lightning Talk Schedule ([6ee3678](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6ee3678b0c4d5a286c22191f02c3118685c34e3f)), closes [#2193](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2193)
* Major updates to portal page ([ab50632](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ab5063242f85f8a864df9ce8ca578eaf59396e3b))
* Mention Jason as reviewer for Support pages ([31d8d69](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/31d8d690710e9e7c6ea1e6764d6fe87f9009d0b2))
* **meta:** Add Meta title and Description to YML files for /competition page and child pages ([d829ed1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d829ed15604aba7e42ae10270ca16fbde6d8a16f))
* Radovan Bacovic added: Big Data Belgrade No 3 ([eed850a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/eed850ad1db5ccf9041ad4bbf750715d27071a6f))
* Radovan Bacovic: Added AWS user group event ([10ea9d8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/10ea9d8112f80b9689772dc0d74cd57e0bdc15de))
* Remove Integration Services from content/services.yml ([05ab1ab](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/05ab1ab602b356c95fbc94f0d6ef238d09a7bb81))
* Updated the Learn page yaml file ([b4514fb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b4514fbd63597a6dafd28c3735c7c377c07e07fd))

## [1.7.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v1.6.0...v1.7.0) (2023-04-07)


### ⚡ Performance Improvements

* Reduce education page image sizes ([2dcb1d9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2dcb1d96e0e5b501bbec71794481773c5ebc6298))


### 🧪 Tests

* **homepage:** hero painted door abc test ([5886436](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/588643653518f350cd5fb79861a1f38eb4010ec8))


### 🐜 Bug Fixes

* **education page:** next steps component education content fix ([cedd74b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cedd74b608b840c4f98c29165504e0a9454ac085))
* **painted door test:** change order of click events for test ([bd36483](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/bd364833285f0ec47569a05a52bb35e66128d9a1))
* **painted door test:** click event ([445e3c3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/445e3c3b5cb271c64aac4c024462f231c190902f))


### 🗂️ Content change

* Readability improvements to migrated topic pages ([6d802b2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6d802b22616337f1695b1dbaae1e011720a109ff))
* remove IDC reprint; update contact info ([1aef99b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1aef99b4ec36c73db4f5efffcd4199a18a6254bb))
* Update intro text to resources page ([1fd0c67](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1fd0c6749c9a0675e7f2344adcd0690e44e8307d))
* Update professional services page with link to Full catalog ([7cf6cfc](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7cf6cfc4d4a8bcee66e5412a82234c940c7e52db))


### 🚀 Features

* Add greenhouse job post API for careers component ([89aa8be](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/89aa8beda761efe03de668cd7a5596bedb1e1048))
* **calculators:** add personalized partner content to roi calculators ([d95ecf2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d95ecf25212659dbaac9dd21458fdde018f7375f))
* **events:** rsa page ([8c1c1bf](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8c1c1bf9a4d3dcc307780cd55bd09fcb3732d5c3))
* **free trial paid search:** mvc2 ([9014ede](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9014ede3e4ac2eb8cda5e30d261d0230789145f1))
* guest users calculator ([dfa6420](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/dfa642032119f116c4fea2047a9a6f12827767f6))
* **i18n:** Implementation of nuxt i18n for teamops page ([9e44aef](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9e44aef74a3bf48a9778708939f8bc71d1404842))
* Localized Enterprise page ([b220327](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b220327d8b56d9bba2d125d6d5d3758587bace3f))
* **pricing:** add reactive header text to FAQ ([8ae7878](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8ae7878a94b82c8850aaeadf510987d704605a25))
* Schema Generation for Breadcrumbs ([87f6b6b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/87f6b6bdadf399c366bee6c567284c6b6e14525c))
* **topics:** Migrate "/topics/devsecops/beginners-guide-to-container-security/" into BE ([32ab6b6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/32ab6b6bd19d11a8334d281c9240fa953c441668))
* **topics:** migrate beginner devops topics page ([d28b866](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d28b8661a579300fa9a03a005e951800b86e029b))
* **topics:** migrate ci-cd pipeline as code topics page ([cea7dc1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cea7dc1fa0cc10261b5b59d25f9a832458c648c2))
* **topics:** migrate cicd pipeline topics page ([4ce7ef4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4ce7ef4b7a4f5f5cfc8c0b76fe6b9220c8051aad))
* **topics:** migrate shift left devops topics page to buyer experience ([96b2f90](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/96b2f90d5f1e8c827aaa130e45be4a18964309bb))
* **topics:** migrate version control - software team collab topics page to buyer experience ([a7a81d9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a7a81d9dae5e77376fa45d05c2432c0cb6d72eb3))


### 🎨 Styling

* fix pricing page tablet issues ([b146ca8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b146ca872f46736e636bac14a0f98c85b9605a23))
* **rebranding:** Cloud, Technology & Platform Partners ([92d4c7b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/92d4c7b3684b7674d3ca40d91b5f1fa9934dd166))
* **rebranding:** Company ([719e000](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/719e000eb66b15537828efef0b9cd95ecd6356ad))
* **rebranding:** Get Started ([ca1e444](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ca1e444a41cd8aefb0e3af140a2a5fdbded91995))
* **rebranding:** Services ([7addf13](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7addf137ebdea31fc4242554daedc4f341f0b26e))
* **rebranding:** Stages DevOps lifecycle ([73e5db4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/73e5db41d6023a588407e3d25d589dd1a93969a6))


### ♻️ Refactors

* Migrate /topics/ci-cd/continuous-integration-metrics/ page ([8465a25](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8465a254c40ee53c2587010d80e8484b81067754))
* **SEO:** Add Hreflang Attribute in localization pages ([5a3f4b9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5a3f4b989ee70c7ee0e6f84e92020cf5c459bc52))
* **software-faster:** Brand marketing landing page refresh ([3247ec0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3247ec00d68060577bf7f40e2b385a93d4c59cc9))

## [1.6.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v1.5.0...v1.6.0) (2023-03-24)


### 📝 Documentation

* General improvements to version control topics page ([276175a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/276175a8b4abdfcfd6965bdafac8a60a5bd37007))
* Some SEO-focused copy changes ([306466e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/306466e26697a7ef3a150f19df39dd94201fbbeb))


### 🗂️ Content change

* Add more nav images ([ad523f6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ad523f61e0c8f2ad45f2234e7029a552b98ab3c9))
* Add next hackathon event ([3308721](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/33087214f7397f9eb27eb5cbf81947e0d279cc9a))


### ♻️ Refactors

* **calculators:** consolidate shared css and logic ([2042d7b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2042d7b7fe22ab08b663e8b1b497277f1843f769))


### 🚀 Features

* "/topics/gitops/infrastructure-as-code/ migrate to BE" ([dbac2e6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/dbac2e60fa8ad55099711cc6011800504d70f6e0)), closes [#2018](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2018)
* add navigation anchor tabs to pricing page faq ([244b9a7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/244b9a74148be996f298881e84935bb364b736f9))
* Add trial transfer information in Licensing FAQ ([7ba1200](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7ba120093f4a5c2f24ce99c7178dfcbbb3e7a340))
* Create All Customer Case Studies Page ([c4ef54f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c4ef54fb44bb64806eb53a17953b4d907adecabc)), closes [#1978](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1978)
* create new time roi calculator ([6821c4c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6821c4c584e74dbb79632c285bbcfe36941a3711))
* Customer Case Studies Page refresh ([109ed28](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/109ed28d8767865909eb25ccd04a58457cadc9cb)), closes [#1977](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1977)
* Eng free trial page iteration fy24q1 ([dd8b004](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/dd8b0041c8c20e916ac59eac7c859536801c88eb))
* Moving topics/agile-delivery to BE ([b492ea2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b492ea25d8ebb4755b0eab545ad0cc22c8046efc))
* Navigation 3.2.0 release - SMB & Enterprise personalization ([709ee70](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/709ee70b47b403c8591e5417ecbdf4f004f72b02))
* **sales:**  Localized French and Japanese Sales Landing page ([3b56f40](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3b56f403d97ded8ee3a76d83ca6470dfc4f264db))
* **topics:** Migrate /topics/ci-cd/benefits-continuous-integration/ to BE ([14c96c8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/14c96c8a43e6719ec9f4d81a84b7f4bf022ae2bc))


### 🐜 Bug Fixes

* Change URLs to include trailing slash in customers page ([8892684](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/8892684485f2b14d061a4918ea9444c3aacae5cf))
* education logo sizing ([c909f50](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c909f503816a7047d8bd05262421b30ae5a8b24d))
* Home page hero button missing padding ([0264e59](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0264e5975dfc5f03c287d2c2ba15530764c4bb68)), closes [#2071](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/2071)
* **tooltips:** add slptooltip improvements to buyer experience ([fda936c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fda936c51c26a462770408a380da0166e5fdb842))
* Update opengraph image for events/kubecon ([17a4797](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/17a4797871374e9d36420bf2535277e1f7294086))
* update path of new free trial page ([5ca1f79](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5ca1f79f810fdd1c8527c587cf9c1e3497c964c9))

## [1.5.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v1.4.0...v1.5.0) (2023-03-10)


### 📝 Documentation

* add changelog for UX ([d03eb8a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d03eb8aea1c8b5135619b522abbc729aa2bb2b5c))


### 🐜 Bug Fixes

* **pipeline:** Avoid running the changelog on a non-release week ([503e1e5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/503e1e5b94ddd820adbd04769b97df1720a4614f))
* Update URLs and data properties that are incorrect ([b589403](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b58940328ac73c485f8f18aaf2e92fd28202ebcb))


### 🗂️ Content change

* Radovan Bacovic - 3 new events added ([05447e4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/05447e4e4a8f8e6a8baf0282c4de217639e6d757))
* Radovan Bacovic add 2 events ([012738f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/012738f9d4b20b4a8df12fc56db5378f96581060))


### 🎨 Styling

* **branding:** Company page DevOps to DevSecOps ([f719d6a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f719d6a4e2f43427302d92ccecbff42fc18dbdad))
* **branding:** Company page DevOps to DevSecOps ([b4bb684](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b4bb684ef605dd77f37bc827720a335a0d14de23))
* **branding:** DevOps to DevSecOps ([e28f383](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e28f383a27fe487ef442701a97a7cac45798cba0))
* **branding:** DevOps to DevSecOps ([40bbded](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/40bbdedcfcb417683f5a76a594313935fba8e12e))
* **branding:** Partners Page DevOps to DevSecOps ([650f75a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/650f75ac12c12655e53b403320532a51a543f6a1))
* **Rebranding:** DevOps for DevSecOps ([33dd0ee](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/33dd0ee5d0962a9676f5ac8faee7972ab8cde099))
* **Rebranding:** DevOps for DevSecOps ([752e214](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/752e214151a9d6c771263304d060dcc6f67a9674))
* **Rebranding:** DevOps for DevSecOps ([fe9dbe2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fe9dbe22deafb42e8556318c1a47304981bb37da))
* **Rebranding:** DevOps for DevSecOps ([c27c369](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c27c369e0373a06ae5334d5fdf0862ae74b6763c))
* **Rebranding:** DevSecOps for DevOps ([fae9401](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fae9401fadd3079e89fa679237d00f99065e5b06))


### ♻️ Refactors

* Buyer Experience nav release 3.1.2 ([6a382d0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6a382d017c9f8e04f2f4a0ebc0e93d03b6fcaaf4))
* Remove LaunchDarkly from nav component ([5c2c454](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5c2c454527976989038e3065eabda5ecde8e85ca))


### 🚀 Features

* Buyer Experience nav release version 3.1.3 ([9594b10](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9594b1057471164f6d6ee918c1485ee1fee8c1e7))
* Localized German Sales Landing page ([3a10ef5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3a10ef508e0e5246384db60a8cd12ad1ae0f941e))

## [1.4.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v1.3.0...v1.4.0) (2023-02-24)


### 🎨 Styling

* **branding:** changing DevOps to DevSecOps ([a59f1c7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a59f1c7824c7bb85d632c04d7454f04f462ea0aa))
* **branding:** changing DevOps to DevSecOps ([480f471](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/480f471d53f6974976d01580bcc444d742286fff))


### 🐜 Bug Fixes

* resolve slptypography lint errors ([560e420](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/560e420cfa69684fc80948f62959fce667de6e98))


### ♻️ Refactors

* stage icons rename  ([4cab5ae](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4cab5ae4cc9d57de831e2072fa148139bd5d4052)), closes [#1894](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1894)


### 🚀 Features

* [ENG] Update header on feature template ([c5df80b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c5df80b49cd09b34aefe95648eda3c9fa370617b)), closes [#1953](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1953)
* Add new /resources template and page ([7277427](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/72774270cf2a8eea66488b1059e2b87a4c6796cd))
* **lint:** add custom lint rule for checking slptypography for vhtml attribute ([3cda761](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3cda761cce46b28025cf25e3aacbe25312c766bd))


## [1.3.0](v1.2.0...v1.3.0) (2023-02-17)

### 🗂️ Content change

* **partners:** Add more technology partners ([7979971a](7979971a))

### ♻️ Refactors

* **carousel:** switch to using v-show and aria improvements ([60ba9c60](60ba9c60))
* Create reusable categories table component ([e0a665d1](e0a665d1))
* Remove references to pps_aggregate ([76eae192](76eae192))

### 🎨 Styling

* **branding:** DevOps to DevSecOps Tier 1 ([bc8dcc05](bc8dcc05))
* **branding:** DevOps to DevSecOps Tier 1 ([e6352243](e6352243))
* **devsecops:** changing branding from devops to devsecops ([f76b843c](f76b843c))
* **devsecops:** changing branding from devops to devsecops ([b1d8148d](b1d8148d))
* **devsecops:** changing branding from devops to devsecops ([58871845](58871845))

### 🚀 Features

* add methodology pop-up on DevOps Tools ([57cebbb4](57cebbb4)), closes [#1857](#1857)
* Add the search event listener to integrate the navigation and the BE search ([99248afa](99248afa))
* Add the search event listener to integrate the navigation and the BE search ([72107dda](72107dda))
* make entire surface area link to case study ([fc54e978](fc54e978)), closes [#1889](#1889)
* **nav release:** Navigation Release Version 3.1.0 ([80ccb357](80ccb357))
* populate why gitlab feature form using categories file ([26d464c7](26d464c7))

### 🐜 Bug Fixes

* Make resource card images proportionally consistent when resizing ([30c82afe](30c82afe)), closes [#1891](#1891)
* platform table link and order issues ([df6bb08d](df6bb08d))
* wrong URL in open source partners section ([4896b0cc](4896b0cc)), closes [#1925](#1925)


## [1.2.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v1.1.0...v1.2.0) (Jan 30, 2023 - Feb 12, 2023)


### ♻️ Refactors

* new engineering issue template ([e26bf48](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e26bf48938f891882c2a2ebbfee3295577bff6c4))


### 🐜 Bug Fixes

* add marketo script to pricing template pages ([01e4e00](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/01e4e009f752b246e7be6e8b83ddccb57cb5e29b))
* analyst page typography ([7433916](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/74339161fdb0b02d7a88a4ba3406d44358852ab1))
* remove unused images - part 2 ([f9db106](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f9db10623e1ce924a3407d6bfea3362f1fa2e7ec))
* **safari:** devops tools table hover state lag ([12f8d67](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/12f8d679ad9874f987f05913d246222a29fa451f)), closes [#1795](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1795)
* **solutions:** Hackerone customer logo distorted in solutions/dev-sec-ops ([0f505bb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0f505bbebe5888a08a384ae00f279b056ccb6302)), closes [#1847](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1847)
* Style changes to devops tools ([3b63c45](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3b63c451f6ef9c4df2d98b058f8788a571bd6647)), closes [#1850](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1850)
* **support links:** updated link for Instance Migration on Statement of Support page ([9e5496e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9e5496e6cd3c16cfcefd616365033cb4a8c97643))
* youtube video embeds on ci solutions page ([5dc805b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5dc805b7585e65b257a0804c8898ecf5c0ec9a27))


### 🚀 Features

* Added an event for Radovan Bacovic as a speaker ([4c1c847](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4c1c8474e992ef965ddc082a89cd806a64bb60ca))
* **competition:** change table title based on stage ([09a4fa5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/09a4fa5387bf13fde508d63e2fe0ae0062864d29)), closes [#1846](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1846)
* create showMore mixin + add showMore functionality to pricing templates ([b9d5702](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b9d57025dc628871522f92d52e5d4203740a719d))
* Make Search Results a New Page ([0ec06a0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0ec06a0f58d73d4b8bf0956a42e0eec25c4d6e8b))
* solutions/open-source page refresh ([d795d7b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d795d7ba6d266c00f03034925ce8e84f00b4e505)), closes [#1837](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1837)
* solutions/open-source/join page refresh ([9654d1f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9654d1f271be7b762306902a76108dedcbf955f1)), closes [#1840](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1840)

## [1.1.0](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/compare/v1.0.0...v1.1.0) (2023-02-03)


### ♻️ Refactors

* Analyst mvc2 ([720a697](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/720a697f32b85889820ccfb34a19474839950a92))
* **AOS:** Remove AOS in competition pages ([f82fd6b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f82fd6b964bf081279183170d647beaf3f329583))
* **AOS:** Remove AOS in partner pages ([03e860c](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/03e860c77c146c23d0d9298c9fddafeded635270))
* **AOS:** Remove AOS in remaining pages ([870c045](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/870c045afdac2f68677370f04480c731ba5f09dc))
* **AOS:** Remove AOS in topic pages ([cf73866](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cf738664fc6db977372bdb246ac705be84c94767))
* **AOS:** Remove AOS on events pages ([cd779b4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cd779b4fd2483831963727bfef33e4e7db0f305b))
* **AOS:** Remove AOS on single landing pages ([e150db6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e150db65b4c2a537e48433139da39488cbff47fb))
* **AOS:** Remove AOS on solutions pages ([d89397e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d89397e0c4a3b911c8e9525d3a100c0a44f3316b))
* Change getElementByID to Vue refs ([89f9426](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/89f9426550e584fd4dc96321440967cfef12fa34))
* Create mixin that handles screen resizing ([c9ab601](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c9ab60194dabf9808cb3a3fa27d602619b53e4b5))
* **default-layout:** Add animation initialization to default layout ([f2143d8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f2143d8f1405083d9275ca19418ac04a9d9130cc))
* Remove all redundant measure units in css files ([925f262](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/925f262c2d089409d4ccdebf7f70d62a20df518a))
* Remove css invalid elements ([4d3dfa2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4d3dfa2312fa14d67a86cf9a2e0d7cdb661fd389))
* Search and remove unused images part 1 ([fffb988](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fffb988d41536e107b0682e96e28f9a4b2e180e5))


### 🎨 Styling

* add new events to events page ([fe78c08](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fe78c08f16a73b716e92585194e9365b4fcbf636))
* Added col to remove content overlapping ([c55f9b2](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c55f9b2bc542e5dbcad2104d8fd44c3797ffadac))
* competition page ios safari css updates ([a2e4481](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a2e4481dcd55733603e1c005a63fb886b78970eb))
* fix border bug on competition table ([5c5c038](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5c5c038cc4e67517ed75455fa2d130126243623a))
* fix customer page filter dropdown sizing ([b30e7ca](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b30e7cab61dcebcc85ea12913a7a4dc264401bc4))
* fix horizontal scroll on select pages ([0292641](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/02926410ee29c3e4bc148a863e1606e38235ccdf))
* fix quote carousel background bug ([1ecae17](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1ecae178879b5b9af7cacf9801068c420d3ea981))
* force tier pills to stay on same line ([801a8b1](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/801a8b132d76f9f78bb311b762248db7cc688822))
* **get-started:** fix typography and spacing ([3163321](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3163321818982c608db1ab8639917d98b2dda2f2))
* pricing page ios safari css fixes ([32387ea](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/32387eaeb715ecdc2cab39107accbe78d9c834bc))
* styling updates for pricing template pages ([b1c0f32](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b1c0f3237beb6648619391743890906ad2910ae4))


### 🚀 Features

* Add buttons to find a partner ([c1d636a](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c1d636a91f51362344007ae9950a51c57d1d780f))
* Add clarity on limits to pricing and trial page ([13318a6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/13318a6de0e4cdeac540f5a80900e7c8a0f3ca0d))
* Add Deakin University case study ([5d91007](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/5d91007e58f2d9d5a3b67e610de75a6912594804))
* Add deep link into tabs on /competition page ([35022b7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/35022b7bfdbb986a5ccb51fe96e4cc212236b618)), closes [#1770](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1770)
* Add link to GitLab vs pop-ups ([ff0fea8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/ff0fea836e41add08c1f72227a3167fd542c6fd2)), closes [#1733](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1733)
* add new report to CAP page ([e9ad7ed](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e9ad7ed325dc256353c9301b86d979b112897825))
* add photo to GitLab Dedicated page ([b21b8f7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b21b8f7e6341e3d3d7abd90ee6436951c10d3eb9)), closes [#1707](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1707)
* add roi calculator abc test ([9d35776](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9d35776c310d7145bbb9cdb31a402c5d75f7716d))
* Add VSD sign-up page ([48988ef](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/48988ef8f8f391a057aa7defef2ed892229bc56d)), closes [#1800](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1800)
* Added 2 events for Radovan Bacovic as a speaker ([b89eaf3](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b89eaf3dd496714606d3db8fb2dbacdf42092132))
* Adding Notion, Firefly, Gitbook & AnalyticsVerse to alliance partners ([7c6f215](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7c6f215d512cd18c1c276ebb28ae883302b2056c))
* Adding Tech Talk: GitLab for Governance and Compliance to the events page ([3c18a79](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3c18a79a5a2732b9bdfecae379f5700ec14f115d))
* Build business case for GitLab checklist ([bcc5dd6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/bcc5dd643017da91a2807c65c1fa910b564e9488))
* **competition:** Add a hyperlink to DevOpsTools ([39513a7](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/39513a77558ee8981fd1d5b47fad8f6dd2829549)), closes [#1848](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1848)
* Create brand campaign landing page ([e1e8935](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e1e893557ab39ac231de49edddfcf1a60b78176f))
* Create changelog scheduled job to run it only on "main" ([b1eddcb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b1eddcbd766baeea9905490de52a61f3ed4a5523))
* create digital transformation topics page ([6a7f6d8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6a7f6d83db7d8043b13fa17cb2a63c3213e3f88e))
* Create Direct Link to Pricing Page FAQs ([40ccd01](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/40ccd01504e9693a633e0429db63d9bc62b37154)), closes [#635](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/635)
* Deep link from /devops-tool stage to /competition tab" ([36c238e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/36c238eca27f636d101cea8904e426f99d65bc63)), closes [#1773](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1773) [#1772](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1772)
* Education solution page ([21eafd4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/21eafd4bc5331bebed60bf09fce5bf5235524506))
* **nav release:** DEX upgrade be-navigation to 2.2.0 ([b16a814](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b16a81461c5d8b397fcc52a8a6dd3942ad17dc82))
* **nav:** Nav 3.0.2 release ([549a01e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/549a01e9bab1a3aacbb4e416cd867a86523c4e7f))
* new UX for competition VS pages ([4489a29](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/4489a2966511e5d3c2920389efd1907da3324296))
* Refresh TeamOps landing page with content and verbiage updates ([15b60e9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/15b60e9d61788f9c67475531c426ae3fabcf176a))
* Releasing support for notifications in the Slack app. ([9d5d342](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9d5d3420ca92e54771891bf0cc61479b59d67c13))
* remove ROI cta for same-page redirects ([3c8bcc5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3c8bcc500b66e42a2bf6b8213bcd3ecf9f808d0c))
* sync platform page table up with categories.yml file ([0993613](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/09936132264ee045d7d3157744a25f597427cf60))
* update competition page UX ([2ad64ea](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2ad64ea69fe472bf7d924476acb6f4208aeed048))


### 🐜 Bug Fixes

* 404 links ([7d9832b](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7d9832b87011a2cb3777b7f43488623611317244))
* add text to product analytics card ([799b329](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/799b3297871d1bfe745468f0d3b504280944cb6c))
* brand campaign devsecops image ([be5bc90](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/be5bc90d12049357ad805e7eb50f817203872535))
* change date for webcast ([f31aed9](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f31aed9fc31ff2b67bee2a8a779a64fdd1bdae81))
* content: Remove "API Clients" and "CLI Clients" from the technology-partners ([e629a59](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e629a59713bc2307503fd64f3bc15fc7c1a522e8))
* Enterprise and SMB Page Loading Slow ([6528680](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6528680f5a77f81cfbd0bffccc922bb5ad9b5648))
* **gtm/ot:** unblocks gtm from ot ([1b42a3e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/1b42a3e72252aaa0f1c0208ae8ffad0b4ade44cc))
* **gtm:** removing optanon class ([cf31330](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/cf3133098911d06a4bdb571cecc78812b8f356f0))
* info for course to remove duplicate option ([beec9f8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/beec9f858c12d9dc13476a3371353d0facbde43f))
* Navigation increment v2.1.1 ([36b2e70](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/36b2e703c80669c5057a998fc50c4b017b19e5e3))
* **OT/GTM:** removing optanon class from GTM ([fa24bdf](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/fa24bdf84dce2f46fe6a04a6cdd7670d51617041))
* Project Dependency List on Feature Comparison page ([56e03e8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/56e03e8a40916b5111cbb9e7a6981dfc9dd7eb95))
* Quote block layout on mobile ([5437995](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/54379957c9883d2a17abebbf268ae6547d480833)), closes [#1810](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1810)
* random quotation mark on /solutions/continuous-integration/ ([0712c32](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/0712c32f8e6963e48c1789c43668fbccc981d44c)), closes [#1811](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1811)
* remove entry from customers page that leads to 404 ([3d4af09](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3d4af094950bac67c8bc70686702301ddfa3b250))
* Rename side-navigation, improve reliability of scrolling events ([9c9a96e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9c9a96e4c577f5e815a782c60f576eecf465d922)), closes [#1398](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1398)
* Replace all instances of all-caps with title-case ([c223413](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c223413a75ab06f2f33f07c3eaa5f20321669734)), closes [#1563](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1563)
* Resolve "Removing partners without TPAs" ([9810ab5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9810ab5199b2eceac726b2bec0968e633eaaa419))
* **sidenav:** Source code management components displaying incorrectly ([3b1635d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3b1635dedcb391c61cd32e1573e1a6ab74368ed9)), closes [#1794](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1794)
* **sidenavvariant:** now takes up space in dom, updated relevant yml files ([e9e9225](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e9e9225ba021a11760eb30656b9a859511d489e5)), closes [#1572](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1572)
* sitemap with 2 paths to exclude ([c30ccc4](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/c30ccc4c73c48e5d4811a128916704de17494474))
* SlpBreadcrumb on Topics Pages ([7faa2d5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/7faa2d5e259bafa23c2b93e66d0b97929c029ede)), closes [#1780](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1780)
* Small business page components have extra spacing from the sidenav ([2651843](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/2651843311414fda90532298e3cb136216cbb740)), closes [#1813](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1813)
* software faster vimeo analytics are not being tracked ([3c0d01f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/3c0d01f5db45872dbf79b07f4b41f8f886ee1e4e))
* style of pricing button to remove extra space ([d61bca5](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/d61bca56de0ca4776d45f2905d8df2d6cfdf54e2))
* transparent icon on solution page card ([e820d49](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/e820d496336b5938554112a93ead6fba092808da))
* typo in pricing faq ([a7d60eb](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a7d60ebe8547c4cff7ac610742b4ea85e826dbf3))
* typo on gitops page ([6e3c13d](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6e3c13dc26c42db892832852f8b0c101dcffac3c))
* typo on the aws-reinvent page ([553bc48](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/553bc48bcf72812d6fcd59d7ec30489a7d5eb290))
* Update colors on the TeamOps page ([f528946](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/f528946dff4b5e2d2b4fbaebb6d458d1de93472e)), closes [#1753](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1753)
* update dedicated page url ([b8dfaf8](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/b8dfaf8320197ef508554383dc44acc5e22a2790))
* update url for product analytics docs ([959204f](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/959204f61d5b3a2c703ab6e54377f7e3841bea99))
* Updates to VSM pages, Add DORA page links ([6c420f6](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/6c420f6dc667eb3080cafccb7e204c44f3c6487f)), closes [#1650](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/issues/1650)


### 🗂️ Content change

* Add Contributor Days Event ([a086b47](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/a086b475bf33d030edb055f46868d8d245a1cbb0))
* **technology-partners:** Reclassification of technology partners ([9360c7e](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/commit/9360c7e1283668eab27d6fcdb1f9901ef02f541e))
