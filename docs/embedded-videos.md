# Embedded video links

Video URLs are different when they are meant to be used as an embedded video in the page. Please try to adjust and adapt to what seems to be working but as a general rule, follow these formats:

## From Youtube

Please try to follow this format when embedding video from YouTube:

``` bash
https://www.youtube.com/embed/< video id from YT >?enablejsapi=1
```

## From Vimeo

Please try to follow this format when embedding video from Vimeo:

``` bash
https://player.vimeo.com/video/< video id from Vimeo >?h=< second video id from Vimeo >&badge=0&autopause=0&player_id=0&app_id=58479
```

### Internationalization, accessibility, and Vimeo

We have created a [mixin](3484-update-be-project-engineering-docs) to manipulate the Vimeo URL so that it shows captions in a certain language by default. This is dependant on the locale of the page and if captions in that language have been uploaded to the Vimeo video. Most of our videos are in the English language, but we would like to show French subtitles if someone on a /fr-fr/ page plays a video.

If you are adding a video to a component, be sure to add the mixin. You can then use it by doing something similar to this example:

```
    <iframe
        v-if="data.video_url"
        :src="createVideoSource(data.video_url)"
        title="YouTube video player"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowfullscreen
    ></iframe>

```