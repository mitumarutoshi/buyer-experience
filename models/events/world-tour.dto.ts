export interface WorldTourLandingData {
  title: any;
  metadata: any;
  slug: string;
  description: string;
  hero?: any;
  regions?: any;
  form?: any;
  faq?: any;
  banner?: any;
  nextSteps?: any;
  spaceId?: string;
  entryId?: string;
}

export interface WorldTourExecutiveEventsData {
  metadata: any;
  name: string;
  internalName: string;
  slug: string;
  eventType?: string;
  description?: string;
  date?: string;
  endDate?: string;
  location?: string;
  region?: string;
  hero: any;
  blurb: any;
  featuredContent?: any[];
  footnote?: any[];
  nextSteps?: any;
  sponsors?: any;
  breadcrumbs?: any;
  form?: any;
  staticFields?: any;
  spaceId?: string;
  entryId?: string;
}

export interface WorldTourEventsData {
  metadata: any;
  name: string;
  internalName: string;
  slug: string;
  eventType?: string;
  description?: string;
  date?: string;
  endDate?: string;
  location?: string;
  locationState?: string;
  region?: string;
  hero: any;
  agenda: any;
  featuredContent?: any[];
  footnote?: any[];
  nextSteps?: any;
  sponsors?: any;
  breadcrumbs?: any;
  form?: any;
  staticFields?: any;
  spaceId?: string;
  entryId?: string;
}
