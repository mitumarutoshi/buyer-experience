import contentful from 'contentful';

const client = contentful.createClient({
  space: 'spaceIDHere',
  accessToken: 'accessTokenHere',
  host: 'cdn.contentful.com',
});

async function fetchStagesDevopsLifecycleRoutes() {
  try {
    const stages = await client.getEntries({
      content_type: 'page',
      select: 'fields.slug',
      limit: 100,
    });
    // eslint-disable-next-line no-console
    console.log(stages);

    const regex = /stages-devops-lifecycle\/.+/;

    stages.items.map((stage) => {
      // eslint-disable-next-line no-console
      console.log(stage.fields);
    });

    return stages.items
      .filter((stage) => regex.test(stage.fields.slug))
      .map((stage) => `${stage.fields.slug}`);
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error(
      'Error fetching stages of the devops lifecycle routes from Contentful:',
      error,
    );
  }
}

fetchStagesDevopsLifecycleRoutes();
