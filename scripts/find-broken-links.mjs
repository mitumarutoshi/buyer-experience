// https://github.com/JustinBeckwith/linkinator/blob/main/src/index.ts
import fs from 'fs';
import { LinkChecker } from 'linkinator';
import axios from 'axios';

const {
  GITLAB_PROJECT_ACCESS_TOKEN,
  SLACK_DEX_ALERTS_ID,
  SLACK_DEX_ALERTS_TOKEN,
  SLACK_WORKSPACE_ID,
} = process.env;
const GITLAB_API = 'https://gitlab.com/api/v4';
const SLACK_API = 'https://hooks.slack.com/services';
const PROJECT_ID = '28847821';

const checkForBrokenLinks = async () => {
  const fullSiteChecker = new LinkChecker();
  const absolutePath = 'http://about.gitlab.com/';

  // Scan all index.html files in the /dist directory for broken url's.
  // .check() is an AsyncFunction, let's wait for results.
  const results = await fullSiteChecker.check({
    path: '**/index.html',
    timeout: 5000,
    linksToSkip: [
      '_nuxt',
      'node_modules',
      'https://player.vimeo.com',
      'https://gitlab.com/users/sign_in',
      'https://www.linkedin.com',
      'https://ir.gitlab.com/',
      'https://support.gitlab.com/',
      'http://contributors.gitlab.com/',
      'https://gitlab.com/dagytran/diploma-thesis/blob/master/src/main.pdf',

      // Looks like there's bot detection on these
      'https://docs.deepfactor.io/hc/en-us/articles/1500008981941/',
      'https://blog.cloudflare.com/cloudflare-pages-partners-with-gitlab',
      'https://support.phrase.com/hc/en-us/articles/5709668128796/',
      'https://support.workboard.com/hc/en-us/articles/8045455704333-Key-Results-from-GitLab',
      'https://blogs.oracle.com/cloud-infrastructure/post/using-the-gitlab-cicd-pipelines-integrated-workflow-to-build-test-deploy-and-monitor-your-code-with-container-engine-for-kubernetes-and-registry',
      'https://www.make.com/en/integrations/gitlab',
      'https://cloud.google.com/functions',
      'https://certification.opengroup.org/register/ottps-certification',
      'https://federal-support.gitlab.com/',
      'https://gitmate.io/',
      'https://codebots.com/gitlab',
      'https://www.mend.io/',
      'https://www.reddit.com/',
      'https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/uploads/4052f158afc977415fa7d9e761c9f54f/GITLAB__1_.pdf',
      'https://en.wikipedia.org/',
      'https://gitlab.zendesk.com/',
      'https://investinganswers.com/',
      'https://twitter.com',
    ],
  });

  const brokenLinks = results.links.filter((x) => x.state === 'BROKEN');
  const html = brokenLinks
    .map((brokenLink) => `<a href="${absolutePath}${brokenLink.url}"/>`)
    .join(' ');

  // inject all the broken links into a temporary html file.
  // We're going to re-run the scanner against the temporary file. Except this
  // time we're swapping the local path with http:://about.gitlab.com/. This
  // is because the local references to handbook, blog, etc. only work on prod.
  // eslint-disable-next-line no-console
  fs.writeFile('retry.html', html, () => console.log('Created retry.html'));
  const retryChecker = new LinkChecker();
  const retryResults = await retryChecker.check({
    path: 'retry.html',
    timeout: 5000,
  });

  // eslint-disable-next-line no-console
  console.log(`Scanned a total of ${results.links.length} links`);

  const broken = retryResults.links.filter((x) => x.state === 'BROKEN');
  // eslint-disable-next-line no-console
  console.log(`Found ${broken.length} broken links`);

  if (broken.length) {
    const brokenLinks = broken.map((brokenLink) => {
      // eslint-disable-next-line no-console
      const url = brokenLink.url.replace(absolutePath, '');
      console.log(`${brokenLink.status} - ${url}`);

      return url;
    });

    await create404Issue(brokenLinks);

    process.exit(1);
  }
};

const getProjectUpcomingIteration = async () => {
  const url = `${GITLAB_API}/projects/${PROJECT_ID}/iterations?state=upcoming`;
  const { data } = await axios.get(url);
  return data[0];
};

const sendSlackMessage = async (linkCount, issueLink) => {
  const slackPayload = {
    blocks: [
      {
        type: 'header',
        text: {
          type: 'plain_text',
          text: 'Buyer Experience Alert: Broken Links Detected',
          emoji: true,
        },
      },
      {
        type: 'divider',
      },
      {
        type: 'section',
        text: {
          type: 'plain_text',
          text: `${linkCount} broken links were detected across the site. \n\nA new issue has been generated for addressing and reviewing these links.`,
        },
        accessory: {
          type: 'image',
          image_url:
            'https://img.freepik.com/free-vector/404-error-with-landscape-concept-illustration_114360-7888.jpg',
          alt_text: '404 not found',
        },
      },
      {
        type: 'divider',
      },
      {
        type: 'section',
        text: {
          type: 'mrkdwn',
          text: 'View issue in GitLab',
        },
        accessory: {
          type: 'button',
          text: {
            type: 'plain_text',
            text: 'View issue',
            emoji: true,
          },
          url: issueLink,
        },
      },
    ],
  };

  try {
    const url = `${SLACK_API}/${SLACK_WORKSPACE_ID}/${SLACK_DEX_ALERTS_ID}/${SLACK_DEX_ALERTS_TOKEN}`;
    await axios.post(url, slackPayload);
  } catch (error) {
    console.error('An error occurred sending slack message:', error.message);
  }
};

const createIssue = async (issue) => {
  const options = {
    headers: {
      'PRIVATE-TOKEN': GITLAB_PROJECT_ACCESS_TOKEN,
      'Content-Type': 'application/json',
    },
  };
  const url = `${GITLAB_API}/projects/${PROJECT_ID}/issues`;

  try {
    const { data } = await axios.post(url, issue, options);
    console.log('ISSUE CREATED SUCCESSFULLY: ', data.web_url);
    return data;
  } catch (error) {
    console.error('An error occurred creating the new issue:', error.message);
    return null;
  }
};

const create404Issue = async (links) => {
  const upcomingIteration = await getProjectUpcomingIteration();
  const title = `Automated Issue: Fix ${links.length} broken links`;
  const description = `/iteration *iteration:${upcomingIteration.id}
  \n
  Fix the next list of broken links:
  \n
  ${links.map((link) => `- ${link}\n`).join('')}
  `;

  const issue = {
    title,
    description,
    iteration_id: upcomingIteration.id,
    labels: 'dex-status::todo,dex::engineering',
  };

  const createdIssue = await createIssue(issue);
  if (createdIssue) {
    await sendSlackMessage(links.length, createdIssue.web_url);
  }
};

checkForBrokenLinks();
