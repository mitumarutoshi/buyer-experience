/* eslint @typescript-eslint/no-var-requires: 0 */
/* eslint no-console: 0 */
const yaml = require('js-yaml');
const fs = require('fs');
const contentful = require('contentful-management');
const process = require('process');
const accessToken = process.argv[2]; // CMA access token
const spaceId = process.argv[3]; // About.gitlab.com space id
const englishPageId = process.argv[4]; // English entry to be localized
const locale = process.argv[5]; // New locale code
const ymlFile = process.argv[6]; // Yml file with the localized content
const pageName = 'Install';
let entryCount = 0;

async function migrateLocalizedPage(auth) {
  const client = contentful.createClient({
    accessToken: auth.accessToken,
  });

  const parsedFile = parseToJSON(ymlFile);
  client
    .getSpace(auth.spaceId)
    .then((space) => space.getEnvironment(auth.env))
    .then((environment) => {
      environment
        .getEntries({
          'sys.id': englishPageId,
          include: 10,
        })
        .then(async (entries) => {
          const englishEntry = entries.items[0];
          englishEntry.fields.pageContent[locale] = [];
          await addBaseProperties(englishEntry, parsedFile);
          await addSeoMetadata(englishEntry, parsedFile, environment);
          await addSideNav(englishEntry, parsedFile, environment);
          await addHeader(englishEntry, parsedFile, environment);
          await addMainContent(englishEntry, parsedFile, environment);
          englishEntry.update().then(() => {
            console.log('Entry updated');
            console.log('Total entries created: ', entryCount);
          });
        });
    });
}

async function addSeoMetadata(englishEntry, parsedFile, environment) {
  const contentType = 'seo';
  console.log(`creating SEO metadata`);
  try {
    const SEO = await environment.createEntry(contentType, {
      fields: {
        title: {
          'en-US': parsedFile.title,
        },
        description: {
          'en-US': parsedFile.description,
        },
        ogTitle: {
          'en-US': parsedFile.title,
        },
      },
    });
    console.log('Created SEO metadata');
    englishEntry.fields.seoMetadata[locale] = [
      {
        sys: {
          type: 'Link',
          linkType: 'Entry',
          id: SEO.sys.id,
        },
      },
    ];
  } catch ({ name, message }) {
    const parsedError = JSON.parse(message);
    console.log(parsedError.message);
  }
}

async function addMainContent(englishEntry, parsedFile, environment) {
  const contentType = 'cardGroup';
  // eslint-disable-next-line
  const { title, description, side_menu, header, ...mainContent } = parsedFile;
  try {
    for (const block in mainContent) {
      const blockData = mainContent[block];
      console.log('adding content for: ', block);
      const cardReferences = await createCards(blockData.cards, environment);

      const CardGroup = await environment.createEntry(contentType, {
        fields: {
          internalName: {
            'en-US': `[${locale}] ${pageName} ${blockData.id}`,
          },
          header: {
            'en-US': blockData.title,
          },
          subheader: {
            'en-US': blockData.subtitle,
          },
          description: {
            'en-US': blockData.text,
          },
          card: {
            'en-US': cardReferences,
          },
          customFields: {
            'en-US': {
              id: blockData.id,
            },
          },
        },
      });
      console.log('Created card block entry');
      entryCount++;
      englishEntry.fields.pageContent[locale].push({
        sys: {
          type: 'Link',
          linkType: 'Entry',
          id: CardGroup.sys.id,
        },
      });
    }
  } catch ({ name, message }) {
    const parsedError = JSON.parse(message);
    console.log(parsedError.message);
  }
}

async function createCards(cardData, environment) {
  const contentType = 'card';
  const result = [];
  try {
    for (const card of cardData) {
      console.log('creating card');
      const ButtonReference = await createCardButton(card, environment);

      const options = {
        fields: {
          internalName: {
            'en-US': `[${locale}] ${pageName} ${card.title}`,
          },
          title: {
            'en-US': card.title,
          },
          subtitle: {
            'en-US': card.subtext,
          },
          button: {
            'en-US': ButtonReference,
          },
          customFields: {
            'en-US': {
              id: card.id,
              dropdown: card.dropdown,
            },
          },
        },
      };

      if (card.icon) {
        options.fields.iconName = {
          'en-US': card.icon.name,
        };
      }

      const CardEntry = await environment.createEntry(contentType, options);
      console.log('Created Card');
      entryCount++;
      result.push({
        sys: {
          type: 'Link',
          linkType: 'Entry',
          id: CardEntry.sys.id,
        },
      });
    }
  } catch (error) {
    console.log(error);
  }
  return result;
}

async function createCardButton(card, environment) {
  const contentType = 'button';
  console.log('creating card Button');
  const cardEntry = await environment.createEntry(contentType, {
    fields: {
      internalName: {
        'en-US': `[${locale}] ${pageName} ${card.link_text}`,
      },
      text: {
        'en-US': card.link_text,
      },
      externalUrl: {
        'en-US': card.link_url,
      },
      dataGaName: {
        'en-US': card.data_ga_name,
      },
      dataGaLocation: {
        'en-US': card.data_ga_location,
      },
    },
  });
  entryCount++;
  return {
    sys: {
      type: 'Link',
      linkType: 'Entry',
      id: cardEntry.sys.id,
    },
  };
}

function addBaseProperties(englishEntry, parsedFile) {
  const { description } = parsedFile;
  englishEntry.fields.description[locale] = description;
  entryCount++;
}

async function addHeader(englishEntry, parsedFile, environment) {
  const { header } = parsedFile;
  const contentType = 'eventHero';
  console.log(`creating Hero entry`);
  try {
    const Hero = await environment.createEntry(contentType, {
      fields: {
        internalName: {
          'en-US': `[${locale}] ${pageName} Hero`,
        },
        title: {
          'en-US': header.title,
        },
        subheader: {
          'en-US': header.subtitle,
        },
      },
    });
    console.log('Created Hero entry');
    entryCount++;
    englishEntry.fields.pageContent[locale].push({
      sys: {
        type: 'Link',
        linkType: 'Entry',
        id: Hero.sys.id,
      },
    });
    return Hero.sys.id;
  } catch ({ name, message }) {
    const parsedError = JSON.parse(message);
    console.log(parsedError.message);
  }
}

async function addSideNav(englishEntry, parsedFile, environment) {
  const { side_menu } = parsedFile;
  const contentType = 'sideMenu';
  console.log('creating side menu');
  try {
    const anchorReferences = await createAnchors(
      side_menu.anchors.data,
      environment,
    );
    const hyperlinkReferences = await createHyperlinks(
      side_menu.hyperlinks.data,
      environment,
    );

    const SideNav = await environment.createEntry(contentType, {
      fields: {
        internalName: {
          'en-US': `[${locale}] ${pageName} Side Navigation`,
        },
        anchorsText: {
          'en-US': side_menu.anchors.text,
        },
        anchors: {
          'en-US': anchorReferences,
        },
        hyperlinksText: {
          'en-US': side_menu.hyperlinks.text,
        },
        hyperlinks: {
          'en-US': hyperlinkReferences,
        },
      },
    });
    console.log('Created side menu entry');
    entryCount++;
    englishEntry.fields.pageContent[locale].push({
      sys: {
        type: 'Link',
        linkType: 'Entry',
        id: SideNav.sys.id,
      },
    });
    return SideNav.sys.id;
  } catch ({ name, message }) {
    const parsedError = JSON.parse(message);
    console.log(parsedError.message);
  }
}

async function createAnchors(anchorData, environment) {
  const contentType = 'anchorLink';
  const result = [];
  for (const anchor of anchorData) {
    console.log('creating anchor');
    const AnchorEntry = await environment.createEntry(contentType, {
      fields: {
        internalName: {
          'en-US': anchor.text,
        },
        linkText: {
          'en-US': anchor.text,
        },
        anchorLink: {
          'en-US': anchor.href,
        },
        dataGaName: {
          'en-US': anchor.data_ga_name,
        },
        dataGaLocation: {
          'en-US': anchor.data_ga_location,
        },
      },
    });
    entryCount++;
    result.push({
      sys: {
        type: 'Link',
        linkType: 'Entry',
        id: AnchorEntry.sys.id,
      },
    });
  }
  return result;
}

async function createHyperlinks(hyperlinkData, environment) {
  const contentType = 'button';
  const result = [];
  for (const hyperlink of hyperlinkData) {
    console.log('creating anchor');
    const HyperlinkEntry = await environment.createEntry(contentType, {
      fields: {
        internalName: {
          'en-US': hyperlink.text,
        },
        text: {
          'en-US': hyperlink.text,
        },
        externalUrl: {
          'en-US': hyperlink.href,
        },
        dataGaName: {
          'en-US': hyperlink.data_ga_name,
        },
        dataGaLocation: {
          'en-US': hyperlink.data_ga_location,
        },
      },
    });
    entryCount++;
    result.push({
      sys: {
        type: 'Link',
        linkType: 'Entry',
        id: HyperlinkEntry.sys.id,
      },
    });
  }
  return result;
}

function parseToJSON(filePath) {
  console.log('parsing press files to JSON');
  const rawYML = fs.readFileSync(filePath, { encoding: 'utf8', flag: 'r' });
  const updatedYML = makeImagePathsAbsolute(rawYML);
  const parsedYML = yaml.load(updatedYML, { json: true });
  console.log(`Successfully parsed Locale file`);
  return parsedYML;
}

function makeImagePathsAbsolute(markdownText) {
  const regex = /(!\[.*?\]\()(\/[^)]+)\)/g;

  const prefix = 'https://about.gitlab.com';

  const newMarkdownText = markdownText.replace(regex, `$1${prefix}$2)`);

  return newMarkdownText;
}

migrateLocalizedPage({
  accessToken,
  spaceId,
  env: 'master',
});
