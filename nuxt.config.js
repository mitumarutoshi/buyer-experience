import fs from 'fs';
import path from 'path';
import glob from 'glob';
import uniqBy from 'lodash';
import './scripts/find-not-localized.js';
import { areCriticalRoutesGenerated } from './scripts/routeChecker.js';

import ignoredFiles from './route.ignore.js';
import contentfulRoutes, {
  fetchCustomerRoutes,
  fetchCustomRoutes,
  fetchEventRoutes,
  fetchMicrositeRoutes,
  fetchPages,
  fetchTopicsRoutes,
} from './route.contentful.js';

// If the commit is not on the default branch, this is not a production deploy,
// and we should disable OneTrust.
const oneTrustDisabled =
  process.env.NODE_ENV === 'development' ||
  process.env.CI_COMMIT_BRANCH !== process.env.CI_DEFAULT_BRANCH;

// If OneTrust is disabled, don't load it in the head config.
// Otherwise, return the oneTrust script tags.
// This is used in the `script` key of the `meta` object in the configuration below.
const oneTrustScripts = oneTrustDisabled
  ? []
  : [
      {
        hid: 'oneTrustSDK',
        src: 'https://cdn.cookielaw.org/scripttemplates/otSDKStub.js',
        type: 'text/javascript',
        charset: 'utf-8',
        'data-domain-script': '7f944245-c5cd-4eed-a90e-dd955adfdd08',
      },
      {
        hid: 'oneTrustAutoBlocking',
        src: 'https://cdn.cookielaw.org/consent/7f944245-c5cd-4eed-a90e-dd955adfdd08/OtAutoBlock.js',
        type: 'text/javascript',
      },
      {
        hid: 'oneTrustGeolocation',
        src: 'https://geolocation.onetrust.com/cookieconsentpub/v1/geo/location/geofeed',
        type: 'text/javascript',
      },
    ];

// Replaces `string` method removed from markdown-it-anchor package https://www.npmjs.com/package/markdown-it-anchor#user-friendly-urls
const createSlug = (s) =>
  encodeURIComponent(
    String(s)
      .trim()
      .toLowerCase()
      .replace(/[^a-zA-Z ]/g, '')
      .replace(/\s+/g, '-'),
  );

export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',
  generate: {
    // Don't crawl the site links and generate pages - we assume some pages will be built by Middleman or elsewhere.
    crawler: false,
    // To allow the app to redirect to error page
    // https://nuxtjs.org/docs/configuration-glossary/configuration-router/
    fallback: '404.html',
    // Interval of 100ms to avoid "Too Many Request" error
    // https://v2.nuxt.com/docs/configuration-glossary/configuration-generate/#interval
    // interval: 300,
    // concurrency: 20,
    routes: async () => {
      const routeIndex = process.argv.indexOf('-r');
      if (routeIndex !== -1) {
        // it will never be 0 as that would be the node/nuxt command
        // eslint-disable-next-line
        console.log('generating single route: ', process.argv[routeIndex + 1]);
        return [process.argv[routeIndex + 1]];
      } else {
        const customerRoutes = await fetchCustomerRoutes();
        const topicsRoutes = await fetchTopicsRoutes();
        const customRoutes = await fetchCustomRoutes();
        const standardEventRoutes = await fetchEventRoutes();
        const pages = await fetchPages();
        const micrositeRoutes = await fetchMicrositeRoutes();

        return new Promise((resolve, reject) => {
          // eslint-disable-next-line consistent-return
          glob('content/**/*', (err, result) => {
            if (err) {
              return reject(err);
            }
            // Remove partial and ignored files
            const filteredResult = result
              .filter((file) => !ignoredFiles.includes(file))
              .filter((file) => !file.includes('partial'));
            // Add Contentful routes
            const transformedRoutes = [
              ...contentfulRoutes,
              ...customerRoutes,
              ...topicsRoutes,
              ...customRoutes,
              ...standardEventRoutes,
              ...pages,
              ...micrositeRoutes,
            ].map((page) => ({
              route: page,
            }));

            // Generate routes based on content files
            const routesRegex = /content|\.yml|\/index\/?/gim;

            // Combine $content and Contentful routes
            const routes = uniqBy(
              [
                ...filteredResult
                  .map((route) => ({
                    route: route.replace(routesRegex, ''),
                  }))
                  .filter((item) => item.route !== ''),
                ...transformedRoutes,
              ],
              'route',
            );

            resolve(routes);
          });
        });
      }
    },
  },
  hooks: {
    generate: {
      done(generator) {
        // eslint-disable-next-line
        console.log('Generate hook executed!');
        const dbFilePattern = path.join(
          generator.distPath,
          '_nuxt',
          'content',
          'db-*.json',
        );
        const dbFiles = glob.sync(dbFilePattern);

        dbFiles.forEach((file) => {
          // eslint-disable-next-line
          console.log('Emptying:', file);
          fs.writeFileSync(file, '{}');
        });

        const routeIndex = process.argv.indexOf('-r');
        if (routeIndex !== -1) {
          return;
        } else {
          if (!areCriticalRoutesGenerated(generator.generatedRoutes)) {
            throw new Error('Not all expected routes were generated');
          }
        }
      },
    },
  },
  image: {
    // The screen sizes predefined by `@nuxt/image`:https://image.nuxtjs.org/api/options
    screens: {
      xs: 320,
      sm: 640,
      md: 768,
      lg: 1024,
      xl: 1280,
      xxl: 1536,
      '2xl': 1536,
    },
  },
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    __dangerouslyDisableSanitizersByTagID: {
      gtagConsent: ['innerHTML'],
      schemaOrg: ['innerHTML'],
    },
    title: 'GitLab The One DevOps Platform',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'contentSecurityPolicy',
        'http-equiv': 'Content-Security-Policy',
        content: `
          default-src 'self' https: http:;
          script-src 'self' 'unsafe-inline' 'unsafe-eval' https: http: *.googletagmanager.com;
          style-src 'self' 'unsafe-inline' https: http: https://fonts.googleapis.com;
          object-src https: http:;
          base-uri 'self';
          connect-src 'self' https: http: wss: ws: *.google-analytics.com *.analytics.google.com *.googletagmanager.com;
          frame-src 'self' https: http:;
          img-src 'self' https: http: data: *.google-analytics.com *.googletagmanager.com;
          manifest-src 'self'; media-src 'self' https: http:;
          child-src 'self' blob: https: http:;
          font-src 'self' https: http: data: https://fonts.gstatic.com;
        `,
      },
      {
        hid: 'description',
        name: 'description',
        content: 'Code, test and deploy with GitLab. Everyone can contribute!',
      },
      {
        hid: 'twitter:description',
        name: 'twitter:description',
        content: 'Code, test and deploy with GitLab. Everyone can contribute!',
      },
      {
        hid: 'og:description',
        name: 'og:description',
        content: 'Code, test and deploy with GitLab. Everyone can contribute!',
      },
      {
        hid: 'formatDetection',
        name: 'format-detection',
        content: 'telephone=no',
      },
      {
        hid: 'twitter:card',
        name: 'twitter:card',
        content: 'summary_large_image',
      },
      { hid: 'twitter:site', name: 'twitter:site', content: '@GitLab' },
      {
        hid: 'msapplication-TileImage',
        name: 'msapplication-TileImage',
        content: '/nuxt-images/ico/mstile-144x144.png?cache=20220414',
      },
      {
        hid: 'msapplication-config',
        name: 'msapplication-config',
        content: '/nuxt-images/ico/browserconfig.xml',
      },
      { hid: 'robots', name: 'robots', content: 'index, follow' },
    ],
    link: [
      {
        rel: 'shortcut icon',
        type: 'image/x-icon',
        href: '/nuxt-images/ico/favicon.ico?cache=20220414',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '192x192',
        href: '/nuxt-images/ico/favicon-192x192.png?cache=2022041',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '160x160',
        href: '/nuxt-images/ico/favicon-160x160.png?cache=2022041',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '96x96',
        href: '/nuxt-images/ico/favicon-96x96.png?cache=2022041',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '32x32',
        href: '/nuxt-images/ico/favicon-32x32.png?cache=2022041',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '16x16',
        href: '/nuxt-images/ico/favicon-16x16.png?cache=2022041',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '57x57',
        href: '/nuxt-images/ico/apple-touch-icon-57x57.png?cache=2022041',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '60x60',
        href: '/nuxt-images/ico/apple-touch-icon-60x60.png?cache=2022041',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '72x72',
        href: '/nuxt-images/ico/apple-touch-icon-72x72.png?cache=2022041',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '76x76',
        href: '/nuxt-images/ico/apple-touch-icon-76x76.png?cache=2022041',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '114x114',
        href: '/nuxt-images/ico/apple-touch-icon-114x114.png?cache=2022041',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '120x120',
        href: '/nuxt-images/ico/apple-touch-icon-120x120.png?cache=2022041',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '144x144',
        href: '/nuxt-images/ico/apple-touch-icon-144x144.png?cache=2022041',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '152x152',
        href: '/nuxt-images/ico/apple-touch-icon-152x152.png?cache=2022041',
      },
      {
        rel: 'apple-touch-icon',
        sizes: '180x180',
        href: '/nuxt-images/ico/apple-touch-icon-180x180.png?cache=2022041',
      },
      {
        rel: 'alternate',
        type: 'application/atom+xml',
        title: 'Blog',
        href: '/atom.xml',
      },
      {
        rel: 'alternate',
        type: 'application/atom+xml',
        title: 'All Releases',
        href: '/all-releases.xml',
      },
      {
        rel: 'alternate',
        type: 'application/atom+xml',
        title: 'Security Releases',
        href: '/security-releases.xml',
      },
      {
        rel: 'alternate',
        type: 'application/atom+xml',
        title: 'Major Releases',
        href: '/releases.xml',
      },
    ],
    script: [
      {
        hid: 'gtagConsent',
        innerHTML: `
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}

        const defaultConsents = window.dataLayer.filter(item => item.length && Array.from(item).indexOf('default') >= 0)

        if(defaultConsents.length <= 2){
          gtag('consent', 'default', {
            'analytics_storage': 'granted',
            'ad_storage': 'granted',
            'functionality_storage': 'granted',
            'wait_for_update': 500
          });
          gtag('consent', 'default', {
            'analytics_storage': 'denied',
            'ad_storage': 'denied',
            'functionality_storage': 'denied',
            'region': ['AT','BE','BG','HR','CY','CZ','DK','EE','FI','FR','DE','GR','HU','IE','IT','LV','LT','LU','MT','NL','PL','PT','RO','SK','SI','ES','SE','IS','LI','NO','RU','GB','PE','CO','KR','CA-QC'],
            'wait_for_update': 500
          });
        }

        window.geofeed = (options) => {
          dataLayer.push({
            'event' : 'OneTrustCountryLoad',
            'oneTrustCountryId': options.country.toString(),
            'oneTrustStateId': options.state?.toString()
          })
        }
      `,
      },
      ...oneTrustScripts,
      {
        hid: 'schemaOrg',
        innerHTML: `{"@context":"http://schema.org","@type":"Organization","name":"GitLab","legalName":"GitLab Inc.","url":"https://about.gitlab.com","logo":"https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo.png","foundingDate":"2011","founders":[{"@type":"Person","name":"Sid Sijbrandij"},{"@type":"Person","name":"Dmitriy Zaporozhets"}],"address":{"@type":"PostalAddress","streetAddress":"268 Bush Street #350","addressLocality":"San Francisco","addressRegion":"CA","postalCode":"94104","addressCountry":"USA"},"sameAs":["https://www.facebook.com/gitlab","https://twitter.com/gitlab","https://www.linkedin.com/company/gitlab-com","https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg"]}`,
        type: 'application/ld+json',
      },

      {
        hid: 'bizible',
        src: '//cdn.bizible.com/scripts/bizible.js',
        type: 'text/javascript',
      },
      {
        hid: 'munchkin',
        src: '//munchkin.marketo.net/munchkin.js',
        type: 'text/javascript',
      },
      {
        src: 'https://extend.vimeocdn.com/ga/115027220.js',
        defer: true,
        type: 'text/javascript',
      },
    ],
  },
  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'slippers-ui/src/styles/_variables.scss',
    'slippers-ui/src/styles/base.scss',
    'slippers-ui/dist/slippers-core.css',
    'be-navigation/lib/style.css',
    'aos/dist/aos.css',
    'vue-slick-carousel/dist/vue-slick-carousel.css',
    'vue-slick-carousel/dist/vue-slick-carousel-theme.css',
    '~/assets/css/fonts',
    '~/assets/css/base',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/slippers-ui.ts',
    '~/plugins/be-navigation.ts',
    '~/plugins/services.ts',
    '~/plugins/content-preview.js',
    { src: '~/plugins/oneTrust.js', mode: 'client' },
    { src: '~/plugins/gtm.js', mode: 'client' },
    { src: '~/plugins/swiftype.js', mode: 'client' },
    { src: '~/plugins/munchkin.js', mode: 'client' },
    { src: '~/plugins/floating-vue.js', mode: 'client' },
    { src: '~/plugins/vue-pluralize.js', mode: 'client' },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    [
      '@nuxt/typescript-build',
      {
        typeCheck: false,
      },
    ],
    // https://image.nuxtjs.org/
    '@nuxt/image',
    ...(process.env.NODE_ENV !== 'production' ? ['@nuxtjs/dotenv'] : []),
    '@/lib/sitemapHelper.js',
  ],
  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/style-resources',
    '@nuxt/content',
    '@nuxtjs/i18n',
    '@nuxtjs/markdownit',
    '@nuxtjs/component-cache',
    '@nuxtjs/sitemap', // Should be at the end of the array
  ],
  styleResources: {
    scss: ['slippers-ui/src/styles/_variables.scss'],
  },
  i18n: {
    locales: [
      { code: 'de-de', iso: 'de-DE', file: 'de-DE.json' },
      { code: 'fr-fr', iso: 'fr-FR', file: 'fr-FR.json' },
      { code: 'ja-jp', iso: 'ja-JP', file: 'ja-JP.json' },
      { code: 'en-us', iso: 'en-US', file: 'en-US.json' },
    ],
    defaultLocale: 'en-us',
    vueI18n: {
      fallbackLocale: 'en-us',
      silentFallbackWarn: true,
    },
    vueI18nLoader: true,
    strategy: 'prefix_except_default',
    lazy: true,
    langDir: 'locales/',
    detectBrowserLanguage: false,
  },
  sitemap: {
    path: '/buyer-experience/sitemap.xml',
    hostname: 'https://about.gitlab.com/',
    trailingSlash: true,
    exclude: [
      // This will not work if you add a trailing slash to the route
      '/events/devsecops-world-tour/executive',
      '/events/summit-las-vegas',
      '/404',
      '/search',
      '/custom-page-preview',
      '/events-page-preview',
      '/de-de/custom-page-preview',
      '/fr-fr/custom-page-preview',
      '/ja-jp/custom-page-preview',
      '/microsite-preview',
      '/partner',
    ],
  },
  markdownit: {
    runtime: true, // Support `$md()`
    linkify: false, // Avoid auto generated hyperlinks (i.e gitlab.com)
    html: true, // Support html source tags
    use: [
      ['markdown-it-attrs'],
      [
        'markdown-it-anchor',
        {
          level: 1,
          permalink: true,
          slugify: createSlug,
          // renderPermalink: (slug, opts, state, permalink) => {},
          // permalinkBefore: true,
          permalinkClass: 'header-anchor',
          permalinkSymbol:
            '<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12.2426 3.75736C11.4615 2.97631 10.1952 2.97631 9.41416 3.75736L7.99995 5.17157C7.60942 5.56209 6.97626 5.56209 6.58573 5.17157C6.19521 4.78105 6.19521 4.14788 6.58573 3.75736L7.99995 2.34314C9.56205 0.781046 12.0947 0.781046 13.6568 2.34314C15.2189 3.90524 15.2189 6.4379 13.6568 8L12.2426 9.41421C11.8521 9.80473 11.2189 9.80473 10.8284 9.41421C10.4379 9.02369 10.4379 8.39052 10.8284 8L12.2426 6.58578C13.0236 5.80473 13.0236 4.5384 12.2426 3.75736Z" fill="#333333" /><path d="M10.5355 5.4645C10.926 5.85502 10.926 6.48819 10.5355 6.87871L6.87863 10.5356C6.4881 10.9261 5.85494 10.9261 5.46441 10.5356C5.07389 10.145 5.07389 9.51188 5.46441 9.12135L9.12127 5.4645C9.51179 5.07397 10.145 5.07397 10.5355 5.4645Z" fill="#333333" /><path d="M3.75742 9.41422C2.97637 10.1953 2.97637 11.4616 3.75742 12.2426C4.53847 13.0237 5.8048 13.0237 6.58584 12.2426L8.00006 10.8284C8.39058 10.4379 9.02375 10.4379 9.41427 10.8284C9.8048 11.219 9.8048 11.8521 9.41427 12.2426L8.00006 13.6569C6.43796 15.219 3.9053 15.219 2.3432 13.6569C0.781107 12.0948 0.781107 9.56211 2.3432 8.00001L3.75742 6.5858C4.14794 6.19527 4.78111 6.19527 5.17163 6.5858C5.56216 6.97632 5.56215 7.60948 5.17163 8.00001L3.75742 9.41422Z" fill="#333333" /></svg>',
        },
      ], // Generate anchor links on markdown heading as shown here https://stackoverflow.com/questions/60436593/how-to-use-markdown-it-plugins-options-in-nuxt-js
      // Plugin README: https://www.npmjs.com/package/markdown-it-anchor
      [
        'markdown-it-multimd-table',
        {
          multiline: true,
        },
      ], // Allow to have multilines inside markdown tables. README: https://github.com/RedBug312/markdown-it-multimd-table#multiple-lines-of-row
    ],
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: ['contentful', 'slippers-ui', 'be-navigation'],
    babel: { compact: true },
    quiet: false,
    extractCSS: {
      ignoreOrder: true,
    },
  },
  log: {
    level: 'debug', // ('debug', 'info', 'warn', 'error', 'silent')
  },
  // Used for live previewing custom pages that have not been published.
  publicRuntimeConfig: {
    preview: process.env.CTF_PREVIEW_ACCESS_TOKEN,
    space: process.env.CTF_SPACE_ID,
  },
  // Pass build-time variables to the client: https://nuxtjs.org/docs/configuration-glossary/configuration-env
  env: {
    // If the commit is not on the default branch, this is not a production deploy,
    // and we should disable OneTrust.
    oneTrustDisabled,
  },
};
